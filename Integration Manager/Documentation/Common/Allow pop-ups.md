<tags data-value="pop-up, pop-ups"></tags>

:fa-ban: Allow pop-ups
=====
___

## :fa-info: Information
Integration Manager uses **pop-up** Windows/Forms for download of various files (message payloads, zip and [Export][ImportExport] files).

    NOTE: Make sure your browser allow pop-ups!

## Chrome
Information about how to allow pop-ups in **Chrome** can be found [here](https://support.google.com/chrome/answer/95472?co=GENIE.Platform%3DDesktop&hl=en)

## Internet Explorer
Information about how to allow pop-ups in **Internet Explorer** can be found [here](https://support.microsoft.com/en-us/help/17479/windows-internet-explorer-11-change-security-privacy-settings)

## Mozilla Firefox
Information about how to allow pop-ups in **Mozilla Firefox** can be found [here](https://support.mozilla.org/en-US/kb/pop-blocker-settings-exceptions-troubleshooting)

___


#### :fa-cubes: Related
:fa-globe: [Web Client][]    
<!--References -->

[png_ComponentServices]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/Common/TransactionsMSDTC/ComponentServices.png
[https://technet.microsoft.com/en-us/library/cc759136(v=ws.10).aspx]:[https://technet.microsoft.com/en-us/library/cc771891.aspx]
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Install]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Overview.md?at=master

[ImportExport]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Import%20Export.md?at=master