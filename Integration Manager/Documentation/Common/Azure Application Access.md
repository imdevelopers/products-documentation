<tags data-value="Azure, Security"></tags>

:fa-user-secret: Azure Subscription Access and Manage Account(s) 
=====
				
___

## :fa-info: Information
The Azure related [Log-][Log Agents] and [Monitor-Agents][Monitor Agents] requires Azure connection details to query the Azure Service Management API. This is a generic documentation for creating and/or retreiving the required values.

The Service Management API is a REST API that provides programmatic access to much of the functionality available through the Azure Management Portal.
All API operations are performed over SSL and mutually authenticated using X.509 v3 certificates.

The following fields are usually part of the configuration for the agents:
* 1. **SubscriptionId** 
* 2. **ResourceGroup**
* 3. **TenantId**
* 4. **ClientId** and **ClientSecret**

![Common][1]

The Azure Directory Application from where the ClientId and the ClientSecret is retreived needs appropriate rights set (documented below). 

## 1. SubscriptionId  
A Microsoft Azure subscription is a unique user account in Azure. All resources available via the Service Management API are organized beneath the subscription. When you create an Azure subscription, it is uniquely identified by a subscription ID. The subscription ID forms part of the URI for every call that you make to the Service Management API.

* The *SubscriptionId* is a GUID
* An Azure subscription may have many storage accounts.
* A storage account may have many containers.

To acquire the *SubscriptionId* simply open up the *Resource Group* in the Azure portal:  
![SubscriptionId][2]

## 2. ResourceGroup
Resource groups provide a way to monitor, control access, provision and manage billing for collections of assets that are required to run an application/service/function.
To acquire the *ResourceGroup* simply open up the *Resource Group* in the Azure portal:
![ResourceGroup][3]

## 3. TenantId
The *TenantId* is the GUID uniquely identifying the Azure Active Directory instance.

There are at least two easy ways to acquire the *TenantId*, see example images below:

<table>
<tr>
<th>From the Old Portal</th>
<th>From Help | Diagnostics</th>
</tr>
<tr>
<td><img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Azure/TenantId.png"/></td>
<td><img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Azure/TenantIdFromDiagnostics.png"/></td>
</tr>
<table>

* one or more applications must be created for users to be authenticated against your directory before accessing the resources within the application.  
* An application is requires to issue authentication tokens when authenticating users. 

See next bullet 4 for additional details on applications. 

## 4. ClientId and ClientSecret
In order to create retreive the ClientId and the ClientSecret an Application must first exist/be created.

The following steps are required to create a new Application:  
![CreateApplication][4]

* 1. From the Selected Directory instance, click on *App registrations*
* 2. Click on the *Add* Button
* 3. Enter the name of the *Application*
* 4. Select the *'Web app / API'* option
* 5. Enter the URL to your user management web site (can be changed later)

    NOTE: This can be any address like http://yournonexistinguserportal.nowhere.org

* 6. Click the *'Create'* button to begin the creation process, this may take some time

The Application will be created. Now a Key must be created.  
![CreateKey][5]

* 1. Click on the newly created *Application*
* 2. Expand the *'Key'* accordion

Create a new Key, follow the steps below  
![SaveKey][6]

* 1. Enter a *Name* for the Key (max 16 chars)
* 2. Select the period for the validity of the Key
* 3. Click the *Save* button
* 4. Copy the value (**ClientSecret**)

    NOTE: The Value is only available for a brief moment. It cannot be retreived later! You have one shot only of documenting the Key elsewhere.

![CopyTheKeyNotification][7]

The **ClientId** is represented as the *ApplicationId* for the Application. Simply select the Application and copy the GUID.    
![ClientId][8]

## Application rights
Once the application has been created, you will have to configure the permissions. 

In order to assign users (the application) to the Resource Group in the new portal, the application must first be assigned some rights in the old portal (this must be due to bugs within the new Azure portal)

## 1. Open up the [old portal](https://manage.windowsazure.com)  
![Select][12]
1. Click the Azure Directory icon
2. Click on the Directory instance
3. Select from the list of available 'Applications my company owns'
4. Click the Apply filter button
5. Click on the Application (earlier created in this tutorial)

## 2. Click on the *'Configure'* Button  
![Configure][13]

## 3. Click on the *'Add Application'* button  
![AddApp][14]

## 4. In the 'Permissions to other applications' dialogue:  
![Permissions][15]

1. Make sure to select 'Microsoft Apps'
2. Apply the filter
3. Select the 'Windows Azure Service Management API'
3. Click Apply


## 5. :fa-edit: Add access
As the next step, make sure to select the 'Access Azure Service Management as organization user' (Delegated Permissions)    
![DelegatedPermissionsOldPortal][16]

## 6. :fa-save: Save:  
Make sure to click on the Save button    
![SaveAppOldPortal][17]

### :fa-exclamation-triangle: REMEMBER TO COPY THE KEY!
If you created the Application in the Old portal, remember to Copy the Secret since it will only be displayed upon first save!  
![SaveAppOldPortal][18]

## :fa-plus: Add access to Resource group for Application
The *Application* must then be added to the *Resource Group*. 

* 1. Go back to the new Azure Portal.

From the new Azure portal:  
![AddAccess][19]

* 1. Click on the *'Resource Group'* button to the left
* 2. Click on the appropriate*'Resource Group'* name
* 3. Click on the *'Access control (IAM)'* button
* 4. Click on the *'Add'* button

In the 'Add access' dialogue:  
![AddContributor][20]
* 1. Click on the *'Select a role'* button
* 2. Click on the *'Contributor'* role

![AddUser][21]

* 3. Click on the *'Add users'* button
* 4. Enter a Filter string to find your *Application* (NOTE: Not listed by default!)
* 5. Click to Select the *Application*

Click on the *'Select'* button  
![Select][22]

Click on the *'Ok'* button  
![Ok][23]

:fa-flag-checkered: User (Application) in Resource group
When finished, the *User (Application)* will be listed as part of the *Contributor role* for the selected *Resource Group*.  
![AccessControlForResourceGroup][24]

___

### :fa-hand-o-right: Next Step  

##### :fa-cubes: Related  
:fa-archive: [Log Agents][]

<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Azure/AzureCommonConnectionDetails.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Azure/SubscriptionId.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Azure/ResourceGroup.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Azure/CreateApplication.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Azure/ExpandKeys.png 
[6]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Azure/SaveKey.png
[7]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Azure/CopyTheKeyNotification.png
[8]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Azure/ClientId.png
[9]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Azure/AddPermissions.png
[10]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Azure/SelectAnAPI.png
[11]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Azure/SelectPermissions.png
[12]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Azure/SelectAnApplicationOldPortal.png
[13]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Azure/ConfigureApplicationOldPortal.png
[14]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Azure/AddApplicationRightsOldPortal.png
[15]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Azure/PermissionsToOtherAppsOldPortal.png
[16]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Azure/SelectDelegatedPermissions.png
[17]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Azure/SaveApplicationOldPortal.png
[18]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Azure/CopyAppSecretOldPortal.png
[19]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Azure/AddAccess.png
[20]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Azure/AddContributor.png
[21]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Azure/AddUser.png
[22]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Azure/Select.png
[23]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Azure/OK.png
[24]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Azure/AccessControlForResourceGroup.png


[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master
[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/Log%20Agents.md?at=master
