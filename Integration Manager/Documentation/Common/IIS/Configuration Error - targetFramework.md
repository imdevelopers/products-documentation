<tags data-value="IIS, 500, Target Framework 4.0, Application Pool, runtime"></tags>

:fa-globe: Configuration Error - Target Framwork 2.0 instead of 4.x
=====
___

## :fa-info: Information
Invalid configuration of the IIS Application Pools will render Integration Manager not operational.

All of Integration Managers Web Applications uses .NET 4.5 or later.  
:fa-globe: [Web Client][]      
:fa-cloud: [Web API][]    
:fa-cloud-download: [LogAPI][]  

During [installation][Install] of IM Update and the creation of new application pools for Integration Manager in IIS, the default target framework on Windows 2008 R2 and erlier will get .NET 2.0 as default instead of 4.x and the Web Applications will therefor not work/run.


![HTTP Runtime][png_HttpRuntime_TargetFramework]  

## :fa-wrench: Solution
Make sure the Application Pools for IM related Web Applications run with .NET 4.x framework

### Change the .NET version in IIS for selected Application Pools

* Click **Start**, click **Run**, type `inetmgr` and then click **OK** to open *Internet Information Services (IIS) Manager*
* In the tree, click to expand **Web Server** and then click **Application Pools**
* Look for the appropriate Application Pool, for exampel `Integration Manager Update`
* Right click on `Integration Manager Update` and click **Basic Settings** to display the **Edit Application Poool** dialog box
* Change the **.NET CLR version** from `.NET CLR Version v2.0.5727` to `.NET CLR Version v4.0.30319`
* Click **OK**
* Reload page in browser

![IIS Settings][png_iis_application_pool_change_v2_to_v4]
___

### :fa-hand-o-right: Next Step
:fa-rocket: [Install Integration Manager][Install]  

#### :fa-cubes: Related
:fa-globe: [Web Client][]    
:fa-cloud: [Web API][]   
:fa-cloud-download: [LogAPI][]  

<!--References -->
[png_iis_application_pool_change_v2_to_v4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/Common/IIS/iis_application_pool_change_v2_to_v4.png
[png_HttpRuntime_TargetFramework]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/Common/IIS/httpRuntime_targetFramework.png
[https://technet.microsoft.com/en-us/library/cc759136(v=ws.10).aspx]:[https://technet.microsoft.com/en-us/library/cc771891.aspx]
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Install]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Overview.md?at=master
[Web API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Web%20API/Overview.md?at=master
[LogAPI]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Log%20API/Overview.md?at=master