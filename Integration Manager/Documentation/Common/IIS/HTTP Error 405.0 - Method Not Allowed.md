<tags data-value="IIS, 405, Method Not Allowed, WebDAV"></tags>

:fa-globe: HTTP Error 405.0 - Method Not Allowed
=====
___

## :fa-info: Information
If [WebDAV][] is installed and if you get a **405 error** on edit (Update/Delete) of any resource in the [WebAPI][Web API] or [WebClient][Web Client]. You may use the solution described in this article to solve the problem,
You can do this for the whole Web Site (usually named **Default Web Site**) if this is OK, or you have to do it for each Integration Manager Application within  IIS.

* [WebClient][Web Client]
* [WebAPI][Web API]

## Example error message as shown in the WebClient  
![png_WebClientUnknownError][]


## Full error message 
![png_WebDAVModuleMethodNotAllowed][]

## :fa-wrench: Solution  
Remove WebDav from **Modules** and from **Handler Mappings** in IIS  
![png_IISHome][]

## Modules
![png_ModuleWebDAVRemove][]


## Handler Mappings
![png_HandlerWebDAVRemove][]
 

    NOTE: Restart IIS or Recycle the Application pool after this operation

Web.config for [WebAPI][Web API] or [WebClient][Web Client] should have the following exclusions:
```XML
...
<system.webServer>
    <modules runAllManagedModulesForAllRequests="true">
      <remove name="WebDAVModule" />
    </modules>
    <handlers>
      <remove name="ExtensionlessUrlHandler-Integrated-4.0" />
      <remove name="OPTIONSVerbHandler" />
      <remove name="TRACEVerbHandler" />
      <remove name="WebDAV" />
      <add name="ExtensionlessUrlHandler-Integrated-4.0" path="*." verb="*" type="System.Web.Handlers.TransferRequestHandler" preCondition="integratedMode,runtimeVersionv4.0" />
    </handlers>
  </system.webServer>
...
```

___


#### :fa-cubes: Related
:fa-globe: [Web Client][]     
:fa-cloud: [Web API][]   


<!-- References -->

[WebDAV]:https://en.wikipedia.org/wiki/WebDAV


[png_WebClientUnknownError]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/Common/IIS/405/WebClientUnknownError.png
[png_WebDAVModuleMethodNotAllowed]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/Common/IIS/405/WebDAVModuleMethodNotAllowed.png

[png_IISHome]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/Common/IIS/405/IISHome.png
[png_ModuleWebDAVRemove]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/Common/IIS/405/ModuleWebDAVRemove.png
[png_HandlerWebDAVRemove]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/Common/IIS/405/HandlerWebDAVRemove.png
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Web API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Web%20API/Overview.md?at=master