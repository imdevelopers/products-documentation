<tags data-value="IIS, 500, Target Framework 4.0"></tags>

:fa-globe: Login Prompt You for a Password
=====
___

## :fa-info: Information
Integration Manager relies on Windows Authentication and therefor the IIS must be properly configured with appropriate firewall settings in order to communicate with the domain controllers.

The proper setup for IIS and Windows is decribed [here][Install Integration Manager].

Related links
[Internet Explorer May Prompt You for a Password:](https://support.microsoft.com/en-us/help/258063/internet-explorer-may-prompt-you-for-a-password)

___

#### :fa-cubes: Related
:fa-rocket: [Install Integration Manager][]   
<!--References -->



[https://support.microsoft.com/en-us/help/258063/internet-explorer-may-prompt-you-for-a-password]:[https://support.microsoft.com/en-us/help/258063/internet-explorer-may-prompt-you-for-a-password]
[Install Integration Manager]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Installation/Overview.md?at=master