<tags data-value="Logon As A Service"></tags>

:fa-user-secret: Logon As A Service
=====
					
___

## :fa-info: Information
**Log on as a service** is a local policy set by an administrator on server level or domain level using group policies.

This security setting allows a security principal to log on as a service. Services can be configured to run under the Local System, Local Service, or Network Service accounts, which have a built in right to log on as a service. Any service that runs under a separate user account must be assigned the right.

Default setting: None

### Add user to policy

![Administrative Tools][png_AdminTools]  
Open Administrative Tools in Control Panel

![LocalSecurity Policy][png_LocalSecurityPolicy]  
Open Local Security Policy

![Add User][png_LocalSecurityPolicy_AddUser]  
Add the account to use for policy **'Log on as a service'**. The account if in use needs to logon/restart to acquire the new set of privileges. 

Integration Manager Service Accounts are used for:
* [Monitoring Service][] - Windows Service Account  
    * Read/Write access to [IMConfig][] and [Log Databases][]
* [Logging Service][] - Windows Service Account
    * Read/Write access to [IMConfig][] and some additional rigths on [Log Databases][]
* [Monitor Agents][] - Windows Service Account

    NOTE: AppPool accounts do not require local admin

You can reuse a single account for all services or use different account according to your policies. See section **Hardening** in the installation [Overview][install]
___

### :fa-hand-o-right: Next Step  
:fa-rocket: [Install][] Integration Manager

##### :fa-cubes: Related  
:fa-globe: [Web Client][]    
:fa-file-text-o: [Release Notes][]  
:fa-database: [IMConfig][]  
:fa-database: [Log Databases][]  

<!--References -->

[png_AdminTools]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/Common/LogonAsAService/AdminTools.png
[png_LocalSecurityPolicy]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/Common/LogonAsAService/LocalSecurityPolicy.png
[png_LocalSecurityPolicy_AddUser]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/Common/LogonAsAService/LocalSecurityPolicy_AddUser.png

[Release Notes]:http://support.integrationsoftware.se/support/discussions/forums/1000227207 "Access the release notes for Integration Manager"
[Install]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Overview.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Web API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Web%20API/Overview.md?at=master
[Log API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Log%20API/Overview.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[IMConfig]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Configuration%20Database/IMConfig.md?at=master
[Log Databases]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Log%20Databases/Log%20Databases.md?at=master
