<tags data-value="Port 8000, Monitoring Service, Monitor Agents, Source"></tags>

:fa-sign-in: Shared Port 8000
=====

:fa-upload: [Monitor Agent][Monitor Agents] :fa-long-arrow-right:
:fa-download: [Source][Sources]  :fa-long-arrow-right:
:fa-desktop: [Monitoring Service][] 
___

## :fa-info: Information
The [Monitoring Service][] uses the connection information from a [Source][Sources] to talk to the [Monitor Agents][]. 

The [Monitor Agents][] support two different protocols :
* 1. TCP (default incoming port 8000), may be reconfigured using other allowed available TCP port 
* 2. [Service Bus Relaying][Service Bus relay service from Microsoft] (outbound Port 80 and 443)

![defaultport8000][1]

All [Monitor Agents][] by default uses a shared TCP/IP Port 8000. This default port may be altered by an administrator.

 :fa-exclamation-triangle: *Not all servers allow the use of TCP port 8000 by default and in order for the agent to work communication must be allowed*.

    NOTE: Changes on agent side must be reflected in Source configuration in order for the Monitoring Service to be able to talk to the agent

The firewall must allow whatever port(s) the [Monitor Agent][Monitor Agents] is configured to run with. 

    NOTE: Port 8000 is just the default and running other ports is supported

* The [Source][Sources] must be configured to use whatever protocol/port the [Monitor Agent][Monitor Agents] is configured to use.

![ConfigFilePort8000.png][0]


## :fa-exclamation-triangle: Common problems
Common problems are either **Security** or **Firewall** settings. The agents may also have requirements on specific [3rd party libraries][] that needs to be installed prior to installation and configuration.

### Service does not have appropriate local rights
A common problem is that this port is not allowed since the service account running the [Monitor Agent][Monitor Agents] Windows Service when try is not a local administrator (and does not always require so).

*Exception Example from the log file:*
`HTTP could not register URL http://+:8000/IM/Monitor/Agent/Servicename/. your process does not have access rights to this namespace (see http://go.microsoft.com/fwlink/?LinkId=70353 for details).`	

### :fa-magic: Solution
Grant the service account allowance to the url access control list.


Replace the `DOMAIN\USER` in the example below and run the command in an elevated command prompt (Administrator).  

This command grants the service account `DOMAIN\USER` allowance to the url access control list.

``` dos
netsh http add urlacl url=http://+:8000/IM user=DOMAIN\user
```

### :fa-hand-o-right: Next Step
:fa-cloud-upload: [Monitor Agents][]    
:fa-cloud-download: [Source][Sources]

#### :fa-cubes: Related
:fa-cloud: [Microsoft Service Bus Relaying][Service Bus relay service from Microsoft]  
:fa-download: [Monitoring Service][]  
:fa-user-secret: [Logon as Service Rights][How to set Logon As A Service]  

[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Service Bus relay service from Microsoft]:https://azure.microsoft.com/en-us/documentation/articles/service-bus-dotnet-how-to-use-relay
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master  
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/Common/ConfigFilePort8000.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/Common/Default8000.png


[How to set Logon As A Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Logon%20As%20A%20Service.md?at=master

[3rd party libraries]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/3rd%20party%20libraries/Overview.md?at=master