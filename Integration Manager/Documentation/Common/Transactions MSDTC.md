<tags data-value="MSDTC, Transactions"></tags>

:fa-link: Transactions MSDTC
=====
___

## :fa-info: Information
Integration Manager relies heavily on the use of transactions for many critical operations. Also, the [Log Agents][] uses the Microsoft DTC Window Service. The transactions are governed by the Microsoft DTC service (MSDTC). The Distributed Transaction Coordinator (DTC) service coordinates transactions that update transaction-protected resources, such as databases, message queues, BizTalk Server and so on. These transaction-protected resources may be contained on a single system or distributed across a network of systems. Integration Manager is built so scale and the services can be hosted on many distributed servers.

Read more in this [technet](https://technet.microsoft.com/en-us/library/cc759136(v=ws.10).aspx) article.

### Check the local DTC
On all Windows servers where Integration Manager components and services are installed, make sure to set the MSDTC with the following settings:

* Click **Start**, click **Run**, type `dcomcnfg` and then click **OK** to open *Component Services*.
* In the console tree, click to expand **Component Services**, click to expand **Computers**, click to expand **My Computer**, click to expand **Distributed Transaction Coordinator** and then click **Local DTC**.
* Right click **Local DTC** and click **Properties** to display the **Local DTC Properties** dialog box.
* Click the **Security** tab.
* Check mark "**Network DTC Access**" checkbox.
* Finally check mark "**Allow Inbound**" and "**Allow Outbound**" checkboxes.
* Click **Apply**, **OK**.
* A message will pop up about **restarting** the service.
* Click **OK** and That's all.

![Component Services][png_ComponentServices]  

    Note: On Microsoft Windows Cluster the MSDTC service may be clustered in many different cluster roles. These settings should be applied on each instance.

### Troubleshoot
How to troubleshoot Microsoft DTC firewall issues.
* [How to troubleshoot connectivity issues in MS DTC by using the DTCPing tool](https://support.microsoft.com/en-us/help/918331/how-to-troubleshoot-connectivity-issues-in-ms-dtc-by-using-the-dtcping)
___

### :fa-hand-o-right: Next Step
:fa-rocket: [Install Integration Manager][Install]  

#### :fa-cubes: Related
:fa-globe: [Web Client][]    
:fa-archive: [Log Agents][]    
<!--References -->

[png_ComponentServices]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/Common/TransactionsMSDTC/ComponentServices.png
[https://technet.microsoft.com/en-us/library/cc759136(v=ws.10).aspx]:[https://technet.microsoft.com/en-us/library/cc771891.aspx]
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Install]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Overview.md?at=master

[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/Log%20Agents.md?at=master
