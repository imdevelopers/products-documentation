<tags data-value="Open Source, 3rd party, Libraries"></tags>

:fa-cloud-download: 3rd party libraries
=====
					
___

## :fa-info: Information

Integration Manager is built using the following open source and 3rd party libraries, licensing form varies and is provided component by component:

* [Bootstrap][] (MIT License)
* [toastr][] (MIT License)
* [Font Awesome][fontawesome] (MIT License)
* [DTASpy][] (Integration Manager uses a copy with changes since submitted bugs are not fixed on GitHub) (MIT License)
    * [SharpZipLib][]   (GNU GPL license with special exception)
* [Swagger][Swagger] - Documentation and development support for our [Web API][Web API] (Apache license)
* [Log4Net][Log4NetApache] - Used internally and in the [Log4Net appender][Log4Net]. (Apache license)
    * [Log4Net on NuGet][Log4NetNuGet] 
* [jsPlumb][jsPlumb] - Integration Landscape (MIT License)  
* [Mustache][] - Remote Configuration and Forms from [Monitor Agents][]  ([MIT License]([https://github.com/mustache/mustache.github.com/blob/master/LICENSE.md]))
* [Summernote][] - For [Articles][] creation and design. (MIT License)  
* [CsvHelper ][] - For [Export to CSV][] in both the [Web Client][] and the following [Monitor Agents][] (dual License, [Microsoft Public License][MS-PL] and Apache 2.0 license, more info [here](https://github.com/JoshClose/CsvHelper/blob/master/LICENSE.txt)) 
    * [Dynamics CRM][]
* [Apache NMS][] (Apache license)  
    * [Web API][Web API] - Resend of messages to ActiveMQ
    * [Message Queues][] - List queues and number of events
    * [Pickup Service][] - Read and write messages from and to ActiveMQ
* [Apache NMS ActiveMQ][] (Apache license)  
    * [Web API][Web API] - Resend of messages to ActiveMQ
    * [Message Queues][] - List queues and number of events
    * [Pickup Service][] - Read and write messages from and to ActiveMQ
* [CodeMirror][] ([MIT License](https://github.com/codemirror/CodeMirror/blob/master/LICENSE))
    * [Web Client][] - View/Repair Message Payload/Body  
* [LazyCache][] - Manages Caching in most parts of Integration Manager ([MIT License](https://github.com/alastairtree/LazyCache/blob/master/LICENSE)) 
* [Date Range Picker][] - Selects date and time in [Web Client][] ([MIT License](http://www.daterangepicker.com/#license))
___

### :fa-hand-o-right: Next Step  
* :fa-home: [Home][Latest Version]

#### :fa-cubes: Related  
[Bootstrap]:https://github.com/twbs/bootstrap
[fontawesome]:https://fontawesome.github.io
[Latest version]:http://documentation.integrationmanager.se/#/ "Latest version of the documentation Online"
[DTASpy]:https://github.com/niik/DtaSpy
[SharpZipLib]:http://icsharpcode.github.io/SharpZipLib/
[Swagger]:http://swagger.io
[jsPlumb]:https://github.com/jsplumb/jsPlumb/
[Summernote]:http://summernote.org/

[Apache NMS]:http://activemq.apache.org/nms/apachenms.html
[Apache NMS ActiveMQ]:http://activemq.apache.org/nms/apachenmsactivemq.html

[Web API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Web%20API/Overview.md?at=master

[Log4Net]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Log4Net/Log4Net%20Appender.md?at=master
[Log4NetApache]:http://logging.apache.org/log4net/index.html
[Log4NetNuGet]:https://www.nuget.org/packages/log4net/

[Mustache]:https://github.com/mustache/mustache.github.com
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Articles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/8.%20Articles/Overview.md?at=master


[MS-PL]:https://opensource.org/licenses/MS-PL
[CsvHelper]:https://github.com/JoshClose/CsvHelper/
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master

<!-- Log Agents -->
[Pickup Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents%2FPickup%20Service%2FOverview.md

<!-- Monitor Agents -->
[Dynamics CRM]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Dynamics%20CRM/Overview.md?at=master
[Message Queues]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents%2FMessage%20Queues%2FOverview.md

[toastr]:http://codeseven.github.io/toastr/
[CodeMirror]:https://github.com/codemirror/CodeMirror

[LazyCache]:https://github.com/alastairtree/LazyCache
[Date Range Picker]:http://www.daterangepicker.com
