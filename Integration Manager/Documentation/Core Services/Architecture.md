<tags data-value="Information,About,Documentation"></tags>

Architecture
=====

## Information
This is how the [Web Client][Web Client], [Web API][Web API], **core services** and databases are connected to each other in an overview.

![Core Architecture][png_CoreArchitecture]

Note that some control messages can be sent between services but most of the communication is done using the IMConfig database.

### :fa-hand-o-right: Next Step  
:fa-globe: [Web Client][Web Client]  

##### :fa-cubes: Related  
:fa-cloud: [Web API][Web API]  

___

[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[png_CoreArchitecture]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/CoreArchitecture.png
[Web API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Web%20API/Overview.md?at=master
