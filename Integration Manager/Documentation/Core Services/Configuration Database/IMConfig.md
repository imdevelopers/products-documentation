<tags data-value="Database"></tags>

:fa-database: IMConfig
=====
					

___

## :fa-info: Information
Integration Manager is a very configurable product where all common data is stored in a central shared SQL Database named **IMConfig**. The actual name may vary between different installations, see more about this in the documentation about [installation][InstallIMUpdate].

The **IMConfig** database is part of the [Core Services][CoreServicesArchitecture] which means that the [IMUpdate installer][IMUpdate] takes care about installation and updates.

* The configuration database is typically very small
* It is recommended to run the **IMConfig** database in full recovery mode (in contrast to [Log Databases][])
* The SQL Instance hosting the **IMConfig** database must use **Linked Servers** to access [Log Databases][]
    * It is possible to use multiple SQL instances for the [Log Databases][]    
    
    NOTE: You must use linked server even when hosting the Log Databases within the same instance as the IMConfig database

* There is one and only one IMConfig database per instance (environment) of Integration Manager
* Multiple IMConfig databases with unique database names may be installed on the same SQL Instance
* There are no SQL Agent jobs (due to the versatile compatibility of Integration Manager with SQL Express)
* This database must be backuped according to customer / end user standards/policys
* SQL database Read/Write permissions must be set, see more [here][How to set Logon As A Service] and [here][InstallIMUpdate]
* Any version of SQL Server may be used ranging from SQL Server 2008 R2 and later, also SQL Express 2008 R2 and later


## Examples
In the displayed example below different run-time examples are shown.

![IMConfig][0]

___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-database: [Add or manage Log Database][]  

#### :fa-cubes: Related  
:fa-database: [Log Databases][]  
:fa-user-secret: [How to set Logon As A Service][]


[Log Databases]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Log%20Databases/Log%20Databases.md?at=master
[Add or manage Log Database]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Log%20Databases/2.%20Add%20or%20manage%20Log%20Database/Add%20or%20manage%20Log%20Database.md?at=master&fileviewer=file-view-default
[CoreServicesArchitecture]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Architecture.md?at=master

[InstallIMUpdate]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Install%20IM%20Update.md?at=master
[IMUpdate]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Overview.md?at=master

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/IMConfig.png


[How to set Logon As A Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Logon%20As%20A%20Service.md?at=master
