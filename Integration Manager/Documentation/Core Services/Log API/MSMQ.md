<tags data-value="LogAPI, MSMQ"></tags>

:fa-cloud-download: Log API - MSMQ
=====

The Log API can pick up events logged to MSMQ and put them into Integration Manager.

To support MSMQ you should add net.msmq as a protocol in IIS  
![png_IISAdvancedSettings][]

![png_IISEnabledProtocols][]

troubleshooting https://support.integrationsoftware.se/support/solutions/articles/1000160886-log-api-msmq-queue-access-denied



[png_IISEnabledProtocols]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/LogAPI/IISEnabledProtocols.png
[png_IISAdvancedSettings]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/LogAPI/IISAdvancedSettings.png
