<tags data-value="LogAPI, Messages, Events"></tags>

:fa-cloud-download: Log API
=====
					
___

## :fa-info: Information
The **Log API** is used to **log** data such as **events** and **messages** from any .NET, JAVA (and many more WS* compliant platforms), data is stored/archived in Integration Manager which then allows you to do further role based search using [Log Views][]. There are multiple ways to log data into Integration Manager using either REST or WCF. Other options are also available using [Log Events][] in combination with the [Pickup Service][PickupService]

The following [Log Agents][] uses the **Log API**:

* **[BizTalk][BizTalk]** - Using pipeline components for Integration Manager  
* **[Log4Net][Log4Net]** - Simply add some dll:s and a new appender to the app.config file of your custom .NET application 
* **[Mule][Mule]** - Uses Log4J from Logger and Business Events (code)
* **[VB6/COM/VBScript][VBScript]** - Log from VB6/VBScript code
* **[IBM Integration Bus Log Agent][]** - Get events and messages from IBM Integration Bus using IBM Monitor Events

### The Log API - REST supports the following protocols:
* HTTP
* HTTPS

NOTE: when using https the certificate must be valid when browsing the **Log API**

If the browser returns that the certificate is not secure, then the **Log API** will not work (since it can't be called upon).  
![notvalidcert][1]

### The Log API - WCF supports the following protocols:
* HTTP (synchronous and asynchronous)
* HTTPS (synchronous and asynchronous)
* TCP (synchronous and asynchronous)
* MSMQ (asynchronous)

From .NET based clients the Log API may enlist in distributed transactions if logging is mandatory part of your custom solution. See [DTC][MSDTC] for additional information. 

    NOTE: WCF Activation must be properly installed using Server Manager (Add roles and features)

![WCFActivation][2]


### Name of the services found on the adress (replace *localhost* as appropriate)
 Default adress is `http://localhost/IM/LogAPI/`  
* LogApiService.svc (HTTP)
* LogApiServiceSecure.svc (HTTPS)
* LogApiServiceOneWay.svc (MSMQ)
* LogApiServiceSessionful.svc (TCP)
* LogApiServiceBase.svc (HTTP, This service is an alive checker and support  updating of the Log API itself)


## :fa-tags: Context Options
Logged events can carry information that controls the behaviour when being processed by the [Logging Service][], see more in [here][Context Options].


## Message Type
When logging to the Log API, you might want to control the name of the Messages Type (especially for non XML compliant payload like flatfiles, EDI...). 

___

### :fa-hand-o-right: Next Step
:fa-archive: [Log Agents][Log Agents]    

#### :fa-cubes: Related
:fa-hdd-o: [Logging Service][Logging Service]

<!-- References -->

[BizTalk]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Microsoft%20BizTalk%20Server/Overview.md?at=master
[Log4Net]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Log4Net/Log4Net%20Appender.md?at=master
[Mule]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/Overview.md?at=master
[VBScript]:https://localhost/

[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master

[Log4Net Appender]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Log4Net/Log4Net%20Appender.md?at=master
[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/Log%20Agents.md?at=master

[MSDTC]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Transactions%20MSDTC.md?at=master
[Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/Search%20Fields.md?at=master


[IBM Integration Bus Log Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/IBM%20Integration%20Bus/Overview.md?at=master
[PickupService]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Pickup%20Service/Overview.md?at=master
[Log Events]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Log%20Event/Overview.md?at=master
[Context Options]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Log%20Event/Context%20Options.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master


[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/Common/LogonAsAService/CertNotValid.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/LogAPI/WCFServices.png


