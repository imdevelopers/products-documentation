<tags data-value="Log, Log Views, Messages, Events, Search Fields"></tags>

:fa-hdd-o: Logging Service
=====
					


___

## :fa-info: Information
The **Logging Service** processes, and reindexes messages. This is a Windows Service part of the [Core][CoreServicesArchitecture] services.

* This service should always be running.
* This service may be clustered IF installed on a shared volume (the Windows Fail over cluster manager must move the disk resource that the service depends on)
* The **Logging Service** may have to be restarted when changing some of the [System Parameters][System Parameters] or [Log Agent Source settings][Log Agent]

### :fa-database: SQL Rights
The account running the **Logging Service** must have the following SQL rights assigned:
   
* **SQL Instance(s)** - Where `IMConfig` and `IMLog*` databases are located.
    * public - Right to logon to instances and databases
    * dbcreator - right to create new IMLog databases
    * diskadmin - rights to create database files for new databases
    * securityadmin - rights to assign rights on new databases
    * ddl_admin*
    * :fa-compress: **Shrink**, in order for the Logging service to be able to shrink IMLog* databases, the [service acccount][How to set Logon As A Service] requires membership in the **sysadmin** fixed server role **or** the **db_owner** fixed database role. See more [here](https://docs.microsoft.com/en-us/sql/t-sql/database-console-commands/dbcc-shrinkdatabase-transact-sql?view=sql-server-2017)

```
    NOTE: *ddl_admin is required in order for the service account to have proper rights to read statistics. Without this permission performance may be degraded, especially true for remote servers (linked servers). Read more [here](https://thomaslarock.com/2013/05/top-3-performance-killers-for-linked-server-queries/). Contact our support if you have any questions about this.  
```    

* **About Integration Manager specific databases**
* IMConfig
    * dbatareader
    * dbdatawriter
    * ddl_admin
    * sysadmin or db_owner**
    
* IMLog* (can be multiple )
    * dbatareader
    * dbdatawriter
    * ddl_admin
    * sysadmin or db_owner**

```
    NOTE: **See specific comment for SQL instance above for membership in either sysadmin and/or db_owner for automatic shrink of IM related datbases
```    
    
## :fa-area-chart: Performance 
The performance of this service heavily depends on the current workload on CPU, memory, network, SQL and disk IO on involved machines.
User Configuration that affects processing comes from [Message Types][] and [Search Fields][]. The amount of work also depends on the size of the payload/Context, the current amount of data in the Log Database, current workload 

Expected performance depending on setup and amount of data

<table style="border:1px solid">
<tr>
<th>Average processing time</th>
<th>Number of messages/second</th>
<th>SQL Disk Read performance MB/s (Volume for FileGroup Index)</th>
<th>SQL Disk Write performance MB/s (Volume for FileGroup Index)</th>
<th>#SQL Cores</th>
<th>#Cores Logging Service</th>
<th>#RAM SQL GB (memory limited)</th>
<th>#RAM Logging Service GB</th>
<th>Comment</th>
</tr>
<tr>
<td>32 ms</td>
<td>>30</td>
<td>>1350</td>
<td>>52</td>
<td>2</td>
<td>2</td>
<td>7</td>
<td>7</td>
<td>Azure Demo environment</td>
</tr>
<table>

___

### :fa-hand-o-right: Next Step  
:fa-key: [System Parameters][]   
:fa-search: [Search Fields][]  

#### :fa-cubes: Related  
:fa-file: [Message Types][]  

[Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/7.%20Included%20Message%20Type/Included%20Message%20Type.md

[Log Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/Log%20Agents.md?at=master

[CoreServicesArchitecture]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Architecture.md?at=master
[System Parameters]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/System%20Parameters/Overview.md?at=master

[Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/Search%20Fields.md?at=master

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/BizTalk/SQLInstanceSecurityRights.png

[How to set Logon As A Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Logon%20As%20A%20Service.md?at=master