<tags data-value="Monitor Views, Monitor Agents, Sources"></tags>

:fa-download: Monitoring Service
=====
					


___

## :fa-info: Information
Integration Manager communicates with the  [Monitor Agents][] using a Windows Service named **MonitoringService** part of the [Core Services][CoreServicesArchitecture].

This Windows Service is installed and configured during initial setup, see more in the [installation][Install] documentation.

The current state of the *Monitoring Service* is listed in the **Status** icon within the [Web Client][].  
![status][3]

By **Default** the Monitor Agents uses TCP port 8000 for communication. **Service Bus relaying** is also possible for hybrid/cloud enabled solutions.
* [TCP Port, default 8000][Port8000]
* [Service Bus Relaying][]

![defaultport8000][1]

The actual whereabouts of the **Monitoring Service** can be found from the [Overview][] in the [Administration][] of the [Web Client][]  
![monitoringservicefromoverview][2]
___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-download: [Add the Monitor Agent as a Source][Sources]  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]      

#### :fa-cubes: Related  
:fa-desktop: [View All Monitor Views][]    
:fa-upload: [Monitor Agents][]  
:fa-globe: [Web Client][]    


[Overview]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master



[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master  
[View All Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/1.%20View%20All%20Monitor%20Views/View%20All%20Monitor%20Views.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master

[Port8000]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Shared%20Port%208000.md?at=master
[Service Bus Relaying]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/ServiceBusRelaying.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Install]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Overview.md?at=master
[Status]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Menu%20-%20Status.md?at=master

[CoreServicesArchitecture]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Architecture.md?at=master


[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/Common/Default8000.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/MonitoringService/OverviewInformation.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Status.png