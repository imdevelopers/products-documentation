<tags data-value="Information,About,Documentation, Reports, Power BI, Analytics"></tags>

:fa-pie-chart: Reports and Power BI
=====
Create insights and take advantage of your precious data using the built in powerful [Web API][]. You can easily create reports using for example:

* Microsoft Excel
* [Microsoft Power BI][Power BI]
* QlikView
* Any client that consumes JSON data.

## Power BI Report template
Contact our support to get the latest version of the free generic Power BI template that we have created for you to expand further with.


## Reporting examples
![RepositoryPowerBI.png][2]  
*Power BI example from Repository model*

![powerbimonitor][0]  
*Power BI example from monitor data*

![powerbidashboard][1]  
*Embedded Power BI example from logged data in the Dashboard*


### :fa-hand-o-right: Next Step  
:fa-cogs: [Administration][]  
:fa-cloud: [Web API][]   

##### :fa-cubes: Related  
:fa-sitemap: [Repository][]  


<!--References -->
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
[Web API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Web%20API/Overview.md?at=master

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Reports/PowerBIMonitorExample.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Reports/PowerBIDashboard.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/Reports/RepositoryPowerBI.png


[Power BI]:https://powerbi.microsoft.com/en-us/