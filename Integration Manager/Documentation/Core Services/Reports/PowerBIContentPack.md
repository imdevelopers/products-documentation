<tags data-value="Information,About,Documentation, Reports, Power BI, Analytics"></tags>

:fa-pie-chart: Connecting Power BI to Integration Manager using the Content Pack
=====

## :fa-info: Information
[Power BI][] is a reporting tool from Microsoft that you can use to visualize and share your data like:

* :fa-hdd-o: [Log][1] - Create reports based on messaging
* :fa-desktop: [Monitoring][2] - Create reports and insights about the resources used for the system integration solutions
* :fa-sitemap: [Repository Model][Repository] - Create reports and insights about your service portfolio
 
 With Power BI, you can do additional things like create graphs and charts from Integration Manager, as well as display Integrations, Monitoring resources, and for example cities from Logged order data on a geographical map.

## Power BI Content Pack
Due to restrictions set by Microsoft our Content Pack for the Azure Market place is not yet available. Please contact our support to find out more about this.

### :fa-hand-o-right: Next Step  
:fa-cogs: [Administration][]  

##### :fa-cubes: Related  
:fa-sitemap: [Repository][]  

<!--References -->
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
[Web API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Web%20API/Overview.md?at=master
[Power BI]:https://powerbi.microsoft.com/en-us/
[1]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/Log.md?at=master
[2]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/Monitor.md?at=master
