<tags data-value="Log, Log Views, Messages, Events, Search Fields"></tags>

:fa-cloud: Web API
=====
					


___

## Information
The **Web API** is a REST based API on which Integration Manager is built. This also makes it possible for partners and customers to build their own clients. Typical usage would be custom dashboards and mobile apps with custom alerts.   
![webapi][1].

Any and all consumers of this API must authenticate using Windows Domain credentials for [users][Users] part of a [Role][Roles] assigned in [Monitor Views][] and [Log Views][]. The API is built to enforce the integrity and policy set by **Administrators** of Integration Manager. Do note that all operations that changes information, and also some operations like download/read messages are being logged into a tamper resistant **LogAudits** table.  

If access is not granted through [Log Views][] and/or [Monitor Views][] the [user][Users] must be part of the built in **Administrators** [Role][Roles] in order to use this **Web API**

The App Pool identity running the **Web Api** must have read, write and Execute permissions on all database objects part of Integration Manager ([IMConfig][] and the [Log Databases][]).

## :fa-pie-chart: Web API for Power BI and Reports
Use the [Web API][] to create your own [reports][Reports]. Simply copy the link for the data you are currently looking at from within the [Web Client][Web Client].  
![copyapiurl][3]

## Web API Help
**Web API Help Page**  
![help][2]

## Swagger
The [Web API][] has [Swagger][Swagger] support. With the Swagger-enabled API, you get interactive documentation, client SDK generation and discoverability. Swagger is a simple yet powerful representation of the RESTful API.

![swagger][4]


___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]  
:fa-plus-square: :fa-hdd-o: [Add or manage Log Views][]  
:fa-plus-square: :fa-user: [Add or Manage Users][]  
:fa-plus-square: :fa-group: [Add or Manage Roles][]  

##### :fa-cubes: Related  
:fa-desktop: [Monitor Views][]  
:fa-hdd-o: [Log Views][]  
:fa-user: [Users][]  
:fa-group: [Roles][]  

[IMConfig]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Configuration%20Database/IMConfig.md?at=master
[Log Databases]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Log%20Databases/Log%20Databases.md?at=master


[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/WebAPI/RESTFulAPI.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/WebAPI/WebAPIHelp.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/WebAPI/CopyAPIURL.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/WebAPI/swagger.png

[Add or manage Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/2.%20Add%20or%20manage%20Role/Add%20or%20manage%20Role.md?at=master
[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master

[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Add or manage Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/2.%20Add%20or%20manage%20User/Add%20or%20manage%20User.md?at=master

[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[Reports]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Reports/Overview.md?at=master

[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Swagger]:http://swagger.io
[Web API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Web%20API/Overview.md?at=master