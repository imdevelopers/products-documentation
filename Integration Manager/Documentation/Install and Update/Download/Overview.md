<tags data-value=""></tags>

:fa-arrow-circle-o-down: Download
=====
					
___

## :fa-info: Information
There are two ways to get the latest version of Integration Manager
1. Direct download in Update Client, this requires Internet access, see chapter **Download from Internet**
2. Manual download to local file system (including external media like USB...) See Chapter **Manual upload**
    * Once downloaded locally, the file can be uploaded to the **IM Update Web Application**


The current installed version of Integration Manager is displayed in the [Web Client][], read more [here][Version].
___

### 1. Download from Internet

![Direct download of Integration Manager][png_DownloadFile]  
Click **Download file** to get the latest officially released version (requires Internet connection)

![Confirmation downloaded][png_DownloadFile_Downloaded]  
The file was downloaded

___
### 2. Manual upload
Alternative method for private releases or when there is no Internet connection available.

![Select file for manual upload][png_Manual_SelectFile]  
Manual_SelectFile

![Confirmation uploaded][png_Manual_Uploaded]  
The file was uploaded


### :fa-hand-o-right: Next Step  
:fa-rocket: [Install][] (new installation)  
:fa-arrow-circle-o-up: [Update][] 

##### :fa-cubes: Related  
:fa-globe: [Web Client][]  
:fa-file-text-o: [Release Notes][]   

<!--References -->
[png_DownloadFile]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Download/DownloadFile.png  
[png_DownloadFile_Downloaded]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Download/DownloadFile_Downloaded.png

[png_Manual_SelectFile]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Download/Manual_SelectFile.png  
[png_Manual_Uploaded]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Download/Manual_Uploaded.png


[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Update]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Update/Overview.md?at=master
[Install]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Installation/Overview.md?at=master
[Release Notes]:http://support.integrationsoftware.se/support/discussions/forums/1000227207 "Access the release notes for Integration Manager"
[Version]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Menu%20-%20Version.md?at=master
