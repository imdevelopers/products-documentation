<tags data-value="IMUpdate"></tags>

:fa-rocket: Install IM Update
=====
___

## :fa-info: Information
In this section you will learn how to install the **IM Update Application** software. This installation has two features:  
1. **IM Update Windows Service** (to be installed on one or more servers)
2. **IM Update Web Application**

The **IM Update Web Application** helps you download and install/update your **Integration Manager** environments.

### :fa-check-square-o: Installation Checklist

**PreReq**: You must have downloaded the latest **IM Update** version to install and configured the Windows and SQL Server machines prior to installation.

:fa-download: [Download Latest version](http://download.integrationsoftware.se)

You need to perform installation and update locally using a remote desktop connection on the server hosting the IIS **IM Update Web Application** . 

:fa-user-secret: The Service account for the **IM Update Service** be a local administrator and must have the [Logon as Service Right][How to set Logon As A Service].
The WCF ports in use must be allowed, see [Port8000][] for more details.


### :fa-database: SQL Server

* **SQL Instance(s)** - Where `IMConfig` and `IMLog*` databases are located.

* SQL Binaries installed, see sub chapter Common Errors (Missing SQL package).
* SQL can have any CI AS compatible collation
* Linked Servers setup (for Example localhost or localhost\instancename)
    
* Account rights 
    * public - Right to logon to instances and databases
    * dbcreator - right to create new IMLog databases
    * diskadmin - rights to create database files for new databases
    * securityadmin - rights to assign rights on new databases
    * ddl_admin*

```
    NOTE: *ddl_admin is required in order for the service account to have proper rights to read statistics. Without this permission performance may be degraded, especially true for remote servers (linked servers). Read more [here](https://thomaslarock.com/2013/05/top-3-performance-killers-for-linked-server-queries/). Contact our support if you have any questions about this.  
```    

```
    NOTE: If you are using High Availability SQL Servers, accounts and rights must be applied on all instances since this information is not being synchronized
```    



The following steps describes the steps required to install the **IM Update Application**.

## :fa-step-forward: Start the installer
![Start installer for IMUpdate][png_Start]  

Double click on the IM.Update.Installer-4.x.x.x.msi
This will present a **'Welcome'** screen.

![First step klick next][png_FirstStep]  
Click **'Next'** to continue with the installation process or click **'Cancel'** to quit the installer.

![Select what to install][png_SelectType]  
Select location and the components to install. Default settings are recommended. 

1. Select to include the **IM Update Windows Service**. This Service is almost always part of each install, on each server hosting Integration Manager components.
2. Select to include the **IM Update Web Application**. This is the Web based Client and needs IIS to be installed.

Click **'Next'** to continue with the installation process or **'Cancel'** to quit the installer.

![Enter credentials][png_Credentials]  
Enter the credentials for the Account to run the App Pool account

1. **Domain**: The name of the Domain the Server belongs to. If a local account is being used, use the name of the server or . (dot) as Domain  
2. **Account**: The name of the account  
3. **Password**: The password for the account.   

        Note: The account must have 'Log on as Service Right'

:fa-user-secret: Remember the account running the `IM Update Windows Services` needs logon as service right, see [How to set Logon As A Service][] for additional information.

The WCF ports in use must be allowed, see [Port8000][] for more details.

Click **'Next'** to continue with the installation process or **'Cancel'** to quit the installer.

![Start installation][png_Install]  
Start the installation by clicking on the **'Install'** button or **'Cancel'** to quit the installer.

![Problem to start the service][png_Problem_LogOnAsAService]  
If you get this message, the most common reason is that the account is not allowed to run as a service or is not local Administrator on the server. Follow the guide [How to set Logon As A Service][] and click retry. If the problem remains, click cancel and restart. Make sure that the credentials are typed correctly! 

    Tip: Check the local event log if there are errors (both System and Application)

![Start installation][png_Finish]  
When the setup has finished the default browser will open the address [http://localhost/IMUpdate/UpdateClient][], click the **'Finish'** button to Exit and quit the installer.
___

### :fa-hand-o-right: Next Step  
:fa-download: [Download the latest version of Integration Manager][Download]

##### :fa-cubes: Related  
:fa-rocket: [Install Integration Manager][]   
:fa-arrow-circle-o-up: [Update Integration Manager][Update]  

<!--References -->

[http://localhost/IMUpdate/UpdateClient]:http://localhost/IMUpdate/UpdateClient

[png_Start]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Setup/Start.png
[png_FirstStep]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Setup/FirstStep.png
[png_SelectType]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Setup/SelectType.png
[png_Credentials]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Setup/Credentials.png
[png_Install]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Setup/Install.png
[png_Problem_LogOnAsAService]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Setup/Problem_LogOnAsAService.png
[png_Finish]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Setup/Finish.png

[How to set Logon As A Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Logon%20As%20A%20Service.md?at=master

[Port8000]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Shared%20Port%208000.md?at=master


[Install Integration Manager]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Installation/Overview.md?at=master
[Update]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Update/Overview.md?at=master

[Download]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Download/Overview.md?at=master