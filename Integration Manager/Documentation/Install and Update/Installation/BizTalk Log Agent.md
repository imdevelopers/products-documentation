<tags data-value=""></tags>

:fa-edit: Configure BizTalk Log Agent
=====
					
## :fa-info: Information
Add or remove logging for any number of BizTalk Server environments (like 1 or more production, 1 test and 1 prod, etc.)

## :fa-check-square-o: Prerequisites
Before configuring the *BizTalk Log Agent* make sure all [prerequisites][BizTalkPreReqs] are in place.

## :fa-cogs: Configuration
Adding additional Log Source for BizTalk is easy.

In the [Update tool][IMUpdate] from the selected environment, click on the *BizTalk Log Agents* menu item.  
![BizTalkLogAgentMenuItem][0]

Click on the **Add Log Agent Source** button to add the new *BizTalk Log Agent*.   
![AddBizTalkLogAgentButton][1]

### Log Source
1. **Name** - Give the Log Agent a user friendly name (include the name of the target environment)
2. **Log Agent Value Id** - A unique Id for this Log Agent Source. This value should never change once set! 
3. **Description** - A user friendly description about this log source
4. **Log Agent - BizTalk settings** - Check to mark this source as a BizTalk on premise log source

![LogAgentSourcePart1][2]

### :fa-database: BizTalk Databases
1. **BizTalkMgmtDb Server** - Enter the name of the SQL instance hosting the BizTalk management database (this is the linked server information from IM SQL instance against BizTalk SQL Instance)
2. **BizTalkMgmtDb Name** - Enter the name of the BizTalk management database
3. **BizTalkDTADb Server** - Enter the name  of the SQL instance hosting the BizTalk tracking database (this is the linked server information from IM SQL instance against BizTalk SQL Instance)
4. **BizTalkDTADb Name** - Enter the name of the BizTalk tracking database

![LogAgentSourcePart2][3]

### :fa-archive: Tracking

1. **Polling Interval** - Set the polling interval for the [Logging Service][]. This is the time between attempts to ask for new logged data. Default is 30 seconds.
2. **Max number of Events to copy** - Set the maximum number of events to fetch on each run. A large number may impact performance. Default is 5000.
3. **Max number of Message/Context data rows to copy** - Set the maximum number of payloads and context rows to fetch on each run. Default is 500.

![LogAgentSourcePart3][4]

### :fa-sign-in: EndPoints
All ports from BizTalk can be synchronized as [End Points][] available within the [Repository][] model.
 **Sync End Points Polling Interval** - Set the poll period in seconds between synchronization attempts
 **Include End Points Tracking status** - Should be checked when using BizTalk

![LogAgentSourcePart4][5]

### :fa-trash-o: Remove
A BizTalk Log agent may be removed by clicking the **Remove BizTalk Logging** button.

### :fa-save: Save
Click the **Save** button to save changes to configuration.

    NOTE: The Logging Service must be restarted to honor the changes saved.
___

### :fa-hand-o-right: Next Step  
:fa-plus: [Add or manage Log Agents][]  
:fa-cogs: [Administer Integration Manager][Administration]  
:fa-rocket: [InstallIMUpdate][]

##### :fa-cubes: Related  
:fa-check-square-o: [BizTalkPreReqs][]  
:fa-hdd-o: [Logging Service][]    
:fa-sign-in: [End Points][]      
:fa-sitemap: [Repository][]  
  
[InstallIMUpdate]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Install%20IM%20Update.md?at=master
[IMUpdate]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Overview.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[BizTalkPreReqs]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Microsoft%20BizTalk%20Server/1.%20PreRequisites.md?at=master
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/BizTalk/ConfigureBizTalkLogAgentMenu.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/BizTalk/AddBizTalkLogAgentButton.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/BizTalk/LogAgentSourcePart1.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/BizTalk/LogAgentSourcePart2.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/BizTalk/LogAgentSourcePart3.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/BizTalk/LogAgentSourcePart4.png


[End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/8.%20Included%20End%20Points/Included%20End%20Points.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Add or manage Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/2.%20Add%20or%20manage%20Log%20Agent%20Source/Add%20or%20manage%20Log%20Agent%20Source.md?at=master
