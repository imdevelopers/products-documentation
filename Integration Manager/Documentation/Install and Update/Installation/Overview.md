<tags data-value=""></tags>

:fa-rocket: Install Integration Manager Core Services
=====
					


___

## :fa-info: Information
This document will guide you through the steps required to install the Core Services for Integration Manager. Installation is easily being performed by an administrator using the [IM Update Application][InstallIMUpdate]. The install process usually takes less than 15 minutes.

During the installation you must click inside every single text box to verify/validate the content. When valid, a :fa-check: (green) check appears on the left side of the label and you can move on to the next input field.

:fa-exclamation-triangle: Make sure to download, install and use the latest version of the [IM Update Application][InstallIMUpdate] before installing or [updating][Update] the Core Services of Integration Manager.

### :fa-check-square-o: Installation Checklist
The following pre-requisites must be met before installing the core services for **Integration Manager**:

### :fa-windows: Windows
* One or more Servers (Windows and SQL) configured as described in the PDF/DOCX **Appendix 1**, part of the business agreement.  
* Service Accounts has at least [Logon as Service Right][How to set Logon As A Service]. Service accounts are used within 
    * IM Core Services
        * Logging Service
        * Monitoring Service
        * LogApi
        * WebApi        
    * Monitor Agents
* User Accounts 
    * Must be part of one or more groups that has the ["Access this computer from the network"](https://docs.microsoft.com/en-us/windows/security/threat-protection/security-policy-settings/access-this-computer-from-the-network) local policy set 

### :fa-database: SQL Server
* SQL Binaries installed, see sub chapter Common Errors (Missing SQL package).
* SQL can have any CI AS compatible collation
* Linked Servers setup
    * BizTalk if applicable ()
    
* Account rights ([Logging Service][], [Monitoring Service][], [BizTalk Server Monitor Agent][BizTalk Agent], [SQL Server Monitor Agent][SQL Agent], [IMUpdate Service][InstallIMUpdate]).
```
    NOTE: If you are using High Availability SQL Servers, accounts and rights must be applied on all instances since this information is not being synchronized
```    
The Database reports may require additional assigned rights, if you can't open the report, add the following grant:
```sql
USE MASTER
GO
GRANT VIEW SERVER STATE TO [DOMAIN\user];
GO
```        

### :fa-eye: Integration Manager
* Using the latest version of [IM Update Application][InstallIMUpdate]
    * [Downloaded][Download] the latest software package of **Integration Manager** 

### :fa-globe: IIS
* IIS Application Pools  

    * [Web API][] - App Pool (should run as AppPoolIdentity (low degree of access rights) to enforce security policies and avoid breaches to security policies) - Uses Impersonation from Install configuration (encrypted in web.config)
        * :fa-database: The configured account used in installation (**Installation account name**) must have the following SQL Rights for the impersonation to properly work:
            * [IMConfig][]
                * dbo
                * db_datareader
                * db_datawriter
            * IMLog databases
                * db_datareader
                * db_datawriter   
    * [Web Client][] - App Pool (should run as AppPoolIdentity (low degree of access rights) to enforce security policies and avoid breaches to security policies)
    * [Log API][Log API] - App Pool (should run as a named domain account)
        * :fa-database: The account used must have the following SQL Rights:
            * IMConfig
                * dbo
                * db_datareader
                * db_datawriter
            * IMLog databases
                * db_datareader
                * db_datawriter 

    Example for the application pools for a prod or test environment
    - Integration Manager Test (Used by the [Web Client][] and [Web API][])  
    - Integration Manager LogAPI Test (Used by the [Log API][Log API])  

    ![png_IISAppPools][]


    NOTE: The named AppPool accounts must be either local administrator or part of the IIS_IUSRS group  

    
    ![png_AppPoolRights][] 
___

## :fa-step-forward: Step #1

Using the IM Update Web Application from a modern browser. You must first have:
* Installed the [IM Update Application][InstallIMUpdate])
* [Download][] a version to install

:fa-rocket: **Click** on the Install button to open the installation dialogue.  
![png_Menu_Install_Button][]

### :fa-info-circle: General
The first step of installing **Integration Manager** involves the configuration of the unique specific instance. This step includes among other things setting up details for the Licensee.   

The following configuration steps must be followed in order:  
![Install][png_Installation_Start]  
1. **Customer Name**, Enter the company/customer name (Associated with the Product Key for Licensee)   
2. **Enter key**, Enter the Product Key (Associated with the Customer Name for Licensee)
3. **Environment Name**, Enter the mandatory custom name of the target environment (QA/DEV/PROD)
4. **Update Service Address**, AutoPopulated to **localhost**
    
    Tip: Integration Manager supports the use of a multiple Update Services, installed on many different servers useful for hardened environments. 

5. **Installation account name**, Enter credentials for the Service Account running the [Monitoring Service][], [Logging Service][] and the [WebAPI][Web API] (can be overridden individually later on in the installation)  
    4.1 Set User Name for Service Account    
6. **Installation account password** Enter the password
7. Click on **`Validate Account`** to validate that user and password information is correct

When every :fa-check: (green) is shown for every field the setup will automatically continue to the next installation step.

## :fa-step-forward: Step #2

### :fa-database: Configuration database
The second step of installing **Integration Manager** involves the setup of the configuration database called [IMConfig][]. 

The following configuration steps must be followed in order:  
![Install][png_Installation_IMConfig] 

1. **Use default Update Service**, When checked, the installation will use the Update Service information provided in the General section. For additional information about advanced configurations see [IM Update Application][InstallIMUpdate] for more information.
The default value is `http://localhost:8000/IM/Service/Update`  
![CustomInstallationUpdateService][]  
2. **Use default installation account**, When checked, the installation will use the Service credentials provided in the General section  
![CustomInstallationAccount][]  
3. **Use previously installed configuration database**, When checked, an existing configuration database will be used. This is typically chosen when performing in place updates (for example v3.x -> v4.x)   
Enter the SQL server where the database will be placed, if every parts of Integration Manager including the SQL is running och the same server you could use localhost, else you must enter the server name/address


![CustomInstallationOldIMConfig][]
*The user account running the installation application

4. **Database Server**, Enter the **Name** of the SQL Server database server
5. **Database Name**, Enter the name of the configuration database.. To avoid mixups the provided naming scheme like  `IMConfig_<environment>` should be honored.
6. **Install**, Click `Install` to install (or for advanced setups upgrade) the configuration database

## :fa-step-forward: Step #3

### Logging Service
![Install][png_Installation_LoggingService] 

1. **Folder**, Set the root path where the [Logging Service][] will be installed
2. Name of the [Logging Service][], to make mix-ups less possible now and in the future the name is `IM <environment> Logging Service` so it is recommended to use this naming scheme.
3. Click `Install` to install the Logging Service.

## :fa-step-forward: Step #4

### Monitoring Service
Install the Monitoring Service.

![Install][png_Installation_MonitoringService] 

1. **Folder**, Set the root path where the [Monitoring Service][] will be installed 
2. **Service Name**, Set the name of the [Monitoring Service][], to make mixups less possible now and in the future the name is `IM <environment> Monitoring Service` so its recommended to use this naming scheme.
3. Click `Install` to install the Monitoring Service.

## :fa-step-forward: Step #5

### Web API
Install the [Web API][].    
![png_Installation_WebApi][]
1. **Use Default Update Service**, to use the local update service (default) make sure to have this checkbox checked. If the [Web API][] is installed on some other IIS Server then the **IM Update Service** on that server must be used when updating Integration Manager. 

    NOTE: Only uncheck this checkbox for advanced setups (preferrably in dialogue with our technical support)

2. **Use default account for installation**, When checked, the installation will use the Service credentials provided in the General section. If not checked, enter account and password to use when installing the [Web API][].

3. **Local Installation**, Check to install on localhost (default and recommended)

4. **Folder**, the physical file path location to where to install the [Web API][]

5. **User default account as Impersonation Account**, When checked, the installation will use the Service credentials provided in the General section to configure impersonation for the [Web API][]

6. **IIS Web Site**, Enter the name of the web site where to install the [Web API][]

7. **IIS Application Pool**, Enter the name of the IIS App Pool (should run as **AppPoolIdentity** as also described in the Installation Checklist)

8. **IIS Virtual Path**, Enter the name of the IIS Virtual Path. Default is IM or IM-%Environment%

9. **Install**, click on the **Install** button to start the installation of the [Web API][] 

## :fa-step-forward: Step #6

### Web Client
Install the [Web Client][].  
![png_Installation_WebClient][]

1. **Use Default Update Service**, to use the local update service (default) make sure to have this checkbox checked. If the [Web Client][] is installed on some other IIS Server then the **IM Update Service** on that server must be used when updating Integration Manager. 

    NOTE: Only uncheck this checkbox for advanced setups (preferrably in dialogue with our technical support)

2. **Use default account for installation**, When checked, the installation will use the Service credentials provided in the General section. If not checked, enter account and password to use when installing the [Web Client][].

3. **Local Installation**, Check to install on localhost (default and recommended)

4. **Folder**, the physical file path location to where to install the [Web Client][]

5. **IIS Web Site**, Enter the name of the web site where to install the [Web Client][]

6. **IIS Application Pool**, Enter the name of the IIS App Pool (should run as **AppPoolIdentity** as also described in the Installation Checklist)

7. **IIS Virtual Path**, Enter the name of the IIS Virtual Path. Default is IM or IM-%Environment%

8. **Install**, click on the **Install** button to start the installation of the [Web Client][] 


## :fa-step-forward: Step #7

### Log API
Install the [Log API][].  

![png_Installation_LogAPI][]

1. **Use Default Update Service**, to use the local update service (default) make sure to have this checkbox checked. If the [Web Client][] is installed on some other IIS Server then the **IM Update Service** on that server must be used when updating Integration Manager. 

    NOTE: Only uncheck this checkbox for advanced setups (preferrably in dialogue with our technical support)

2. **Use default account for installation**, When checked, the installation will use the Service credentials provided in the General section. If not checked, enter account and password to use when installing the [Log API][].

3. **Local Installation**, Check to install on localhost (default and recommended)

4. **Folder**, the physical file path location to where to install the [Log API][]

5. **IIS Web Site**, Enter the name of the web site where to install the [Log API][]

6. **IIS Application Pool**, Enter the name of the IIS App Pool (should run as domain account with specific rights in the [Log Databases][] as also described in the Installation Checklist)

![png_Installation_LogApiAppPool][]

    NOTE: You should use an IIS AppPool with a named domain account with specific rights in the databases, as also described in the Installation Checklist.

7. **IIS Virtual Path**, Enter the name of the IIS Virtual Path. Default is IM or IM-%Environment%

8. **Install**, click on the **Install** button to start the installation of the [Log API][] 

## :fa-step-forward: Step #8
Click next after installing all the core services. The next step involves setting up the default behavior for Integration Manager

### :fa-cogs: Configuration
  
![png_Installation_Configure][]

## :fa-step-forward: Step #9

### General
![png_Installation_Configure_General][]

## :fa-step-forward: Step #10

### Logging
![png_Installation_Configure_Logging][]

## :fa-step-forward: Step #11
### Monitoring
![png_Installation_Configure_Monitoring][]

## :fa-step-forward: Step #12
### Active Directory Support
![png_Installation_Configure_AD][]

## :fa-step-forward: Step #13
### Next
![png_Installation_Configure_General_Next][]

## :fa-step-forward: Step #14
### Start Services
![png_Installation_StartServices][]

## :fa-step-forward: Step #15

### Start Logging Service
![png_Installation_StartLoggingService][]
![png_Installation_StartLoggingServiceStarted][]

After successful start, information about the newly created IMLog database will be shown:  
![png_Installation_IMLogDatabaseInformation][]

## :fa-step-forward: Step #16

### Start Monitoring Service
![png_Installation_StartMonitoringService][]
![png_Installation_StartMonitoringServiceStarted][]

## :fa-step-forward: Step #17

### :fa-flag-checkered: Finish
![png_Finish][]


## :fa-step-forward: Step #18

### Post Installation steps
After installation make sure to assign who has the right to update Integration Manager, see more [here][IMUpdateSettings]

Also make sure the appropriate SQL rights are set for new IM [Log Databases][], see more [here][ImLogAccessRolesImLogServiceUsers].

Additional tuning can be performed altering [System Parameters][]


## :fa-error: Common errors
Each text box must be clicked to validate content. If you do not click on each text box, the validation process will not be performed and the installation can not continue.

### #1: Missing SQL package

The file SqlPackage.exe was not found on the server 'localhost'  
![Missing SQL package][png_Installation_IMConfig_MissingSQLPackage]

Solution
Download and install the following three files from :fa-external-link: [here](http://download.integrationsoftware.se/prerequisites/SQL%20Server%202014/Data-Tier/64-bit/)

1. `DACFramework.msi`    
2. `SqlDom.msi`  
3. `SQLSysClrTypes.msi`  

![Data tier][png_Installation_IMConfig_Install_DataTier]

## #2: :fa-user-secret: IIS Authentication Settings 
The following authentication settings must be set on the **Web Client**, **Web Api** and the **Log Api**

### #3: :fa-globe: Web Client
![IISWebClientAuthenticationSettings][0]

### #4: :fa-globe: Web Api
Due to the password protection for the impersonated user you cannot edit this application without removing content from the web.config file.  
![IISWebApiAuthenticationSettings][1]

* 1. Make a copy of the web.config file
* 2. Remove the encrypted section from the web.config file and save      

![identity][3]  

*remove identity section*  
* 3. Make sure the **Authentication settings** are set as provided in the following example image:  
![IISWebApiAuthenticationSettings2][4]  

* 4. Replace web.config with your original file from step 1 (or re-encrypt it).  

### Decrypt
C:\Windows\Microsoft.NET\Framework64\v4.0.30319\aspnet_regiis.exe -pd system.web/identity -app "/IM/WebAPI" -site "Default Web Site" 

### Encrypt
C:\Windows\Microsoft.NET\Framework64\v4.0.30319\aspnet_regiis.exe -pe system.web/identity -app "/IM/WebAPI" -site "Default Web Site" 

    NOTE: You may need to change site and name of WebApi depending on installation/settings in IIS

    NOTE: Passwords with & must be encoded as &amp;

### #5: :fa-globe: Log Api  
![IISWebClientAuthenticationSettings][2]    


### #6: :fa-globe: Unauthorized
If end users can't login to either the [Web Client][] or the [Web API][] altough they are registered as either [Users][] or [Windows AD Groups][], make sure the end users have the local policy: ["Access this computer from the network"](https://docs.microsoft.com/en-us/windows/security/threat-protection/security-policy-settings/access-this-computer-from-the-network) set   
![png_accessthiscomputerpolicy][]  
*default settings example for Windows 2016*
___

### :fa-hand-o-right: Next Step  
:fa-cogs: [Administer Integration Manager][Administration]  
:fa-edit: [Configure BizTalk LogAgent][]

##### :fa-cubes: Related  
:fa-arrow-circle-o-down: [Download][]  
:fa-arrow-circle-o-up: [Update][]  
:fa-globe: [Web Client][]  
:fa-file-text-o: [Release Notes][]  
:fa-hdd-o: [Logging Service][]  
:fa-download: [Monitoring Service][]    
:fa-database: [Log Databases][]  

<!--References -->
[png_IISAppPools]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/IISAppPools.png
[png_AppPoolRights]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/AppPoolRights.png

[png_Menu_Install_Button]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/Menu_Install_Button.png

[png_Installation_Start]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/Installation_Start.png
[png_Installation_IMConfig]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/Installation_IMConfig.png
[png_Installation_IMConfig_MissingSQLPackage]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/Installation_IMConfig_MissingSQLPackage.png
[png_Installation_IMConfig_Install_DataTier]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/Installation_IMConfig_Install_DataTier.png
[png_Installation_WebApi]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/Installation_WebApi.png
[png_Installation_WebClient]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/Installation_WebClient.png
[png_Installation_LogApiAppPool]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/LogApiAppPool.png
[png_Installation_Next]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/Next.png
[png_Installation_Configure]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/Configure.png
[png_Installation_Configure_General]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/Configure_General.png
[png_Installation_Configure_Logging]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/Configure_General_Logging.png
[png_Installation_Configure_Monitoring]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/Configure_General_Monitoring.png
[png_Installation_LogAPI]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/Installation_LogAPI.png
[png_accessthiscomputerpolicy]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/AccessThisComputerFromTheNetworkLocalPolicy.png


[png_Installation_Configure_AD]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/Configure_General_AD.png

[png_Installation_Configure_General_Next]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/Configure_General_Next.png

[png_Installation_StartServices]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/StartGeneral.png
[png_Installation_StartLoggingService]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/StartLoggingService.png
[png_Installation_StartLoggingServiceStarted]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/StartLoggingServiceStarted.png
[png_Installation_StartMonitoringService]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/StartMonitoringService.png
[png_Installation_StartMonitoringServiceStarted]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/StartMonitoringServiceStarted.png
[png_Installation_IMLogDatabaseInformation]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/IMLogDatabaseInformation.png
[png_Finish]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/Finish.png

[CustomInstallationAccount]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/Installation_IMConfig_CustomInstallationAccount.png
[CustomInstallationUpdateService]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/Installation_IMConfig_CustomInstallationUpdateService.png
[CustomInstallationOldIMConfig]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/Installation_IMConfig_CustomInstallationOldIMConfig.png

[png_Installation_LoggingService]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/Installation_LoggingService.png  
[png_Installation_MonitoringService]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/Installation_MonitoringService.png    

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/IISWebClientAuthenticationSettings.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/IISWebApiAuthenticationSettings.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/IISLogApiAuthenticationSettings.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/IISWebApiWebConfig.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Install/IISWebApiAuthenticationSettings2.png


[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Update]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Update/Overview.md?at=master
[Download]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Download/Overview.md?at=master

[Version]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Menu%20-%20Version.md?at=master
[Release Notes]:http://support.integrationsoftware.se/support/discussions/forums/1000227207

[InstallIMUpdate]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Install%20IM%20Update.md?at=master
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master

[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master
[Web API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Web%20API/Overview.md?at=master
[How to set Logon As A Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Logon%20As%20A%20Service.md?at=master
[Log API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Log%20API/Overview.md?at=master

[Configure BizTalk LogAgent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Installation/BizTalk%20Log%20Agent.md?at=master
[Log Databases]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Log%20Databases/Log%20Databases.md?at=master
[System Parameters]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/System%20Parameters/Overview.md?at=master
[IMUpdateSettings]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Settings/Overview.md?at=master
[ImLogAccessRolesImLogServiceUsers]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/System%20Parameters/ImLogServiceUsers%20and%20ImLogAccessRoles.md?at=master
[IMConfig]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Configuration%20Database/IMConfig.md?at=master
[Log Databases]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Log%20Databases/Log%20Databases.md?at=master
[BizTalk Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Overview.md?at=master
[SQL Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Overview.md
[Windows AD Groups]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/3.%20Windows%20AD%20Groups/Overview.md?at=master
[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master