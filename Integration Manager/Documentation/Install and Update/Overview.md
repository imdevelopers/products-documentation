<tags data-value=""></tags>

Integration Manager Installation Overview
=====
___

## :fa-info: Information
**Integration Manager** is a software product that must be installed and configured in each customer/partner instance. Each instance is unique and guarantees isolation, performance and so on between individual environments.   

Integration Manager uses many software components with different requirements depending on intended usage (Like security policies, high availability, performance, network isolation):

## :fa-check-square-o: Prerequisites
Detailed instructions on prerequisites are also available in the PDF (Appendix 4, part of your order). Customer demands for performance, high availability, scalability differ and Integration Manager is built to scale.
Below is a list of general prerequisites for running Integration Manager and associated services:

* Windows Server 2008 R2 or later
* Windows domain 
* SQL Server 2008 R2 or later (including Express)
* IIS

* .NET Framework 4.5 or later (some log and monitor agents may require later versions!)
* DTC configuration as described [here][MSDTC]
* [Logon as Service Rights][How to set Logon As A Service]
* The Log and Monitor Agents have specific SW, Acccounts and HW requirements, see [Monitor Agents][] for additional information
* Azure Subscription
  * Some agents require [Azure Applications Access][AzureApplicationsAccess]
  * [Service Bus Relaying][] for connectivity to off-site agents when no VPN/WAN link exists


## :fa-exclamation-triangle: About FIPS

Pre 4.4.x.x releases FIPS must be disabled. 4.4 and later versions does no longer has this requirement.

* FIPS must me Disabled (pre 4.4), read more [here](https://www.howtogeek.com/245859/why-you-shouldnt-enable-fips-compliant-encryption-on-windows/)

```
    NOTE: “FIPS mode” doesn’t make Windows more secure. It just blocks access to newer cryptography schemes that haven’t been FIPS-validated. That means it won’t be able to use new encryption schemes, or faster ways of using the same encryption schemes. In other words, it makes your computer slower, less functional, and arguably less secure.
```    
  

## Core Services
Installation of the the *Core Services* (see [Architecture][CoreServicesArchitecture] for additional details) is managed using a dedicated web based install and update tool (named [IM Update Application][InstallIMUpdate]).  
The following software components are installed and updated using the [IM Update Application][InstallIMUpdate]:

* :fa-globe: [Web Client][]  
* :fa-cloud: [Web API][]  
* :fa-download: [Log API][]  
* :fa-search: [Logging Service][]  
* :fa-desktop: [Monitoring Service][]  
* :fa-database: [IMConfig][]  
* :fa-database: [Log Databases][]  

## :fa-desktop: Monitor Agents
Monitor Agents provide end to end monitoring for your system integration solutions.

## :fa-archive: Log Agents
Log Agents provide logging capabilities end to end for your system integration solutions.

### :fa-user-secret: Service Account Rights
See [How to set Logon As A Service][] for details.  

## IM Update Application

The **IM Update Application** installs and helps you keep Integration Manager up to date. The **IM Update Application** has 2 parts:  
1. **IM Update Web Client** - A Web Front end 
2. **IM Update Windows Services** - A Windows Service responsible for executing install/update commands

### IM Update Web Client
The update client is running on IIS (Internet Information Services) and requires .NET Framework 4.5 (Same as Integration Manager [Web Client][])  

The `IM Update Web Client` is typically accessed with the following URL (replace localhost with appropriate server name if applicable) 

Default URL from any compatible Web Browser: [http://localhost/IMUpdate/UpdateClient]   

### IM Update Windows Service
The **IM Update Service** is a Windows Service that must be installed on each unique server hosting any of the **Core Services**. This Service is responsible for executing local update commands, issued from the `IM Update Web Client`. 

:fa-user-secret: Remember the account running the `IM Update Windows Services` needs logon as service right, see [How to set Logon As A Service][] for additional information. 

The `IM Update Windows Service` is part of the Installer for the [IM Update Application][InstallIMUpdate].

### :fa-lightbulb-o: Common Installation Design decisions
There are typically two types of installation with a basic setup for Test and Production environments.

1. Environments are separated and the integration engine run on diffrent servers and so are the Integration Manager servers. As long as the credentials are the same you could use one UpdateClient else you should use one for prod and one for test.
2. Environments are separated but the Test and Production instances of IM are running on a single Integration Manager server, use one Installation of the UpdateClient.

Consider partitioning the [Web Client][], [Log API][], [Web API][] using different [web sites][Web Sites] with different allowed protocols, firewall rules.

The [Web API][] should be protected from usage from outside the IIS server for local single server installations for example.

Configuring IIS according to company policies are not part of the support for Integration Manager. Please consult your IT-department for more information.

## :fa-lock: Hardening
We strongly recommend the usage of server based certificates to enforce the use of the [https][] protocol and for the protection of the privacy and integrity of data sent between the [Web Client][] and the Browser.

The [Web Client][], [Log API][], [Web API][] all supports https. There is a performance overhead using https. You need to decide if the [Web Client][] and the [Web API][] needs a secure transport in between. One way to overcome this still being secure is to only allow local calls to the [Web Api][], isolating the different IIS Applications in different IIS Sites (with different enabled protocols and bindings).

1. Make sure the IIS server hosting the [Web Client][] has a static IP adress (dynamic IP adresses requires some kind of [dynamic dns][dynamicdns] solution)
2. Create a DNS record pointing to the [Web Client][] server.
3. Create a valid certificate (NOTE: SHA1 based certificates are being deprecated, read more [here][sha1])  
    a. Reuse from existing company policies  
    b. Issue and manage a free certificate, for example using [Let's Encrypt][letsencrypt]   
4. [Install a valid certificate on the IIS][IISCERT]  
4. Make sure to [redirect][] incoming calls (multiple solutions to accomplish this exists)  
     

*web.config example for redirecting incoming http calls -> https*
```xml
<?xml version="1.0" encoding="UTF-8"?>
<configuration>
<system.webServer>
    <rewrite>
      <rules>
        <rule name="Redirect to HTTPS" stopProcessing="true">
          <match url="(.*)" />
          <conditions>
            <add input="{HTTPS}" pattern="^OFF$" />
          </conditions>
          <action type="Redirect" url="https://{HTTP_HOST}/{R:1}" redirectType="SeeOther" />
        </rule>                  
      </rules>
    </rewrite>
    <directoryBrowse enabled="true" />
    <security>
      <requestFiltering>
        <hiddenSegments>

        </hiddenSegments>
      </requestFiltering>
    </security>
  </system.webServer>
</configuration>
```

    NOTE: If your IIS does not support the http protocol additional configuration of web.config files. 
___

### :fa-hand-o-right: Next Step  
:fa-rocket: Install the [IM Update Application][InstallIMUpdate]  

##### :fa-cubes: Related  
:fa-globe: [Web Client][]  
:fa-home: [Latest version][Latest version]  


<!--References -->
[http://localhost/IMUpdate/UpdateClient]:http://localhost/IMUpdate/UpdateClient

[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Web API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Web%20API/Overview.md?at=master
[Log API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Log%20API/Overview.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Latest version]:http://documentation.integrationmanager.se/#/ "Latest version of the documentation Online"
[Version]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Menu%20-%20Version.md?at=master
[Install]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Installation/Overview.md?at=master
[Update]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Update/Overview.md?at=master
[CoreServicesArchitecture]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Architecture.md?at=master
[InstallIMUpdate]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Install%20IM%20Update.md?at=master
[IMConfig]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Configuration%20Database/IMConfig.md?at=master
[Log Databases]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Log%20Databases/Log%20Databases.md?at=master
[How to set Logon As A Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Logon%20As%20A%20Service.md?at=master

[https]:https://en.wikipedia.org/wiki/HTTPS
[IISCERT]:https://support.microsoft.com/en-us/kb/324069
[letsencrypt]:https://letsencrypt.org
[redirect]:https://www.iis.net/downloads/microsoft/url-rewrite
[Web Sites]:https://support.microsoft.com/en-us/kb/323972
[dynamicdns]:https://en.wikipedia.org/wiki/Dynamic_DNS
[sha1]:https://blog.qualys.com/ssllabs/2014/09/09/sha1-deprecation-what-you-need-to-know
[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/Log%20Agents.md?at=master
[MSDTC]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Transactions%20MSDTC.md?at=master

[Service Bus Relaying]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/ServiceBusRelaying.md?at=master

[AzureApplicationsAccess]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Azure%20Application%20Access.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master