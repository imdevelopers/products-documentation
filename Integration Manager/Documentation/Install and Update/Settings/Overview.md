<tags data-value="Installation, Configuration, Security"></tags>

 :fa-cogs: Settings
=====
					
___

## :fa-info: Information
In the **Settings** page you can administer who has the right to update **Integration Manager**.   
![overview][1]  

It is very important that you add yourself (and anyone else who should have this right) directly after the  installation of the [IM Update tool][IMUpdate]

### :fa-edit: Configuration file
Users and groups may be edited in the **app.im.json** file if you lock yourself out of the tool.

![app][2]

``` json
ActiveDirectoryUsers": [
    {
      "Name": "MYDOMAIN\\JohnDoe"
    },
    {
      "Name": "MYDOMAIN\\JaneDoe"
    }
  ],
  "ActiveDirectoryGroups": [
    {
      "Name": "MYDOMAIN\\administrators"
    }
  ]
```

## :fa-exclamation-triangle: Note

    NOTE: Make sure the IIS application for 'UpdateClient' has Windows Authentication enabled

    NOTE: Restart IIS the AppPool after changes

![IIS][0]


### :fa-hand-o-right: Next Step
:fa-cogs: [Administration][]  

#### :fa-cubes: Related
* :fa-rocket: [IM Update tool][IMUpdate]

<!--References -->

[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Settings/app.im.json.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Settings/SettingsOverview.png
[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Settings/IISSettingsWindowsAuthentication.png

[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
[IMUpdate]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Overview.md?at=master
