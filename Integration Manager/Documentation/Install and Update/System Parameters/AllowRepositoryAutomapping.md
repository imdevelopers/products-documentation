<tags data-value=""></tags>

:fa-arrow-circle-o-up: System Parameter - AllowRepositoryAutomapping
=====
					


___

## :fa-info: Information
The system parameter **AllowRepositoryAutomapping** is used to globally set if IM should should create entities in the Repository from logging.



## Example

Valid values are `true` or `false`

This feature is new from version 4.3.0.76

### :fa-hand-o-right: Next Step
:fa-cogs: [Administration][]  

#### :fa-cubes: Related
* :fa-rocket: [Install and Update Client][InstallUpdate]  

<!--References -->

[png_SystemParameters_Environments]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/SystemParameters/SystemParameters_Environments.png
[png_SystemParameters_Keys]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/SystemParameters/SystemParameters_Keys.png
[png_SystemParameters_UpdateValue]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/SystemParameters/SystemParameters_UpdateValue.png

[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master

[InstallUpdate]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Overview.md?at=master
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
