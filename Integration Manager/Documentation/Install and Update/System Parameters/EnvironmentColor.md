<tags data-value=""></tags>

:fa-arrow-circle-o-up: System Parameter: EnvironmentColor
=====
					
___

## :fa-info: Information

## :fa-paint-brush: Environment Color
It is possible to apply a color scheme on a bar to easier separate multiple environments


![EnvironmentColored][2]

* :fa-external-link: Color examples can be explored [here](http://www.w3schools.com/colors/colors_names.asp) 

This setting is governed by the following **SystemParameter** (change process is described [here][System Parameters]):
* **EnvironmentColor** - Enter the [HEX Code](http://www.w3schools.com/colors/colors_names.asp)  for the color to use and click **Save** to apply. 

    NOTE: This is a global setting affecting all users of the [Web Client][].
  
![SPEnvironmentColor][3]
 
___

##### :fa-cubes: Related  
:fa-globe: [Web Client][]    
:fa-key: [System Parameters][] 
  
<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Environment.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/EnvironmentColored.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/SPEnvironmentColor.png

[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[System Parameters]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/System%20Parameters/Overview.md?at=master

