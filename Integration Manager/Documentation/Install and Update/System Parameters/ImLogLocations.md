<tags data-value=""></tags>

:fa-arrow-circle-o-up: System Parameter - ImLogLocations
=====
					


___

## :fa-info: Information
The system parameter **ImLogLocations** is used to override the default location of SQL, to allow IM Log databases and/or files to be placed on different disks with a round robin pattern.

To disable this parameter and the default SQL Server location set this value to **null**

## Example


Example using 4 disks, 

data, index and imagedata is rotaded over 3 disks (K:, L: and M:) and the primary and transaction log is placed on the 4:th disk (N:).

``` json
{
	"Configurations": [{
		"CustomPathData": "K:\\MSSQLSERVER\\DATA\\",
		"CustomPathImagedata": "L:\\MSSQLSERVER\\DATA\\",
		"CustomPathIndex": "M:\\MSSQLSERVER\\DATA\\",
		"DefaultPath": "N:\\MSSQLSERVER\\DATA\\",
		"DefaultPathLog": "N:\\MSSQLSERVER\\LOG\\",
		"Order": 0
	},
	{
		"CustomPathData": "M:\\MSSQLSERVER\\DATA\\",
		"CustomPathImagedata": "K:\\MSSQLSERVER\\DATA\\",
		"CustomPathIndex": "L:\\MSSQLSERVER\\DATA\\",
		"DefaultPath": "N:\\MSSQLSERVER\\DATA\\",
		"DefaultPathLog": "N:\\MSSQLSERVER\\LOG\\",
		"Order": 1
	},
	{
		"CustomPathData": "L:\\MSSQLSERVER\\DATA\\",
		"CustomPathImagedata": "M:\\MSSQLSERVER\\DATA\\",
		"CustomPathIndex": "K:\\MSSQLSERVER\\DATA\\",
		"DefaultPath": "N:\\MSSQLSERVER\\DATA\\",
		"DefaultPathLog": "N:\\MSSQLSERVER\\LOG\\",
		"Order": 2
	}],
	"LastOrderDeployed": 0
}
```
___

Example to overide the default SQL Path, 

data, index, imagedata and log are placed on the same disk H:.

``` json
{
	"Configurations": [{
		"CustomPathData": "H:\\MSSQLSERVER\\DATA\\",
		"CustomPathImagedata": "H:\\MSSQLSERVER\\DATA\\",
		"CustomPathIndex": "H:\\MSSQLSERVER\\DATA\\",
		"DefaultPath": "H:\\MSSQLSERVER\\DATA\\",
		"DefaultPathLog": "H:\\MSSQLSERVER\\LOG\\",
		"Order": 0
	}],
	"LastOrderDeployed": 0
}
```
___


```json``` JSON text string with valid values

This feature is new from version 4.3.0.26

### :fa-hand-o-right: Next Step
:fa-cogs: [Administration][]  

#### :fa-cubes: Related
* :fa-rocket: [Install and Update Client][InstallUpdate]  

<!--References -->

[png_SystemParameters_Environments]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/SystemParameters/SystemParameters_Environments.png
[png_SystemParameters_Keys]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/SystemParameters/SystemParameters_Keys.png
[png_SystemParameters_UpdateValue]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/SystemParameters/SystemParameters_UpdateValue.png

[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master

[InstallUpdate]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Overview.md?at=master
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
