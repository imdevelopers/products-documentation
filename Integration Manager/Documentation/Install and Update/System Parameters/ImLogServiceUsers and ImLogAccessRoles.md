<tags data-value=""></tags>

:fa-arrow-circle-o-up: System Parameter - ImLogServiceUsers and ImLogAccessRoles
=====
					


___

## :fa-info: Information
The SQL Accounts configuration is governed by 2 [System Parameters][] used to assign configurable SQL rights on [IMLog databases][Log Databases] created by the [Logging Service][]
* 1. **ImLogServiceUsers**
* 2. **ImLogAccessRoles**

## **ImLogServiceUsers**
The system parameter **ImLogServiceUsers** controls who gets added as a login on newly created [IMLog databases][Log Databases] created by the [Logging Service][] .


Since multiple accounts can be used on the different [Core Services][CoreServicesArchitecture] and they all need appropriate SQL rights. Each of these accounts must be listed in the system parameter **ImLogServiceUsers**

### Example
Separate the different accounts using a ; (semicolon)

### :fa-check-square-o: Prerequisites
The accounts listed **MUST** be created within the SQL instance(s) hosting the [IMLog databases][Log Databases] with rights assigned from the system parameter **ImLogAccessRoles**


## **ImLogAccessRoles**
The system parameter **ImLogAccessRoles** controls what rights gets added on newly created [IMLog databases][Log Databases] created by the [Logging Service][] .

### Example
Separated by ; (semicolon). Example ```db_datareader;db_datawriter``` to allow read and write.

### :fa-hand-o-right: Next Step
:fa-cogs: [Administration][]  

#### :fa-cubes: Related
* :fa-database: [Log Databases][]
* :fa-rocket: [Install and Update Client][InstallUpdate]  

<!--References -->

[png_SystemParameters_Environments]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/SystemParameters/SystemParameters_Environments.png
[png_SystemParameters_Keys]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/SystemParameters/SystemParameters_Keys.png
[png_SystemParameters_UpdateValue]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/SystemParameters/SystemParameters_UpdateValue.png

[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master

[InstallUpdate]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Overview.md?at=master
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master

[Log Databases]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Log%20Databases/Log%20Databases.md?at=master
[CoreServicesArchitecture]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Architecture.md?at=master
[System Parameters]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/System%20Parameters/Overview.md?at=master