<tags data-value=""></tags>

:fa-arrow-circle-o-up: System Parameters
=====
					


___

## :fa-info: Information
**System parameters** determine various run-time behaviours for Integration Manager.


The default values are the **recommended** values but due to the fact they are system parameters also means that user customization is allowed.

    NOTE: Changing the values and/or settings may disrupt the functionality of Integration Manager. Alter with care and make sure to document your changes.

Affected Core Services:
* [Web Client][]
* [Monitoring Service][]
* [Logging Service][]
___

## Alter settings
To change the settings/parameters of Integration Manager follow the guidelines next.

### :fa-globe: Select environment
The system parameters are stored in the configuration database. There is one configuration database per environment.

![Select environment][png_SystemParameters_Environments]  
1. Click on the **Action** button 
2. Click on the **System Parameters** menu item

#### Select System Parameter  
![Select environment][png_SystemParameters_Keys]  
1. Click on the **name** of the system parameter to change.

#### :fa-exchange: Change value  
![Select environment][png_SystemParameters_UpdateValue]  
1. Enter the new value for the named parameter in the **Value** text box
2. Make sure that the content is valid according to its' type
    * ```double``` must be a **number**
    * ```boolean``` must be set to either **true** or **false**
    * ```string``` Text string with valid values according to function 
    * ```json``` JSON text string with valid values

3. Click on the 'Save' button to persist the value into the database table 
 
  
#### :fa-exclamation-triangle: Note
Some parameters will not take effect before the services like the [Logging Service][] and/or IM [Monitoring Service][] is restarted.

### :fa-hand-o-right: Next Step
:fa-cogs: [Administration][]  

#### :fa-cubes: Related
* :fa-rocket: [Install and Update Client][InstallUpdate]  

<!--References -->

[png_SystemParameters_Environments]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/SystemParameters/SystemParameters_Environments.png
[png_SystemParameters_Keys]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/SystemParameters/SystemParameters_Keys.png
[png_SystemParameters_UpdateValue]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/SystemParameters/SystemParameters_UpdateValue.png

[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master

[InstallUpdate]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Overview.md?at=master
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
