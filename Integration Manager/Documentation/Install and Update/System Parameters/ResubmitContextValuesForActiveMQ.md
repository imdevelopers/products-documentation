<tags data-value=""></tags>

:fa-arrow-circle-o-up: System Parameter - ResubmitContextValuesForActiveMQ
=====
					


___

## :fa-info: Information
The system parameter **ResubmitContextValuesForActiveMQ** is used to control the resubmit of context values to ActiveMQ for the matching context keys, and use Group Match as the Key for this value, should be entered in JSON format like [{"MessageTypeIds":[],"KeyRegEx":"Test\\\\#(.+)","KeyGroup":1}], if MessageTypeIds = [] or null this will be applied on all Message Types.  

This also allow you to only pick the key and ignore for example the namespace


## Example

Example to set Key to Filename and the value will be intact.

Context key: **Test#Filename**
- For all Message Types =  []
- Check for Test# and use the any value as the new Key (.+)
- Select key group 1 to pick the part in (.+) 

Group 0 will take the whole key.

``` json
[{
	"MessageTypeIds": [],
	"KeyRegEx": "Test\\#(.+)",
	"KeyGroup": 1
}]
```

To enter more than one value...

``` json
[{
	"MessageTypeIds": [],
	"KeyRegEx": "Test\\#(.+)",
	"KeyGroup": 1
},
{
	"MessageTypeIds": [1,2,3],
	"KeyRegEx": "IM\\.DefaultProperties\\/1\\.0\\#(Filename)",
	"KeyGroup": 1
}]
```

___


```json``` JSON text string with valid values

This feature is new from version 4.4.0.36

### :fa-hand-o-right: Next Step
:fa-cogs: [Administration][]  

#### :fa-cubes: Related
* :fa-rocket: [Install and Update Client][InstallUpdate]  

<!--References -->

[png_SystemParameters_Environments]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/SystemParameters/SystemParameters_Environments.png
[png_SystemParameters_Keys]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/SystemParameters/SystemParameters_Keys.png
[png_SystemParameters_UpdateValue]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/SystemParameters/SystemParameters_UpdateValue.png

[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master

[InstallUpdate]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Overview.md?at=master
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
