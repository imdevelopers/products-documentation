<tags data-value="Uninstall"></tags>

:fa-trash: Uninstall
=====
This page describes how to completely remove all parts of Integration Manager.


Integration Manager has software components that may be installed on many servers. A report of all installed components is available for a user part of the  Administrator role.  Please print/make a hard copy of the [Overview][Administration] page before performing the removal steps below.

### :fa-sort-numeric-asc: Removal Steps  

* Stop the [Monitoring Service][Monitoring Service]
    * Uninstall from Windows Services using
    ```bat
        SC delete "%Name of Monitoring Service%"
    ```
* Stop the [Logging Service][]
    * Uninstall from Windows Services using
    ```bat
        SC delete "%Name of Logging Service%"
    ```

* Stop all [Monitor Agent(s)][Monitor Agents]
    * Uninstall all Monitor Agents using MSI or "Change or Remove a Program/Uninstall" within Windows

* Stop all [IM Update Service(s)][InstallIMUpdate]
    * Uninstall using the MSI

    ```
        NOTE: There may be multiple instances of the IM Update Service on different servers (IIS Server with Web Componentes, Logging Service, Monitoring Service)
    ```

* Stop the [Pickup Service][] if you were using this service
    * Uninstall using the MSI
    ```
        NOTE: If you used the Pickup Service you will need to alter all the solutions that sent LogEvents to no longer do so
    ```

* If you were using [Log4Net][] remove all configurations using Log4Net appender

* If you were using the [LogApi][LogAPI] make sure clients no longer push data 

* Stop all IM related AppPools in IIS

* Delete all IIS applications one by one
    * Delete the virtual directory
    ```
        NOTE: Do not delete the root folder, traverse the tree bottom up, delete one by one
    ```
* Delete all IM related AppPools

* Remove folders from file system on the IIS server (default c:\program files (x86)\Integration Software) 

* Drop all IM related SQL databases 
    ```
        NOTE1: Remember to Check Close existing connections
    ```

    ```
        NOTE2: You do not have to loose data and configuration, Old databases may be re-used when re-installing Integration Manager
    ```

* Remove Linked Servers used by Integration Manager in SQL MMC if they are not needed any longer

* Remove all associated [IM accounts][How to set Logon As A Service] where applicable:
    * SQL
    * AD / Local rights
    * Policies
    * File shares
    
    Monitor Related access rights
    * BizTalk roles, SQL, MSMQ, ActiveMQ, Azure, Datapower, ftps, ... 
   

* Re-adjust firewall settings, Integration Manager default uses [port 8000][Port8000] for example

* Decomission no longer needed Servers, Licenses, disks, CPUs, ...


### :fa-hand-o-right: Next Step  
:fa-rocket: [Install Integration Manager][]

##### :fa-cubes: Related  
:fa-cogs: [Administration][]  


<!--References -->
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master

[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master
[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/Log%20Agents.md?at=master

[Log4Net]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Log4Net/Log4Net%20Appender.md?at=master
[LogAPI]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Log%20API/Overview.md?at=master

[How to set Logon As A Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Logon%20As%20A%20Service.md?at=master
[InstallIMUpdate]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Install%20IM%20Update.md?at=master
[Port8000]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Shared%20Port%208000.md?at=master
[Install Integration Manager]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Installation/Overview.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Pickup Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents%2FPickup%20Service%2FOverview.md