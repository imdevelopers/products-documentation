<tags data-value=""></tags>

:fa-arrow-circle-o-up: Manually update IMConfig or IMLog databases
=====
					
___

## :fa-info: Information
Updates manually should be done with an account that is sysadmin on the SQL instance or preferred the IM Logging Service service account.
___

## :fa-info: Update

![IMConfig manually update][png_Update_IMConfig_Modal]  


### :fa-hand-o-right: Next Step  
:fa-rocket: [Update][]  

##### :fa-cubes: Related  
 
:fa-file-text-o: [Release Notes][]  


<!--References -->
[Update]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Update/Overview.md?at=master

[png_Update_IMConfig]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Update/Update_IMConfig.png
[png_Update_IMConfig_Modal]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Update/Update_IMConfig_Modal.png

[Release Notes]:http://support.integrationsoftware.se/support/discussions/forums/1000227207 "Access the release notes for Integration Manager"
