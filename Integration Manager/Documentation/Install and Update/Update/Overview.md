<tags data-value="Update"></tags>

:fa-arrow-circle-o-up: Update
=====
					


___

## :fa-info: Information

Update will be done with the highest version of uploaded packages, only upgrade is possible, no downgrade.   

Make sure to [download][Download] the latest package (either manually or using the **IM Update** application) before upgrading.  
:fa-exclamation-triangle: Always use the latest version of the **IM Update** application. 


Version information for Integration Manager `[Major].[Minor].0.[Build]`

* **Major** release - usually requires manual update of IMLog* databases.  
* **Minor** release - contains new features.  
* **Build** release - contains small features and bug fixes, IMConfig change on stored procedures and/or tables.

There are many types of update, the most common is only application and a small **IMConfig** database change in some cases, this can be done fast in couple of minutes usually.

Notice that **:fa-refresh: Update All** button is enabled if there is an uninstalled download package available.
___

#### Select environment
![Select environment][png_Update_Environments]  
1. Click on the environment name  
2. Or Click on Action -> Update
---

### IMLog
Update of the IMLog databases must be done [manually][], we try to only release IMLog updates to minor releases of Integration Manager

### Example of an Application Update
![Application update][png_Update_Applications]  
1. IMConfig requires no update.  
2. LogAPI and the other applications are a build release behind latest version.  
3. Click on **:fa-refresh: Update All** to open the update modal.
---

### Example of an Database and Application Update
![IMConfig and application update][png_Update_IMConfig]  
1. [IMConfig][] requires update, this can be done manually if any problem occures on **:fa-refresh: Update All** click on **:fa-info-circle: Information** to open the information modal.  
    1.1 [Manually update IMConfig][Update Manually update databases]   
2. Click on **:fa-refresh: Update All** to open the update modal.


## Update All

![png_Update_Modal_Password][]  
Enter password for the installation/service account(s) and click **:fa-check: Validate Acccount**.  

![png_Update_Modal_StartUpdate][]  
When an account is validated, can you initialize the update process by click on the **:fa-refresh: Update Integration Manager**.  

![png_Update_Modal_ProgressBar][]  
Wait for the update to run.  

![png_Update_Modal_Success][]  
On completed update, click close.  

### :fa-hand-o-right: Next Step  


##### :fa-cubes: Related  
:fa-globe: [Web Client][]  
:fa-file-text-o: [Release Notes][]  

<!--References -->
[manually]:Manually%20update%20databases.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master

[Version]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Menu%20-%20Version.md?at=master
[Install]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Installation/Overview.md?at=master
[Download]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Download/Overview.md?at=master

[Update Manually update databases]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Update/Manually%20update%20databases.md?at=master

[png_Update_Environments]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Update/Update_Environments.png
[png_Update_Applications]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Update/Update_Applications.png

[png_Update_IMConfig]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Update/Update_IMConfig.png
[png_Update_Modal_Password]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Update/Update_Modal_Password.png
[png_Update_Modal_StartUpdate]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Update/Update_Modal_StartUpdate.png
[png_Update_Modal_ProgressBar]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Update/Update_Modal_ProgressBar.png
[png_Update_Modal_Success]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/IMUpdate/Update/Update_Modal_Success.png
[IMConfig]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Configuration%20Database/IMConfig.md?at=master

[Release Notes]:http://support.integrationsoftware.se/support/discussions/forums/1000227207 "Access the release notes for Integration Manager"
