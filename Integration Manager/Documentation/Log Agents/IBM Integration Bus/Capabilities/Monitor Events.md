<tags data-value="Logging,IBM, IBM Integration Bus, Monitor Events"></tags>

:fa-check-square-o: Monitor Events
=====
	
___

## :fa-info: Information
This page describes the how Monitor Events are mapped to IM Log Event.

## :fa-flash: Monitor Events
You must configure IBM Integration Bus to emit Monitor Events to one or more queues, read more [here][IBM Integration Bus Monitor Events].


```xml
<?xml version="1.0" encoding="UTF-8"?>
<wmb:event xmlns:wmb="http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0/monitoring/event">
	<wmb:eventPointData>
		<wmb:eventData wmb:eventSourceAddress="MQInput1.terminal.in" wmb:eventSchemaVersion="6.1.0.3" wmb:productVersion="7000">
			<wmb:eventIdentity wmb:eventName="MQInput event"/>
			<wmb:eventSequence wmb:creationTime="2017-09-23T12:00:00+02:00" wmb:counter='2'/>
			<wmb:eventCorrelation wmb:localTransactionId="123" wmb:parentTransactionId="456" wmb:globalTransactionId="789"/>
		</wmb:eventData>
		<wmb:messageFlowData>
			<wmb:broker wmb:UUID="d53122ae-1c01-0000-0080-b1b02528c6bf" wmb:name="myBroker"/>
			<wmb:executionGroup wmb:UUID="d43122ae-1c01-0000-0080-b1b02528c6bf" wmb:name="default"/>
			<wmb:messageFlow wmb:UUID="e6d224ae-1c01-0000-0080-9100cd1a61f7" wmb:name="myMessageFlow" wmb:threadId="4201" wmb:uniqueFlowName="myBroker.default.myMessageFlow"/>
			<wmb:node wmb:nodeLabel="MQInput1" wmb:nodeType="ComIbmMqInputNode" wmb:terminal="in" wmb:detail="MYMESSAGEFLOW.IN"/>
		</wmb:messageFlowData>
	</wmb:eventPointData>
	<wmb:applicationData xmlns="">
		<wmb:simpleContent wmb:name="invoiceNo" wmb:targetNamespace="" wmb:dataType="string" wmb:value="567"/>
		<wmb:complexContent wmb:elementName="customerName" wmb:targetNamespace="">
			<customerName>
				<firstName>Steve</firstName>
				<lastName>Bloggs</lastName>
			</customerName>
		</wmb:complexContent>
	</wmb:applicationData>
	<wmb:bitstreamData>
		<wmb:bitstream wmb:encoding="base64Binary">TUQgIAIAAAAAAAAACAAAAP////8AAAAAIgIAALUBAAAgICAgICAgIAAAAAAAAAAAQU1RIFFNMSAgICAgICAgIHo640ggABsHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFFNMSAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg</wmb:bitstream>
	</wmb:bitstreamData>
</wmb:event> 

```


SimpleContent context will be converted to Integration Managers Key Value context.

From:   
`<wmb:simpleContent wmb:name="invoiceNo" wmb:targetNamespace="" wmb:dataType="string" wmb:value="567"/>`  
  
To:  
Key: `invoiceNo` Value: `567`  
  
ComplexContent context will be set as a value on the Key: `Environment.Monitoring.ComplexContent` Value: `<customerName><firstName>Steve</firstName><lastName>Bloggs</lastName></customerName>`


Other values extracted form the Monitor Event and placed in context are the event correlation `<wmb:eventCorrelation wmb:localTransactionId="123" wmb:parentTransactionId="456" wmb:globalTransactionId="789"/>`  

Key: `Environment.Monitoring.EventCorrelation.LocalTransactionId` Value: `123`  
Key: `Environment.Monitoring.EventCorrelation.ParentTransactionId` Value: `456`  
Key: `Environment.Monitoring.EventCorrelation.GlobalTransactionId` Value: `789`  