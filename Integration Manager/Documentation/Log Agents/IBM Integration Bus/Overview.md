<tags data-value="IBM, IBM Integration Bus, Message Broker, Log, Logging Service"></tags>

:fa-hdd-o: Track, Archive and find Log Events and Data from IBM Integration Bus (formerly known as Message Broker)
=============================================
					
## :fa-info: About
IBM® Integration Bus can be configured to emit a monitoring event  (technical, informational and payload details). Events are typically emitted to support transaction monitoring, transaction auditing, and business process monitoring.

Each **IBM Integration Bus** environment (Event Source) can be distinguished by setting a unique [Log Agent][AdminLogAgents] source id. See [Configuration][] for details.

## :fa-sliders: Log Capabilities
* Events
    * Messages (payload)
    * Context Properties

## :fa-lock: Security
:fa-user-secret: [Users][] gain access to logged events and payload using restricted [Log Views][].

## :fa-check-square-o: Prerequisites
* You must have one or more System Integration Solutions running in one or more **IBM Integration Bus** environment(s).
* You must configure [Monitor Events][IIBAudit]
* Also, The **IBM Integration Bus** Log Agent harvests all your events and messages that stem from [IBM Integration Bus Monitor Events][] with a queue destination. Currently the following queue providers are supported:
    * [IBM WebSphere MQ][]
    
    Future support may be added if customers requests so for:
    * [MQTT][]

For additional details see the [PreRequisites][].

## :fa-arrows-alt: Supported Versions

* **IBM Integration Bus 10**
* **IBM Integration Bus 9**

Older versions may very well work, contact our [Support][] if you need our technical assistance configuring Integration Manager against older versions of the IBM Message Broker.
___
  
### :fa-hand-o-right: Next Step
:fa-rocket: [Install Log Agent][]  
:fa-plus-square: :fa-hdd-o: [Add or manage Log Views][]  

#### :fa-cubes: Related
:fa-file-text-o: [Release Notes][]  
:fa-hdd-o: [Log Views][]    
:fa-archive: [Log Agents][]  


[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Overview.md?at=master
[AdminLogAgents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/Log%20Agents.md?at=master

[Add or manage Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/2.%20Add%20or%20manage%20Log%20Agent%20Source/Add%20or%20manage%20Log%20Agent%20Source.md?at=master&fileviewer=file-view-default

[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master
                                                                                                

[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master

[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Support]:http://support.integrationsoftware.se

[Install Log Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/IBM%20Integration%20Bus/2.%20Install.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/IBM%20Integration%20Bus/3.%20Configuration.md?at=master
[PreRequisites]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/IBM%20Integration%20Bus/1.%20PreRequisites.md?at=master
[Release Notes]:https://support.integrationsoftware.se/discussions/forums/1000229325

[IBM Integration Bus]:http://www-01.ibm.com/support/docview.wss?rs=849&uid=swg27006913

[IIBAudit]:https://www.ibm.com/developerworks/community/blogs/546b8634-f33d-4ed5-834e-e7411faffc7a/entry/auditing_and_logging_messages_using_events_in_ibm_integration_bus_message_broker?lang=en

[IBM Integration Bus Monitor Events]:https://www.ibm.com/support/knowledgecenter/en/SSMKHH_9.0.0/com.ibm.etools.mft.doc/ac60386_.htm
[IBM WebSphere MQ]:http://www-03.ibm.com/software/products/en/ibm-mq
[MQTT]:http://mqtt.org/