<tags data-value="Context Options, Log, Log Events"></tags>

:fa-tags: Context options to automate and control logged events during processing in Integration Manager
=====

## :fa-info: Information
Add context values on [Log Events][] to determine behaviour on how the [Logging Service][] processes events and messages.

The following table shows all assignable context options that determine behaviour for the [Logging Service][]:  
<table class="table table-striped table-bordered">
<thead>
<tr>
<th>#</th>
<th>Category</th>
<th>Name</th>
<th>Values/Example</th>
</tr>
</thead>
<tr>
<td colspan="4">1. Control Processing</td>
</tr>
<tr>
<td>1.1</td>
<td><i class="fa fa-file"></i></td>
<td>IM.DefaultProperties/1.0#MessageTypeName</td>
<td>http://party.country.biz/Customers/1.0#Customer</td>
</tr>
<tr>
<td>1.2</td>
<td><i class="fa fa-file"></i></td>
<td>IM.DefaultProperties/1.0#MessageTypeExtractFromBody</td>
<td>`true` or `false`</td>
</tr>
<tr>
<td colspan="4">2. Repository Binding</td>
</tr>
<tr>
<td>2.0</td>
<td><i class="fa fa-font"></i></td>
<td>IM.DefaultProperties/1.0#RepositoryBinding</td>
<td>Json object bind Transport Contract</td>
</tr>
<tr>
<td>2.1.a</td>
<td><i class="fa fa-font"></i> <i class="fa fa-puzzle-piece"></i></td>
<td>IM.DefaultProperties/1.0#IntegrationName</td>
<td>INT001 Invoiceflow Northwind to ACME</td>
</tr>
<tr>
<td>2.2.a</td>
<td><i class="fa fa-font"></i> <i class="fa fa-cog"></i></td>
<td>IM.DefaultProperties/1.0#ServiceName</td>
<td>SVC001 Send XML Invoice from Northwind</td>
</tr>
<tr>
<td>2.2.b</td>
<td><i class="fa fa-font"></i> <i class="fa fa-laptop"></i></td>
<td>IM.DefaultProperties/1.0#ServiceSystemName</td>
<td>`string` value</td>
</tr>
<tr>
<td>2.2.c</td>
<td><i class="fa fa-exchange"></i></td>
<td>IM.DefaultProperties/1.0#ServiceDirection</td>
<td>`int` or `string` value</td>
</tr>
<tr>
<td>2.3.a</td>
<td><i class="fa fa-font"></i> <i class="fa fa-cog"></i></td>
<td>IM.DefaultProperties/1.0#ExternalServiceRelationName</td>
<td>SVC001 Send XML Invoice from Northwind</td>
</tr>
<tr>
<td>2.3.b</td>
<td><i class="fa fa-font"></i> <i class="fa fa-laptop"></i></td>
<td>IM.DefaultProperties/1.0#ExternalServiceRelationSystemName</td>
<td>`string` value</td>
</tr>
<td>2.4.a</td>
<td><i class="fa fa-font"></i> <i class="fa fa-cog"></i></td>
<td>IM.DefaultProperties/1.0#InternalServiceRelationName</td>
<td>SVC001 Send XML Invoice from Northwind</td>
</tr>
<tr>
<td>2.4.b</td>
<td><i class="fa fa-font"></i> <i class="fa fa-exchange"></i></td>
<td>IM.DefaultProperties/1.0#InternalServiceRelationDirection</td>
<td>`int` or `string` value</td>
</tr>
<tr>
<td>2.5.a</td>
<td><i class="fa fa-font"></i> <i class="fa fa-handshake-o"></i></td>
<td>IM.DefaultProperties/1.0#ContractName</td>
<td>`string` value</td>
</tr>
<tr>
<td>2.5.b</td>
<td><i class="fa fa-font"></i> <i class="fa fa-laptop"></i></td>
<td>IM.DefaultProperties/1.0#ContractSystemName</td>
<td>`string` value</td>
</tr>
<tr>
<td colspan="4">3. Web Client / Web API</td>
</tr>
<tr>
<td>3.1</td>
<td><i class="fa fa-file"></i></td>
<td>
IM.DefaultProperties/1.0#Filename<br/>
IM.DefaultProperties/1.0#FullFilePath<br/>
Filename<br/>
FileName<br/>
filename<br/>
originalFilename
</td>
<td>`string` value</td>
</tr>
</table>


## 1. Control Processing
Controll the processing of the message in Logging Service  

### 1.1 :fa-file: Message Type Name
Control/override the [MessageType][Message Types] name. The Message Type name is essential for logging in Integration Manager and must be set in order to have something to do the setup for [Search Fields][]. The actual [Log Agent][Log Agents] should have extracted the name or know what message is being logged and sets the Message Type accordingly in the [Log Event][Log Events] and/or the context key `IM.DefaultProperties/1.0#MessageTypeName`. 

Context Key: `IM.DefaultProperties/1.0#MessageTypeName`  
Value: Any string that also should makes the MessageType name unique/distinguishable

### 1.2 :fa-file: Extract Message Type Name from message/payload
If context `IM.DefaultProperties/1.0#MessageTypeName` is **NOT set** and `IM.DefaultProperties/1.0#MessageTypeExtractFromBody` = `true` (True or true as a string) the [Logging Service][Logging Service] will try to extract the message type name from the body/payload. This works very well for XML messages where the messagetype will be translated as `TargetNameSpace#RootNodeName`. 

Context Key: `IM.DefaultProperties/1.0#MessageTypeExtractFromBody`  
Value: `TargetNameSpace#RootNodeName` for XML based messages, else field for MessageType Name from [Log Event][Log Events] will be used.

    NOTE: Target namespace should be versioned, using /1.0 or /2016/05/17 notation in order to provide uniquness

### 2. :fa-sitemap: Repository Binding
Automatically populates the [Repository][] and binds the Events End Point + Message Type to a Transport Contract, within a Service.

The following properties can be set for an **Integration**:

* 2.1 Integration
    * 2.1.a Integration Name
* 2.2 Service
    * 2.2.a. Name
    * 2.2.b. System
    * 2.2.c. Direction
* 2.3 Service Relation
    * 2.3.a. Name
    * 2.3.b. System
* 2.4 Service Relation (internaly in the system)
    * 2.4.a. Name
    * 2.4.b. Direction
* 2.5 Contract
    * 2.5.a. Name
    * 2.5.b. System

Valid direction values for services
<table class="table table-striped table-bordered">
<thead>
<tr>
<th>string</th>
<th>int</th>
</tr>
</thead>
<tr>
<td>'None'</td>
<td>-2</td>
</tr>
<tr>
<td>'Unknown'</td>
<td>-1</td>
</tr>
<tr>
<td>'Receive'</td>
<td>0</td>
</tr>
<tr>
<td>'Send'</td>
<td>1</td>
</tr>
<tr>
<td>'TwoWayReceive'</td>
<td>10</td>
</tr>
<tr>
<td>'TwoWaySend'</td>
<td>11</td>
</tr>
</table>

:fa-history: This function is available from Integration Manager version **4.3.0.72** and fully functional from version **5.0.0.0**

### 2.0 Repository Binding
Use this instead of the other 2.1 - 2.5 context properties to set all information about the 
Context key `IM.DefaultProperties/1.0#RepositoryBinding`  

![Repository Binding - Service Relations][png_RepositoryBinding_ServiceRelations]
Example of a logged event at Integration `INT004 - Bank (2 Way)` in the System `Mule`, Service is `Receive Quotes`, Transport Contract name will be set to `Receive Quotes via JSON`
The Service origin of the event is `Broker A - Get Quote` in System `Broker A`.
The Service `Receive Quotes` will in the integration platform Mule (System) publish this message an be picked up by the Service `Bank - Loan Request` which is a two way send service
```json
{
	"Name": "Receive Quotes via JSON",
	"Service": {
		"Name": "Receive Quotes",
		"Direction": "TwoWayReceive",
		"System": {
			"Name": "Mule"
		},
		"Integration": {
			"Name": "INT004 - Bank (2 Way)"
		},
		"ExternalServiceRelations": [{
			"Name": "Broker A - Get Quote",
			"System": {
				"Name": "Broker A"
			}
		}],
		"InternalServiceRelations": [{
			"Name": "Bank - Loan Request",
			"Direction": "TwoWaySend"
		}]
	}
}
```
### 2.1 a :fa-puzzle-piece: Integration

#### 2.1 a :fa-puzzle-piece: Integration name
Set the name for the [Integration][Integrations]. 

    NOTE: Integration is only created when either a Service or a Related Service is created. (For Integration Manager with Contract enabled Integration is also set for the Contract) 

Context key `IM.DefaultProperties/1.0#IntegrationName`  
Value : string with the Name of the Integration to create like **INT001 - X to Y**  

### 2.2 :fa-cogs: Service
Set of properties needed to assign a [Service][Services]

#### 2.2.a :fa-font: :fa-cog: Service Name
Set the name for the [Service][Services].  

Context key `IM.DefaultProperties/1.0#ServiceName`  
Value : string with the Name of the Service to create like **SVC001 - Y**  

#### 2.2.b :fa-font: :fa-laptop: System Name
Set the name for the [System][Systems].  

Context key `IM.DefaultProperties/1.0#ServiceSystemName` is optional  
Value: string with the Name of the System this Service is bound to will be created if it it not exists

#### 2.2.c :fa-exchange: Service Direction
Set the direction for the . If not set the direction of the [End Point][End Points] will be used

Context key `IM.DefaultProperties/1.0#ServiceDirection` is optional    
Value : string or int from table below

### 2.2 Service Relation to a external Service
This Service is an externa Service to bind a Service Relation to

#### 2.3.a :fa-font: :fa-cog: Service Name
Set the name for the [Service][Services].  

Context key `IM.DefaultProperties/1.0#ExternalServiceRelationName`  
Value : string with the Name of the Service to create like **SVC001 - Y**  

#### 2.3.b :fa-font: :fa-laptop: System Name
Set the name for the [System][Systems].  

Context key `IM.DefaultProperties/1.0#ExternalServiceRelationSystemName` is optional  
Value: string with the Name of the System this Service is bound to will be created if it it not exists

### 2.4 Service Relation to a internal Service
This Service is an internal Service within the same System to bind a Service Relation to

#### 2.4.a :fa-font: :fa-cog: Service Name
Set the name for the [Service][Services].  

Context key `IM.DefaultProperties/1.0#InternalServiceRelationName`  
Value : string with the Name of the Service to create like **SVC001 - Y**  

#### 2.4.c :fa-exchange: Service Direction
Set the direction for the . If not set the direction of the [End Point][End Points] will be used

Context key `IM.DefaultProperties/1.0#InternalServiceRelationDirection` is optional    
Value : string or int from table below
  
  
  
### 2.5 :fa-handshake-o: Contract
Set of properties needed to assign a [Contract][] (For Integration Manager with Contract enabled Integration is also set for the Contract) 

The following properties can be set for **Contract**:
* Contract Name
* System Name

    NOTE: When sending a Contract you are also supposed to send information about connected Service

#### 2.5.a. :fa-font: :fa-handshake-o: Contract Name 
Set the name for the **Contract**

Context key `IM.DefaultProperties/1.0#ContractName`   
Value : The Name of the **Contract**. For example **SVC001A - X**. Data type is string. If the **Contract** does not exist, it will be created.

#### 2.5.b. :fa-font: :fa-laptop: System Name
Set the name for the [System][Systems].

Context key `IM.DefaultProperties/1.0#ContractSystemName` optional     
Value: The name of the System the **Contract** belongs to. Datatype is string. If the **System** does not exist, it will be created.

## 3 Web Client / Web API
Control function in the Web Client and Web API  

### 3.1 :fa-file: File name 
Pre-sets the name for **file** when performing **resend**, **Repair and resubmit*** and **Save** (Download) operations from within the [Web Client][]. 

Context key `IM.DefaultProperties/1.0#Filename`, `IM.DefaultProperties/1.0#FullFilePath`, `Filename`, `FileName`, `filename` or `originalFilename`
Value : Any valid file name, read more [here](https://msdn.microsoft.com/en-us/library/windows/desktop/aa365247(v=vs.85).aspx)

    NOTE: Requires a Log with body/payload
___

### :fa-hand-o-right: Next Step
* :fa-puzzle-piece: [Integrations][]
* :fa-laptop: [Systems][]
* :fa-cog: [Services][]
* :fa-sign-in: [End Points][]
* :fa-file: [Message Types][]

#### :fa-cubes: Related
:fa-hdd-o: [Logging Service][Logging Service]  
:fa-archive: [Log Agents][Log Agents]    

<!-- References -->
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/Log%20Agents.md?at=master
[Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master
[Log Events]:./Overview.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master
[Integrations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/1.%20Integrations/Integrations.md?at=master

[End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master
[Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/Systems.md?at=master
[Search Fields]:https://bytebucket.org/imdevelopers/products-documentation/src/ef08c79941c19f4f42cb242558a6dba6210f107d/Integration%20Manager/Documentation/5.%20Administration/1.%20Log/5.%20Search%20Fields/Search%20Fields.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master

[png_RepositoryBinding_ServiceRelations]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/LogAgents/LogEvent/RepositoryBinding_ServiceRelations.png