<tags data-value="End Point Direction"></tags>

:fa-exchange: End Point Directions
=====

```
EndPointDirection | Direction            | Note
-------------------------------------------------------
0                 | Receive              |
1                 | Send                 |
10                | Two way Receive      |
11                | Two way Send         | 
-2                | None                 |
-1                | Unknown              | (Please avoid this)

```
To see the full list for your installation, check the LogAPI [http://localhost/IM/LogAPI/api/LogEvent/EndPointDirection](http://localhost/IM/LogAPI/api/LogEvent/EndPointDirection). Replace `http://localhost/IM` as appropriate for your installation.

### Log API Swagger
![EndPointDirectionsFromSwagger][1]


[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/LogAPI/EndPointDirectionsFromSwagger.png

