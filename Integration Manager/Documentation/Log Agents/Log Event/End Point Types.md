<tags data-value="End Point Type"></tags>

:fa-plug: End Point Types
=====
The following **EndPointTypeId:s** are used by [End Points][] within Integration Manager. 
These are normally automatically set by either the [Log Agents][] or the [Log Events][]

```
EndPointTypeId | End Point Type                 | Note
-------------------------------------------------------
0              | Unknown                             | (Please avoid this)
1              | BizTalk - Receive                   |
2              | BizTalk - Send                      |
3              | BizTalk - Dynamic Send              |
4              | BizTalk - Logical Receive           | **
5              | BizTalk - Logical Send              | **

9              | Log4Net                             | *

10             | MSMQ                                |
11             | ActiveMQ                            | *
12             | RabbitMQ                            | *
13             | AMQP                                | *
14             | AnypointMQ                          | **

20             | AppFabric                           |

25             | Mule                                | **
26             | CloudHub                            | **

30             | AppFabric(Azure)                    | 

40             | IBM MQ                              | Also known as WMQ / WebSphere MQ
41             | IBM Integration Bus                 | *
42             | IBM Data Queue                      | ***

50             | WCF                                 |

60             | File                                |
61             | Dropbox                             | *

65             | SMTP                                | ***

70             | Microsoft Azure Service Bus         |
71             | Microsoft Azure API Management      |
72             | Microsoft Azure API Apps            |
73             | Microsoft Azure Logic Apps          |
74             | Microsoft Azure Log Audits          |

80             | FTP                                 | *
81             | FTPS                                | *
82             | SFTP                                | *
83             | Connect:Direct                      | ***

85             | HTTP                                | *
86             | HTTPS                               | *
87             | REST API                            | *
88             | Web API                             | *

90             | ODBC                                | *
91             | JDBC                                | *
92             | SQL                                 | *
93             | Database                            | *
94             | SSIS                                | *
95             | Amazon DynamoDB                     | **
96             | Amazon S3                           | **

130            | MLLP (Minimum Lower Layer Protocol) | ***

* = New in 4.2
** = New in 4.3
*** = New in 4.4
```


To see the full list for your installation, check the LogAPI [http://localhost/IM/LogAPI/api/LogEvent/EndPointTypes](http://localhost/IM/LogAPI/api/LogEvent/EndPointTypes). Replace `http://localhost/IM` as appropriate for your installation.

### Log API Swagger
![EndPointTypesFromSwagger][1]


##### :fa-cubes: Related  
:fa-desktop: [Log Agents][]    
:fa-plus-square: :fa-sign-in: [Add or manage End Points][]  
:fa-sign-in: [End Points][]  

[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Overview.md?at=master
[Add or manage End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/2.%20Add%20or%20manage%20End%20Point/Add%20or%20manage%20End%20Point.md?at=master
[Log Events]:,/Overview.md?at=master
[End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/LogAPI/EndPointTypesFromSwagger.png
