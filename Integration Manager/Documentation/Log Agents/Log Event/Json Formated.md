<tags data-value="Log Event, Rest API, LogAPI"></tags>

Json formatted Log Event
=====

## :fa-info: Information
Event logged to integration manger Log API (Rest API) must be a correctly formatted JSON. Examples available further down in this article.

This **JSON formatted Log event** may be sent directly to the [LogAPI][] or sent to a queue/folder for asynchronous fetch by the [Pickup Service][PickupService] (recommended approach).

### :fa-minus-square: Required fields
The first 7 fields are mandatory.

* LogAgentValueId - read more [here][Log Agents]
* EndPoint - read more [here][End Point]
  * EndPointName
  * EndPointUri
  * EndPointDirection
  * EndPointTypeId
* OriginalMessageTypeName - read more [here][Message Type]
* LogDateTime (and must be between 1990-01-01 00:00:00Z and 9999-12-12 23:59:59Z)

### :fa-plus-square: Optional fields
The rest of the fields are optional. By providing additional details about the event, end users may have a better user experience.

### :fa-superscript: Data Type Format for the fields
* LocalInterchangeId - must be of type GUID or null
* ServiceInstanceActivityId - must be of type GUID or null
* Body - is a [base64 encoded](https://www.base64encode.org/) string / file or null/not provided
* Context - is a Key Value collection or null
* LogDateTime - Set timezone to get correct date time (-01:00, +02:00 or Z) if not set UTC will be used, The LogDateTime will be converted to UTC and stored as that in Integration Manager

Example: 2017-11-22T08:44:22.309+02:00 (+02:00 against UTC)


### :fa-sitemap: Repository
By providing additional details in the **Context**, the [Repository][] may be populated from information from the Event.
Se more in the documentation for [Context Options][ContextOptions].

### :fa-check-square-o: Example #1 
Complete LogEvent with all available properties set, optional and mandatory
```
{
  "LogAgentValueId": 42,
  "EndPointName": "receive hello world",
  "EndPointUri": "C:\\temp\\in",
  "EndPointDirection": 0,
  "EndPointTypeId": 60,
  "OriginalMessageTypeName": "My hello world file",
  "LogDateTime": "2017-11-22T08:44:22.309Z",
  "ProcessingUser": "DOMAIN\\user",
  "SequenceNo": 0,
  "EventNumber": 0,
  "LogText": "File OK",
  "ApplicationInterchangeId": "",
  "LocalInterchangeId": null,
  "LogStatus": 0,
  "ProcessName": "My Process",
  "ProcessingMachineName": "localhost",
  "ProcessingModuleName": "INT101-HelloWorld-Application",
  "ProcessingModuleType": "FilePickup",
  "ServiceInstanceActivityId": null,
  "ProcessingTime": 80,
  "Body": "SGVsbG8gV29ybGQ=",
  "Context": {
      "CorrelationId": "064205E2-F7CF-43A6-B514-4B55536C2B67",
      "FileName": "Hello.txt"
  }
}
```

___

### :fa-hand-o-right: Next Step
* **[Log Api][LogAPI]** - Log from code (C# and Java)  

#### :fa-cubes: Related
:fa-archive: [Log Agents][]  - Events and messages   
:fa-hdd-o: [Logging Service][] - Processes (indexing) the messages  
:fa-hdd-o: [Log Views][] - Manage [User][Users] access to events and messages cross platform  
:fa-database: [Log Databases][] - Keep as much data as you like  
:fa-sitemap: [Repository][]

[PickupService]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Pickup%20Service/Overview.md?at=master
[LogAPI]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Log%20API/Overview.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Log Databases]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/Log%20Agents.md?at=master
[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[ContextOptions]:./Context%20Options.md?at=master
[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/Log%20Agents.md?at=master
[Message Type]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master
[End Point]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master