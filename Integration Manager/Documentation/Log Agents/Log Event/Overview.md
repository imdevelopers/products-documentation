<tags data-value="Log Events, LogAPI"></tags>

:fa-flash: Log Events
=====

Events, messages and context are logged as **Log Events** providing support for end to end logging across many platforms.
					
## :fa-info: About
Integration Manager provides **end to end logging** capabilities either from the [Log Agents][] or from your own custom built solutions that may emanate Log Events. There is an appropriate [LogAPI][] that can be used for all custom needs.

* **REST**
* **WCF**

![LogAPI][1]

This part of the documentation covers the **REST** based API. There is a WCF based API for legacy applications.

![LogApiREST][2]

* :fa-code: [JSON Log Event][]  
* :fa-plug: [End Point Types][EndPointTypes]  
* :fa-exchange: [End Point Directions][]  

## :fa-sitemap: Repository
**Log Events** can also, depending on settings "auto populate" the [Repository][]. This will drastically reduce the amount of adminstration and the know how is transfered from developers to the business/IT Operations/AM-teams.
___
  
### :fa-hand-o-right: Next Step
:fa-cloud-download: [LogAPI][]   
:fa-code: [JSON Log Event][]   
:fa-tags: [Context Options][ContextOptions]  

#### :fa-cubes: Related
:fa-sitemap: [Repository][]  
:fa-hdd-o: [Log Views][]       
:fa-archive: [Log Agents][]  

[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Overview.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master

[JSON Log Event]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Log%20Event/Json%20Formated.md?at=master
[End Point Directions]:./End%20Point%20Directions.md?at=master

[ContextOptions]:./Context%20Options.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[LogAPI]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Log%20API/Overview.md?at=master

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/LogAPI/LogApi.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/CoreServices/LogAPI/RestBasedLogAPI.png

[EndPointTypes]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Log%20Event/End%20Point%20Types.md?at=master