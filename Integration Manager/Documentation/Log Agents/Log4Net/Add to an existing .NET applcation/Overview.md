<tags data-value="Log,Log4Net,Log4Net Appender,Appender"></tags>

How to add Log4Net for Integration Manager to your .NET application
=============================================

1. Copy IM.Libraries.Contracts.LogApi.dll, IMLogAppender.dll and WCFExtras.dll to the bin folder for the application

2. Open your web/app.config and add binding and end point to the LogAPI, update the address to the LogAPI 
 
   ```xml
   ...
   <system.serviceModel>
       <bindings>
           <basicHttpBinding>
           <binding name="BasicHttpBinding_ILogApiService" />
           </basicHttpBinding>
       </bindings>
       <client>
           <endpoint address="http://localhost/im/logapi/LogApiService.svc"
           binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_ILogApiService"
           contract="IM.Libraries.Contracts.LogApi.ILogApiService" name="BasicHttpBinding_ILogApiService" />
       </client>
   </system.serviceModel>
   ...
   ```
   
3. Add appender section to log4net and add IMAppender as a appender-ref

   ```xml
   ...
   <log4net>
      <appender name="IMAppender" type="IM.Log4NetAppender,IM.LogAgent.Log4NetAppender">
         <!-- Custom Parameters -->
         <OriginalMessageType value="IM.LogAgent.Log4NetAppender/2.0#DefaultMessageType" />
         <MessageTypeExtractFromBody value="false" />
         <LogAgentID value="101"/>
         <EventNumber value="0"/>
         <EndPointName value="Log4Net Unit Test"/>
         <EndPointUri value="VS.local.log4net.test"/>
         <LogText value =""/>
         <LogStatusId value ="255"/>
         <ProcessingUser value="Administrator"/>
         <ModuleType value="unit test"/>
         <ProcessName value="unit test"/>
         <ProcessingMachineName value="DEV"/>
         <ProcessingModuleType value="unit test"/>
         <ProcessingModuleName value="VS"/>
         <LogApiServiceURI value="http://localhost/IM/LogAPI/LogApiService.svc"/>
         <AppInterchangeIDStr value="{99106FF5-C7BB-4244-9EB7-F99040190F32}"/>
         <LocInterChangeIDStr value="{E4CDDF18-9925-4A79-8E9F-71BC0D4C5172}"/>
         <ServiceInstanceActivityIDStr value="{E4CDDF18-9925-4A79-8E9F-71BC0D4C5173}"/>
      </appender>
      <root>
         <level value="DEBUG"/>
         <appender-ref ref="IMAppender"/>
      </root>
   </log4net>
   ...
   ```



