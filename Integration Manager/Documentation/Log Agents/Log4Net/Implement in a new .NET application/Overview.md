<tags data-value="Log,Log4Net,Log4Net Appender,Appender"></tags>

How to implement Log4Net for Integration Manager in your .NET application
=============================================

1. Start by adding the Log4Net Nuget Package to your project (or simply reference in the log4net.dll file) ![Install Log4Net using NuGet][NuGetLog4Net]

2. Add IM.Libraries.Contracts.LogApi.dll, IMLogAppender.dll and WCFExtras.dll as a references to your project, WCFExtras can also be found as a NuGet package and is installed in the same way as Log4Net ![Add Log4Net Appender, IM.Libraries.Contracts.LogApi and WCFExtras][Log4NetAppenderInVs]

3. Add a service reference that points to the Log API of Integration Manager and name the service reference "LogApiService". ![Add Service Reference to Integration Manager LogAPI][AddServiceReferenceInVs]

4. In the file/files where the user wants to enable logging the user has to add using directives for Log4Net. Create a new object of log4net.Logmanager. Use the object created where the user wants to have logging event. 
  
   In the example below it is basically a unit test that logs an info message.  
   As mentioned earlier there are five different logging levels, Debug, Info, Warn, Error and Fatal that the user are able to use. The message between the brackets will be displayed as log text as standard in the Integration Manager message view.
  
   ```cs
   var log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType); 
   log.Info("First logged event with Log4Net for Integration Manager");
   ```

5. Add the line: \[assembly: log4net.Config.XmlConfigurator(Watch = true)\] in the main class to tell make sure that Log4Net looks at the configuration file.

6. Open your web/app.config and make sure that the service address uses the correct contract. 
   When adding the service reference it will automatically add the contract used by the service reference, but in this case it adds the wrong namespace.  
   Change it from LogApiService.ILogApiService to IM.Libraries.Contracts.LogApi.ILogApiService
   
   ```xml
   ...
   <system.serviceModel>
       <bindings>
           <basicHttpBinding>
           <binding name="BasicHttpBinding_ILogApiService" />
           </basicHttpBinding>
       </bindings>
       <client>
           <endpoint address="http://localhost/im/logapi/LogApiService.svc"
           binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_ILogApiService"
           contract="IM.Libraries.Contracts.LogApi.ILogApiService" name="BasicHttpBinding_ILogApiService" />
       </client>
   </system.serviceModel>
   ...
   ```
   
7. The user uses a configuration file to customize the message that is being sent. This file is automatically created as "app.config" or "web.config" for web applications when the user creates the service reference.
  
   All the different parameters are assigned automatically with fitting information but if the user wants to send custom information with the different parameters it is possible to input that information using the configuration file.
   
   The user also have the possibility to choose at what level logging should be enabled by changing the value of the level element in the root category. This will also change which messages will be sent to Integration Manager.  
   To make it easier for the user a standard configuration sections can be found in section 1.2 of this document.  
   
   We recommend that the user sets an Endpoint name, this will create a new custom endpoint when sending the first message with a new endpoint name. It is also good if the user sets an Original Message Type to collect all the messages from IMLogAppender as one message type.  



[NuGetLog4Net]: https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/Log4NetAppender/nuget_install_log4net.png
[Log4NetAppenderInVs]: https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/Log4NetAppender/add_log4net_appender_in_vs.png
[AddServiceReferenceInVs]: https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/Log4NetAppender/add_service_reference_logapi.png

