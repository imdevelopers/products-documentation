<tags data-value="Log,Log4Net,Log4Net Appender,Appender"></tags>

Log4Net Appender
=============================================

## :fa-info: About
This log agent allows you to log from an exsisting application that uses Log4Net directly into Integration Manager.  

Log4Net is a tool to help to library is a tool to help the user output log statements from .NET applications. We have implemented support to use Integration Manager Message view as an output option for Log4Net.
  
There five different levels of logging: DEBUG, INFO, WARN, ERROR and FATAL.
  
To learn more about Log4Net please visit [http://logging.apache.org/log4net/index.html][].




[http://logging.apache.org/log4net/index.html]:http://logging.apache.org/log4net/index.html
