<tags data-value="BizTalk Server, Log, Logging Service"></tags>

BizTalk Log Agent  
![logo][1] 
=============================================
The BizTalk Log Agent for Integration Manager Logs events and messages from the [Microsoft BizTalk Server][] Integration Platform.
					
## :fa-info: About
Get tracked data from any number of BizTalk Server groups within a few seconds with nearly 0 effort!

The **Biztalk Log Agent** logs event and messages including the promoted properties from one or more **Microsoft BizTalk Servers** for ports and Orchestrations with **Tracking enabled**. Since the agent copies data from the BizTalk Tracking database there is no need to keep large amounts of data within BizTalk. Using Integration Manager will therefore make your BizTalk Server environment run faster and be less error prone due to tracking related problems.

 You can also track events and messages using the [Log Pipeline Components][] for Integration Manager. 

## :fa-sliders: Log Capabilities

* Events
    * Messages (payload)
    * Context Properties

## :fa-arrows-alt: Supported Versions
Integration Manager runs smoothly with the following versions and editions:

All versions from BizTalk 2006 and onwards are supported. Integration Manager is built and tested for the latest available Cumulative Update (CU) at any given time.

:fa-external-link: For the latest CU, please visit: [KB2555976](https://support.microsoft.com/en-us/kb/2555976)

* BizTalk 2016
* BizTalk 2013 R2
* BizTalk 2013 
* BizTalk 2010
* BizTalk 2009 
* BizTalk 2006 R2
* BizTalk 2006 
  

All editions are supported:
* Enterprise
* Standard
* Developer (which is Enterprise)
* Trial (which is Enterprise) 

## :fa-hdd-o: Logged data
[Users][] access logged data using roles based information restricted [Log Views][].

## :fa-rocket: Installation
The BizTalk Logging is a built in feature of the [Logging Service][]. Part of the installation process of Integration Manager configuration for logging from BizTalk is configured. Changes can be done using the System Parameters table. 

## :fa-refresh: Automatic synchronization
The **BizTalk Log Agent** always keeps track of last tracked event and body in the BizTalk Tracking database and synchronized whenever both Integration Manager and BizTalk is available. 

* Reboot anything at any time - no worries, Integration Manager will synchronize automatically
    * Any of the [Core Services][CoreServicesArchitecture]
    * Any of the BizTalk nodes (Processing nodes and SQL nodes) 
* Original log date time is used and there will be no "holes" in time for logged events
* Updates can be performed at any time
* BizTalk Tracking database can be cleared using BizTalk Terminator tool/BHM tool. 

    NOTE: Make sure that last transaction in BizTalk tracking database is available in the online [Log Databases][] before running the purge/reset command
     
* Do note that the BizTalk SQL Job **TrackedMessages_Copy_BizTalkMsgBoxDb** by default run once every minute. This in effect means that tracked data is typically visible in Integration Manager 90 seconds from when the event originally occured. This latency may be tuned using the System Parameters set during installation/configuration, read more [here][Configure BizTalk LogAgent]
* If the online [Log Database][Log Databases] is lost for whatever reason, restore from last backup and the **BizTalk Log Agent** will automatically fetch the missing tracked events and data
* The **BizTalk Log Agent** MUST not be stopped for a longer period than the purge period set in the BizTalk SQL Agent job **DTA Purge and Archive (BizTalkDTADb)**

    NOTE: You will eventually lose data if the **BizTalk Log Agent** cannot reach the tracking database (changes in network, firewalls, security, policies and more) or the service is/has stopped for too long time

    TIP: Use the [Non-Events agent][Non Events Agent] to make sure you are alerted whenevere there is data outage
* If the **BizTalk Log Agent** has been stopped for a long time, it may take a while to catch up. This is mainly dependent on disk performance on BizTalk Tracking database and the [Log Databases][] 

___
  
### :fa-hand-o-right: Next Step
:fa-check-square-o:  [BizTalk Logging Agent PreReqs][BizTalkPreReqs]  
:fa-plus-square: :fa-hdd-o: [Add or manage Log Views][]  

#### :fa-cubes: Related
:fa-hdd-o: [Log Views][]    
:fa-archive: [Log Agents][]

[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Overview.md?at=master
[Add or manage Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/2.%20Add%20or%20manage%20Log%20Agent%20Source/Add%20or%20manage%20Log%20Agent%20Source.md?at=master&fileviewer=file-view-default

[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master

[Log Pipeline Components]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Microsoft%20BizTalk%20Server/Capabilities/Pipeline%20Components.md?at=master

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/BizTalk/BizTalkLogo.png
[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master

[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[BizTalkPreReqs]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Microsoft%20BizTalk%20Server/1.%20PreRequisites.md?at=master
[Microsoft BizTalk Server]:https://www.microsoft.com/en-us/cloud-platform/biztalk
[Log Databases]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Log%20Databases/Log%20Databases.md?at=master
[Non Events Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Non-Events/Overview.md?at=master

[CoreServicesArchitecture]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Architecture.md?at=master
[Configure BizTalk LogAgent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Installation/BizTalk%20Log%20Agent.md?at=master
