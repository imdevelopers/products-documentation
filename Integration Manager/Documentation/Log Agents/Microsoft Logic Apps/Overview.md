<tags data-value="Logic Apps, Cloud, Log, Logging Service"></tags>

Track, Archive and find Log Events and Data from Microsoft Logic Apps 
=============================================
					
## :fa-info: About
The **Logic Apps** Log Agent easily and automatically harvests all your events with input and output data. Any number of Azure Subscriptions can be used simultaneously. 
Each subscription is distinguished by setting a unique [Log Agent][AdminLogAgents] source id. [Users][] gain access to logged data using restricted [Log Views][].

:fa-hdd-o: The Log Agent is embedded and hosted within the [Logic Apps Monitor Agent][LogicAppsAgent].

## :fa-sliders: Log Capabilities
List of resources that can be logged by using this agent

* Logic App
    * Run history with and the specific actions
    * Messages (payload)
    * Context Properties (Tracked properties)

## :fa-arrows-alt: Supported Versions
Microsoft Logic Apps in Azure, always latest version and mostly with pre-view features, contact our techical support if you need additional help.

## :fa-hdd-o: Logged data
[Users][] access logged data using roles based information restricted [Log Views][].


## :fa-check-square-o: Prerequisites
You must have one or more active Azure Subscriptions with Logic Apps deployed.  

:fa-lock: Azure access rights
The Log Agent needs Azure Application permissions to interact with your Logic Apps, see details [here][AzureApplicationsAccess]

___
  
### :fa-hand-o-right: Next Step
:fa-rocket: [Install Agent][]  
:fa-plus-square: :fa-hdd-o: [Add or manage Log Views][]  

#### :fa-cubes: Related
:fa-hdd-o: [Log Views][]    
:fa-archive: [Log Agents][]  
:fa-file-text-o: [Release Notes][]  

[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Overview.md?at=master
[AdminLogAgents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/Log%20Agents.md?at=master

[Add or manage Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/2.%20Add%20or%20manage%20Log%20Agent%20Source/Add%20or%20manage%20Log%20Agent%20Source.md?at=master&fileviewer=file-view-default

[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master

[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master

[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[AzureApplicationsAccess]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Azure%20Application%20Access.md?at=master

[LogicAppsAgent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20App%20Services/Azure%20Logic%20Apps/Overview.md
[Install Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20App%20Services/2.%20Install.md?at=master
[Release Notes]:http://support.integrationsoftware.se/support/discussions/forums/1000229074

