<tags data-value="Mule,Logging,Log,Log Agent"></tags>

![logo][png_MuleLogo] Log from Business Events
=============================================
  				
## :fa-info: About
Log your messages from Mule with the Custom Business Event element.

    Note: Custom Business Events are only available in Mule Enterprise Edition.
    
## :fa-sliders:  Log Capabilities
* Events
    * Messages
    * Context Properties
    
## :fa-edit: Configuration

### BusinessEventHandlerListener

To be able get the business events into the log file, the following XML snippet must be added in the Mule Flow before the &lt;flow&gt; element.  

```xml
<!-- Custom Business Events to log -->
<spring:beans>   
	<spring:bean class="se.integrationsoftware.integrationmanager.BusinessEventHandlerListener" id="notificationListener" />
</spring:beans>
<notifications>   
	<notification-listener ref="notificationListener" />   
</notifications>
```

![CustomBusinessEvent_HandlerListener][CustomBusinessEvent_HandlerListener]  

The class **se.integrationsoftware.integrationmanager.BusinesEventHandlerListener** must also be part of the project.  

![JavaPackage_Studio][JavaPackage_Studio]  

```java
package se.integrationsoftware.integrationmanager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mule.api.MuleContext;
import org.mule.api.context.MuleContextAware;
import com.mulesoft.mule.tracking.event.EventNotification;
import com.mulesoft.mule.tracking.event.EventNotificationListener;

public class BusinessEventHandlerListener implements EventNotificationListener<EventNotification>, MuleContextAware
{
	MuleContext context;

	protected static final Log logger = LogFactory.getLog("IMLog");
	
	@Override
	public void onNotification(EventNotification notification)
	{	
		String metaDatas = notification.getMetaDatas().toString();
		if (metaDatas != null)
		{
			if (metaDatas.startsWith("{"))
			{
				metaDatas = metaDatas.substring(1);
			}
			if (metaDatas.endsWith("}"))
			{
				metaDatas = metaDatas.substring(0,  metaDatas.length() - 1);
			}
		}
		
		logger.info(metaDatas);
	}
	
	@Override
	public void setMuleContext(MuleContext context)
	{
		this.context = context;
	}
}
```

### Mule Flow - Custom Business Event element
Use the Custom Business Event element in the Mule Flow to log messages to Integration Manager. 

![CustomBusinessEvent_Element][CustomBusinessEvent_Element]

Configure the Business Event element: 

![CustomBusinessEvent_Configuration][CustomBusinessEvent_Configuration]

```xml
        <tracking:custom-event event-name="IMLog" doc:name="Custom Business Event">
            <tracking:meta-data key="im_logText" value="File received"/>
            <tracking:meta-data key="im_endPointName" value="File transfer business event receive"/>
            <tracking:meta-data key="im_endPointDirection" value="Receive"/>
            <tracking:meta-data key="im_messageContext" value="#[message]"/>
            <tracking:meta-data key="im_messageBody" value="#[message.payloadAs(java.lang.String)]"/>
        </tracking:custom-event>
``` 

Supported properties:  

**im_logText** - informal text, Optional  
**im_endPointName** -  the End Point name, Mandatory  
**im_endPointDirection** - the End Point direction, Valid values are: **None**, **Unknown**, **Receive**, **Send**, **TwoWayReceive**, **TwoWaySend**
**im_endPointTypeId** - the End Point type id, **Integer** see below for valid codes, Optional  
**im_endPointUri** - the End Point URI, Optional (overrides the End Point URI set by the log agent)  
**im_messageType** - the Message Type, Optional  
**im_messageContext** - the message context properties, **#[message]**, Optional  
**im_messageContextCustom** - a custom message context properties, key value pair of type <string,string> **{"key1":"value1", "key2":"value2"}** example **{"transactionId":"FA0CAE5C-8968-44DF-836D-1DAA57D7831B","orderId":"1234"}** , Optional  
**im_messageBody** - the message payload, **#[message.payloadAs(java.lang.String)]**, Optional  
**im_logStatusCode** - the message status, **Signed integer**, Optional  
**im_integrationName** -the name of the [Integration][Integrations]. If this value is provided together with **im_serviceName** and/or **im_contractName** this Integration will be set for the Service and/or Contract, Optional  
**im_serviceName** - the name of the [Service][Services]. A Transport Contract will be created with Message Type and End Point, if **im_contractName** is set only the Message Type will be set on the Transport Contract, Optional  
**im_serviceDirection** - the direction of the Service, if not set the direction of the End Point will be used, Optional  
**im_serviceSystemName** - the name of the System used for the Service, Optional  
**im_contractName** - the name of the Contract. If this value is provided together with **im_serviceName**, the service will be set on the Contract, A Transport Contract will be created with Message Type and End Point, Optional  
**im_contractSystemName** - the name of the System used for the Contract, Optional  
  
An event is logged to Integration Manager if the mandatory properties exists.  

Integration Manager can extract the message type automatically from XML messages.  

Log status code can be used to highlight errors, i.e. in a Mule exception handling element.  

Exception information can be logged with  
 **#[groovy:message.getExceptionPayload().getException().getCause()]** and  
 **#[groovy:message.getExceptionPayload().getException()]**

 #### End Point Type Id
 These codes are valid codes to be used with the **im_endPointTypeId** property:

 See the list of [End Point Types][] for available id:s to use
___

### :fa-hand-o-right: Next Step
:fa-plus-square: :fa-hdd-o: [Add or manage Log Views][]  

#### :fa-cubes: Related
:fa-hdd-o: [Log Views][]    
:fa-hdd-o: [Add or manage Log Agents][]  
:fa-archive: [Log Agents][]    
:fa-rocket: [Install Mule Log Agent][Install]    
:fa-edit: [Configuration][] of the agent  

<!-- References -->
[png_MuleLogo]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/Mule/MuleLogo.png

<!-- Repository -->
[Integrations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/1.%20Integrations/Integrations.md?at=master
[Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/Systems.md?at=master
[Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master
[End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master
[Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master

[End Point Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Log%20Event/End%20Point%20Types.md?at=master

[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/Log%20Agents.md?at=master
[Add or manage Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/2.%20Add%20or%20manage%20Log%20Agent%20Source/Add%20or%20manage%20Log%20Agent%20Source.md?at=master&fileviewer=file-view-default
[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master

[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master

[PreRequisites]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/1.%20PreRequisites.md?at=master
[Install]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/2.%20Install.md?at=master
[Update]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/4.%20Update.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/3.%20Configuration.md?at=master
[Uninstall]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/3.%20Uninstall.md?at=master

[CustomBusinessEvent_HandlerListener]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/Mule/BusinessEvent_HandlerListener.png
[CustomBusinessEvent_Element]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/Mule/BusinessEvent_Element.png
[CustomBusinessEvent_Configuration]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/Mule/BusinessEvent_Properties.png
[JavaPackage_Studio]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/Mule/JavaPackage_Studio.png