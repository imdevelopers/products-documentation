<tags data-value="Mule,Logging,Log,Log Agent,Log4J"></tags>

![logo][png_MuleLogo] Log4J Configuration
=============================================
  
## :fa-info: About
Mule messages and context are retrieved from the log framework Log4J. This is page describes the configuration needed to be done in Log4J.  

## :fa-edit: Configuration

The Log4J configuration file, log4j2.xml, is normally found at this location in a Mule project:  

![Log4J_Studio][Log4J_Studio]

It is also possible to edit the Log4J configuration file after it has been deployed. Here is an example of where it can be located:   

![Log4J_Apps][Log4J_Apps]

A custom appender is needed to filter the needed log files to Integration Manager. Add the `<IMLogFileAppender>` snippet under the `<Appenders>` section. 
The `<Configuration>` element must have the attribute **packages** set to **se.integationsoftware.integrationmanager**. 
The directory in `filePath` attribute must exist and the Log-Agent service account must have read/write permissions.   

```xml
<?xml version="1.0" encoding="utf-8"?>
<Configuration packages="se.integrationsoftware.integrationmanager">
    <Appenders>
        <!--  IMLog File Appender -->
        <IMLogFileAppender name="file_IMLog" 
            filePath="${sys:mule.home}${sys:file.separator}logs${sys:file.separator}IMLog${sys:file.separator}filetransfer-">
    		    		
            <ScriptFilter onMatch="ACCEPT" onMisMatch="DENY">
                <Script name="GroovyFilter" language="groovy"><![CDATA[
		            if (logEvent.getMessage().getFormattedMessage().contains("im_") ) {
		                return true;
		            }
		            return false;
                ]]></Script>
            </ScriptFilter>
    		
            <PatternLayout pattern="%d [%t] %-5p %c - %m%n" />
        </IMLogFileAppender>
    </Appenders>    
    <Loggers>
        <!-- Mule classes -->
        <AsyncLogger name="org.mule" level="INFO"/>
        <AsyncLogger name="com.mulesoft" level="INFO"/>
        
        <AsyncRoot level="INFO">
            <AppenderRef ref="file_IMLog" />
        </AsyncRoot>
    </Loggers>
</Configuration>
```

The class **se.integrationsoftware.integrationmanager.IMLogFileAppender** must also be part of the project.  

![JavaPackage_Studio][JavaPackage_Studio]  

```java
package se.integrationsoftware.integrationmanager;

import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.UUID;
import java.util.concurrent.locks.*;
import org.apache.logging.log4j.core.*;
import org.apache.logging.log4j.core.appender.*;
import org.apache.logging.log4j.core.config.plugins.*;
import org.apache.logging.log4j.core.layout.PatternLayout;

@Plugin(name="IMLogFileAppender", category="Core", elementType="appender", printObject=true)
public final class IMLogFileAppender extends AbstractAppender {

    private static final long serialVersionUID = -2207607975964605180L;
	
    private final ReadWriteLock rwLock = new ReentrantReadWriteLock();
    private final Lock readLock = rwLock.readLock();

    private String filePath = "";

    private static long sequenceNumber = 0;
    
    protected IMLogFileAppender(String name, Filter filter, Layout<? extends Serializable> layout, final boolean ignoreExceptions) {
        super(name, filter, layout, ignoreExceptions);
    }

    protected IMLogFileAppender(String name, Filter filter, Layout<? extends Serializable> layout, final boolean ignoreExceptions, String filePath) {
        super(name, filter, layout, ignoreExceptions);
        
        this.filePath = filePath;
    }
    
    @Override
    public void append(LogEvent event) {
        readLock.lock();
        
        try {	
            final byte[] bytes = getLayout().toByteArray(event);
            
            UUID uuid = UUID.randomUUID();
            FileOutputStream fos = new FileOutputStream(filePath + String.format("%019d", sequenceNumber++) + "-" + uuid.toString() + ".log");
            fos.write(bytes);
            fos.close();           
        } catch (Exception ex) {
            if (!ignoreExceptions()) {
                throw new AppenderLoggingException(ex);
            }
        } finally {
            readLock.unlock();
        }
    }

    @PluginFactory
    public static IMLogFileAppender createAppender(
            @PluginAttribute("name") String name,
            @PluginElement("Layout") Layout<? extends Serializable> layout,
            @PluginElement("Filter") final Filter filter,
            @PluginAttribute("filePath") String filePath) {
    	
        if (name == null) {
            LOGGER.error("No name provided for IMLogFileAppender");
            return null;
        }
        
        if (layout == null) {	
            layout = PatternLayout.createDefaultLayout();
        }
        
        return new IMLogFileAppender(name, filter, layout, true, filePath);
    }
}
```

___

### :fa-hand-o-right: Next Step
:fa-plus-square: :fa-hdd-o: Configure [Business Events][] or [Logger][]  

#### :fa-cubes: Related
:fa-hdd-o: [Log Views][]    
:fa-hdd-o: [Add or manage Log Agents][]  
:fa-archive: [Log Agents][]    
:fa-rocket: [Install Mule Log Agent][Install]    
:fa-edit: [Configuration][] of the agent  


<!-- References -->
[png_MuleLogo]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/Mule/MuleLogo.png

[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/Log%20Agents.md?at=master
[Add or manage Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/2.%20Add%20or%20manage%20Log%20Agent%20Source/Add%20or%20manage%20Log%20Agent%20Source.md?at=master&fileviewer=file-view-default
[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master

[PreRequisites]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/1.%20PreRequisites.md?at=master
[Install]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/2.%20Install.md?at=master
[Update]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/4.%20Update.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/3.%20Configuration.md?at=master
[Uninstall]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/3.%20Uninstall.md?at=master

[Log4J_Apps]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/Mule/Log4J_Apps.png
[Log4J_Studio]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/Mule/Log4J_Studio.png

[JavaPackage_Studio]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/Mule/JavaPackage_Studio.png

[Business Events]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/Categories/Business%20Events.md
[Logger]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/Categories/Logger.md