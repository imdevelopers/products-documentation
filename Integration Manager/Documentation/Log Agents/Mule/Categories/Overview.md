<tags data-value="Mule,Logging,Log,Log Agent"></tags>

![logo][png_MuleLogo] Overview - Mule logging
=============================================
  				
## :fa-info: About
The Mule Log Agent uses the Log4J framework to retrieve context and message body from Mule. Configuration of Log4J is described [here][Log4J].

To correlate messages within Mule Flows a session variable must be configured. This is described in the [prerequisites][PreRequisites].

Events and Messages (payload) are logged using any of the following techniques:  
 * [Custom Business Events][BusinessEvents] (requires Mule Enterprise edition)
 * [Logger][Logger]


The required code is written within the Mule Flow(s).

___

### :fa-hand-o-right: Next Step
:fa-plus-square: :fa-hdd-o: Configure the necessary [prerequisites][PreRequisites]  

#### :fa-cubes: Related
:fa-hdd-o: [Log Views][]    
:fa-hdd-o: [Add or manage Log Agents][]  
:fa-archive: [Log Agents][]    
:fa-rocket: [Install Mule Log Agent][Install]    
:fa-edit: [Configuration][] of the agent  

<!-- References -->
[png_MuleLogo]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/Mule/MuleLogo.png

[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/Log%20Agents.md?at=master
[Add or manage Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/2.%20Add%20or%20manage%20Log%20Agent%20Source/Add%20or%20manage%20Log%20Agent%20Source.md?at=master
[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master

[PreRequisites]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/1.%20PreRequisites.md?at=master
[Install]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/2.%20Install.md?at=master
[Update]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/4.%20Update.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/3.%20Configuration.md?at=master
[Uninstall]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/3.%20Uninstall.md?at=master

[Log4J]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/Categories/Log4J.md
[PreRequisites]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/Categories/PreRequisites.md
[BusinessEvents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/Categories/Business%20Events.md
[Logger]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/Categories/Logger.md