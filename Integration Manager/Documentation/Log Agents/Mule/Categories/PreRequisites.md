<tags data-value="Mule,Logging,Log,Log Agent"></tags>

:fa-check-square-o: Logging prerequisites
=====
					

___

## :fa-info: Information
This page describes the prerequisites for logging of Mule Flows.

## :fa-edit: Configuration

### Correlation
To be able to correlate on several messages within the same Mule Flow, a session variable is required.  

![SessionVariable_Element][SessionVariable_Element]  

The session variable is named 'RunID' and is placed before the first logging point.  

![SessionVariable_Flow][SessionVariable_Flow]  

```xml
<set-session-variable variableName="runID" value="#[java.util.UUID.randomUUID().toString()]" doc:name="Session Variable"/>
``` 

### Log4J

The Mule Log Agent uses the Log4J framework to log messages. Configuration of Log4J within the Mule Flows is described [here][Log4J].  

### :fa-hand-o-right: Next Step  
:fa-rocket: Configure the [Log4J][]  

##### :fa-cubes: Related  
:fa-hdd-o: [Log Views][]    
:fa-hdd-o: [Add or manage Log Agents][]  
:fa-archive: [Log Agents][]    
:fa-rocket: [Install Mule Log Agent][Install]    
:fa-edit: [Configuration][] of the agent  

<!--References -->
[Install Mule Log Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/2.%20Install.md?at=master

[How to set Logon As A Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Logon%20As%20A%20Service.md?at=master
[Mule Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/Overview.md
[Log4J]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/Categories/Log4J.md
[SessionVariable_Element]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/Mule/SessionVariable_Element.png
[SessionVariable_Flow]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/Mule/SessionVariable_Flow.png