<tags data-value="Log,Mule,Log Agent"></tags>

![logo][png_MuleLogo] Mule Log Agent
=============================================
Log your messages from Mule Server with ease.
					
## :fa-info: About
The Mule Log Agent logs messages and associated promoted properties from **Mule** for flows with logger setup.

[Users][] gain access to logged data using restricted [Log Views][].

## :fa-sliders:  Log Capabilities

* Mule On Premise  
* Cloudhub / Anypoint

The following log options are supported:
* Events ([Logger][] and [Business Events][])
    * Messages
    * Context Properties

## :fa-arrows-alt: Supported Versions
All versions from Mule 3.7.x and onwards are supported.

All editions, **standalone runtime** and **Enterprise** are supported.
 
___

### :fa-hand-o-right: Next Step
:fa-rocket: [Install Mule Log Agent][Install]    
:fa-edit: [Configuration][] of the agent  

#### :fa-cubes: Related
:fa-hdd-o: [Add or manage Log Agents][]  
:fa-plus-square: :fa-hdd-o: [Add or manage Log Views][]   
:fa-hdd-o: [Log Views][]    
:fa-archive: [Log Agents][]    
:fa-user: [Users][]

<!-- References -->
[png_MuleLogo]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/Mule/MuleLogo.png

[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/Log%20Agents.md?at=master
[Add or manage Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/2.%20Add%20or%20manage%20Log%20Agent%20Source/Add%20or%20manage%20Log%20Agent%20Source.md?at=master&fileviewer=file-view-default
[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master

[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master

[PreRequisites]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/1.%20PreRequisites.md?at=master
[Install]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/2.%20Install.md?at=master
[Update]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/4.%20Update.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/3.%20Configuration.md?at=master
[Uninstall]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/3.%20Uninstall.md?at=master

[Logger]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/Categories/Logger.md?at=master
[Business Events]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/Categories/Business%20Events.md?at=master