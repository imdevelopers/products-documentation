<tags data-value="Monitor,Views,Live,Health,Graph"></tags>
<keywords data-value="Integration Manager,Monitor"></keywords>
<description data-value="Overview of Log Agents and the possibilities."></description>

:fa-hdd-o: Log Agents
=====
					


___

## :fa-info: Information

 :fa-flash: **Event** :fa-long-arrow-right: 
 :fa-archive: [Log Agent][Log Agents] :fa-long-arrow-right:
 :fa-download: [LogApi][]  :fa-long-arrow-right:
 :fa-database: [Log Databases][] :fa-long-arrow-right:
 :fa-cloud: [Web API][] :fa-long-arrow-right:
 :fa-globe: [Web Client][] :fa-long-arrow-right:
 :fa-hdd-o: [Log Views][] :fa-long-arrow-right:
 :fa-group: [Roles][] :fa-long-arrow-right:
 :fa-user: [User][Users]

Integration Manager offers a cloud-based and on-premise tool that log data in near real time.
This mission critical information about events and data gives organizations the insights needed from troubleshooting to decision support. 

Integration Manager has support for logging from multiple integration platforms and custom applications.
This powerful feature comes from the use of the [Log API][LogAPI] Web Service (follow link for details).

The design goal here is to be able to **Log  events and messages from any system or application** and provide self service and reporting capabilities with relevant and up to date information to stakeholders.   

The following **Log Agents** are provided out of the box and makes you see what’s happening across distributed environments in near real time:

* **[BizTalk][]** - Log using DTA Tracking and/or Pipeline Components  
* **[Logic Apps][LogicAppsAgent]** - Harvests the logs from your Azure Subscriptions
* **[Log4Net][]** - Log from your existing Log4Net enabled applications - Centralized logging
* **[Mule][]** - Get your events and messages from on-premis Mule Services or from your Anypoint environments
* **[IBM Integration Bus Log Agent][]** - Get events and messages from IBM Integration Bus using IBM Monitor Events
* **[Log Api][LogAPI]** - Log from code (C# and Java)
* **[VB6, COM, VBScript][VBScript]** - Log from VB6/VBScript code
* **[WCF Diagnostics Feature][]** - Log events and payload from WCF based solutions
* **[PickupService][]** - Fetches [Log Events][] from many sources, enables asynchronous messaging

![LogFromAnySource][1]

You can easily integrate logging support from your own custom  .NET/Java based applications using the [Log API][LogAPI].   

___

## :fa-hand-o-right: Next Step
* **[BizTalk][]** - Log using DTA Tracking and/or Pipeline Components  
* **[Logic Apps][LogicAppsAgent]** - Harvests the logs from your Azure Subscriptions
* **[Log4Net][]** - Log from your existing Log4Net enabled applications - Centralized logging
* **[Mule][]** - Get your events and messages from on-premis Mule Services or from your Anypoint environments
* **[IBM Integration Bus Log Agent][]**
* **[Log Api][LogAPI]** - Log from code (C# and Java)  
* **[VB6/COM/VBScript][VBScript]** - Log from VB6/VBScript code


## :fa-cubes: Related
:fa-archive: [Log Agents][]  - Events and messages   
:fa-hdd-o: [Logging Service][] - Processes (indexing) the messages  
:fa-hdd-o: [Log Views][] - Manage [User][Users] access to events and messages cross platform  
:fa-database: [Log Databases][] - Keep as much data as you like  

<!--References -->

[BizTalk]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Microsoft%20BizTalk%20Server/Overview.md?at=master
[Log4Net]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Log4Net/Log4Net%20Appender.md?at=master
[Mule]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/Overview.md?at=master
[IBM Integration Bus Log Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/IBM%20Integration%20Bus/Overview.md?at=master
[LogAPI]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Log%20API/Overview.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/LogFromAnySource.png
[Web API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Web%20API/Overview.md?at=master
[Log Databases]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master

[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master

[Log Databases]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Log%20Databases/Log%20Databases.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/Log%20Agents.md?at=master
[LogicAppsAgent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20App%20Services/Overview.md

[WCF Diagnostics Feature]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/WCF%20Diagnostics/Overview.md?at=master

[PickupService]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Pickup%20Service/Overview.md?at=master
[Log Events]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Log%20Event/Overview.md?at=master
