<tags data-value="Log,Rest API,Pickup Service,Log Agent"></tags>

:fa-hand-paper-o: Pickup Service Log Agent
=============================================
Fetches Log events/messages from any message broker and/or custom service that can produce a [json formatted log event][JSON Log Event].
					
## :fa-info: About
The Pickup Service Log Agent is not a Log Agent with logic like other Log Agents, it just read files from disk or queues and posts them to the **Rest API** on the IM [Log API][]  
The logging is done using a HTTP/HTTPS POST of a [Log Event][Log Events] to `api/LogEvent/LogEvent`

## :fa-sliders: Log Capabilities

* Disk / Folder
* ActiveMQ
* Azure ServiceBus
* Any other please contact our support, [support@integrationsoftware.se](mailto:support@integrationsoftware.se)

## :fa-arrows-alt: Supported Versions
All versions of ActiveMQ
 
___

### :fa-hand-o-right: Next Step
:fa-rocket: [Install Pickup Service][Install]    
:fa-edit: [Configuration][] of the agent  

#### :fa-cubes: Related
:fa-hdd-o: [Add or manage Log Agents][]  
:fa-plus-square: :fa-hdd-o: [Add or manage Log Views][]   
:fa-hdd-o: [Log Views][]    
:fa-archive: [Log Agents][]    

<!-- References -->

[Log API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Log%20API/Overview.md?at=master

[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/Log%20Agents.md?at=master
[Add or manage Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/2.%20Add%20or%20manage%20Log%20Agent%20Source/Add%20or%20manage%20Log%20Agent%20Source.md?at=master&fileviewer=file-view-default
[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master

[PreRequisites]:1.%20PreRequisites.md?at=master
[Install]:2.%20Install.md?at=master
[Configuration]:3.%20Configuration.md?at=master
[Update]:4.%20Update.md?at=master
[Uninstall]:5.%20Uninstall.md?at=master
[Overview]:Overview.md?at=master

[JSON Log Event]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Log%20Event/Json%20Formated.md?at=master

[Log Events]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Log%20Event/Overview.md?at=master
