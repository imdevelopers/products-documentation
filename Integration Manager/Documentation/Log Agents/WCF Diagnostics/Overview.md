<tags data-value="IIS, Log, Logging Service, Pickup Service"></tags>

IIS WCF Diagnostics  
=====
![logo][1] 

The WCF Diagnostics feature for Integration Manager Logs events and messages for [WCF Solutions](https://docs.microsoft.com/en-us/dotnet/framework/wcf/whats-wcf) hosted in IIS.
					
## :fa-info: About

:fa-flash: Event :fa-long-arrow-right: 
 :fa-folder-o: Folder :fa-long-arrow-right:
 :fa-hand-paper-o:  [Pickup Service][]  :fa-long-arrow-right:
 :fa-cloud-download: [Log API][LogAPI] :fa-long-arrow-right:
 :fa-database: [Log Databases][] :fa-long-arrow-right:
 :fa-cloud: [Web API][] :fa-long-arrow-right:
 :fa-globe: [Web Client][] :fa-long-arrow-right:
 :fa-hdd-o: [Log Views][] :fa-long-arrow-right:
 :fa-group: [Roles][] :fa-long-arrow-right:
 :fa-user: [User][Users]

Provides detailed debug logging from enabled WCF Solutions hosted in IIS.

The **WCF Diagnostics feature** logs event and messages including any configured Web Application/Web Service using the WCF stack. 

## :fa-sliders: Log Capabilities

* Events
    * Messages (payload)
    
## :fa-arrows-alt: Supported Versions
Integration Manager runs smoothly with the following versions and editions:

All IIS versions from version 7.5 and onwards are supported.

* Windows Server 2008 R2 (IIS 7.5)
* Windows Server 2012 (IIS 8.0)
* Windows Server 2012 R2 (IIS 8.5)
* Windows Server 2016 R2 (IIS 10)
  

All editions are supported:
* Enterprise
* Standard
* Developer (which is Enterprise)
* Trial (which is Enterprise) 

## :fa-hdd-o: Logged data
[Users][] access logged data using roles based information restricted [Log Views][].


    TIP: Use the Non Events Agent to make sure you are alerted whenevere there is data outage and/or the File monitor Agent to monitor the destination folders for log files not consuming all available disk space over time

    NOTE: If the **Pickup Service** has been stopped for a long time, it may take a while to catch up
___
  
### :fa-hand-o-right: Next Step
:fa-check-square-o:  [WCF Diagnostics Prerequisites][WCFDiagPreReqs]  
:fa-plus-square: :fa-hdd-o: [Add or manage Log Views][]  

#### :fa-cubes: Related
:fa-hdd-o: [Log Views][]    
:fa-archive: [Log Agents][]

[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Overview.md?at=master
[Add or manage Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/2.%20Add%20or%20manage%20Log%20Agent%20Source/Add%20or%20manage%20Log%20Agent%20Source.md?at=master&fileviewer=file-view-default

[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master


[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/WCFDiagnostics/WCFLogo2.png

[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master

[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Log Databases]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Log%20Databases/Log%20Databases.md?at=master
[Non Events Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Non-Events/Overview.md?at=master

[CoreServicesArchitecture]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Architecture.md?at=master

[WCFDiagPreReqs]:/1.%20PreRequisites.md
[Pickup Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Pickup%20Service/Overview.md?at=master
[LogAPI]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Log%20API/Overview.md?at=master
[Web API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Web%20API/Overview.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master