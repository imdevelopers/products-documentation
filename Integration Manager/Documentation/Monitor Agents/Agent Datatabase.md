<tags data-value=""></tags>

:fa-arrow-circle-o-up: Manually install or update Monitor Agent databases
=====
					
___

## :fa-info: Information
Updates and installations should be done with an account that is sysadmin on the SQL instance or preferred the IM Monitor Agent service account.  
This functionality requires that the [Data-tier Applications][] SqlPackage.exe is installed on the server. [Download][] from our download page or from Microsoft.
___

## :fa-info: Install
Open the `IM.MonitorAgent.%AgentName%.json` file and update the MonitorAgentConnectionString. With the server where the database should be installed and the name of the database.  

```xml
...
"MonitorAgentConnectionString": "Server=localhost;Database=IM_MonitorAgent_%AgentName%_Test;Integrated Security=SSPI;Connection Timeout=60",
...
```
:fa-exclamation-triangle: Must be json formated, if not default instance write as `Server=localhost\\instance`

Run the `IM.Installer.MonitorAgent.%AgentName%Database.exe` using the the correct account.

![Agent database install][png_AgentDatabase_Install]  


## :fa-info: Update

Run the `IM.Installer.MonitorAgent.%AgentName%Database.exe` using the the correct account.

![Agent database update][png_AgentDatabase_Update]  


### :fa-hand-o-right: Next Step  
<!-- :fa-rocket: [Update][]   -->

##### :fa-cubes: Related  
 
<!-- :fa-file-text-o: [Release Notes][]   -->


<!--References -->
[Update]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Update/Overview.md?at=master

[png_AgentDatabase_Install]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/AgentDatabase_Install.png
[png_AgentDatabase_Update]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/AgentDatabase_Update.png

[Data-tier Applications]:https://docs.microsoft.com/en-us/sql/relational-databases/data-tier-applications/data-tier-applications

[Download]:http://download.integrationsoftware.se/prerequisites/
