<tags data-value="Azure, Logic Apps,Logging, Monitoring, Azure"></tags>

:fa-edit: Configuration of Azure Logic Apps
=====
					


___

## :fa-info: Information
In this section you will learn how to configure the **IM Azure Logic Apps Agent**.  

The file `IM.MonitorAgent.LogicApps.json` contains the configuration for the agent. Edit with Notepad++ or Notepad (remember to run as Administrator)

You can change the following options.

* Environment - is ususet to **Test**, **Prod**, **QA** or your name of the environment this agent is running in.
* Debug - true or false, in a production environment this should be set to false
* Version - READ ONLY, this is internal for the agent to know if to update the configuration to latest version **do not change**
* CultureInfo - is not implemented yet, should be used to format the output of date and so on.

![settings][2]

* Subscriptions is an array of connection information objects, see [Azure Application Access][AzureApplicationsAccess] for additional information.
    * **SubscriptionId** 
    * **ResourceGroup**
    * **TenantId**
    * **ClientId** 
    * **ClientSecret**

* **Log**, determines if logging for this configuration is enabled or not and the corresponding [Log Agent][] Source Id.
    * Enabled - true or false
    * LogAgentId - is the unique **Log Agent Value Id** in Integration Manager so you konw wich Log Agent was used to send data to Integration Manager

    NOTE: You should not change the LogAgentId once events and messages has started to be transfered from Azure to Integration Manager

* **Monitor**, determines if Monitoring of Logic Apps for this configuration is enabled or not
    * Enabled - true or false

## 
```json
{
  "Environment": "IBSSDEMO",
  "Debug": false,
  "Version": "1.1",
  "CultureInfo": "sv-SE",
  "Subscriptions": [
    {
      "SubscriptionId": "e887306b-XXXX-YYYY-ad4c-7b7fa27d2622",
      "ResourceGroup": "INTSOFT",
      "TenantId": "639f222d-XXXX-YYYY-8ef9-e7307426f85e",
      "ClientId": "609b47d3-XXXX-YYYY-9995-9df6b393847e",
      "ClientSecret": "yoursecrethere=",

      "Log": {
        "Enabled": true,
        "LogAgentId": 1000
      },
      "Monitor": {
        "Enabled": true
      }
    }
 ]
}
```
    NOTE: You must restart the agent to reflect changes in the configuration file


___

### :fa-hand-o-right: Next Step
* :fa-plus-square: :fa-cloud-download: [Add or manage a Source][]  
* :fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]  
* :fa-user-secret: [Azure Application Access][AzureApplicationsAccess]

#### :fa-cubes: Related
* :fa-rocket: [Install Azure Logic Apps Agent][]    
* :fa-hdd-o: [Logic Apps Log Agent][LogicAppsAgent]
* :fa-hdd-o: [Logging Service][]

[Install Azure Logic Apps Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20App%20Services/2.%20Install.md?at=master

[Log Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Overview.md?at=master

[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Log/1.%20Log%20Views/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master
[View All Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Log/1.%20Log%20Views/1.%20View%20All%20Log%20Views/View%20All%20Log%20Views.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master

[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Log/2.%20Sources/Sources.md?at=master  
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master

[Log4J]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/Categories/Log4J.md
[PreRequisites]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/Categories/PreRequisites.md
[BusinessEvents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/Categories/Business%20Events.md
[Logger]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/Categories/Logger.md

[AzureApplicationsAccess]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Azure%20Application%20Access.md?at=master

[LogicAppsAgent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20App%20Services/Azure%20Logic%20Apps/Overview.md?at=master

[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/LogicApps\LogApiURL.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/LogicApps\Settings.png
