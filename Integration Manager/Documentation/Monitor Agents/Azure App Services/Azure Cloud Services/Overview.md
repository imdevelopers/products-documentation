<tags data-value="Monitor,Azure,Service,Cloud Service"></tags>

Microsoft Azure Cloud Service
=============================================

Monitor [Microsoft Azure Cloud Services][1] with Integration Manager's monitor agent for [Microsoft Azure Cloud Services][1]

## About
This agent allows you to monitor [Microsoft Azure Cloud Services][1].

Cloud Services is an example of Platform-as-a-Service (PaaS). Like App Services, this technology is designed to support applications that are scalable, reliable, and cheap to operate. Just like an App Services are hosted on VMs, so too are Cloud Services, however, you have more control over the VMs. You can install your own software on Cloud Service VMs and you can remote into them.

## Monitor Capabilities
List of resources that can be monitored by using this agent

* State (Running, Stopped)
* Worker Roles

## Actions

* Start / Stop web jobs
* Details
  * List of worker roles


[1]: https://azure.microsoft.com/en-us/services/cloud-services/
