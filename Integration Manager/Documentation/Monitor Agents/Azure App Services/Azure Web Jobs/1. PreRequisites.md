<tags data-value="Monitoring, Web Jobs"></tags>

:fa-check-square-o: The Web Jobs Monitoring Agent has the following pre requisites
=====
					

___

## :fa-info: Information
This page describes the prerequisites for installing and running the [Web Jobs Monitor Agent][Web Jobs Agent].

## Software requirements

* :fa-windows: Microsoft Windows Server 2008 R2 or later
    * .NET Framework 4.0 or later*
* Security/User rights (listed below)
* Firewall
    * Default outgoing port 443 to Internet for Service Bus Relayed connections
    * Default inbound and outbound port 8000 **IM Monitor Agent** and **IM Web Jobs Monitoring Agent**


The agent can be installed On-Premise using TCP/IP and in the Cloud using [Service Bus Relaying][ServiceBusRelaying].

## :fa-windows: Windows User Rights
* Local named account or domain account (preferred). Follow this guide '[How to set Logon As A Service][]' for more information.  

## :fa-cloud: Azure User rights
The agent must be configured to use information from your Azure subscription. See more [here][AzureApplicationsAccess]

### :fa-hand-o-right: Next Step
:fa-rocket: [Install Web Jobs Monitor Agent][]  

#### :fa-cubes: Related
:fa-plus-square: :fa-download: [Add or manage a Source][]  
:fa-desktop: [Monitor Agents][]    
:fa-cogs: [Administration][]    
:fa-cloud-download: [Sources][]

<!--References -->
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[ServiceBusRelaying]:https://azure.microsoft.com/sv-se/documentation/articles/service-bus-dotnet-how-to-use-relay/

[Install Web Jobs Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20Web%20Jobs/2.%20Install.md?at=master
                            
[How to set Logon As A Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Logon%20As%20A%20Service.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Web Jobs Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20Web%20Jobs/Overview.md?at=master
[AzureApplicationsAccess]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Azure%20Application%20Access.md?at=master