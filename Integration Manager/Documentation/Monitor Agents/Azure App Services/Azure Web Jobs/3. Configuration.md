<tags data-value="Configuration, Source, Web Jobs"></tags>

:fa-edit: Source Configuration
=====
					


___

## :fa-info: Information
In this section you will learn how to configure the **IM Web Jobs Monitor Agent** as a new [Source][Sources].

    Note: You must have installed the agent before configuration of the Source can be performed.
    
## :fa-rocket: [Install Web Jobs Monitor Agent][]  
    
### :fa-sort-numeric-asc: Steps  
The following steps are required and must be performed in order after [installation][Install] before accessing the remote configuration:

1. Add a new [Source][Sources] 
After successful [Installation][Install] you must add a [Source][Sources] in the [Web Client][] to access the [Remote Configuration][] for the installed agent.  
:fa-plus-square: :fa-cloud-download: [Add or manage a Source][]  

2. Configure Communication options between the [Monitoring Service][] and the [Monitor Agent][]  
Either use:  
    2.1 TCP/IP (HTTP)  
    2.2 Service Bus Relaying

3. Use [Remote Configuration][] to configure the agent. Continued on this page below.
  
    Note: [Remote Configuration][] is available if the [Source][Sources] is authenticated or uses [Service Bus Relaying][]. 

4. Create [Monitor Views][] that uses the [Resources][] from the [Source][Sources] according to business needs.  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]

5. Fine tune specific settings using Actions if allowed by the [Monitor View][Monitor Views]  

:fa-edit: Remote Configuration of Web Jobs
=====
The [Remote Configuration][] for the IM Web Jobs Monitor Agent provides the ability to configure the agent remotely, even if the agent is installed off site.
Many specific configurations can also be made directly on the [Resource][Resources] using the **Action** button if the [Monitor View][Monitor Views] allows for Actions.

## :fa-wrench: Settings

![png_WebJobsServerSettings][]

### Environment
Part of the common features shared with all [Monitor Agents][Monitor Agent], there is an option to set the name of the target **Environment**, for example TEST, QA, PROD.  

### Debug
Part of the common features shared with all [Monitor Agents][Monitor Agent], there is an option to set the **Debug** flag for additional file logging that can be enabled/disabled as needed. Default is unchecked.

### Culture Information
Gets or sets the culture information that defines the culturally appropriate format of displaying dates and times

## :fa-dropbox: Applications
The name of the Application is automatically created based on subscription details.    
![png_WebJobsServerApplication][]

Read more about [Applications][] here:
* :fa-dropbox: [Applications][]  


## :fa-plus-square: Add resource groups
The list of resource groups are automatically populated.

<!--
![png_SQLServerAddFirst][]

Click on **:fa-plus: Add** to add a SQL Web Jobs configuration
-->

### Configure the added server

![png_WebJobsResourceGroupAccordion][]

Click on the Accordion to expand 


### :fa-folder: Web Jobs Categories
For configuration of specific Web Jobs Categories, see:

* [Triggered Web Jobs][]
    * General settings
    * Specific settings (per named web job)
* [Continous Web Jobs][]
    * General settings
    * Specific settings (per named web job)

## Save
You must click **Save** for changes to be written to the agent and take effect.   
![png_SaveAndClose][]

    Note: Depending on the synchronization interval set for the agent, there might be a delay before the Web Client reflects upon the change. You can choose to force the agent to synchronize from the configuration of the Source. 

**Save and close**, saves and closes the dialog.

**Cancel** Close the dialog without saving any changes.
 
<!--References -->
[png_applications]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/RemoteConfiguration/SQLServerAddFirst.png
[png_SQLServerAddFirst]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebJobs/RemoteConfiguration/WebJobsServerAddFirst.png
[png_WebJobsServerAccordion]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebJobs/RemoteConfiguration/WebJobsServerAccordion.png
[png_WebJobsServerBasic]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebJobs/RemoteConfiguration/WebJobsServerBasic.png
[png_WebJobsServerApplication]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebJobs/Applications.png
[png_WebJobsServerSettings]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebJobs/Settings.png
[png_WebJobsResourceGroupAccordion]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebJobs/png_WebJobsResourceGroupAccordion.png
[png_WebJobsAzureAdd]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebJobs/AzureDatabaseAddGeneral.png
[png_SaveAndClose]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/SaveAndClose.png
___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-cloud-download: [Add or manage a Source][]    
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]  

##### :fa-cubes: Related  
:fa-rocket: [Install Web Jobs Monitor Agent][]     
:fa-cloud-download: [Source][Sources]    
:fa-dropbox: [Applications][]    
:fa-history: [Web Jobs Agent][]
:fa-folder: [Triggered Web Jobs][]
:fa-folder: [Continous Web Jobs][]

[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Remote%20Configuration/Remote%20Configuration.md?at=master
[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Web Jobs Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20Web%20Jobs/Overview.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[View All Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/1.%20View%20All%20Monitor%20Views/View%20All%20Monitor%20Views.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master

[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master  
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[YouTube]:https://www.youtube.com/watch?v=-ky8QqyQAHE**
[Service Bus Relaying]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/ServiceBusRelaying.md?at=master

[Install Web Jobs Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20Web%20Jobs/2.%20Install.md?at=master
[Applications]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/Applications.md?at=master

[Azure - SQL Size Checks]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20Web%20Jobs/Categories/Size%20Checks/Azure%20-%20SQL%20Size%20Checks.md
[SQL Jobs]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20Web%20Jobs/Categories/Jobs/Overview.md
[SQL Statements]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20Web%20Jobs/Categories/SQL%20Statements/Overview.md
[SQL Backups]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20Web%20Jobs/Categories/SQL%20Backups/SQL%20Backups.md
[Size Checks]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20Web%20Jobs/Categories/Size%20Checks/SQL%20Size%20Checks.md

[Triggered Web Jobs]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20Web%20Jobs/Categories/Triggered%20Web%20Jobs.md?at=master
[Continous Web Jobs]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20Web%20Jobs/Categories/Continous%20Web%20Jobs.md?at=master
