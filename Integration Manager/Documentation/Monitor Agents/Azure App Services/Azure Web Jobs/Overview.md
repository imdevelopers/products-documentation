<tags data-value="Monitor,Azure,Web Jobs,App Services"></tags>

:fa-history: Microsoft Azure Web Jobs
=============================================

Monitor [Microsoft Azure Web Jobs](https://azure.microsoft.com/sv-se/documentation/articles/web-sites-create-web-jobs/) with Integration Manager's monitor agent for [Microsoft Azure Web Jobs](https://azure.microsoft.com/sv-se/documentation/articles/web-sites-create-web-jobs/).

This agent has been developed from demands and input set from some Microsoft MVP:s. This agent is typically used by our cloud customers creating modern system integration solutions based on the Azure stack.

<img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebJobs/FailedWebJobs.png" height="265" />


**WebJobs** is a complete compute solution on a managed platform. The runtime offers many choices on how your code can run (cron, service, manually). Using Azure Websites as a platform means that WebJobs inherits tons of cool features. Azure Scheduler brings a plethora of scheduling choices. The ASP.NET team built the WebJobs SDK, which makes writing code that interfaces with the different Azure components (Blob, Queues, Tables, Service Bus) a snap! And the rich Visual Studio tooling wraps everything together seamlessly. Goes to show that sometimes the whole is bigger than the sum of its parts.

## :fa-info: About
This agent allows you to monitor the performance and outcome from Web Jobs. Also, the ability exists to perform remote actions on [Microsoft Azure Web Jobs](https://azure.microsoft.com/sv-se/documentation/articles/web-sites-create-web-jobs/) hosted in your Azure subscription(s) without the need of the Azure portal.

* General and specific settings for last run (The longest allowed timespan before issuing awarning for last run)
    * Warning 
    * Error
* General and specific settings for duration (The longest allowed run duration for Web Job in seconds)
    * Warning 
    * Error
* State - make sure the Web Jobs are started and stopped accidentally
* Web Jobs from **multiple subscriptions** can be monitored from the same installed agent
* Remote Actions are available on some resources which removes the need to log on to the Azure portal
    * Ability to delegate control to non Azure administrators
* Multiple agents can be deployed on multiple servers
* Agents can be local or in the cloud, and even off site (partner/customer location)
* Low overhead with least privileges policy
* Report capabilites

:fa-sitemap: Documentation support is provided by the [Repository][] model. 
 
## :fa-sliders: Monitor Capabilities
The [Resources][Resources] are grouped by [Categories][]. The following  [Web Jobs Categories][Web Jobs Categories] exists:

* [Triggered Web Jobs][]
    * General settings
    * Specific settings (per named web job)
* [Continous Web Jobs][]
    * General settings
    * Specific settings (per named web job)

The agent provides capabilites for monitoring and remote management of: 
* Web Jobs State (running, stopped)
* Recent job outcome (whether the last job has succeeded or not)

## :fa-flash: Actions
Get insights, view and fix your Web Jobs related problems with ease from a distance without the Azure portal..

Using the Web Client for Integration Manager, Actions can be sent to the monitor agent for Web Jobs requesting operations to be performed on the monitored resources. With the existing privilege model you can allow certain users to perform operation on selected resources.

List of Actions on the Resources of Web Jobs Categories that can be executed from using this agent:

* Start / Stop web jobs
* Run web jobs (on demand)
* View output from last run 
* View History

## :fa-list-alt: Release Log
For information about the latest release, see [Support](https://support.integrationsoftware.se/discussions/forums/1000229322)
___

### :fa-hand-o-right: Next Step
:fa-rocket: [Install Web Jobs Monitor Agent][]  
:fa-edit: [Configuration][] of the agent  
:fa-folder: [Triggered Web Jobs][]
:fa-folder: [Continous Web Jobs][]

#### :fa-cubes: Related
:fa-check-square-o: [PreRequisites for Web Jobs Monitor Agent][]  

[Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/Categories.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Reports]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Reports/Overview.md?at=master
[Power BI]:https://powerbi.microsoft.com/en-us/
[Web API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Web%20API/Overview.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master

[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20Web%20Jobs/3.%20Configuration.md?at=master
[Install Web Jobs Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20Web%20Jobs/1.%20Install.md?at=master  
[PreRequisites for Web Jobs Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20Web%20Jobs/1.%20PreRequisites.md?at=master
[Web Jobs Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20Web%20Jobs/Categories/Overview.md?at=master
[Triggered Web Jobs]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20Web%20Jobs/Categories/Triggered%20Web%20Jobs.md?at=master
[Continous Web Jobs]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20Web%20Jobs/Categories/Continous%20Web%20Jobs.md?at=master

[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master