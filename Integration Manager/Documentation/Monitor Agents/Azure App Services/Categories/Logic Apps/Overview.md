<tags data-value="Monitor,Azure,App Services,Logic Apps"></tags>

:fa-rocket: Monitoring Azure Logic Apps
=============================================

Easily Monitor your [Microsoft Azure App Service - Logic Apps][1] with Integration Manager's monitor agent for [Microsoft Azure App Service - Logic Apps][].

## :fa-hdd-o: Logging 
Track, Archive and the find your transactions using the Log Capabilities, read more [here](https://support.microsoft.com/en-us/kb/906736).
					
## :fa-info: About
This agent allows you to monitor any number of [Microsoft Azure App Service - Logic Apps][] from any number of Azure subscriptions.

## :fa-sliders: Monitor Capabilities
The Logic Apps agent allows you to Log Events Messages and Monitor the state of your Logic Apps.
The Agent automatically adds your API apps used in the logic apps to be monitored. There is no need to code or change anything within your Logic App flows.

* **Monitor** the state of each available logic app (Running, Stopped)
* **Log** messages (Events and payload) from all available logic apps

## :fa-flash: Actions
The following Actions are available:
* **Enable** - Enables the Logic App
* **Disable** - Disables the Logic App
* **Details** - Get details about the Logic App
___

### :fa-hand-o-right: Next Step
* :fa-check-square-o: [PreRequisites for Azure Logic Apps Agent][]   
* :fa-rocket: [Install Azure Logic Apps Agent][]     

#### :fa-cubes: Related
* :fa-hdd-o: [Logic Apps Logging][LogicAppsAgent] - Harvests the logs from your Azure Subscriptions  
* :fa-archive: [Log Agents][]  - Events and messages   
* :fa-hdd-o: [Logging Service][] - Processes (indexing) the messages  
* :fa-hdd-o: [Log Views][] - Manage [User][Users] access to events and messages cross platform  
* :fa-database: [Log Databases][] - Keep as much data as you like  
* :fa-user-secret: [Azure Application Access][AzureApplicationsAccess]   
* :fa-file-text-o: [Release Notes][]  

<!--References -->


[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/LogFromAnySource.png
[Log Databases]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log

[Log Databases]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Log%20Databases/Log%20Databases.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/Log%20Agents.md?at=master
[LogicAppsAgent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20App%20Services/Azure%20Logic%20Apps/Overview.md?at=master
[Release Notes]:http://support.integrationsoftware.se/support/discussions/forums/1000229074

[Microsoft Azure App Service - Logic Apps]:https://azure.microsoft.com/en-us/services/app-service/logic/        "Microsoft Azure App Service - Logic Apps"

[AzureApplicationsAccess]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Azure%20Application%20Access.md?at=master

[Install Azure Logic Apps Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20App%20Services/2.%20Install.md?at=master

[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[PreRequisites for Azure Logic Apps Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20App%20Services/1.%20PreRequisites.md?at=master
