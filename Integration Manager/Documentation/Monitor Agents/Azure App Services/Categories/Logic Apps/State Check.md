<tags data-value="Monitor,Azure,App Services,Logic Apps"></tags>

:fa-rocket: Azure Logic Apps
=============================================

## :fa-info: Information
Monitor the state of your **Azure Logic Apps**. The [Azure Logic Apps Agent][Azure Logic Apps Agent] creates one [Resource][Resources] per Azure Logic App and can be monitored using suitable [Monitor Views][].   
___

## :fa-question-circle: Whats evaluated
The following rules are evaluated for **Azure Logic Apps**:

#### :fa-check-circle-o: OK - Used to indicate normal operation
* The Logic App is **Enabled**

#### :fa-times-circle: Error - Used to indicate an error/fatal condition
* The Logic App is **Disabled**

The evaluated state may be reconfigured using **Expected State** functionality that exists on every [Resource][Resources] within Integration Manager.
___

## :fa-edit: Configuration
    
___

## :fa-flash: Actions
The [Azure Logic Apps Agent][] has support for remote actions. The folllowing Actions are exposed:  
* Enable the Logic App
* Disable the Logic App
* Get details about the Logic App

 ![actions][1]

<br/>
      
#### :fa-toggle-on: Enable

The dialog displayed from the action **Enable**  

 ![actions][3]
  
#### :fa-toggle-off: Disable

The dialog displayed from the action **Disable**  

 ![actions][4]
  
#### :fa-info: Logic App Details

The information retrieved from the action **Details**  

 ![actions][2]
  
___

## Next step
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]  

#### :fa-cubes: Related  
:fa-lightbulb-o: [Resources][]  
:fa-cloud-download: [Monitor Agents][]  
:fa-desktop: [Monitor Views][]    

<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/LogicApps/LogicApps-Actions.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/LogicApps/LogicApps-Action-Details.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/LogicApps/LogicApps-Action-Enable.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/LogicApps/LogicApps-Action-Disable.png


[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master

[Azure Logic Apps Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20App%20Services/Azure%20Logic%20Apps/Overview.md?at=master
