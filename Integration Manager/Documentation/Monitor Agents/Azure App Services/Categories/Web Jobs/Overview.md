<tags data-value="Web Jobs,Actions,Monitor,View,Resource"></tags>

:fa-folder: Web Jobs Monitor Categories
=====
					


___

## :fa-info: Information

The Web Jobs Monitoring Agent supports the execution of remote actions on resources using the Integration Manager Web Client tool or by programmatically calling the [Web API][].
In order to execute remote actions some [PreRequisites][PreRequisites for Web Jobs Monitor Agent] must be satisfied. 

The user must be logged on to Integration Manager using a Windows Credential. The user must also be assigned a monitor view with Web Jobs [Resources][] where [Actions][] are allowed. See [Add or manage Monitor Views][] for more details.
___

The various monitoring capabilities for the Web Jobs Server agent are expressed as [Resources][] and are grouped by [Categories][].
The **Categories** are unique for each [Monitoring Agent][Monitor Agents]. 

The following Categories are exposed by the **Web Jobs Monitor Agent**.

## :fa-folder: Categories

* [Triggered Web Jobs][] 
* [Continous Web Jobs][]

![png_Categories][1]

 ## :fa-edit: Remote Configuration
 First the [Web Jobs Agent Monitor][Web Jobs Agent] must be configured using [Remote Configuration][] set on the [Source][Sources].

 The [Web Jobs Agent Monitor][Web Jobs Agent] has support for [Remote Configuration][] and monitors configured Web Jobs provided by the configuration. 
 

___

### :fa-hand-o-right: Next Step
:fa-folder: [Triggered Web Jobs][]
:fa-folder: [Continous Web Jobs][]

#### :fa-cubes: Related
:fa-edit: [Configuration][] of the agent  
:fa-check-square-o: [PreRequisites for Web Jobs Monitor Agent][]

<!--References -->
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20Web%20Jobs/3.%20Configuration.md?at=master

[Monitor View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/3.%20Monitor/Monitor.md?at=master
[Web Jobs]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20Web%20Jobs/Overview.md?at=master
[Actions]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/1.%20View%20Monitor%20Views/2.%20View%20Single%20Monitor%20View/Actions/Actions.md?at=master

[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[Web API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Web%20API/Overview.md?at=master
[PreRequisites for Web Jobs Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20Web%20Jobs/1.%20PreRequisites.md?at=master

[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Remote%20Configuration/Remote%20Configuration.md?at=master
[Web Jobs Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20Web%20Jobs/Overview.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/Categories.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master

[Triggered Web Jobs]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20Web%20Jobs/Categories/Triggered%20Web%20Jobs.md?at=master
[Continous Web Jobs]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20Web%20Jobs/Categories/Continous%20Web%20Jobs.md?at=master

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebJobs/Categories.png
