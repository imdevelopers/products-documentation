<tags data-value="Monitor,Azure,App Services,Logic App, Web Jobs, Control Center"></tags>

:fa-cloud: Microsoft Azure App Services Agent
=============================================

[Monitor][] and [Log][]  events and data from your **Web** and **mobile apps**.

 [Microsoft Azure App Services](https://azure.microsoft.com/en-us/services/app-service) is a cloud platform to build powerful web and mobile apps that connect to data anywhere, in the cloud and/or on-premises.

## :fa-info: About
The **Microsoft Azure App Services Agent** has many features that helps you log events and data, also getting alerts to make sure business critical solutions stay healthy over time.
If there is a problem, the agent can detect and perfom remote actions on selected [Resources][] thereby solving your problems without the need for the Azure Web Portal.

Configuration and specific details are provided separately for each app service function. 
* **[Web Jobs Agent][]**

The following topics are shared and are the very same for each app service function (since it is the same agent):
* [PreRequisites][]
* [Install][]
* [Update][]
* [Uninstall][]

## Other Azure related Agents
* Service Bus
* Dynamics CRM Online
* SQL Databases
* Web Services (everything and anything with an http/https endpoint)

<!-- 
## :fa-hdd-o: Logging support
Logging support is provided by the following App Services: 

* **Logic Apps**
	* [Logging from Logic Apps][]

Logged data is processed by the [Logging Service][Logging Service].

:fa-sitemap: Documentation support is provided by the [Repository][] model. 
 -->

## :fa-sliders: Monitor Capabilities
List of resources that can be monitored by using this agent

<!-- :fa-rocket: Azure Logic Apps -->

## :fa-flash: Actions
* **Web Jobs*'
	* Start / Stop web jobs
	* Run web jobs (on demand)
	* View output from last run
	* View History

## :fa-list-alt: Release Log
For detailed information about the features and bug fixes, please see the [Release Log](https://support.integrationsoftware.se/discussions/forums/1000229074)


### :fa-hand-o-right: Next Step
:fa-rocket: [Install][]

#### :fa-cubes: Related
:fa-desktop: :fa-rocket: Monitoring [Logic Apps Agent][]    
:fa-desktop: :fa-history: Monitoring [Web Jobs Agent][]  
:fa-desktop: :fa-eye: [Monitor Views][]  
:fa-desktop: :fa-cloud-upload: [Monitor Agents][]  
:fa-hdd-o: [Logging][Log]   
:fa-desktop: [Monitoring][]  
      

[PreRequisites]:[(https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20App%20Services/1.%20PreRequisites.md)]
[Install]:[https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20App%20Services/1.%Install.md]
[Update]:[https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20App%20Services/1.%Update.md]
[Uninstall]:[https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20App%20Services/1.%Uninstall.md]
[Monitor Views]:[https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master]
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Web Jobs Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Azure%20Web%20Jobs/Overview.md?at=master
[Logic Apps Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/%2FLogic%20Apps%2FOverview.md?at=master

[Log]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/Log.md?at=master

[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master

[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Monitor]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/Monitor.md?at=master

