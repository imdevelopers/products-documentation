<tags data-value="Monitoring, BizTalk Server"></tags>

:fa-check-square-o: The BizTalk Monitoring Agent has the following pre requisites
=====
					

___

## :fa-info: Information
This page describes the prerequisites for installing and running the [BizTalk Monitor Agent][BizTalk Agent].

## Software requirements

* :fa-windows: Microsoft Windows Server 2008 R2 or later
    * .NET Framework 4.0 or later*
    * BizTalk administrative tools requires .NET framework 2.0/3.5 Windows feature.
* BizTalk Server Client Tools in a version equal or higher than target BizTalk Environment
* Security/User rights (listed below)

*/ .NET framework version depends on installed BizTalk version

## :fa-free-code-camp: Firewall

* Default outgoing port 443 to Internet for Service Bus Relayed connections
* Access to AD/DNS 
    * Read more [here](https://technet.microsoft.com/en-us/library/dd772723(v=ws.10).aspx)
* Default inbound and outbound port 8000 **IM Monitor Agent** and **IM BizTalk Monitoring Agent**
* SQL Ports in use between **SQL Server** and **IM BizTalk Monitoring Agent**
    * Read more [here](https://msdn.microsoft.com/en-us/library/cc646023.aspx)
    
## :fa-windows: Windows User Rights
* Local named account or domain account (preferred). Follow this guide '[How to set Logon As A Service][]' for more information.  
* Service Account running BizTalk Monitor Agent requires to be in the Windows Group used for the **BizTalk Server Administrators** role
* Service Account running BizTalk Monitor Agent requires to be in the Windows Group used for the **SSO Administrators** role
* Account must be local administrator on all BizTalk nodes in order to read performance counters and allow start/stop of host instances (Windows Services)

## :fa-database: SQL User Rights
The service account running the [Logging Service][] must have the following rights assigned:

### :fa-level-down: least privileges (basic usage)
* BizTalkMGMTDb
    * DataReader
    * BTS_OPERATORS    

* BizTalkDTADb
    * DataReader
    * DataWriter (for BizTalk Health check related operations/actions)
    * ddl_admin (if this database runs in its own instance or on other server than BizTalkMGMTDb)

* BizTalkMSGBoxDb
    * DataReader
    * DataWriter (for BizTalk Health check related operations/actions)    
    * ddl_admin (recommended since this database should run in its own instance or on other server, See [KB899000](https://support.microsoft.com/en-us/help/899000/the-parallelism-setting-for-the-instance-of-sql-server-when-you-config))  


    

    NOTE: ddl_admin is required in order for the service account to have proper rights to read statistics. Without this permission performance may be degraded, especially true for remote servers (linked servers). Read more [here](https://thomaslarock.com/2013/05/top-3-performance-killers-for-linked-server-queries/). Contact our support if you have any questions about this.


### :fa-level-up: Privileges for actions (extended)
Most Remote Actions require the service account running the BizTalk agent must be part of the following BizTalk Roles:
* **SSO Administrators** role (Details, Start and Stop ports)
* **BizTalk Server Administrators** role (View messages, resume, Terminate, ...)

### :fa-hand-o-right: Next Step
:fa-rocket: [Install BizTalk Monitor Agent][]  

#### :fa-cubes: Related
:fa-plus-square: :fa-download: [Add or manage a Source][]  
:fa-desktop: [Monitor Agent][]    
:fa-cogs: [Administration][]    

[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master

<!--References -->
[connectionstrings]:https://www.connectionstrings.com/sql-server/
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[ServiceBusRelaying]:https://azure.microsoft.com/sv-se/documentation/articles/service-bus-dotnet-how-to-use-relay/
[Install BizTalk Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/2.%20Install.md?at=master
                            
[How to set Logon As A Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Logon%20As%20A%20Service.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[BizTalk Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Overview.md?at=master
