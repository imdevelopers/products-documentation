<tags data-value="Monitor,BizTalk"></tags>

:fa-heart-o: Active Instances
=====
					
  
___

## Information
The **BizTalk Monitor Agent** for Integration Manager has built in support for specific **Aktive Incestances** related functions for BizTalk Server.

![MonitorView][0]

Remote Configuration for Active Instances  
![RemoteConfigHealthCheck][1]

## :fa-question-circle: Whats evaluated
The following rules are evaluated for Active Instanses:

* Warning when default pipelines looses tracking settings 
* Messages with negative ref count
* Messages without ref count
* Orphaned DTA Service Instances
* Throttling  
  Throttling is normal in BizTalk, we evaluate if throttling has been for more than 15 minutes for example
  - Message delivery throttling state  
  - Message publishing throttling state  
  Read more about [Host Throttling Performance Counters][httpHostThrottlingPerformanceCounters]

##### :fa-check-circle-o: OK - Used to indicate normal operation
* OK - Threshold not reached

##### :fa-times-circle: Resource not available
* Error - Used to indicate an error/fatal condition, common causes are user rights

##### :fa-times-circle: Error
* Error - threshold reached

#### :fa-exclamation-triangle: Warning 
* Warning threshold reached

The evaluated state may be reconfigured using **Expected State** functionality that exists on every [Resource][Resources] within Integration Manager.

## :fa-line-chart: Metrics
All BizTalk Host Instances with throttling are displayed in one graph using the **Metrics** functionality of this agent.

* Throttling
  * Throttling over time

___

### :fa-hand-o-right: Next Step  
:fa-desktop: [Monitor Views][]  
:fa-filter: [Configuration][]  

##### :fa-cubes: Related  
:fa-plus-square: :fa-download: [Add or manage a Source][]  
:fa-desktop: [Monitor Agents][]    
:fa-cogs: [Administration][]    
:fa-cloud-download: [Sources][]  


[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master

<!--References -->
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/3.%20Configuration.md?at=master

[httpHostThrottlingPerformanceCounters]:https://msdn.microsoft.com/en-us/library/aa578302.aspx  

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/ActiveInstanses/BiztalkActiveInstanses.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/ActiveInstanses/BizTalkActiveInstancesRemoteconfig.png
