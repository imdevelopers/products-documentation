
<tags data-value="Monitor,BizTalk,Suspended Instances"></tags>

:fa-heartbeat: Health Check -  Suspended Instances
=====
					
  
___

## Information
Suspended Instances

## :fa-question-circle: Whats evaluated
The following rules are evaluated for health checks:


##### :fa-check-circle-o: OK - Always
* OK - Threshold not reached

##### :fa-times-circle: Resource not available
* Error - Used to indicate an error/fatal condition, common causes are user rights

![png_SuspendedInstances_Action_Menu][]
## :fa-flash: Actions
* Details - Shows a history report

![png_SuspendedInstances_Details_Modal][]


## :fa-line-chart: Metrics
All Suspended Instances are displayed in one graph using the **Metrics** functionality of this agent.

* Metrics
  * Today - Active
  * Today - Aggregated

![png_SuspendedInstances_Metrics][]



NOTE: To use this feature it must be manually enabled by an Administrator using Remote Configuration
___

### :fa-hand-o-right: Next Step  
:fa-desktop: [Monitor Views][]  
:fa-filter: [Configuration][]  

##### :fa-cubes: Related  
:fa-plus-square: :fa-download: [Add or manage a Source][]  
:fa-desktop: [Monitor Agents][]    
:fa-cogs: [Administration][]    
:fa-cloud-download: [Sources][]  


[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master

<!--References -->
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master

[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/3.%20Configuration.md?at=master

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/HealtCheck/MonitoViewFilteredByHealthCheck.png


[png_SuspendedInstances_Action_Menu]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/HealtCheck/SuspendedInstances_Action_Menu.png
[png_SuspendedInstances_Details_Modal]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/HealtCheck/SuspendedInstances_Details_Modal.png
[png_SuspendedInstances_Metrics]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/HealtCheck/SuspendedInstances_Metrics.png



