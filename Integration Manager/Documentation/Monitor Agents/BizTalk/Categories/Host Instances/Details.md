<tags data-value="Monitor,BizTalk,Control Center"></tags>

:fa-flash: Action: **Details for Host Instance**
=============================================

					
## :fa-info: Information

You can use the Administration console of Integration Manager to view **Details** for BizTalk host instances.

Detailed information is displayed grouped by
* Host 
    * Host Instance(s) from all nodes in the BizTalk group, Cluster Aware
* Applications


![biztalkcategoryhostinstances][2]

## :fa-info: Details

In order to view the **details** for  a BizTalk Host Instance, press the **Action** button

![ActionButtonDetails][0]

Then the result of the operation will be displayed

## :fa-database: Host/Host Instances
Information about host and configured host instances can be seen in the 'Host / Host instances' tab. Host instances from all nodes are presented, also this is cluster aware.

![DetailsModal][1]

## :fa-dropbox: Applications

![ApplicationDetails][3]

## :fa-sign-in: Receive Location

![RLDetails][4]

## :fa-sign-out: Send Ports
![SPDetails][5]

    NOTE: Dynamic send ports are NOT listed!

## :fa-sitemap: Orchestrations  
![Orchestrations][7]
Two tables, one for ReceiveHandlers, and one for Send Handlers. For example, if you have a Send Host named **sndHost32**, there should be no receive handlers assosiated with this host. These tables will help you configure your BizTalk environment to reflect actual intent.

![Adapter][6]

___


### :fa-hand-o-right: Next Step
:fa-play: [Start][]  
:fa-stop: [Stop][]  
:fa-bar-chart: [Throttling Metrics][Metrics]

#### :fa-cubes: Related
:fa-folder: [BizTalk Host Instances][Host Instances]

[Host Instances]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Overview.md?at=master

[Start]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Start.md?at=master
[Stop]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Stop.md?at=master
[Metrics]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Metrics.md?at=master

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/HostInstances/ActionButtonDetails.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/HostInstances/DetailsModal.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/HostInstances/MonitorViewCategoryHostInstances.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/HostInstances/ApplicationDetails.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/HostInstances/ReceiveLocationDetails.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/HostInstances/SendPortDetails.png
[7]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/HostInstances/OrchestrationDetails.png
[6]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/HostInstances/AdapterDetails.png


