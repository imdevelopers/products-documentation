
<tags data-value="Monitor,BizTalk,Control Center"></tags>

:fa-line-chart: Action: **Throttling metrics for Host Instance**
=============================================

					
## :fa-info: Information

You can use the Administration console of Integration Manager to view **Throttling Metrics** for indivdual BizTalk host instances.

## :fa-info: Details

In order to view the **throttling metrics** for a BizTalk Host Instance, press the **Action** button

![ActionButtonMetrics][0]

Then the result of the operation will be displayed

![MetricsModal][1]

___


### :fa-hand-o-right: Next Step
:fa-play: [Start][]  
:fa-stop: [Stop][]  
:fa-info: [Details][]  

#### :fa-cubes: Related
:fa-folder: [BizTalk Host Instances][Host Instances]

[Host Instances]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Overview.md?at=master

[Start]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Start.md?at=master
[Stop]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Stop.md?at=master
[Details]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Details.md?at=master

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/HostInstances/ThrottlingChart.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/HostInstances/MetricsConfiguration.png

