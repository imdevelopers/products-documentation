<tags data-value="BizTalk,Actions,Monitor,View,Resource"></tags>

:fa-folder: BizTalk Host Instances
=====
					


___

## :fa-info: Information

The **BizTalk Monitor Agent** for Integration Manager has built in support to monitor and control **BizTalk Host instances**. New (or removed) Hosts and Host Instances are automatically managed by the agent.  All changes in your BizTalk Server environment is automatically picked up by the Monitor Agent for BizTalk. All *Host Instances* are grouped by the Category **Host Instance**:

![MonitorViewCategoryHostInstances.png][2]

## :fa-question-circle: Whats evaluated
The following rules are evaluated for a **Host Instance**:

#### :fa-check-circle-o: OK - Used to indicate normal operation
* **Enabled**
* **Tracking hosts** at least one instance configured for Host Tracking per node in the BizTalk group 
* **Throttling** below configured thresholds


#### :fa-times-circle: Resource not available - 
* Error - Used to indicate an error/fatal condition

#### :fa-times-circle: Error
* A **Stopped** host instance renders as state 'Error' (if not configured as disabled and not part of a Windows fail over cluster resource group)

#### :fa-exclamation-triangle: Warning  - Used to indicate an unexpected but not invalid state
* **Throttling** above configured thresholds (NOTE Integration Manager does not render throttling ever as an error)


The evaluated state may be reconfigured using **Expected State** functionality that exists on every [Resource][Resources] within Integration Manager.


## :fa-key: **Key features are**:
* Cluster support
    * Acknowledges clustered services

* Dedicated Tracking Host 
    * Alerts if there is not a dedicated Tracking host

* Host instances on hosts
    * Alerts if not all host instances are created

* Running host instances
    * Alerts if a host instances is stopped (and not configured as disabled)

* :fa-flash: Remote [Actions][]
    * :fa-play: [Start][]
    * :fa-stop: [Stop][]
    * :fa-info: [Details][]
    * :fa-bar-chart: [Throttling Metrics][Metrics]

* :fa-bart-chart: [Metrics][]
    * Throttling
        * Individual Host Instances
        * Grouped (all host instances with throttling behaviour)

The BizTalk Monitoring Agent supports the execution of remote actions on resources using the Integration Manager Web Client tool or by programmatically calling the [Web API][].
In order to execute remote actions some [PreRequisites][PreRequisites for BizTalk Monitor Agent] must be satisfied. 
The user must be logged on to Integration Manager using a Windows Credential. The user must also be assigned a monitor view with BizTalk [Resources][] where [Actions][] are allowed. See [Add or manage Monitor Views][] for more details.

![Actions][0]

## :fa-pie-chart: Metrics

### :fa-bar-chart: Host Instance Throttling
Individual throttling can be viewed using the Metrics graphs.

![ThrottlingChart.png][1]


## :fa-filter: Filter
The BizTalk monitor agent has support to filter unwanted resources, see [Configuration][] for details.

___

### :fa-hand-o-right: Next Step
:fa-play: [Start][]
:fa-stop: [Stop][]
:fa-info: [Details][]  
:fa-bar-chart: [Throttling Metrics][Metrics]

#### :fa-cubes: Related
:fa-edit: [Configuration][] of the agent  
[BizTalk Monitor Agent][BizTalk]

<!--References -->
[BizTalk]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Overview.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/3.%20Configuration.md?at=master
[Monitor View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/3.%20Monitor/Monitor.md?at=master
[Actions]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/1.%20View%20Monitor%20Views/2.%20View%20Single%20Monitor%20View/Actions/Actions.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[Web API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Web%20API/Overview.md?at=master
[PreRequisites for BizTalk Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/1.%20PreRequisites.md?at=master


[Start]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Start.md?at=master
[Stop]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Stop.md?at=master
[Details]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Details.md?at=master

[Metrics]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Metrics.md?at=master

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/HostInstances/Actions.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/HostInstances/ThrottlingChart.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/HostInstances/MonitorViewCategoryHostInstances.png
