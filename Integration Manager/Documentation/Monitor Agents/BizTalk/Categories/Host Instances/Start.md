<tags data-value="Monitor,BizTalk,Control Center"></tags>

:fa-flash: Action: **Start Host Instance**
=============================================
					
## :fa-info: Information
You can use the Administration console of Integration Manager to **Start** BizTalk host instances. 

A started host instance can be [Stopped][Stop]. There is no need to use the BizTalk administration MMC to perform this administrative task.

User have access to the BizTalk Host Instance from using [Monitor Views][] where Actions are allowed.

## :fa-play: Start
In order to **Start** a stopped BizTalk Host Instance, press the **Action** button on the selected row:

![ActionButtonStart][2]

The user will then be prompted with a question to continue the **Start** operation:

![ActionStart][0]

Then the result of the operation will be displayed for the user:
![ActionStarted][1]

If **Started**, the host instance will be evaluated as being in the **OK** state:
![ResourceStarted][3]

### :fa-hand-o-right: Next Step
:fa-stop: [Stop][]  
:fa-info: [Details][]  
:fa-bar-chart: [Throttling Metrics][Metrics]

#### :fa-cubes: Related
:fa-folder: [BizTalk Host Instances][Host Instances]  
:fa-desktop: :fa-eye: [Monitor Views][]    
:fa-flash: [Actions][Actions]       

[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Host Instances]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Overview.md?at=master

[Stop]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Stop.md?at=master
[Details]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Details.md?at=master
[Metrics]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Metrics.md?at=master
[Actions]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/1.%20View%20Monitor%20Views/2.%20View%20Single%20Monitor%20View/Actions/Actions.md?at=master

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/HostInstances/ActionStart.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/HostInstances/ActionStarted.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/HostInstances/ActionButtonStart.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/HostInstances/ResourceStarted.png