<tags data-value="Monitor,BizTalk,Control Center"></tags>

:fa-flash: Action: **Stop Host Instance**
=============================================

					
## :fa-info: Information

You can use the Administration console of Integration Manager to stop BizTalk host instances. A started host instance can be stopped and will prevent the instance from from routing messages and/or run Orchestration Instances.

## :fa-stop: Stop

In order to stop a BizTalk Host Instance, press the **Action** button

![ActionButtonStop][2]

The user will then be prompted to continue with the **Stop** operation.

![ActionStop][0]

Then the result of the operation will be displayed

![ActionStopped][1]

When stopped, the host instance will be evaluated as being in the error state
![ResourceStopped][3]

___

After you stop a BizTalk host instance, you can [Start][] it so that it is running and routing messages and orchestration instances. For information about starting a host instance, see [How to Start a Host Instance][Start].

### :fa-hand-o-right: Next Step
:fa-play: [Start][]  
:fa-info: [Details][]  
:fa-bar-chart: [Throttling Metrics][Metrics]

#### :fa-cubes: Related
:fa-folder: [BizTalk Host Instances][Host Instances]

[Host Instances]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Overview.md?at=master

[Start]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Start.md?at=master
[Stop]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Stop.md?at=master
[Details]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Details.md?at=master
[Metrics]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Metrics.md?at=master

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/HostInstances/ActionStop.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/HostInstances/ActionStopped.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/HostInstances/ActionButtonStop.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/HostInstances/ResourceStopped.png