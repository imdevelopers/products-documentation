<tags data-value="BizTalk,Actions,Monitor,View,Resource"></tags>

:fa-folder: BizTalk Monitor Categories
=====
					
[Done this? Next step](#next step)

___

## :fa-info: Information
The various monitoring capabilities for the BizTalk Server agent are expressed as [Resources][] and are grouped by [Categories][].
The **Categories** are unique for each [Monitoring Agent][Monitor Agents]. 

![BizTalkCategories][0]

The BizTalk Monitoring Agent also supports the execution of remote actions on most resources using the Integration Manager Web Client tool or by programmatically calling the [Web API][].
In order to execute remote actions some [PreRequisites][PreRequisites for BizTalk Monitor Agent] must be satisfied. 
The user must be logged on to Integration Manager using a Windows Credential. The user must also be assigned a [Monitor View][Monitor Views] with BizTalk [Resources][] where [Actions][] are allowed. See [Add or manage Monitor Views][] for more details.
___
## :fa-folder: Categories
The following **Categories** are exposed by the **BizTalk Monitor Agent**.

* [Host Instances][] (with Cluster support)
* [Send Ports][]
* [Send Port Groups][]
* [Receive Locations][]
* [Suspended Instances][]
    * [Resumable][]
    * [Not Resumable][NotResumable]
* [Active Instances][]
* [Health Check][]
    * Orphaned DTA Service instance(s)
	* Messages with negative ref count
	* Messages without ref count
* Scheduled Instances
* Dehydrated Orchestration Instances
* Ready to run instances
* [Pipelines][]
___

### :fa-hand-o-right: Next Step
:fa-edit: [Configuration][] of the agent  

#### :fa-cubes: Related
:fa-check-square-o: [PreRequisites for BizTalk Monitor Agent][]  
:fa-desktop: [Monitor Agents][]   

<!--References -->
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/3.%20Configuration.md?at=master

[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[BizTalk]https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Overview.md?at=master
[Actions]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/1.%20View%20Monitor%20Views/2.%20View%20Single%20Monitor%20View/Actions/Actions.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[Web API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Web%20API/Overview.md?at=master
[PreRequisites for BizTalk Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/1.%20PreRequisites.md?at=master

[Host Instances]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Overview.md?at=master
[Send Ports]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Send%20Ports/Overview.md?at=master
[Receive Locations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Receive%20Locations/Overview.md?at=master
[Suspended Instances]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Suspended%20Instances/Overview.md?at=master
[Resumable]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Suspended%20Instances/Resumable.md?at=master
[NotResumable]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Suspended%20Instances/Not%20Resumable.md?at=master

[Send Port Groups]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Send%20Port%20Groups/Overview.md?at=master
[Active Instances]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Active%20Instances/Overview.md?at=master
[Health Check]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Health%20Check/Overview.md?at=master


[Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/Categories.md?at=master
[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/BizTalkCategories.png

[Pipelines]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Pipelines/Overview.md?at=master
