
<tags data-value="Monitor,BizTalk"></tags>

:fa-navicon: Pipelines
=====
					
[Done this? Next step](#next step)  
___

## Information
The **BizTalk Monitor Agent** for Integration Manager has built in support for **Pipelines** (making sure your tracking settings remain as intended) installed on BizTalk Server.

![Pipelines][0]

## :fa-edit: Remote Configuration for Pipelines  
![Pipelines][png_RemoteConfig_Pipelines]

## :fa-question-circle: Whats evaluated
The following rules are evaluated for **Pipelines** when enabled:

* Warning when pipelines looses tracking settings Integration Manager can no longer log and archive messages since they are no longer being tracked for involved ports!!!
* Two types of warning can be set **missing tracking** or **full missing tracking** depending on the remote configuration of the pipeline

##### :fa-check-circle-o: OK - Used to indicate normal operation
* OK - Pipeline Body and Context tracking **enabled**

##### :fa-times-circle: Resource not available
* Error - Used to indicate an error/fatal condition, common causes are user rights

##### :fa-times-circle: Error
* Error - Not implemented

#### :fa-exclamation-triangle: Warning 
* Warning  - Pipeline Body and/or Context tracking **disabled**

The evaluated state may be reconfigured using **Expected State** functionality that exists on every [Resource][Resources] within Integration Manager.

## :fa-ambulance: Auto Healing
    TIP: Use the Auto Healing feature of Integration Manager to make sure Default Pipelines does not lose tracking settings when deploying/redeploying BizTalk Applications. We consider the lost setting of tracking a bug in BizTalk that may be compensated for using the Auto Healing feature of Integration Manager.

## :fa-flash: Actions
* Pipelines
  * :fa-check-square-o: Enable Tracking
  * :fa-square-o: Disable Tracking
  * :fa-check-square-o: Enable Full Tracking
  * :fa-square-o: Disable Full Tracking

![Actions][1]

## :fa-line-chart: Metrics
Not yet implemented, please send a feature request if you need this feature detailing what you would like to see.

___

### :fa-hand-o-right: Next Step  
:fa-desktop: [Monitor Views][]  
:fa-filter: [Configuration][]  

##### :fa-cubes: Related  
:fa-plus-square: :fa-download: [Add or manage a Source][]  
:fa-desktop: [Monitor Agents][]    
:fa-cogs: [Administration][]    
:fa-cloud-download: [Sources][]  


[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master

<!--References -->
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/3.%20Configuration.md?at=master

[httpHostThrottlingPerformanceCounters]:https://msdn.microsoft.com/en-us/library/aa578302.aspx  


[png_RemoteConfig_Pipelines]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/RemoteConfig/Tab_Pipelines.png
[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/Pipelines/DefaultPipelines.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/Pipelines/Actions.png
