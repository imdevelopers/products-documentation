<tags data-value="Monitor,BizTalk,Control Center"></tags>

:fa-flash: Action: **Details**
=============================================
					
## :fa-info: Information
You can use the Administration console of Integration Manager to view **Details** for BizTalk Receive Locations.

## :fa-info: Details

In order to view the **details** for a BizTalk Receive Location, press the **Action** button

![ActionButtonDetails][0]

Then the result of the operation will be displayed

![DetailsModal][1]


### :fa-hand-o-right: Next Step
:fa-play: [Enable][]  
:fa-stop: [Disable][]   
:fa-envelope-open-o: [Tracking][]

#### :fa-cubes: Related
:fa-desktop: :fa-eye: [Monitor Views][]    
:fa-flash: [Actions][Actions]       

[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Actions]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/1.%20View%20Monitor%20Views/2.%20View%20Single%20Monitor%20View/Actions/Actions.md?at=master

[Enable]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Receive%20Locations/Enable.md?at=master
[Disable]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Receive%20Locations/Disable.md?at=master
[Tracking]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Receive%20Locations/Tracking.md?at=master

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/ReceiveLocations/ActionDetails.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/ReceiveLocations/Details.png
