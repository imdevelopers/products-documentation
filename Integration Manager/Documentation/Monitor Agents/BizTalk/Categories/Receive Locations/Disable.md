<tags data-value="Monitor,BizTalk,Control Center"></tags>

:fa-flash: Action: **Disable Receive Location**
=============================================
					
## :fa-info: Information
You can use the Administration console of Integration Manager to **Disable** BizTalk **Receive Locations**. 

A **Disabled** Receive Location can be [Enabled][Enable]. There is no need to use the BizTalk administration MMC to perform this administrative task.

User have access to Receive Locations using [Monitor Views][] where Actions are allowed.

## :fa-play: Disable
In order to **Disable** a enabled BizTalk Receive Location, press the **Action** button on the selected row:

![ActionButtonDisable][2]

The user will then be prompted with a question to continue the **Disable** operation:

![ActionDisable][0]

Then the result of the operation will be displayed for the user:  
![ActionDisabled][1]

If **Disabled**, the BizTalk Receive Location will be evaluated as being in the **Error** state:
![ResourceDisabled][3]

### :fa-hand-o-right: Next Step
:fa-play: [Enable][]  
:fa-info: [Details][]  
:fa-envelope-open-o: [Tracking][]  

#### :fa-cubes: Related
:fa-desktop: :fa-eye: [Monitor Views][]    
:fa-flash: [Actions][Actions]       

[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Actions]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/1.%20View%20Monitor%20Views/2.%20View%20Single%20Monitor%20View/Actions/Actions.md?at=master

[Enable]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Receive%20Locations/Enable.md?at=master
[Details]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Details.md?at=master
[Tracking]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Receive%20Locations/Tracking.md?at=master

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/ReceiveLocations/DisableDialog.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/ReceiveLocations/DisabledDialog.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/ReceiveLocations/ActionDisable.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/ReceiveLocations/ResourceDisabled.png