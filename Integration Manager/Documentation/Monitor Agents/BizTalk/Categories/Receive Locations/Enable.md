<tags data-value="Monitor,BizTalk,Control Center"></tags>

:fa-flash: Action: **Enable Receive Location**
=============================================
					
## :fa-info: Information
You can use the Administration console of Integration Manager to **Enable** BizTalk **Receive Locations**. 

An **Enabled** Receive Location can be [Disabled][Disable]. There is no need to use the BizTalk administration MMC to perform this administrative task.

User have access to **Enable** Receive Locations using [Monitor Views][] where Actions are allowed.

## :fa-play: Enable
In order to **Enable** a disabled BizTalk Receive Location, press the **Action** button on the selected row:

![ActionButtonEnable][2]

The user will then be prompted with a question to continue the **Enable** operation:

![ActionEnable][0]

Then the result of the operation will be displayed for the user:  
![ActionEnabled][1]

If **Enabled**, the BizTalk Receive Location will be evaluated as being in the **OK** state:
![ResourceEnabled][3]

### :fa-hand-o-right: Next Step
:fa-stop: [Disable][]  
:fa-info: [Details][]   
:fa-envelope-open-o: [Tracking][]

#### :fa-cubes: Related
:fa-desktop: :fa-eye: [Monitor Views][]    
:fa-flash: [Actions][Actions]       

[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Actions]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/1.%20View%20Monitor%20Views/2.%20View%20Single%20Monitor%20View/Actions/Actions.md?at=master

[Disable]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Receive%20Locations/Disable.md?at=master
[Details]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Receive%20Locations/Details.md?at=master
[Tracking]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Receive%20Locations/Tracking.md?at=master

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/ReceiveLocations/EnableDialog.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/ReceiveLocations/EnabledDialog.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/ReceiveLocations/ActionEnable.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/ReceiveLocations/ResourceEnabled.png