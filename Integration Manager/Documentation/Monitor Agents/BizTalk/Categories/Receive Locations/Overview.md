<tags data-value="Monitor,BizTalk"></tags>

Receive Locations
=====
					


___

## Information
Receive Locations in BizTalk are listed as [resources][Resources] in [Monitor Views][]. All changes in your BizTalk environment is automatically picked up by the [Monitor Agent][Monitor Agents] for BizTalk.
All *Receive Locations* are grouped by the Category **Receive Location**:  

![filter][png_filter]  

## :fa-flash: Actions
The following actions are implemented on this *Category*:

* **[Enable][]** - Start receiving messages
* **[Disable][]** - Stop receiving messages
* **[Tracking][]** - View and manage Tracking Settings
* **[Details][]** - Displays information about the receive location

## :fa-question-circle: Whats evaluated
The following rules are evaluated for a **Receive Location**:

#### :fa-check-circle-o: OK - Used to indicate normal operation
* **Enabled** 

#### :fa-times-circle: Resource not available - 
* Error - Used to indicate an error/fatal condition

#### :fa-times-circle: Error
* A **disabled** receive location renders as state 'Error'

#### :fa-exclamation-triangle: Warning  - Used to indicate an unexpected but not invalid state
* Not implemented

The evaluated state may be reconfigured using **Expected State** functionality that exists on every [Resource][Resources] within Integration Manager.

## :fa-filter: Filter
The BizTalk monitor agent has support to filter unwanted resources, see [Configuration][] for details.

## :fa-exclamation: Errors
The following problems and solutions may exist depending on your environment and configuration.

### Schedule Task Adapter
Issues may exist with the **BizTalk Schedule Task Adapter**. Read more [here](https://biztalkscheduledtask.codeplex.com/)

#### Problem
The Actions *Enable* and *Disable* for ports using the **Schedule Task Adapter** requires the `Microsoft.BizTalk.Scheduler.dll` to be loaded from the GAC. If you get an error like this executing the Action Enable/Disable, check that the files is really in the GAC on the server running the BizTalk Monitor Agent.

![Error on Action Start or Stop][png_ActionError]

#### Solution 1 using **Developer Comand Prompt for Visual Studio**:
Check if it is installed using Developer Comand Prompt for Visual Studio
```
gacutil /l Microsoft.BizTalk.Scheduler
```

If missing add the solution is to the assembly into the GAC, enter the proper path for the version of BizTalk

```
gacutil /i "C:\Program Files (x86)\Microsoft BizTalk Server 2013 R2\Microsoft.BizTalk.Scheduler.dll"
```
![Add the dll to the GAC][png_DeveloperCommandPrompt]

### Solution 2 using **PowerShell**

*Installation*
1. Run the PowerShell console as Administrator (elevated mode)
2. Enter the following PowerShell, enter the proper path according to installed path of your BizTalk Server

```
[System.Reflection.Assembly]::Load("System.EnterpriseServices, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")
$publish = New-Object System.EnterpriseServices.Internal.Publish
$publish.GacInstall("C:\Program Files (x86)\Microsoft BizTalk Server 2013 R2\Microsoft.BizTalk.Scheduler.dll")
```
![Add the dll to the GAC][png_PowerShell]
<!--References -->
___

### :fa-hand-o-right: Next Step  
:fa-play: [Enable][]  
:fa-stop: [Disable][]  
:fa-envelope-open-o: [Tracking][]  
:fa-info: [Details][]  
:fa-desktop: [Monitor Views][]   
:fa-filter: [Configuration][]   

##### :fa-cubes: Related  
[Send Ports][]  
:fa-plus-square: :fa-download: [Add or manage a Source][]  
:fa-desktop: [Monitor Agents][]    
:fa-cogs: [Administration][]    
:fa-cloud-download: [Sources][]  


[png_ActionError]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Scheduler_ActionError.png
[png_DeveloperCommandPrompt]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Scheduler_DeveloperCommandPrompt.png
[png_PowerShell]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Scheduler_PowerShell.png

[png_filter]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/MonitorViewFilterCategoryReceiveLocation.png

[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master



<!--References -->
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/3.%20Configuration.md?at=master
[Send Ports]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Send%20Ports/Overview.md?at=master
[Enable]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Receive%20Locations/Enable.md?at=master
[Disable]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Receive%20Locations/Disable.md?at=master
[Details]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Receive%20Locations/Details.md?at=master
[Tracking]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Receive%20Locations/Tracking.md?at=master