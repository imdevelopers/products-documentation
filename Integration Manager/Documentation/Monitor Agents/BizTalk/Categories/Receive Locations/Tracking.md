<tags data-value="Monitor,BizTalk,Control Center"></tags>

:fa-flash: Action: **Tracking**
=============================================

					
## :fa-info: Information

You can use the Administration console of Integration Manager to view and manage **Tracking** for BizTalk Receive locations and Send Ports.

## :fa-info: Details

In order to view and manage the **Tracking** settings for a BizTalk ports, press the **Action** button

![ActionButtonTracking][0]

Then the result of the operation will be displayed

![TrackingModal][1]

## :fa-save: Save  
Click on the 'Save' button to persist changes for the BizTalk Environment.
___
### :fa-hand-o-right: Next Step
:fa-play: [Start][]  
:fa-stop: [Stop][]  
:fa-info: [Details][]  

#### :fa-cubes: Related
Tracking for Send Ports 


[Start]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Start.md?at=master
[Stop]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Stop.md?at=master
[Details]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Receive%20Locations/Details.md?at=master

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/ReceiveLocations/ActionTracking.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/ReceiveLocations/Tracking.png