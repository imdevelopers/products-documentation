<tags data-value="Monitor,BizTalk,Control Center"></tags>

:fa-flash: Action: **Enlist Send Port**
=============================================
					
## :fa-info: Information
You can use the Administration console of Integration Manager to **Enlist** BizTalk **Send Ports**. 

An **Enlisted** Send Port can be [Unenlisted][Unenlist], [Stopped][Stop] or [Started][Start]. There is no need to use the BizTalk administration MMC to perform this administrative task.

User have access to **Enlist** Send Ports using [Monitor Views][] where Actions are allowed.

## :fa-play: Enlist
In order to **Enlist** a BizTalk Send Port, press the **Action** button on the selected row:

![ActionButtonEnlist][2]

The user will then be prompted with a question to continue the **Enlist** operation:

![ActionEnlist][0]

Then the result of the operation will be displayed for the user:  
![ActionEnlistd][1]

If **Enlisted**, the BizTalk Send Port will be evaluated as being in the **Warning** state:
![ResourceEnlistd][3]

### :fa-hand-o-right: Next Step
:fa-stop: [Disable][]  
:fa-info: [Details][]   
:fa-envelope-open-o: [Tracking][]

#### :fa-cubes: Related
:fa-desktop: :fa-eye: [Monitor Views][]    
:fa-flash: [Actions][Actions]       

[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Actions]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/1.%20View%20Monitor%20Views/2.%20View%20Single%20Monitor%20View/Actions/Actions.md?at=master

[Disable]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Receive%20Locations/Disable.md?at=master
[Details]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Receive%20Locations/Details.md?at=master
[Tracking]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Receive%20Locations/Tracking.md?at=master

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/ReceiveLocations/EnlistDialog.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/ReceiveLocations/EnlistdDialog.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/ReceiveLocations/ActionEnlist.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/ReceiveLocations/ResourceEnlistd.png