<tags data-value="Monitor,BizTalk, Send Ports"></tags>

Send Ports
=====
					


___

## Information
Send Ports in BizTalk are listed as [resources][Resources] in [Monitor Views][]. All changes in your BizTalk environment is automatically picked up by the [Monitor Agent][Monitor Agents] for BizTalk.
All *Send Ports* are grouped by the Category **Send Ports**:  

![filter][png_filter]  

## :fa-flash: Actions
The following actions are implemented on this *Category*:

* **Enable** - Start receiving messages
* **Disable** - Stop receiving messages
* **Tracking** - Change Tracking Settings
* **Details** - Displays information about the **Send Port**

## :fa-question-circle: Whats evaluated
The following rules are evaluated for a **Receive Location**:

#### :fa-check-circle-o: OK - Used to indicate normal operation
* **Enabled** 

#### :fa-times-circle: Resource not available - 
* Error - Used to indicate an error/fatal condition

#### :fa-times-circle: Error
* A **disabled** receive location renders as state 'Error'

#### :fa-exclamation-triangle: Warning  - Used to indicate an unexpected but not invalid state
* Not implemented

The evaluated state may be reconfigured using **Expected State** functionality that exists on every [Resource][Resources] within Integration Manager.

## :fa-filter: Filter
The BizTalk monitor agent has support to filter unwanted resources, see [Configuration][] for details.


___

### :fa-hand-o-right: Next Step  
:fa-desktop: [Monitor Views][]  
:fa-filter: [Configuration][]  

##### :fa-cubes: Related  
[Send Ports][]  
:fa-plus-square: :fa-download: [Add or manage a Source][]  
:fa-desktop: [Monitor Agents][]    
:fa-cogs: [Administration][]    
:fa-cloud-download: [Sources][]  


[png_filter]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/SendPorts/filterbycategory.png

[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master


<!--References -->
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/3.%20Configuration.md?at=master
[Send Ports]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Send%20Ports/Overview.md?at=master