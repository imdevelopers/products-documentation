<tags data-value="Monitor,Views,Live,Health,Graph"></tags>

Suspended instances
=====
					


___

## :fa-info: Information

Suspended instances consist of two types **resumable** and **not resumable**.  
The different types are shown as two different Categories in the [Monitor View][]
- Suspended instance(s) (resumable)
- Suspended instance(s) (not resumable)

As the name say not resumable can not be resumed and have no resume action.  

Manage suspended instances allow you to see the instances in a view and do actions with them. Support actions like terminate, resume, download. View the context for each instance.


___

## Instructions



![Actions][png_Actions]



<!--References -->

[png_Actions]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/SuspendedInstances/Actions.png




[Monitor]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/Monitor.md?at=master
[Monitor View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/1.%20View%20Monitor%20Views/2.%20View%20Single%20Monitor%20View/View%20Single%20Monitor%20View.md?at=master

___

### Next step
#### Related
