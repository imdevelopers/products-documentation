<tags data-value="Monitor,Views,Live,Health,Graph"></tags>

Resumable - suspended instances
=====
					


___

## :fa-info: Information

[View Single Monitor View][]

## Instructions

### Control center and Metrics menu
![Actions][png_Actions]

### Manage Suspended Instances
![Manage Suspended Instances][png_Manage]

### Suspended Instances history
![Suspended Instances history][png_History]

### Metrics
![Metrics][png_Metrics]






<!--References -->

[png_Actions]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/SuspendedInstances/Resumable/Actions.png
[png_Manage]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/SuspendedInstances/Resumable/Manage.png
[png_History]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/SuspendedInstances/Resumable/History.png
[png_Metrics]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/SuspendedInstances/Resumable/Metrics.png

[View Single Monitor View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/1.%20View%20Monitor%20Views/2.%20View%20Single%20Monitor%20View/View%20Single%20Monitor%20View.md?at=master

___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  