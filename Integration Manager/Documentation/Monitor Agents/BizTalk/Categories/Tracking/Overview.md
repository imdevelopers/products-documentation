
<tags data-value="Monitor,BizTalk, Tracking"></tags>

## :fa-archive: Tracking
=====
					
[Done this? Next step](#next step)  
___

## Information
The **BizTalk Monitor Agent** for Integration Manager has built in support for remotely managing Tracking Settings for multiple ports and Orchestrations with just a few clicks. This feature gives you both an overview as well as easier administration that saves your time (and thereby money).

The following Resources for Category **Tracking** are available:
* :fa-sitemap: **Orchestrations**
* Receive Ports
* Receive Ports Request/Response
* Send Ports
* Send Ports Solicit/Response

![TrackingResources][0]

With these features there is no need to logon to the BizTalk Server or using the Management Console (also requiring membership in either the BizTalk Administrators group and/or BizTalk Server Operators). E.g. A non BizTalk Administrator may change these settings using the [Web Client][].


## :fa-flash: Actions
All [Resources][] of Category **Tracking** has the following Action:
* **List**

![Actions][1]

## :fa-flash: Additional Actions within modals
* :fa-refresh: Reload
* :fa-save: Save to BizTalk
* Show Applications

### :fa-refresh: Reload
Click the **Reload** button to refresh settings from BizTalk. 
![Reload][3]

    NOTE: Pending Changes will be lost

### :fa-save: Save to BizTalk
Save pending changes to BizTalk and reloads the modal.  
![SaveToBizTalk][4]

### Show Applications
Group ports and orchestrations by BizTalk Application or by name.  
![ShowApplications][5]

<table>
<tr>
<th>Example 1: Send Ports grouped by BizTalk Application</th>
<th>Example 2: Send Ports grouped by name</th>
</tr>
<tr>
<td><img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/Tracking/SendPorts.png"/></td>
<td><img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/Tracking/SendPortsByName.png"/></td>
</tr>
<table>

## :fa-sitemap: **Orchestrations**  
![Actions][2]  
*Example image for **Orchestrations***

## **Receive Ports**
![ReceivePorts][6]  
*Example image for **Receive Ports***

## **Send Ports**
![SendPorts][7]  
*Example image for **Send Ports***

## **Receive Ports Request/Response**

## **Send Ports Solicit/Response**

## :fa-line-chart: Metrics
Not yet implemented, please send a feature request if you need this feature detailing what you would like to see.

___

### :fa-hand-o-right: Next Step  
:fa-desktop: [Monitor Views][]  
:fa-filter: [Configuration][]  

##### :fa-cubes: Related  
:fa-connectdevelop: [BizTalk Agent][]  
:fa-plus-square: :fa-download: [Add or manage a Source][]  
:fa-desktop: [Monitor Agents][]    
:fa-cogs: [Administration][]    
:fa-cloud-download: [Sources][]  

[BizTalk Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Overview.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master

<!--References -->
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/3.%20Configuration.md?at=master

[httpHostThrottlingPerformanceCounters]:https://msdn.microsoft.com/en-us/library/aa578302.aspx  


[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/Tracking/TrackingResourcesList.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/Tracking/ActionList.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/Tracking/OrchestrationsList.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/Tracking/ReloadButton.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/Tracking/SaveToBizTalkButton.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/Tracking/ShowApplicationsCheckBox.png
[6]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/Tracking/ReceivePorts.png
[7]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/Tracking/SendPorts.png


