<tags data-value="Monitor,BizTalk,Control Center"></tags>

:fa-connectdevelop: Microsoft BizTalk Server Monitor Agent for Integration Manager
=============================================

Monitor your [Microsoft BizTalk Server][Microsoft BizTalk Server] environments with the **BizTalk Server Agent** for Integration Manager and get alerts when there is a problem. Solve problem(s) secure and remotely using remote actions.

## :fa-info: About
The monitoring agent for Microsoft BizTalk Server has many features to help you detect and also solve common problem's within your BizTalk Server environment(s).

:fa-hdd-o: Logging and Archiving is uses BizTalk built in Tracking and is goverened by the [Logging Service][Logging Service].

:fa-sitemap: Documentation support is provided by the [Repository][] model. 

## :fa-sliders: Monitor Capabilities
All BizTalk Server related artifacts are grouped by the following list of [BizTalk Categories][Categories] and can be monitored and managed using [Monitor Views][].

* [Host instances][] (with cluster support)
	* Tracking host configuration and state (make sure at least one per server)
	* Throttling metrics
	* Details 
* [Send Ports][]
* [Receive Locations][]
* Orchestrations
* Send Port Groups
* Suspended instance(s) (resumable)
	* Metrics
* Suspended instance(s) (not resumable)
* Scheduled instance(s)
* Dehydrated orchestration instance(s)
* Active instance(s)
* Ready to run instance(s)
* Health Check
	* Orphaned DTA Service instance(s)
	* Messages with negative ref count
	* Messages without ref count
	* Throttling 
	* Default Pipelines Tracking 
* Tracking on [Pipelines][]
	
## :fa-flash: Actions
Fix your BizTalk related problems with ease from a distance without the MMC. 

Using the [Web Client][] for Integration Manager, Actions can be sent to the monitor agent for BizTalk Server requesting operations to be performed on the monitored resources. With the existing privilege model you can allow certain users to perform operation on selected resources.

Following is a list of **Actions** on BizTalk related [Resources][] that can be remotely executed from [Monitor Views][]:

### Host Instances
The following operations are supported on BizTalk [Host Instances][]  
   * :fa-play: [Start][]  
   * :fa-stop: [Stop][]  
   * :fa-info: [Details][]  

### Receive Locations / Receive Ports
The following operations are supported on BizTalk [Receive Locations][]  
   * :fa-play: [Enable][RLEnable]
   * :fa-stop: [Disable][RLDisable]  
   * :fa-info: [Details][RLDetails]
   * :fa-envelope-open-o: Enable/Disable [Tracking][RLTracking]

### Send Ports
* [Send Ports][] 
* [Pipelines][]  
* Start/Stop/Enlist/Unenlist/Details for Send Ports
* Start/Stop/Enlist/Unenlist/Details for Orchestrations
* Resume all suspended instances per application
* Terminate all suspended instances per application (Resumable)
* Terminate all suspended instances per application (Not Resumable)
* Manage suspended specific messages from paged list
	* View Context (if context is allowed to be displayed from global setting)
	* View body  (first 1000 bytes)  (if body is allowed to be displayed from global setting)
	* View service instance details
	* Download full payload (if body is allowed to be displayed from global setting)
	* Resume
	* Terminate
* Remove 
 * Orphaned DTA Service instance(s)
 * Messages with negative ref count
 * Messages without ref count

## :fa-arrows-alt: Supported Versions
* All Versions ranging **2006 - 2016** are fully supported.

* All Editions are also supported and customer use/configuration (Cores/nodes/...) does not have any impact on the licence model / costs.
	* BizTalk Server Standard Edition 
	* BizTalk Server Enterprise Edition
	* BizTalk Server Evaluation Edition
	* BizTalk Server Developer Edition

See :fa-check-square-o: [PreRequisites for BizTalk Monitor Agent][] for additional details installing and running the agent.

## Miscellaneous
Integration Manager’s agent for [Microsoft BizTalk Server][Microsoft BizTalk Server] enables you to control and monitor Microsoft BizTalk Server resources. The monitoring agent must be installed with **1 instance per BizTalk Group**. The agent should not be clustered due to the use of local configuration files. The agent can be clustered if the service is installed on a shared disc, managed by the Windows fail over cluster.

In order to monitor a BizTalk group the agent must be installed on a server with BizTalk Administration Console and that has local network access to BizTalk's SQL database (management and messagebox). The BizTalk administration console version must be the same or higher compared to the BizTalk group being controlled and monitored. 

## :fa-list-alt: Release Log
For detailed information about the features and bug fixes, please see the [Release Log](http://support.integrationsoftware.se/support/discussions/forums/1000229123)
___

### :fa-hand-o-right: Next Step
:fa-rocket: [Install BizTalk Monitor Agent][]  
:fa-edit: [Configuration][] of the agent  

#### :fa-cubes: Related
:fa-check-square-o: [PreRequisites for BizTalk Monitor Agent][]  
:fa-desktop: [Monitor Views][]    


[Install BizTalk Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/2.%20Install.md?at=master  
[PreRequisites for BizTalk Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/1.%20PreRequisites.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/3.%20Configuration.md?at=master
[Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Overview.md?at=master
         
[Microsoft BizTalk Server]:https://www.microsoft.com/en-us/cloud-platform/biztalk
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master

[Host Instances]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Overview.md?at=master
[Send Ports]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Send%20Ports/Overview.md?at=master
[Start]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Start.md?at=master
[Stop]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Stop.md?at=master
[Details]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Host%20Instances/Details.md?at=master
[Pipelines]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Pipelines/Overview.md?at=master

[Receive Locations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Receive%20Locations/Overview.md?at=master
[RLEnable]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Receive%20Locations/Enable.md?at=master
[RLDisable]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Receive%20Locations/Disable.md?at=master
[RLDetails]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Receive%20Locations/Details.md?at=master
[RLTracking]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Categories/Receive%20Locations/Tracking.md?at=master
