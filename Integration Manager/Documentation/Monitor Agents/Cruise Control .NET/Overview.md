<tags data-value="Monitor,Cruise Control .NET,Build Server"></tags>

Cruise Control .NET Agent
=============================================

Monitor Cruise Control .NET with Integration Manager's monitor agent for Cruise Control .NET.
					
## About
When monitoring applications/integrations it is not only important to monitor each and every component when they are running – it is also important to monitor the development stages, that means monitoring the build processes is important.

This agent gives you the insight into your build server when using Cruise Control .Net, letting you know when builds fail and giving you the error message indicating why it failed.

* Verify build state
* Get insights to the number of projects being built and tested


## Monitor Capabilities
List of resources that can be monitored by using this agent

* Build state
* Test execution status

## Actions
No actions have yet been implemented.