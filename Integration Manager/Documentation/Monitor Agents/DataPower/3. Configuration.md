<tags data-value="Monitor,DataPower"></tags>

:fa-edit: Remote Configuration of IM Monitor Agent for DataPower
=====
					


___

## :fa-info: Information
In this section you will learn how to configure the **IM DataPower Monitor Agent**.  
Remote Configuration is not available if the [Source][] is not authenticated or is using [Microsoft Service Bus Relaying][]. 

The following steps must first be performed and is required for the usage of the [Remote Configuration][] of the agent:
1. Establish communication between the [Monitoring Service][] and the [Monitor Agent][]  
using:  
1.1 TCP/IP (HTTP)  
1.2 Service Bus Relaying
 
2. Configure the Source 
In order to use this agent from Integration Manager a source must exist that points to this agent.  
2.1. A [Source][Sources] must be defined within the [Web Client][]  
**IM: http://SERVER/IM/WebClient/Admin/Sources  
YouTube: https://www.youtube.com/watch?v=-ky8QqyQAHE**  
2.2 Name the source (mandatory)  
2.3 Give the source a short description (optional)    
2.4 Enter the Service URI using the information from 1.1 or 1.2 (mandatory)  
    NOTE: This URI might need to be changed to localhost/proper DNS/Alias name depending on infrastructure
2.5 Enter a polling interval for this source. A short poll interval might affect performance, tune this setting according to your needs and resources.    
2.6 Enter the Authentication Key using the information from file **SourceInformation.txt** -- Authentication Key: **THEKEY**     
2.7 **Save**

3. Use Remote Configuration to configure the agent

4. Create [Monitor Views][] that uses the [Source][Sources] or selected [Resources][] according to business demands.

## :fa-chain: Communication Options

### 1.1 TCP/IP (HTTP)

### 1.2 Service Bus Relaying
You must enable **Service Bus Relaying** if the Agent is running off site or in the cloud without the ability to communicate directly using TCP/IP.
  
## 2. Add a Source 
After successful [Installation][Install] you must add a [Source][Sources] in the [Web Client][] to access the [Remote Configuration][] for the installed agent.

## :fa-edit: Remote Configuration of DataPower Servers

The [Remote Configuration][] for the IM DataPower Monitor Agent provides the ability to configure the agent remotely, even if the agent is installed off site.
Many specific configurations can also be made directly on the [Resource][Resources] using the **Action** button if the [Monitor View][Monitor Views] allows for Actions.  
![png_RemoteConfig][]

:fa-folder: Protocols
The following protocols are supported by the agent:
* SNMP

## :fa-wrench: Settings
There are some general settings for the agent available in the **Settings** tab.
* **Environment**
* **Debug**
* **Culture Information**

![png_tab_settings][]

#### :fa-globe: Environment
Part of the common features shared with all [Monitor Agents][Monitor Agent], there is an option to set the name of the target **Environment**, for example TEST, QA, PROD.  

#### :fa-bug: Debug
Part of the common features shared with all [Monitor Agents][Monitor Agent], there is an option to set the **Debug** flag for additional file logging that can be enabled/disabled as needed. Default is unchecked.

#### :fa-clock-o: Culture Information
The **Culture Information** setting determines how the [Web Client][] displays the date and time for file [Resources][] in [Monitor Views][].


## About SNMP
To test the settings you could use an external tool like [SnmpB](https://sourceforge.net/projects/snmpb/)
You find the SNMP V3 EngineId in DataPower

NOTICE that you should remove the 0x and only use like 8000395d030242ac110002 when enter the settings in remote config.  
![png_DataPower_SNMPV3_EngineId][]
 
## :fa-folder: Datapower
Any number of Datapower instances can be monitored, add by clicking the **Add** button in the 'Data Power SNMP' tab.  
![TabDatapower][]

Expand the Accordion to edit content.

The following tabs exist:  
![png_DataPowerContentTabs][]

* Connection
* Disk
* CPU
* Memory
* Services
* Notification Rules

### :fa-edit: Connection
The **Connection** tab lets you configure SNMP related settings needed to fetch SNMP and SNMP Trap events from your Datapower instance.
![png_ConnectionTab][]

* **Display Name**
* **SNMP Trap Port**
* **Server**
* **Port**
* **SNMP Version**
* **Security Name**
* **Security Level**
* **Context Name**
* **Context Engine ID**

**Display Name**
The user friendly name for [Resource][].

**SNMP Trap Port**
Local port on site for the monitor agent to receive SNMP traps on, default port is **162**.

**Server**
The IP adress of your datapower instance to monitor

**Port**
Port to use when communicating directly with datapower instance, default **161**

**SNMP Version**  
Select version to use when agent connects to your Datapower instance. Default **V3**
![png_SNMPVersion][]

**Security Name**

**Security Level**  
![png_SecurityLevel][]

**Context Name**

**Context Engine ID**

## :fa-hdd-o: Disk


## Save
You must click **Save** or **Save and close** for changes to be written to the agent and take effect for next synchronization.   
![png_SaveAndClose][]

    Note: Depending on the synchronization interval set for the agent, there might be a delay before the Web Client reflects upon the change. You can choose to force the agent to synchronize from the configuration of the [Source][Sources]. 

**Save and close**, saves and closes the dialog.

**Cancel** Close the dialog without saving any changes.    
 ___


### :fa-hand-o-right: Next Step
:fa-edit: [DataPower][]   
:fa-desktop: [Monitor Views][]   

#### :fa-cubes: Related
:fa-rocket: [Install DataPower Monitor Agent][Install]    
:fa-cloud-download: [Source][Sources]



[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Install]:2.%20Install.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Remote%20Configuration/Remote%20Configuration.md?at=master
[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master

[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[View All Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/1.%20View%20All%20Monitor%20Views/View%20All%20Monitor%20Views.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master

[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master  
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Applications]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/Applications.md?at=master
[Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/Categories.md?at=master


[Paths]:Categories/Paths/Overview.md?at=master
[FTP]:Categories/FTP/Overview.md?at=master
[SFTP]:Categories/SFTP/Overview.md?at=master


[png_RemoteConfig]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/DataPower/RemoteConfig/RemoteConfig.png
[png_ApplicationList]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/FileFolder/ApplicationList.png
[png_ConfigureApplications]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/FileFolder/ConfigureApplications.png
[png_SaveAndClose]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/SaveAndClose.png
[png_DataPower_SNMPV3_EngineId]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/DataPower/RemoteConfig/datapowersnmpv3engineid.png
[png_tab_settings]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/DataPower/RemoteConfig/tab_settings.png
[png_tab_datapower]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/DataPower/RemoteConfig/tab_datapower.png
[png_DataPowerContentTabs]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/DataPower/RemoteConfig/DataPowerContentTabs.png
[png_Save]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/DataPower/RemoteConfig/SaveAndClose.png
[png_ConnectionTab]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/DataPower/RemoteConfig/ConnectionTab.png
[png_SNMPVersion]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/DataPower/RemoteConfig/SNMPVersion.png
[png_SecurityLevel]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/DataPower/RemoteConfig/SecurityLevel.png

[Disk]:./Categories/Disk.md?at=master
