<tags data-value="Monitor,DataPower,IBM DataPower Gateway,Queue"></tags>

:fa-reorder: IBM DataPower Gateway Agent
=============================================


Monitor [IBM DataPower Gateway](https://www-03.ibm.com/software/products/sv/datapower-gateway) with Integration Manager's **DataPower Monitor agent**.
					
## :fa-info: About
This agent allows you to monitor IBM DataPower Gateway related resources like cpu, memory, disk, services and notification rules. For example, the **DataPower Monitor agent** can evaluate the CPU load over time, also disk space and memory by setting up tresholdss. If services are running as expected.
Notification rules are for exampel any user failed to logon 5 times last 2 min than a alarm is raised (warning or error) or any other event push out on the SNMP trap.


## :fa-sliders: Monitor Capabilities
List of resources that can be monitored by using this agent

* [Disk][] - Fixed or procent tresholds with warning and error treshold
    * Encrypted space
    * Temporary space
    * Internal space

* CPU
    * Load Warning Threshold %
    * Load Error Threshold %

* **Memory** with warning and error based on your tresholds
    * Fixed
    * Procent

* **Services**
    * Stopped services
    * Support for specific settings if a service should be stopped.

* **Notification Rules**
    * Just about anything, customize as needed, for example
        * Certificate(s) expired
        * To many failed logins

## :fa-flash: Actions
* Notification Rules
    * List Notifications

## :fa-usd: Pricing
This agent and all other Monitor agents are part of the license for Integration Manager. No additional costs exists for the use of the non-event Monitor Agent for Integration Manager licencees. 

## :fa-list-alt: Release Log
For detailed information about the features and bug fixes, please see the [Release Log](https://support.integrationsoftware.se/support/discussions/forums/1000229375)
___

### :fa-hand-o-right: Next Step
:fa-rocket: [Install DataPower Monitor Agent][]  
:fa-edit: [Configuration][] of the agent  

#### :fa-cubes: Related
:fa-check-square-o: [PreRequisites for DataPower Monitor Agent][]  
:fa-desktop: [Monitor Views][]    

[PreRequisites for DataPower Monitor Agent]:1.%20PreRequisites.md?at=master
[Install DataPower Monitor Agent]:2.%20Install.md?at=master  
[Configuration]:3.%20Configuration.md?at=master
[Categories]:Categories/Overview.md?at=master


[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Disk]:./Categories/Disk.md?at=master
