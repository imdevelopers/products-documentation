
<tags data-value="Monitor,Dynamics CRM,CRM,Control Center"></tags>

:fa-clock-o: Logon History
=============================================

## :fa-info: Information
View logon statistics with ease. This table can easily be included in the Dashboard by an administrator.  
![logonhistory][0]

## :fa-flash: Actions
The following Actions are implemented within the **Logon History** category.
* 


![actions][1]

___

### :fa-hand-o-right: Next Step
:fa-rocket: [Install][]  
:fa-edit: [Configuration][] of the agent  

#### :fa-cubes: Related
:fa-check-square-o: [PreRequisites for Dynamics CRM Online Monitor Agent][PreRequisites]  

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/DynamicsCRMOnline/UserLoginHistory.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/DynamicsCRMOnline/UserAccessActions.png
