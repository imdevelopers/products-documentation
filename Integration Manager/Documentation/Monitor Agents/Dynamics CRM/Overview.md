<tags data-value="Monitor,Dynamics CRM,CRM,Control Center"></tags>

:fa-users: Microsoft Dynamics CRM Online Agent
=============================================

Monitor your [Microsoft Dynamics CRM Online](https://www.microsoft.com/en-us/dynamics/crm.aspx) environments with the **Dynamics CRM Online Agent** for Integration Manager and get alerts when there is a problem. Solve problem(s) secure and remotely using remote actions.

## :fa-info: About
The monitoring agent for Microsoft Dynamics CRM Online has many features to help you detect and solve problem's within your Dynamics CRM Online environment.
The Licence inventory and monitoring Capabilities may even save some money for your organization.

## :fa-sliders: Monitor Capabilities
List of **Microsoft Dynamics CRM Online** resources that can be monitored by using this agent

* Users currently logged on to CRM
	* With Action “View history” (note: the history is limited by dynamics CRM online settings) 
* License consumption with alerting option
* Running/Failed workflows with alerting option
* Running/Failed Processing Steps with alerting option

## :fa-flash: Actions
Fix your Microsoft Dynamics CRM Online related problems with ease from a distance without the Azure Portal / Dynamics CRM Online site. 

Using the [Web Client][Web Client] for Integration Manager, Actions can be sent to the monitor agent for Microsoft Dynamics CRM Online Monitoring Agent requesting operations to be performed on the monitored resources. With the existing privilege model you can allow certain users to perform operation on selected resources.

List of actions that can be executed from remote locations using this agent:

* [View User logon history][]
* View detailed information about license Usage
* View detailed information about space/disk Usage
* View details for selected workflow
* Change alert options for workflows (duration)
	
## :fa-arrows-alt: Supported Versions
* Dynamics CRM Online

## Miscellaneous
In order to monitor a Dynamics CRM Online agent must be installed on a Windows server, see prerequisites for more information.

## :fa-list-alt: Release Log
For detailed information about the features and bug fixes, please see the [Release Log](http://support.integrationsoftware.se/support/discussions/forums/1000229207)
___

### :fa-hand-o-right: Next Step
:fa-rocket: [Install][]  
:fa-edit: [Configuration][] of the agent  

#### :fa-cubes: Related
:fa-check-square-o: [PreRequisites for Dynamics CRM Online Monitor Agent][PreRequisites]  
         
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Install]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Dynamics%20CRM/2.%20Install.md?at=master  
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Dynamics%20CRM/3.%20Configuration.md?at=master
[PreRequisites]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Dynamics%20CRM/1.%20PreRequisites.md?at=master
[LogonHistory]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Dynamics%20CRM/Categoriess/Logon%20History.md?at=master
