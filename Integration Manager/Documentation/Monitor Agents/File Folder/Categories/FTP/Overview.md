<tags data-value="Configuration, Source, File"></tags>

:fa-edit: FTP, FTPS
=====
					


___
## :fa-info: Information
The agent can monitor the age of files on one or more folders. Various filters can be applied to include and/or exclude files and folders. 
Each path will be represented as unique [Resource][Resources] with display name from the settings.


## FTP Connection Mode
For FTP connections the type of connection mode can be selected. The default is **"AutoPassive"**

* **AutoPassive** - This type of data connection attempts to use the EPSV command
        and if the server does not support EPSV it falls back to the
        PASV command before giving up unless you are connected via IPv6
        in which case the PASV command is not supported.
        
* **PASV** - Passive data connection. EPSV is a better
        option if it's supported. Passive connections
        connect to the IP address dicated by the server
        which may or may not be accessible by the client
        for example a server behind a NAT device may
        give an IP address on its local network that
        is inaccessible to the client. Please note that IPv6
        does not support this type data connection. If you
        ask for PASV and are connected via IPv6 EPSV will
        automatically be used in its place.
        
* **PASVEX** - Same as PASV except the host supplied by the server is ignored
        and the data conncetion is made to the same address that the control
        connection is connected to. This is useful in scenarios where the
        server supplies a private/non-routable network address in the
        PASV response. It's functionally identical to EPSV except some
        servers may not implement the EPSV command. Please note that IPv6
        does not support this type data connection. If you
        ask for PASV and are connected via IPv6 EPSV will
        automatically be used in its place.
        
        
* **EPSV** - Extended passive data connection, recommended. Works
        the same as a PASV connection except the server
        does not dictate an IP address to connect to, instead
        the passive connection goes to the same address used
        in the control connection. This type of data connection
        supports IPv4 and IPv6.
        
* **AutoActive** - This type of data connection attempts to use the EPRT command
        and if the server does not support EPRT it falls back to the
        PORT command before giving up unless you are connected via IPv6
        in which case the PORT command is not supported.
        
* **PORT** - Active data connection, not recommended unless
         you have a specific reason for using this type.
         Creates a listening socket on the client which
         requires firewall exceptions on the client system
         as well as client network when connecting to a
         server outside of the client's network. In addition
         the IP address of the interface used to connect to the
         server is the address the server is told to connect to
         which, if behind a NAT device, may be inaccessible to
         the server. This type of data connection is not supported
         by IPv6. If you specify PORT and are connected via IPv6
         EPRT will automatically be used instead.

* **EPRT** - Extended active data connection, not recommended
        unless you have a specific reason for using this
        type. Creates a listening socket on the client
        which requires firewall exceptions on the client
        as well as client network when connecting to a 
        server outside of the client's network. The server
        connects to the IP address it sees the client comming
        from. This type of data connection supports IPv4 and IPv6.
        


## Save
You must click **Save** for changes to be written to the agent and take effect.   
![png_SaveAndClose][]

    Note: Depending on the synchronization interval set for the agent, there might be a delay before the Web Client reflects upon the change. You can choose to force the agent to synchronize from the configuration of the Source. 

**Save and close**, saves and closes the dialog.
 
<!--References -->
[png_SaveAndClose]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/SaveAndClose.png
___


:fa-edit: Paths 
=====
					

___


### :fa-hand-o-right: Next Step  
:fa-desktop: [Monitor Views][] 

##### :fa-cubes: Related  
:fa-rocket: [Install File Folder Monitor Agent][Install]  
:fa-cloud-download: [Source][Sources]

[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Install]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/File%20Folder/2.%20Install.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Remote%20Configuration/Remote%20Configuration.md?at=master
[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master

[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[View All Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/1.%20View%20All%20Monitor%20Views/View%20All%20Monitor%20Views.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master  


[png_Paths]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/FileFolder/Paths.png
