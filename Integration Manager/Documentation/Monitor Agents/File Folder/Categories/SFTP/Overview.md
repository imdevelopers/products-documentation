<tags data-value=""></tags>

:fa-edit: Paths 
=====
					

___

## :fa-info: Information
The agent can monitor the age of files on one or more folders. Various filters can be applied to include and/or exclude files and folders. 
Each path will be represented as unique [Resource][Resources] with display name from the settings.

![Paths][png_Paths]

:fa-cog: Paths
* **Display name** will be de [Resource][Resources] name in Integration Manager, its important to be cautions about changes to this name since there may be monitor views using explicitly named resources.  
* **Application Id** (optional) is the correspondent Id for an Application under the Applications tab. Set 0 to skip use of application.  
* **Category Id** (optional) is the correspondent Id for an Category under the Categories tab and is used to tag your resources, and make them more easier to understand. Set 0 to skip use of category.  
* **Description** (optional) A description of the monitored folder.  
* **Warning Time Span** The longest allowed time span before Warning alert is raised should be in format (days.hours:minutes:seconds e.g 7.12:30:59).  
* **Error Time Span** The longest allowed time span before Error alert is raised should be in format (days.hours:minutes:seconds e.g 7.12:30:59).  
* **Time evaluation type** Evaluate and change state of resource based either on **Created** or **Modified** file time.    
* **Return all file names in the Log Text for the Resource** When checked the filenames will be set in the log text.  
* **Filter** The filter uses RegEx to match, and uses the file name to match only xml files for example `test.xml` you should use `\.xml$`. The filter is case insensitive and ignore multiple whitespaces. (:fa-warning: the Windows style filter \*.txt can not be used!).    
* **Include child folders** If checked all the folders subfolders are searched except **Exclude child folders** and it's children.  
* **Exclude child folders** A full match like `C:\temp\BizTalk\INT001 - Demo\Out` to exclude Out folder and it's children if **Folder** is set to `C:\temp\BizTalk\INT001 - Demo`.  

After configuring the agent the [Resources][] can be used within [Monitor Views][].
 ___

### :fa-hand-o-right: Next Step  
:fa-desktop: [Monitor Views][] 

##### :fa-cubes: Related  
:fa-rocket: [Install File Folder Monitor Agent][Install]  
:fa-cloud-download: [Source][Sources]

[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Install]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/File%20Folder/2.%20Install.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Remote%20Configuration/Remote%20Configuration.md?at=master
[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master

[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[View All Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/1.%20View%20All%20Monitor%20Views/View%20All%20Monitor%20Views.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master  


[png_Paths]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/FileFolder/Paths.png
