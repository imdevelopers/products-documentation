<tags data-value="Monitor,Windows,Folders,Files,FTP,SSH,SFTP,FTPS"></tags>

:fa-file: File and FTP Folder Monitor Agent
=============================================

Monitor file folders with Integration Manager's monitor agent for file folders including FTP/SFTP/FTPS sources.
___
					
## :fa-info: About
The **File and FTP Folder monitor agent** enables you to monitor the file age from folders (SMB and NFS) and FTP/SFTP/FTPS sources.

The current state of the [Resources][] are available in [Monitor Views][] and alerts can be distributed (pushed) to end users (Mail, Ticket handling systems, etc.).  
Alert Metrics can be displayed either in the dashboard or using custom [Reports][Reports] using [Power BI][Power BI] or any other tool that you prefer since data comes from our REST based [Web API][].

## :fa-question-circle: Whats evaluated
The time difference (duration) between the current time and the time the file was created/modified evaluates the [Resources][] as:

#### :fa-check-circle-o: OK - Used to indicate normal operation
* No old files exists  

#### :fa-times-circle: Unavailable - Resource not available 
* The Monitoring Server fails to communicate with the Monitoring Agent and/or an invalid path has been entered   

#### :fa-times-circle: Error - Used to indicate an error/fatal condition
* Files are **older** than the error threshold value

#### :fa-exclamation-triangle: Warning - Used to indicate an unexpected but not invalid state
* Files are **older** than the warning threshold value

The evaluated state may be reconfigured using **Expected State** functionality that exists on every [Resource][Resources] within Integration Manager.


![png_MonitorViewListOfResources][0]

:fa-sitemap: Documentation support is provided by the [Repository][] model. 

## :fa-sliders: Monitor Capabilities
List of Capabilities that can be monitored by using this agent. 

* Any number of file shares can be monitored from a single agent. The supported file protocols are:  
    * [SMB](https://technet.microsoft.com/en-us/library/cc734393(v=ws.10).aspx)
    * [Network File System (NFS)](https://en.wikipedia.org/wiki/Network_File_System)
* Multiple agents can be deployed on multiple Windows servers

* File age (created or modified) verification per folder with options:
	* Root only
	* Root and child folders
    * Root and child folders with multiple exclusion filters
    * RegEx based file folder matching (for example: `\.xml$`)
    * Size

* Supported protocols
    * [Paths][] - Local and network folders (for example: `C:\Incoming\` or `\\share\folder\`)
    * NFS (V2, V3, V4 in Beta)
    * [FTP / FTPS][FTP]
    * [SFTP][] (SSH)

## :fa-flash: Actions
Get insights about your aged files with ease from a distance without any other tool like an FTP Client and Windows Explorer.
Using the [Web Client][Web Client] for Integration Manager, Actions can be sent to the monitor agent requesting operations to be performed on the monitored resources. 
With the existing [role][Roles] based privilege model, you can allow certain [users][Users] to perform operation on selected [resources][Resources]. This behaviour is determined by settings in the [Monitor View][Monitor Views].

The following Actions are available on the [Resources][] from **File Categories**:
* View Details
    * Shows all files in the folder, works the same for local folders ([Paths][] and **NFS**) as for [FTP/FTPS][FTP] and [SFTP (SSH)][SFTP]
    * Action in Action
        * Download - downloads the specific file

## :fa-list-alt: Release Log
For information about the latest release, see [Release Log](http://support.integrationsoftware.se/discussions/forums/1000229030)
___

### :fa-hand-o-right: Next Step
:fa-rocket: [Install File Monitor Agent][Install]  
:fa-edit: [Configuration][] of the agent  

#### :fa-cubes: Related
:fa-check-square-o: [PreRequisites][]  

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/FileFolder/MonitorViewListOfResources.png
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Web API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Web%20API/Overview.md?at=master
[Reports]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Reports/Overview.md?at=master
[Power BI]:https://powerbi.microsoft.com/en-us/
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[PreRequisites]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/File%20Folder/1.%20PreRequisites.md?at=master
[Install]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/File%20Folder/2.%20Install.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/File%20Folder/3.%20Configuration.md?at=master
[Paths]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/File%20Folder/Categories/Paths/Overview.md?at=master
[FTP]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/File%20Folder/Categories/FTP/Overview.md?at=master
[SFTP]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/File%20Folder/Categories/SFTP/Overview.md?at=master
[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master