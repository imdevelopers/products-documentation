<tags data-value="Monitoring, Log File Parser"></tags>

:fa-check-square-o: The Log File Parser Monitoring Agent has the following pre requisites
=====
					

___

## :fa-info: Information
This page describes the prerequisites for installing and running the [Log File Parser Monitor Agent][Log File Parser Agent].

## Software requirements

* :fa-windows: Microsoft Windows Server 2008 R2 or later
    * .NET Framework 4.5 or later*
* Security/User rights (listed below)
    * Running the service
    * Access to log files

## :fa-free-code-camp: Firewall

* Default outgoing port 443 to Internet for Service Bus Relayed connections
* Default inbound and outbound port 8000 **IM Monitor Agent** and **IM Log File Parser Monitoring Agent**

## :fa-windows: Windows User Rights
* Local named account or domain account (preferred). Follow this guide '[How to set Logon As A Service][]' for more information.  
* Service Account running the **Log File Parser Monitor Agent** must be have read access to all locations where log files are stored.

### :fa-hand-o-right: Next Step
:fa-rocket: [Install Log File Parser Monitor Agent][]  

#### :fa-cubes: Related
:fa-plus-square: :fa-download: [Add or manage a Source][]  
:fa-desktop: [Monitor Agent][]    
:fa-cogs: [Administration][]    


<!--References -->
[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master

[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[ServiceBusRelaying]:https://azure.microsoft.com/sv-se/documentation/articles/service-bus-dotnet-how-to-use-relay/
[Install Log File Parser Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Log%20File%20Parser/2.%20Install.md?at=master
                            
[How to set Logon As A Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Logon%20As%20A%20Service.md?at=master
[Log File Parser Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Log%20File%20Parser/Overview.md?at=master
