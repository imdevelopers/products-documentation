<tags data-value="Log File Parser"></tags>

:fa-edit: Log File Events configuration 
=====

___

## :fa-info: Information
The agent monitors the content of log files on one or more specified SMB shares and/or folders. Filters can be applied to include specific files using a RegEx expression. 
Each log file will be represented as unique [Resource][Resources] with a user friendly display name from the remote configuration settings.

![examples][png_PathExamples]

## :fa-question-circle: Whats evaluated
The following rules are evaluated for **Log File Events** checks:

#### :fa-check-circle-o: OK - Used to indicate normal operation
* No match in log file(s) 

#### :fa-times-circle: Unavailable - Resource not available 
* The Monitoring Server fails to communicate with the Monitoring Agent and/or an invalid path has been entered   

#### :fa-times-circle: Error - Used to indicate an error/fatal condition
* ?

#### :fa-exclamation-triangle: Warning - Used to indicate an unexpected but not invalid state
* Content matching one or more checks (newer than last clear date time)  

The evaluated state may be reconfigured using **Expected State** functionality that exists on every [Resource][Resources] within Integration Manager.


:fa-folder: Paths
In the remote configuration dialog the following properties can be set that determines what the file agent should monitor. 
![Paths][png_Paths]

* **Display name** is the [Resource][Resources] name as presented in [Monitor Views][] in Integration Manager.  
     NOTE: Change the name with caution if the resource is used within [Monitor Views][] since changing the name when the resource is explicitly added will render the [Monitor View][] useless.  
* **Folder** The full path (local or UNC)
* **Application Id** (optional) is the correspondent Id for an Application under the Applications tab. Set 0 to skip use of application.  
* **Category Id** (optional) is the correspondent Id for an Category under the Categories tab and is used to tag your resources, and make them more easier to understand. Set 0 to skip use of category.  
* **Description** (optional) A description of the monitored folder.  
* **Warning Time Span** The longest allowed time span before Warning alert is raised should be in format (days.hours:minutes:seconds e.g 7.12:30:59).

    TIP: Use the same time as Errors (or higher) to disable the use for warnings.

* **Error Time Span** The longest allowed time span before Error alert is raised should be in format (days.hours:minutes:seconds e.g 7.12:30:59).  
* **Time evaluation type** Evaluate and change state of resource based either on **Created** or **Modified** file time.    
* **Return all file names in the Log Text for the Resource** When checked the filenames will be set in the log text.  
* **Filter** RegEx based filter that files need to match. The filter is case insensitive and ignore multiple whitespaces. (:fa-warning: the Windows style filter \*.txt can not be used!). Examples:
    * XML Files: like  `someoldfile.xml` = `\.xml$`
    * Text Files: like  `someoldfile.txt` = `\.txt$`  
* **Include child folders** When checked all the subfolders are included in the monitoring  except for optional excluded child folders (and consequently child folders of that child folder)  
* **Exclude child folders** Excludes the child folders matching a full match like `C:\temp\BizTalk\INT001 - Demo\Out` to exclude Out folder and it's children if **Folder** is set to `C:\temp\BizTalk\INT001 - Demo`.  
![png_ExcludeChildFolders][]

After configuring the agent the [Resources][] can be used within [Monitor Views][].

## :fa-lock: Service account requirements 
The service account needs read rights on included **log files**.
___

### :fa-hand-o-right: Next Step  
:fa-desktop: [Monitor Views][] 

##### :fa-cubes: Related  
:fa-rocket: [Install File Folder Monitor Agent][Install]  
:fa-cloud-download: [Source][Sources]

[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Install]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/File%20Folder/2.%20Install.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Remote%20Configuration/Remote%20Configuration.md?at=master
[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master

[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[View All Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/1.%20View%20All%20Monitor%20Views/View%20All%20Monitor%20Views.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master  


[png_Paths]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/FileFolder/Paths.png
[png_PathExamples]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/FileFolder/PathExamples.png
[png_ExcludeChildFolders]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/FileFolder/ExcludeChildFolders.png
