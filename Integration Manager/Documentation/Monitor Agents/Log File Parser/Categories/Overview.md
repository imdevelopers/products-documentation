<tags data-value="Actions,Monitor,View,Resource"></tags>

:fa-folder: Log File Parser Monitor Categories
=====
					
___

## :fa-info: Information
This page is an overview for all the [Categories][] the Log File Parser Monitor agent provides.

The Log File Parser Monitoring Agent supports the execution of remote actions on resources using the Integration Manager Web Client tool or by programmatically calling the [Web API][].
In order to execute remote actions some [PreRequisites][PreRequisites for Log File Parser Monitor Agent] must be satisfied. 

The user must be logged on to Integration Manager using a Windows Credential. The user must also be assigned a monitor view with [Resources][] from this agent where [Actions][] are allowed. See [Add or manage Monitor Views][] for more details.
___

The various monitoring capabilities for the Log File Parser monitor agent are expressed as [Resources][] and are grouped by [Categories][].
The **Categories** are unique for each [Monitoring Agent][Monitor Agents]. 

## :fa-edit: Remote Configuration
 The [Log File Parser Agent][] is configured using [Remote Configuration][]. From the [Configuration][] in a [Source][Sources] an Administrator can add and/or remove Log File Parsers to monitor.  
![png_CategoriesTabs][0]

## :fa-folder: Categories
The following [Categories][] are exposed by the **Log File Parser Monitor Agent**.  

* [Log File Events][]  
___

### :fa-hand-o-right: Next Step
:fa-edit: [Remote Configuration][Configuration] of the agent  

#### :fa-cubes: Related
:fa-check-square-o: [PreRequisites for Log File Parser Monitor Agent][]  
:fa-folder: [Categories][]  
:fa-flash: [Actions][]  
:fa-desktop: :fa-cloud-upload: [Monitor Agents][]      
:fa-cloud-download: [Source][Sources]

<!--References -->

[Monitor View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/3.%20Monitor/Monitor.md?at=master
[Actions]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/1.%20View%20Monitor%20Views/2.%20View%20Single%20Monitor%20View/Actions/Actions.md?at=master

[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[Web API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Web%20API/Overview.md?at=master
[Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/Categories.md?at=master
[Actions]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/1.%20View%20Monitor%20Views/2.%20View%20Single%20Monitor%20View/Actions/Actions.md?at=master
[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Remote%20Configuration/Remote%20Configuration.md?at=master


[Log File Parser Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Log%20File%20Parser/Overview.md?at=master


[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Log%20File%20Parser/3.%20Configuration.md?at=master
[PreRequisites for Log File Parser Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Log%20File%20Parser/1.%20PreRequisites.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master  


[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/LogFileParser/RemoteConfigurationOverview.png


[Log File Events]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Log%20File%20Parser/Categories/Log%20File%20Events.md?at=master
