<tags data-value="Monitor,Log File Parser,Log File,Parser"></tags>

:fa-file-text-o: Log File Parser Monitor Agent
=============================================

Monitor content in log files with Integration Manager's monitor agent **Log File Parser**
					
## :fa-info: About
This agent allows you to monitor content of log files and trigger warnings on:
*  if a specific error text is found
*  Correlation between a start event and another later event
    * Example: Two events should occur after each another within a configurable timespan.

* Any number of Log Files can be monitored from a single agent
* Multiple agents can be deployed on multiple servers for scalability, security and performance
* Different files can be monitored on different file shares/folders

![rcoverview][0]

## :fa-sliders: Monitor Capabilities
The agent has the following list of **Log File Parser** [Categories][]:
* [Log File Events][]

![CategoriesTabs][1]

## :fa-flash: Actions
Using the [Web Client][Web Client] for Integration Manager, Actions can be sent to the monitor agent for Log File Parser requesting operations to be performed on the monitored resources. With the existing privilege model you can allow certain users to perform operation on selected resources.

List of Actions on the [Resources][] of [Log File Parser Categories][Categories] that can be executed from using this agent:

* **Clear error(s)** - Clear the alert from the point of time when executed (do not bother about older errors/warnings). 
* **Show log file(s)** - Open a modal to download and view content from monitored log files
* **Show details** - ...

![Actions][3]

## :fa-line-chart: Metrics
Not yet implemented

## :fa-arrows-alt: Supported Versions
* All text based files from SMB shares and/or Windows File Folders 

See :fa-check-square-o: [PreRequisites for Log File Parser Monitor Agent][] for additional details installing and running the agent.

## :fa-list-alt: Release Log
For detailed information about the features and bug fixes, please see the [Release Log](http://support.integrationsoftware.se/support/discussions/forums/1000229311)

___

### :fa-hand-o-right: Next Step
:fa-rocket: [Install Log File Parser Monitor Agent][]  
:fa-edit: [Configuration][] of the agent  

#### :fa-cubes: Related
:fa-check-square-o: [PreRequisites for Log File Parser Monitor Agent][]  

[Install Log File Parser Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Log%20File%20Parser/2.%20Install.md?at=master  
[PreRequisites for Log File Parser Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Log%20File%20Parser/1.%20PreRequisites.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Log%20File%20Parser/3.%20Configuration.md?at=master
[Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Log%20File%20Parser/Categories/Overview.md?at=master

[Log File Events]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Log%20File%20Parser/Categories/Log%20File%20Events.md?at=master



[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/LogFileParser/RemoteConfigurationOverview.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/LogFileParser/Category.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/LogFileParser/Actions.png

[WindowsServices]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Log%20File%20Parser/Categories/Windows%20Services.md?at=master
[CPU]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Log%20File%20Parser/Categories/CPU.md?at=master
[Memory]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Log%20File%20Parser/Categories/Memory.md?at=master
[ScheduledTasks]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Log%20File%20Parser/Categories/Scheduled%20Tasks.md?at=master
[Disk]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Log%20File%20Parser/Categories/Disk.md?at=master
[Network]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Log%20File%20Parser/Categories/Network.md?at=master



[Actions]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/1.%20View%20Monitor%20Views/2.%20View%20Single%20Monitor%20View/Actions/Actions.md?at=master
