<tags data-value="Monitor,Azure,MSMQ,Queue"></tags>

:fa-reorder: Microsoft MSMQ
=============================================

Support for **Microsoft MSMQ** is provided from within the **[Message Queues Monitor Agent][]**
	

#### :fa-cubes: Related
:fa-check-square-o: [Message Queues Monitor Agent][]  


[Message Queues Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Message%20Queues/Overview.md?at=master  


