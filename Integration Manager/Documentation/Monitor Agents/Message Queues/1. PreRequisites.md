<tags data-value="Monitoring, Message Queues Server"></tags>

:fa-check-square-o: The Message Queues Monitoring Agent has the following pre requisites
=====
					

___

## :fa-info: Information
This page describes the prerequisites for installing and running the [Message Queues Monitor Agent][Message Queues Agent].

## Software requirements

* :fa-windows: Microsoft Windows Server 2008 R2 or later
    * .NET Framework 4.0 or later*
    * Message Queues administrative tools requires .NET framework 2.0/3.5 Windows feature.
* Message Queues Server Client Tools in a version equal or higher than target Message Queues Environment
* Security/User rights (listed below)

*/ .NET framework version depends on installed Message Queues version

## :fa-free-code-camp: Firewall

* Default outgoing port 443 to Internet for Service Bus Relayed connections
* Access to AD/DNS 
    * Read more [here](https://technet.microsoft.com/en-us/library/dd772723(v=ws.10).aspx)
* Default inbound and outbound port 8000 **IM Monitor Agent** and **IM Message Queues Monitoring Agent**
* SQL Ports in use between **SQL Server** and **IM Message Queues Monitoring Agent**
    * Read more [here](https://msdn.microsoft.com/en-us/library/cc646023.aspx)
    * TCP ports in use for Active MQ Brokers (usually 6161 and)
## :fa-windows: Windows User Rights
* Local named account or domain account (preferred). Follow this guide '[How to set Logon As A Service][]' for more information.  
* Service Account running Message Queues Monitor Agent requires to have appropriate user rights on target queues

### :fa-hand-o-right: Next Step
:fa-rocket: [Install Message Queues Monitor Agent][]  

#### :fa-cubes: Related
:fa-plus-square: :fa-download: [Add or manage a Source][]  
:fa-desktop: [Monitor Agent][]    
:fa-cogs: [Administration][]    

[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master

<!--References -->
[connectionstrings]:https://www.connectionstrings.com/sql-server/
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[ServiceBusRelaying]:https://azure.microsoft.com/sv-se/documentation/articles/service-bus-dotnet-how-to-use-relay/
[Install Message Queues Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Message%20Queues/2.%20Install.md?at=master
                            
[How to set Logon As A Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Logon%20As%20A%20Service.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Message Queues Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Message%20Queues/Overview.md?at=master
