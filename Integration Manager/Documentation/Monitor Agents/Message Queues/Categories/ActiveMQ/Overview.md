<tags data-value="Monitor,Windows,ActiveMQ,Queue"></tags>

ActiveMQ Agent
=============================================

Monitor ActiveMQ with Integration Manager's monitor agent for ActiveMQ.
					
## About
This agent allows you to monitor ActiveMQ queues. Checks can be performed on the allowed number of messages within them. Also the maximum age of the first message on the queue can be verified. You decide in the configuration file which server and queue it is you want to monitor, as well as the number of messages on which Integration Manager should warn / fail for. 

This service has been developed due to the fact that in many cases, that if queues contain messages it is in many cases some kind of error, at least if the messages are there longer than an expected time span (e.g. 2 hours). This agent enables you to monitor each and every queue that is important for you and your business.

![activemqresources][0]

## Monitor Capabilities
List of resources that can be monitored by using this agent

* ActiveMQ Brokers
	* The server(s) and core services
* ActiveMQ Queues
	* Set allowed time on queue
	* Count the number of messages on the queue

* Age verification
* Count (warning / error)
* Per queue setting can be overridden from default values

## :fa-flash: Actions
A [User][] with access rights to a [Monitor View][] with ActiveMQ Queues where Actions are allowed can perform the following **Actions**
* List Messages on queue
* Edit thresholds
* Download individual messages from the queue
* Download all messages in a ZIP file

![Actions][1]


## List messages on queue

![listmessages][2]

## Edit Thresholds

You can either use the global settings for queue using remote configuration, or you can individually set different thresholds for different queues.

![EditThresholdsForQueue][5]

	NOTE: Upon save the Resource will be changed to a Specific ActiveMQ Queue (may impact your monitor views depending on filter)

## Download
* Download individual messages from the queue  
![download][3]

* Download all messages in a ZIP file  
![download][4]

___

### :fa-hand-o-right: Next Step
:fa-edit: [Configuration][] of the agent  

#### :fa-cubes: Related
* [MSMQ](https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Message%20Queues/Categories/MSMQ/Overview.md)  
* [Service Bus](https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Message%20Queues/Categories/Service%20Bus/Overview.md)



[Monitor View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/3.%20Monitor/Monitor.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Message%20Queues/3.%20Configuration.md?at=master
[User]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/MessageQueueing/ActiveMQ/ActiveMQResources.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/MessageQueueing/ActiveMQ/Actions.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/MessageQueueing/ActiveMQ/ListMessages.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/MessageQueueing/ActiveMQ/Download.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/MessageQueueing/ActiveMQ/SaveAllAsZip.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/MessageQueueing/ActiveMQ/EditThresholdsForQueue.png
