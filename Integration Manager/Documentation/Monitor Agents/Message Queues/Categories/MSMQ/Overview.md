<tags data-value="Monitor,Windows,MSMQ,Queue"></tags>

MSMQ Agent
=============================================

Monitor MSMQ with Integration Manager's monitor agent for MSMQ.
					
## About
This agent allows you to monitor MSMQ Brokers and queues. Checks can be performed on the allowed number and age of messages on the queues.

![overview][0]

## Monitor Capabilities
List of resources that can be monitored by using this agent

* Public queues
* Private queues
* System queues (Dead letter and journal)
* Age verification (warning / error)
* Count (warning / error)
* Per queue settings override public / default values

## Actions
No actions have yet been implemented.

## :fa-wrench: Connection
Currently you must configure MSMQ using **Remote Configuration**.  
Use Actions from one of the MSMQ [Resources][]  
![EditSource][2]

Repeat with 1 connection for each MSMQ Broker to monitor  
![connection][1]

## MSMQ
Using the **Queues tab** you can further customize what the agent should monitor. Repeat as needed.  
![connection][3]

* Get private queues
* Get public queues
* Get System Queues
* Warning Count
* Error Count
* Warning Time Span
* Error Time Span

### Specific Queue Settings
Override default values with per queue settings. Matches on name of queue. Repeat as needed.  
![specifics][4]

    NOTE: If the name of the queue changes, you must change the configuration as well to reflect this!

### Queue exclude filter
As with many other agents, you can filter out queues based on their names if they should be excluded or not. Repeat as needed.

![excludefilter][5]

:fa-ambulance: Support
If you run into access related problems, make sure to check out the following [article](https://support.integrationsoftware.se/support/solutions/articles/1000160886-log-api-msmq-queue-access-denied)


___

### :fa-hand-o-right: Next Step
:fa-edit: [Configuration][] of the agent  

#### :fa-cubes: Related
* [ActiveMQ](https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Message%20Queues/Categories/ActiveMQ/Overview.md)  
* [Service Bus](https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Message%20Queues/Categories/Service%20Bus/Overview.md)

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/MessageQueueing/MSMQ/MSMQOverview.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/MessageQueueing/MSMQ/ConnectionTab.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/MessageQueueing/MSMQ/EditSource.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/MessageQueueing/MSMQ/QueuesTab.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/MessageQueueing/MSMQ/Specific.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/MessageQueueing/MSMQ/ExcudeFilter.png

[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Message%20Queues/3.%20Configuration.md?at=master