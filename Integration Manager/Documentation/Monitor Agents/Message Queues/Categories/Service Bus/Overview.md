<tags data-value="Monitor,Message Queue,ActiveMQ,Control Center"></tags>

:fa-reorder: Service Bus Overview
=============================================

Monitor [Microsoft Azure Service Bus](https://azure.microsoft.com/en-us/services/service-bus/) with Integration Manager's monitor agent for [Microsoft Azure Service Bus](https://azure.microsoft.com/en-us/services/service-bus/).
					
## About
This agent allows you to monitor Microsoft Azure Service Bus queues & topics. Checks can be performed on the allowed number of messages within them. Also the maximum age of the first message on the queue can be verified. You decide in the configuration file which server and queue it is you want to monitor, as well as the number of messages on which Integration Manager should warn / fail for. 

This service has been developed due to the fact that in many cases, that if queues contain messages it is in many cases some kind of error, at least if the messages are there longer than an expected time span (e.g. 2 hours). This agent enables you to monitor each and every queue that is important for you and your business.

![servicebusresources][1]

## Monitor Capabilities
List of resources that can be monitored by using this agent

* [Service Bus Queues](https://azure.microsoft.com/en-us/documentation/articles/service-bus-fundamentals-hybrid-solutions/#queues)
* [Service Topics](https://azure.microsoft.com/en-us/documentation/articles/service-bus-fundamentals-hybrid-solutions/#topics)
* [Service Bus Relay](https://azure.microsoft.com/en-us/documentation/articles/service-bus-fundamentals-hybrid-solutions/#relays)
* Dead letter 
* Age verification
* Count (warning / error)
* Per queue setting can be overridden from default values

## :fa-flash: Actions
A [User][] with access rights to a [Monitor View][] with Service Bus Resources where Actions are allowed can perform the following **Actions**
* **Service Bus Details**  
![actionservicebus][3]

Select **Service Bus Details** to open a modal with information about the Service Bus:   
![details][2] 


### :fa-hand-o-right: Next Step
:fa-edit: [Configuration][] of the agent  

#### :fa-cubes: Related
* [MSMQ](https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Message%20Queues/Categories/MSMQ/Overview.md)  
* [Active MQ](https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Message%20Queues/Categories/ActiveMQ/Overview.md)


[Monitor View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/3.%20Monitor/Monitor.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Message%20Queues/3.%20Configuration.md?at=master
[User]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/MessageQueueing/SB/ServiceBusResources.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/MessageQueueing/SB/ServiceBusDetails.png

[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/MessageQueueing/SB/ActionsServiceBusInstance.png





