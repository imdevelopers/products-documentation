<tags data-value="Monitor,Message Queue,ActiveMQ, Service Bus, MSMQ, Control Center"></tags>

:fa-reorder: Message Queues Agent
=============================================

Monitor queues and queue managers within your environments with the **Message Queues Agent** for Integration Manager and get alerts when there is a problem. Solve problem(s) secure and remotely using remote actions.

## :fa-info: About
The monitoring agent for message queues has many features to help you detect and solve problem's within the following message queuing environments:
* [ActiveMQ][] - Monitor [Apache ActiveMQ][] (external link)
* [MSMQ][] - Monitor [Microsoft MSMQ][] (external link)
* [Service Bus][] - Monitor [Azure Service Bus](https://azure.microsoft.com/en-us/services/service-bus/) (external link)
  
Other Monitor Agents exists for other Queues and Queue managers:
* [IBM WebSphere MQ (WMQ)][WMQ Agent]
* [RabbitMQ][RabbitMQ Agent]

## :fa-sliders: Monitor Capabilities
Click on link to navigate to specific Queue Manager for additional details:
* [Active MQ][ActiveMQCategory]
* [MSMQ][MSMQCategory]
* [Service Bus][ServiceBusCategory]

## :fa-flash: Actions
Fix your queue related problems with ease from a distance

Using the [Web Client][Web Client] for Integration Manager, Actions can be sent to the monitor agent for message queue requesting operations to be performed on the monitored resources. With the existing privilege model you can allow certain users to perform operation on selected resources.

List of all Actions on the [Resources][] of [Message Queue Categories][Categories] that can be executed from using this agent:


* List Messages
* Edit specific settings
* Purge queue (MSMQ)
* Download single message
* Download multiple selected messages
* Remove Message from Queue

```
	NOTE: There are different actions available for MSMQ, ActiveMQ and Azure ServiceBus
```    


## :fa-arrows-alt: Supported Versions
* ActiveMQ (tested version, may support others)
	* Apache ActiveMQ 5.14.0 and later
* MSMQ
	* All Windows version Windows Server 2008 R2 or later
* ServiceBus
	* Current public version by Microsoft in Azure

## :fa-usd: Pricing
This agent and all other Monitor agents are part of the license for Integration Manager. No additional costs exists for the use of the non-event Monitor Agent for Integration Manager licencees. 

## Miscellaneous

## :fa-list-alt: Release Log
For detailed information about the features and bug fixes, please see the [Release Log](http://support.integrationsoftware.se/support/discussions/forums/1000229198)
___

### :fa-hand-o-right: Next Step
:fa-rocket: [Install Message Queue Monitor Agent][]  
:fa-edit: [Configuration][] of the agent  

#### :fa-cubes: Related
:fa-check-square-o: [PreRequisites for Message Queue Monitor Agent][]  

<!-- Categories -->
[ActiveMQ]:Categories/ActiveMQ/Overview.md?at=master
[MSMQ]:Categories/MSMQ/Overview.md?at=master
[Service Bus]:Categories/Service%20Bus/Overview.md?at=master

[Install Message Queue Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Message%20Queues/2.%20Install.md?at=master  
[PreRequisites for Message Queue Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Message%20Queues/1.%20PreRequisites.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Message%20Queues/3.%20Configuration.md?at=master
[Categories]:Categories/Overview.md?at=master

[ActiveMQCategory]:Categories/ActiveMQ/Overview.md?at=master
[MSMQCategory]:Categories/MSMQ/Overview.md?at=master
[ServiceBusCategory]:Categories/Service%20Bus/Overview.md?at=master

         
[Apache ActiveMQ]:http://activemq.apache.org/
[Microsoft MSMQ]:https://technet.microsoft.com/en-us/library/cc770387(v=ws.11).aspx
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[WMQ Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/WMQ/Overview.md?at=master
[RabbitMQ Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/RabbitMQ/Overview.md?at=master