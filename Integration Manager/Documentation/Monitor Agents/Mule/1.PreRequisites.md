<tags data-value="Monitor, Agent, Mule"></tags>

:fa-check-square-o: The Mule Monitor Agent has the following prerequisites.
=====
					

___

## :fa-info: Information
This page describes the prerequisites for installing and running the [Mule Monitor Agent][Mule Monitor Agent].

## Software requirements

* :fa-windows: Microsoft Windows Server 2008 R2 or later
    * .NET Framework 4.5 or later
    * :fa-server: Mule 3.7.x server or later
* Security/User rights (listed below)

## :fa-free-code-camp: Firewall

* Default inbound and outbound port 8000 **IM Monitor Agent** and **IM Mule Monitor Agent**
* Ports in use between **Mule Instance** and **IM Mule Monitor Agent**

## :fa-user-secret: Privileges
The agent requires a Windows account to run.

### :fa-windows: Windows Rights
* Local named account or domain account (preferred) to run the Windows Service. 
Follow this guide '[How to set Logon As A Service][]' for more information. 
 
## Mule Application
To be able to communicate with the Mule Server, an application has to be installed on each Mule Instance to be monitored.  
    
## :fa-hand-o-right: Next Step
:fa-rocket: [Install Mule Monitor Agent][]  

### :fa-cubes: Related
:fa-plus-square: :fa-download: [Add or manage a Source][]  
:fa-desktop: [Monitor Agent][]    
:fa-cogs: [Administration][]    

<!--References -->
[Mule Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Mule/Overview.md
[How to set Logon As A Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Logon%20As%20A%20Service.md?at=master
[Install Mule Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Mule/2.%20Install.md?at=master
[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master