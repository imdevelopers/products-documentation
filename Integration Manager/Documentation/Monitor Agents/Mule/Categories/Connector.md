<tags data-value="MonitorViews,Mule,Monitoring,MuleConnector"></tags>

:fa-database: Mule Connector
=====



___

## :fa-info: Information
Monitor the status of a **Mule Connector**. [Mule Agent Monitor][Mule Agent] creates one [Resource][Resources] for the 
Mule Connector and can be monitored using suitable [Monitor Views][].  

 ![Status][6]
___

## :fa-question-circle: Whats evaluated
The following rules are evaluated for **Mule Connector**:

#### :fa-check-circle-o: OK - Used to indicate normal operation
* The Mule Connector is running

#### :fa-times-circle: Unavailable - Resource not available 
* The Monitoring Server fails to communicate with the Monitoring Agent  

#### :fa-times-circle: Error - Used to indicate an error/fatal condition
* The Mule Connector is stopped

#### :fa-exclamation-triangle: Warning - Used to indicate an unexpected but not invalid state
* Not implemented

The evaluated state may be reconfigured using **Expected State** functionality that exists on every [Resource][Resources] within Integration Manager.
___

 ## :fa-flash: Actions
The [Mule Agent][] has support for remote actions. The folllowing Actions are exposed:  
* Start  
* Stop  

 ![actions][1]
 
### :fa-edit: Start
Start the Mule Connector.  
 ![Start application][2]

The Mule Monitor Agent contacts the Mule server and starts the application. The result appears on the screen.  
 ![Application started][3]

    Note: Depending on the synchronization interval set for the agent, there might be a delay before the Web Client reflects upon the change. You can choose to force the agent to synchronize from the configuration of the Source. 

### :fa-edit: Stop
Stop the Mule Connector.  
 ![Stop application][4]

The Mule Monitor Agent contacts the Mule server and stops the application. The result appears on the screen.  
 ![Application stopped][5]

    Note: Depending on the synchronization interval set for the agent, there might be a delay before the Web Client reflects upon the change. You can choose to force the agent to synchronize from the configuration of the Source. 

___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]  

##### :fa-cubes: Related  
:fa-database: [Mule Agent][]  
:fa-folder-o: [Mule Categories][]  
:fa-lightbulb-o: [Resources][]  
:fa-cloud-download: [Monitor Agent][]  
:fa-desktop: [Monitor Views][]    

<!-- Image references -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/Mule/Actions/mule-monitor-web-client-action-application.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/Mule/Actions/mule-monitor-web-client-action-connector-start.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/Mule/Actions/mule-monitor-web-client-action-connector-started.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/Mule/Actions/mule-monitor-web-client-action-connector-stop.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/Mule/Actions/mule-monitor-web-client-action-connector-stopped.png
[6]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/Mule/mule-monitor-web-client-status.png

<!-- Monitor references -->
[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master
[Mule Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Mule/Overview.md
[Mule Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Mule/Categories/Overview.md

<!-- Web client references -->
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Applications]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/Applications.md?at=master
[Remote Config]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Remote%20Configuration/Remote%20Configuration.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master