<tags data-value="MonitorViews,Mule,Monitoring"></tags>

:fa-database: Mule Monitor Categories
=====


___

## :fa-info: Information
The various monitoring capabilities for the Mule Monitor Agent are expressed as [Resources][] and are grouped by [Categories][].
The **Categories** are unique for each [Monitoring Agent][Monitor Agent]. 

The following Categories are exposed by the **Mule Monitor Agent**.

## :fa-folder: Categories
* [Application][]
* [Connector][]
* [End Point][]
* [Server][]
  
 ## :fa-edit: Remote Configuration
 The [Mule Agent Monitor][Mule Agent] has support for [Remote Configuration][] and monitors Mule Server Instaces. 
 
 First the [Mule Agent Monitor][Mule Agent] must be configured using [Remote Configuration][Remote Configuration] set on the [Source][Sources].
___

### :fa-hand-o-right: Next Step  
:fa-edit: [Configuration][] of the agent  
:fa-database: [Mule Agent][]

##### :fa-cubes: Related  
:fa-desktop: [Monitor Views][]    
:fa-cloud-download: [Sources][]  
:fa-cloud-upload: [Monitor Agent][]  
:fa-lightbulb-o: [Resources][]  

<!-- Monitor references -->
[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Mule/3.%20Configuration.md?at=master
[Mule Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Mule/Overview.md
[Application]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Mule/Categories/Application.md
[Connector]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Mule/Categories/Connector.md
[End Point]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Mule/Categories/EndPoint.md
[Server]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Mule/Categories/Server.md

<!-- Web Client references -->
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/Categories.md?at=master
[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Remote%20Configuration/Remote%20Configuration.md?at=master