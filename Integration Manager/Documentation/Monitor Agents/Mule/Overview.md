<tags data-value="Monitor,Agent,Mule"></tags>

:fa-connectdevelop: Mule Monitor Agent
======================================

Monitor your [Mule Server][Mule Server] environments with the **Mule Monitor Agent** for Integration Manager and get alerts when there is a problem.

## :fa-info: About
This topic links to documentation resources about how to use the monitoring agent for Mule Server.

The agent provides a way to monitor key features in Mule Server. For instance it might be intresting to be aware of the status of the Mule applications. An alarm is raised when the application status is stopped.

The current state of the [Resources][] are available in [Monitor Views][] and alerts can be distributed (pushed) to end users (Mail, Ticket handling systems, etc.).  
Alert Metrics can be displayed either in the dashboard or using custom [Reports][Reports] using [Power BI][Power BI] or any other tool that you prefer since data comes from our REST based [Web API][Web API].

* Remote Actions are available on some resources which removes the need of the MMC/remote desktop connections  
* Any number of Mule Instances can be monitored from a single agent
* Multiple agents can be deployed on multiple servers
* Servers can be local or in the cloud, and even off site (partner/customer location)
* Low overhead with least privileges policy
* Report capabilities

:fa-sitemap: Documentation support is provided by the [Repository][] model. 

## :fa-sliders: Monitor Capabilities 	
The [Resources][Resources] are grouped by Categories. The following  [Mule Categories][Mule Categories] exists:

* [Application][] - Monitor the status of a Mule application	
* [Connector][] - Monitor  the status of a Mule connector
* [End Point][] - Monitor  the status of a Mule end point
* [Server][] - Monitor  the status of a Mule server

## :fa-flash: Actions

Fix your Mule related problems with ease from a distance without the MMC (Mule Management Console). 

Using the [Web Client][Web Client] for Integration Manager, Actions can be sent to the monitor agent for Mule Server requesting operations to be performed on the monitored resources. With the existing privilege model you can allow certain users to perform operation on selected resources.

List of Actions on the [Resources][] of [Mule Categories][Mule Categories] that can be executed from using this agent:

* [Application][]
	* Start an application
	* Stop an application
* [Connector][]
	* Start a connector
	* Stop a connector
* [End Point][]
	* Connect to an end point
	* Disconnect to an end point
* [Server][]
	* Restart of a Mule server

## :fa-arrows-alt: Supported Versions 
All versions from Mule 3.7.x and onwards are supported.  

All editions, **Standalone runtime** and **Enterprise** are supported.

## :fa-list-alt: Release Log
For information about the latest release, see [Release Log](http://support.integrationsoftware.se/support/discussions/forums/)
___

### :fa-hand-o-right: Next Step
:fa-rocket: [Install Mule Monitor Agent][]  
:fa-edit: [Configuration][] of the agent  

#### :fa-cubes: Related
:fa-check-square-o: [PreRequisites for Mule Monitor Agent][]  

[Mule Server]:https://www.mulesoft.com/
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Reports]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Reports/Overview.md?at=master
[Power BI]:https://powerbi.microsoft.com/en-us/
[Web API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Web%20API/Overview.md?at=master
[Mule Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Mule/Categories/Overview.md?at=master
[Application]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Mule/Categories/Application.md?at=master
[Connector]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Mule/Categories/Connector.md?at=master
[End Point]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Mule/Categories/EndPoint.md?at=master
[Server]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Mule/Categories/Server.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Mule/3.%20Configuration.md?at=master
[Install Mule Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Mule/2.%20Install.md?at=master  
[PreRequisites for Mule Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Mule/1.%20PreRequisites.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md