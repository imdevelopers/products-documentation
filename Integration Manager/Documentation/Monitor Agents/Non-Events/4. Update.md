<tags data-value=""></tags>

:fa-arrow-circle-o-up: Update of Monitor Agent for Non Events Server
=====
					

___

## :fa-info: Information
The Non Events Server agent can be easily updated using the latest software package (MSI).

## :fa-note: Release notes
For information about the releases, please visit the [Release Notes for the Non Events Monitor Agent][Non Events Release Notes].

## Running Version
For information about the installed and running version press the :fa-info-circle: 'Source Information' button for the appropriate [Source][Sources].  
![sourceinformationbutton][11]  

The installed Version can be seen in the **'Version'** text box.  
![sourceinformation][10]  

You can also view the file details property on the local installation  
![fileversion][12]  

## :fa-compress: Compatibility and versioning issues
  
* :fa-exclamation: 4.0 -> 4.2
    * Non Event Agent must match the Version of Integration Manager
* 4.3.0.x -> 4.x.0.y 
    * Additional features have been added, make sure to review settings and affected [Monitor Views][] after an update
    * LogText written by the LogAgents have been modified to never change unless the state does. 
    * The URI from 'Copy API URI' **must** be recopied to non events configurations

**PreReq**: You must have successfully installed the **IM Non Events Monitor Agent** prior to an update. The Service account credentials to use must have [Logon as Service Right][How to set Logon As A Service]. See [PreRequisites for Non Events Monitor Agent][] for more information. You must be a local administrator in order to have the right to install (and thereby update) Windows Services.

#### :fa-play: Start the installer
Double click on the **MSI file** to start the update of the **IM Non Events Monitor Agent**.  
![msi][1]  

Depending on settings in Windows a security warning may be presented. Click **'Run'** to continue.  
![warning][2]   

#### Welcome Screen
On successful execution of the MSI file a **Welcome screen** will be presented.  
![Welcome][3]  

Click **'Next'** button to continue with the update process or the **'Cancel'** button to quit the installer.

#### :fa-wrench: Custom Setup
Select Components and Location of installation. Default settings are recommended.   
![CustomSetup][4]  

    Note: If you change the location your old configuration will be lost

Click **'Next'** button to continue with the update process or the **'Cancel'** button to quit the installer.

#### :fa-user-secret: Service Account Information 
Enter the credentials for the **Account** to run **IM Non Events Monitor Agent**. Depending on the connection string settings this account will be used to log on and query the databases being monitored.  

**Domain**: The name of the Domain the Server belongs to. If a local account is being used, use the name of the server or . (dot) as Domain  
**User**: The name of the account  
**Password**: The password for the account.   

        Note: The account must have 'Log on as Service Right'

![DomainUser][5]

Click **'Next'** button to continue with the update process or the **'Cancel'** button to quit the installer.

##### :fa-exclamation-circle: Insufficient Privileges
Error message when account information does not meet [PreRequisites][PreRequisites for Non Events Monitor Agent].    
![InsufficientPrivileges][6]

If you get this message, the most common reason is that the account is **not allowed to run as a service** or is **not local Administrator** on the server. Follow the guide [How to set Logon As A Service][] and click retry. If the problem remains, click cancel and restart. Make sure that the credentials are typed correctly! 

    Tip: Check the local event log if there are errors (both System and Application)

#### :fa-flag-checkered: Completed setup
The last step of the update process gives either a **success** or a **premature exit**.    
**Successful update example**    
![Completed][8]  

**Failed update example**: A failed update will render a premature exit    
![PrematureExit][7]

Click the **'Finish'** button to Exit and quit the installer (update process).

When the setup has finished the default text editor will open a text file named the 'SourceInformation.txt'. The **SourceInformation.txt** file holds additional information that you need in order to manage the agent as a [Source][Sources].  
![SourceInformation.txt][9]
* :fa-plus-square: :fa-cloud-download: [Add or manage a Source][]

#### :fa-ambulance: Support
Contact our [Support][] for additional guidance if you fail to resolve the update problem.

    Note: There may be additional information written to the Windows Event logs.  
___

### :fa-hand-o-right: Next Step
:fa-edit: [Configuration][] of the agent  

#### :fa-cubes: Related
:fa-check-square-o: [PreRequisites for Non Events Monitor Agent][]  
:fa-plus-square: :fa-cloud-download: [Add or manage a Source][]  
:fa-cloud-download: [Sources][]  
:fa-rocket: [Install Non Events Monitor Agent][]  
:fa-remove: [Uninstall][]  

<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/NonEvents/msi.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/NonEvents/openfilesecuritywarning.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/NonEvents/Welcome.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/NonEvents/CustomSetup.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/NonEvents/DomainUser.png
[6]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/NonEvents/InsufficientPriviliges.png
[7]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/NonEvents/PrematureExit.png
[8]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/NonEvents/Completed.png
[9]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/NonEvents/SourceInformation.txt.png
[10]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/NonEvents/SourceInformation.png
[11]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/Sources/SourceInformationButton.png
[12]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/NonEvents/FileVersion.png

[Install Non Events Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Non-Events/2.%20Install.md?at=master  
[PreRequisites for Non Events Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Non-Events/1.%20PreRequisites.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Non-Events/3.%20Configuration.md?at=master
[Uninstall]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Non-Events/5.%20Uninstall.md?at=master

[How to set Logon As A Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Logon%20As%20A%20Service.md?at=master
[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Support]:http://support.integrationsoftware.se

[Non Events Release Notes]:http://support.integrationsoftware.se/support/discussions/forums/1000229123

