<tags data-value="Monitor Views, Non Events, Monitoring"></tags>

:fa-calendar: Monthly Intervals
=====
					


___

## :fa-info: Information
Monitor data that should occur on **Monthly Intervals**. [Non Events Agent Monitor][Non Events Agent] creates one [Resource][Resources] for all Monthly Intervals and can be monitored using suitable [Monitor Views][].  

___

## :fa-question-circle: Whats evaluated
The following rules are evaluated for **Monthly Intervals**

#### :fa-check-circle-o: OK - Used to indicate normal operation
* Current date is within the interval and not more than the max threshold. Or the last interval was OK  

<!--#### :fa-times-circle: Resource not available
* Error - Used to indicate an error/fatal condition-->  
<!--
#### :fa-times-circle: Error - Used to indicate an error/fatal condition
* The **Text** statement throws an exception with state = 1
* The **Stored Procedure** returns a fault code indicating error

#### :fa-exclamation-triangle: Warning - Used to indicate an unexpected but not invalid state
* The **Text** statement throws an exception with state > 1
* The **Stored Procedure** returns a fault code indicating warning -->

The evaluated state may be reconfigured using **Expected State** functionality that exists on every [Resource][Resources] within Integration Manager.
___

## :fa-edit: Configuration

New Monthly Intervals are added from the [Remote Configuration][]. Simply press **Add**  

![configure][png_AddMonthlyIntervals]
  
 
___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]  

##### :fa-cubes: Related  
:fa-database: [Non Events Agent][]  
:fa-folder-o: [Non Event Categories][]  
:fa-lightbulb-o: [Resources][]  
:fa-cloud-download: [Monitor Agents][]  
:fa-desktop: [Monitor Views][]    
-->

<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/EditSQLStatementAction.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/SQLStatementConfiguration.png
[png_AddMonthlyIntervals]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/NonEvents/AddMonthlyIntervals.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/ConfigureSQLStatement1.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/ConfigureSQLStatement2.png
[6]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/ConfigureSQLStatement3.png

[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master

[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Non%20Events/3.%20Configuration.md

[Non Event Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Non%20Events/Categories/Overview.md

[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master
[Applications]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/Applications.md?at=master

[Non Events Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Non-Events/Overview.md?at=master