<tags data-value="Monitor Views, Non Events, Monitoring"></tags>

:fa-calendar: Partial Floating Intervals
=====
					
___

## :fa-info: Information
Monitor data that should occur within a **Partial Floating Intervals**. [Non Events Agent Monitor][Non Events Agent] creates one [Resource][Resources] for each Partial Intervals and can be monitored using suitable [Monitor Views][].  

The Partial Floating Intervals is an type of a Partial Intervals but with a limited time for when to check for events.  

___

## :fa-question-circle: Whats evaluated
The following rules are evaluated for **Partial Floating Intervals**

#### :fa-check-circle-o: OK - Used to indicate normal operation
* Current time is within the partial interval floating time span (after the start and less then the end + time span).
* Not more events has occurred than the max threshold (checked directly).
* Not less events has occurred than the min threshold (checked after).
* The interval is not active

#### :fa-times-circle: Unavailable
* Error - Used to indicate an error
    * WebAPI not available
    * User has no access to the Log View
    * Bad/Changed setup of the Log View

You expect data at least each hour 7:00 to 16:00 monday to friday you need to add one for each day.  
Then you set the **Partial interval type** to Floating and check to for data each hour you set **Time span - Floating interval** to 1:00:00.

The max count is evaluated directly if to many events has occurred within the time span.  
The min count is evaluated 1 hour in in the partial interval, so a alarm is triggered 8:00 if no events has arrived between 7:00 and 8:00.

If no events has arrived at the end of the day between 15:00 and 16:00, the alarm will be displayed until 17:00, but will only check data between 15:00 and 16:00.


The evaluated state may be reconfigured using **Expected State** functionality that exists on every [Resource][Resources] within Integration Manager.
___

## :fa-edit: Configuration

New Partial Intervals are added from the [Remote Configuration][]. Simply press **Add**  

![configure][png_AddPartialIntervals]  
  

Configuration of the Partial Floating Interval.  

![configure][png_EditPartialInterval]

___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]  

##### :fa-cubes: Related  
:fa-database: [Non Events Agent][]  
:fa-folder-o: [Non Event Categories][]  
:fa-lightbulb-o: [Resources][]  
:fa-cloud-download: [Monitor Agents][]  
:fa-desktop: [Monitor Views][]    
-->

<!--References -->
[png_AddPartialIntervals]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/NonEvents/RemoteConfig/Tab_NonEvents_Tab_PartialIntervals.png
[png_EditPartialInterval]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/NonEvents/RemoteConfig/Tab_NonEvents_Tab_PartialIntervals_Edit_Floating.png

[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master

[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Non%20Events/3.%20Configuration.md

[Non Event Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Non%20Events/Categories/Overview.md

[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master
[Applications]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/Applications.md?at=master

[Non Events Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Non-Events/Overview.md?at=master