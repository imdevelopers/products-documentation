<tags data-value="Monitor,Non Events,Log"></tags>

:fa-clock: Non-Events Agent for Integration Manager
=============================================
The Non Events Monitor agent for Integration Manager alerts you when traffic patterns are of of your specifications. Make sure you get the alert(s) before your customer notices a problem. Improves the experience, quality and credibility of your system integration service. Enables your organization support and maintain integrations according to SLA commitments. Acts as a foundation for charging models. You cannot have SLA commitments without proper tooling like this non-event agent.  
					
## :fa-info: About
This agent allows you to monitor Non-Events on logged data by re-using Integration Manager's security restricted log views for business, IT-ops and other stake holders. This agent is easy to use; Simply copy the API link provided in the log view(s) and define the timespan to evaluate. This agent can be used on data logged from any platform over any selected timespan due to our superior long time logging support.

## :fa-sliders: Monitor Capabilities
* Individual settings for Exceptions and Warnings dependent on customizable thresholds
* Evaluates the actual number of logged events
    * Min - Trigger an alert if not at least number of messages have been executed
    * Max - Trigger an alert if more than expected number of messages have been executed
* Evaluate any message type from any/all endpoint(s)
* Evalute Ack/Nak
    * Correlates on any message type (technical/functional)
    * Correlate and match using data from payload/context
    * Timespan according to your configuration
    * Makes sure you stay within agreed SLA levels
        * Trigger an alert if outside boundaries
    * Alert information provides relevant information     
* Monthly messages - Messages comes within some defined interval, for example between the 10th and the 25th once and only once
* Smart filtering
    * Exclude vacation and or non-business dates
    * Supports user defined business hours
    * Content-based filtering.

### Examples
* Number of orders received in the last 30 days must be bigger than 500, but not bigger than 1500
* Ack/Nak - Send alert if response has not been logged in given time interval.
* Number of accepted orders from customer A on mondays between 8:03 to 13:37 must be less than 500.
* Number of accepted orders from customer A on mondays between 8:03 to wednesday 13:37 must be less than 500.
* Send warning alert if number of orders and invoices to customer A and B is too low, excluded dates: 17/5, 6/7 and 24/12.
* Send exception alert if response (Ack/Nak) has not been received in the given time interval.

## :fa-flash: Actions
* View all delayed Ack/Nacks
* View all missing Ack/Naks
* View current status
    * Full Interval
    * Partial Interval
* Quick **Edit** Remote Settings per Resource

## :fa-usd: Pricing
This agent and all other Monitor agents are part of the license for Integration Manager. No additional costs exists for the use of the non-event Monitor Agent for Integration Manager licencees. 

## :fa-list-alt: Release Log
For detailed information about the features and bug fixes, please see the [Release Log](http://support.integrationsoftware.se/discussions/forums/1000229110)
___

### :fa-hand-o-right: Next Step
:fa-rocket: [Install Non Events Monitor Agent][]  
:fa-edit: [Configuration][] of the agent  

#### :fa-cubes: Related
:fa-check-square-o: [PreRequisites for Non Events Agent][]  
:fa-desktop: [Monitor Views][]    


[Install Non Events Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Non-Events/2.%20Install.md?at=master  
[PreRequisites for Non Events Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Non-Events/1.%20PreRequisites.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Non-Events/3.%20Configuration.md?at=master
[Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Non-Events/Categories/Overview.md?at=master
         
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
