<tags data-value="Monitor,Views,Live,Health,Graph"></tags>

## :fa-cloud-upload: Monitor Agents  
Monitor the whole system integration chain, end to end, everywhere on any device!

___

## :fa-info: Information

 :fa-lightbulb-o: [Resources][] :fa-long-arrow-right: 
 :fa-upload: [Monitor Agent][Monitor Agents] :fa-long-arrow-right:
 :fa-download: [Source][Sources]  :fa-long-arrow-right:
 :fa-desktop: [Monitoring Service][] :fa-long-arrow-right:
 :fa-database: [Log Databases][] :fa-long-arrow-right:
 :fa-cloud: [Web API][] :fa-long-arrow-right:
 :fa-globe: [Web Client][] :fa-long-arrow-right:
 :fa-desktop: [Monitor View][Monitor Views] :fa-long-arrow-right:
 :fa-group: [Roles][] :fa-long-arrow-right:
 :fa-user: [User][Users]

Integration Manager uses the conecpt of **Monitor Agents** to monitor [Resources][]. These [Resources][] can be of any type of service, endpoint or function in the system integration chain. The design goal here is to be able to monitor  **end to end** and provide **metrics** , **alerts** and **remote actions** with relevant and up to date information to stakeholders. To further help organizations create personal independence, the documentation from the [Repository][] including the use of [Articles][] are tightly integrated.   

![MonitorAgents][1]  
*The list of supported agents grows over time. Special focus is currently put on supporting evolving Azure technologies.*

The [Monitor Agents][] are governed by the [Monitoring Service][] through the concept of [Sources][].  A [Source][Sources] configuration holds the connectivity (whereabouts) information for a **specific instance** of the Monitor Agent.

## Any Technology, Any Device
A **Monitor Agent** can be installed on-premise or in the cloud. The implemented design pattern is generic and provides a foundation for us, our customers and partners to extend and build their own [Monitor Agents][]. We provide Templates and SDKs for further custom development, please contact our support at support@integrationsoftware.se if your are interested in exploring this topic further.

The **Monitor Agents** we provide are usually Windows based (.NET) Service. 

* .NET Framework 4.0 or later running on-premise or in the cloud (.NET 4.6 or later is recommended for a full feature set)
    * Windows Server 2008-2016 
* [Node.js][nodejs] based running on just about any device
    * Windows Server 2008-2016
    * Linux
    * Apple
    * IOT devices

![AnyDevice][2]

## :fa-rocket: Installing and running

The Monitor agents must be installed and configured and eventually maintained over time.

Documentation wise the agents generally share the following structure:
1. PreRequisites
2. Install
3. Configure
4. Update
5. Uninstall

In general terms further outlined in this document is:

* :fa-exchange: Connectivity
* :fa-lock: Service account requirements
* :fa-file-text-o: Diagnostics
* :fa-calendar: Culture settings
* :fa-clone: Installation of multiple instances on same server

### :fa-exchange: Connectivity
The agents can be installed locally and/or in the cloud and/or partner/customer location off-site.
 
All Windows based agents supports the use of either **TCP/IP** (local or VPN access) or [Service Bus Relaying][] for communicating with the central [Monitoring Service][].
* TCP/IP based communication - local and VPN access. Is required with an authentication key to enable the [Remote Configuration][].
* [Service Bus Relaying][] - run anywhere. This connectivity type is secure by default.

The monitor agents are designed to conserve bandwidth and uses [**etag**][etag] caching for great performance and less costs (moneywise).

 ### :fa-lock: Service account requirements 
 The account running the Service requires a Windows account and depending on agent additional access rights to cloud- and line of business systems.
 
 * Logon rights
    * On Windows, the account running the **Monitor Agent** Service requires special rights, see [Logon As A Service][How to set Logon As A Service] for more information.
 * Access rights
    * Depending on specific agent

### :fa-file-text-o: Diagnostics
From the [Sources][] administration the diagnostic files can be easily downloaded without having to remote logon to servers using for example RDP.

![DownloadDiagnostics][3]  
This zip file is typically sent to our [support](mailto:support@integrationsoftware.se) whenever there is a question or an incident.

Use the  **Remote Configuration** modal to toggle the **debug** flag to increase verbosity level when logging (default unchecked).
![debugflag][4]

### :fa-calendar: Culture settings
Many of the **Monitor Agents** provides customization for formatting time related data.

Use the  **Remote Configuration** modal to change the **Culture Information**.
![CultureInformation][5]



### :fa-clone: Installation of multiple instances on same server

Any number of Monitor Agents may be installed on the same Windows Server as long as the name from the EndPoint Section in the ``%MonitorAgent%.exe.config`` is unique. **Name** (preferred change) or **Port** must be changed.

```
    NOTE: If you install multiple instances you cannot use the installer when updating other than the first installed service. Manual copy/past operations must then be performed from the first to the latter installations.
```
**1.** Install the first Monitor Agent according to documentation  
    **a.** Remember while installing to add environment name to the installation folder, for example ``PROD``

**2.** Copy the folder and Rename to for example ``TEST``

**3.** Edit the ``%.exe.config`` file using notepad or notepad++ from with elevated privileges (start from administrative cmd prompt)

**4.** Find the **EndPoint** xml element ´´<endpoint ``

![MultipleInstancesEndPoint][0]

**5.** Open Services.msc  
    **a.** Copy the Service Name from the first Monitor Agent to Notepad  
    **b.** Rename the Environment part (for example PROD -> TEST) making this service unique  
    **c.** Copy to clipboard/notepad instance for use in the next step

**6.** Open an Administrative Command Prompt and use the following command as template to install the new Monitor Agent (replace paths and environment)

```bat
sc create "%UNIQUE OTHER NAME OF MONITOR AGENT FROM CLIPBOARD%" binpath="C:\Program Files (x86)\Integration Software\IM Monitor Agent - Windows Server TEST\IM.MonitorAgent.WindowsServerHost.exe"
```
**7.** Change The startup type in the Services MMC to **Automatic (Delayed)**

**8.** Enter credentials

**9.** Start the service

**10.** The SourceInformation.txt file in the run-time folder will now have the proper address for use as a [Source][Sources]
```
    NOTE: Remember to copy (backup) the config file before updating the agent.
```

___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-cloud-download: [Add or manage Sources][]  
:fa-edit: [Remote Configuration][]  
:fa-tags: [Articles][]

##### :fa-cubes: Related  
 
 :fa-lightbulb-o: [Resources][] :fa-long-arrow-right: 
 :fa-upload: [Monitor Agent][Monitor Agents] :fa-long-arrow-right:
 :fa-download: [Source][Sources]  :fa-long-arrow-right:
 :fa-desktop: [Monitoring Service][] :fa-long-arrow-right:
 :fa-database: [Log Databases][] :fa-long-arrow-right:
 :fa-cloud: [Web API][] :fa-long-arrow-right:
 :fa-globe: [Web Client][] :fa-long-arrow-right:
 :fa-desktop: [Monitor View][Monitor Views] :fa-long-arrow-right:
 :fa-group: [Roles][] :fa-long-arrow-right:
 :fa-user: [User][Users]


[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/MultipleInstances.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/MonitorAgents.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/AnyDevice.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/DownloadDiagnostics.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/DebugFlag.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/CultureInformation.png

[Service Bus Relaying]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/ServiceBusRelaying.md?at=master
[etag]:https://en.wikipedia.org/wiki/HTTP_ETag
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master

[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master

[Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/Categories.md?at=master

[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Remote%20Configuration/Remote%20Configuration.md?at=master

[Monitor Views]:https://bytebucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[View All Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/1.%20View%20All%20Monitor%20Views/View%20All%20Monitor%20Views.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master

[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[View All Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/1.%20View%20All%20Monitor%20Views/View%20All%20Monitor%20Views.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master

[Applications]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/Applications.md?at=master
[Edit Applications]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/2.%20Edit%20Application/EditApplication.md?at=master

[Add or manage Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Edit Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/2.%20Edit%20Resource/Edit%20Resource.md?at=master
[View All Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/1.%20View%20All%20Resources/View%20All%20Resources.md?at=master

[Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/Categories.md?at=master
[View All Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/1.%20View%20All%20Categories/View%20All%20Categories.md?at=master
[Edit Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/2.%20Edit%20Category/Edit%20Category.md?at=master


[Authorization]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/Authorization.md?at=master

[Add or manage Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/2.%20Add%20or%20manage%20Role/Add%20or%20manage%20Role.md?at=master
[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[View All Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/18e6667b7d0bb9f0a784cc6ee0aa78ebc11185e6/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Web API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Web%20API/Overview.md?at=master
[Log Databases]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Log%20Databases/Log%20Databases.md?at=master
[nodejs]:https://nodejs.org/en/
[How to set Logon As A Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Logon%20As%20A%20Service.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master

[Articles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/8.%20Articles/Overview.md?at=master
