<tags data-value="Monitor,Windows,Performance Counters"></tags>

Performance Counters Agent
=============================================

Monitor Performance Counters with Integration Manager's monitor agent for Performance Counters.
					
## About
Windows Performance Counters are used to provide information as to how well the operating system or an application, service, or driver is performing. The Windows performance counter data can help determine system bottlenecks and fine-tune system and application performance. 

Monitoring these Windows performance counters becomes therefore important. 

Integration Manager’s agent for Performance Counters enables you to basically monitor each and every counter there is on each and every machine. Getting you the insights you need!

* Any number of Windows Performance counters / server can be monitored from a single agent
* Multiple agents can be deployed

## Monitor Capabilities
List of resources that can be monitored by using this agent

* Any available Performance Counter on any machine

## Actions
No actions have yet been implemented.