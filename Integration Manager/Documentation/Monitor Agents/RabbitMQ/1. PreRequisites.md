<tags data-value="Monitoring, RabbitMQ"></tags>

:fa-check-square-o: The RabbitMQ Monitoring Agent has the following pre requisites 
=====
					

___

## :fa-info: Information
This page describes the prerequisites for installing and running the [RabbitMQ Monitor Agent][RabbitMQ Agent].

## Software requirements

* :fa-windows: Microsoft Server 2008 R2 or later
    * .NET Framework 4.5.1 or later*

* Security/User rights (listed below)
* DTC must have XA transactions allowed

## :fa-free-code-camp: Firewall

* Default channel and listener port 1414 (may be overridden by configuration)
* Default outgoing port 443 to Internet for Service Bus Relayed connections
* Default inbound and outbound port 8000 **IM Monitor Agent** and **IM RabbitMQ Monitoring Agent**


## :fa-windows: Windows User Rights
* Local named account or domain account (preferred). Follow this guide '[How to set Logon As A Service][]' for more information.  
* Service Account running the **RabbitMQ Monitor Agent** must have appropriate MQ rights to read queues and content.

### :fa-hand-o-right: Next Step
:fa-rocket: [Install RabbitMQ Monitor Agent][]  

#### :fa-cubes: Related
:fa-plus-square: :fa-download: [Add or manage a Source][]  
:fa-desktop: [Monitor Agents][]    
:fa-cogs: [Administration][]    


<!--References -->
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master

[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[ServiceBusRelaying]:https://azure.microsoft.com/sv-se/documentation/articles/service-bus-dotnet-how-to-use-relay/
[Install RabbitMQ Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/RabbitMQ/2.%20Install.md?at=master
                            
[How to set Logon As A Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Logon%20As%20A%20Service.md?at=master
[RabbitMQ Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/RabbitMQ/Overview.md?at=master
