<tags data-value="Remote Configuration, Source, RabbitMQ"></tags>

:fa-edit: Configuration
=====
___

## :fa-info: Information
In this section you will learn how to configure and use the **IM RabbitMQ Monitor Agent** as a new [Source][Sources] for use within [Monitor Views][].

    Note: You must have installed the agent before configuration of the Source can be performed.
    
## :fa-rocket: [Install RabbitMQ Monitor Agent][]  
    
### :fa-sort-numeric-asc: Steps  
The following steps are required and must be performed in order after [installation][Install] before accessing the remote configuration:
1. Add a new [Source][Sources] 
After successful [Installation][Install] you must add a [Source][Sources] in the [Web Client][] to access the [Remote Configuration][] for the installed agent.  
:fa-plus-square: :fa-cloud-download: [Add or manage a Source][]  

2. Configure the connection between the [Monitoring Service][] and the [Monitor Agent][]  
Either use:  
    2.1 [TCP/IP (HTTP)][Port8000]
    2.2 [Service Bus Relaying][]

3. Use [Remote Configuration][] to configure the agent. Continued in topic below.

    Note: [Remote Configuration][] is available if the [Source][Sources] is authenticated or uses [Service Bus Relaying][]. 

4. Create [Monitor Views][] that uses the [Resources][] from the [Source][Sources] according to business needs.  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]

5. Fine tune specific settings using Actions if allowed by the [Monitor View][Monitor Views]  



:fa-edit: Remote Configuration
=====
The [Remote Configuration][] for the IM RabbitMQ Monitor Agent provides the ability to configure the agent remotely, even if the agent is installed off site.
Many specific configurations can also be made directly on the [Resource][Resources] using the **Action** button if the [Monitor View][Monitor Views] allows for Actions.

## :fa-wrench: Settings
There are some general settings for the agent available in the **Settings** tab.
* :fa-globe: **Environment**
* :fa-bug: **Debug**
* :fa-clock-o: **Culture Information**

![png_RabbitMQServerSettings][]

### :fa-globe: Environment
Part of the common features shared with all [Monitor Agents][Monitor Agent], there is an option to set the name of the target **Environment**, for example TEST, QA, PROD.  

### :fa-bug: Debug
Part of the common features shared with all [Monitor Agents][Monitor Agent], there is an option to set the **Debug** flag for additional file logging that can be enabled/disabled as needed. Default is unchecked.

### :fa-clock-o: Culture Information
The 'Culture Information' setting determines how date time will be rendered for end users.

## :fa-server: RabbitMQ
The agent can monitor any number of RabbitMQ brokers. Add and remove brokers from the 'RabbitMQ' tab.  
 ![png_RemoteConfigurationAddQueueManagers][]

[Resources][] per host will be grouped by [Application][Applications] and then by [Category][Categories] using the value from the **Name** text field. Read more about [Applications][] here:
* :fa-dropbox: [Applications][]  

![png_ApplicationFilter][]

### :fa-edit: Configure the added server
For each broker, the following properties can be set:

 * **Display Name** - User friendly name for RabbitMQ queue manager
 * **API Uri** - Address to the management API.
 * **User name** - Authenticate as user
 * **Password** - password for user

![png_RabbitMQTabs][]

### :fa-folder: RabbitMQ Categories
The following **Categories** exists and settings and monitor behavior may be further customized:
* Queues

For configuration of specific RabbitMQ Server Categories, see [RabbitMQ Monitor Agent Categories][Categories]:

![categories][0]

 ## Queue
Monitor settings can be **global** and **specific**.

 ### Global Settings Queues
Global settings are used for evaluations of the queues if not specific settings have been set.

* **Evaluation Type**
* **Warning Time Span**
* **Error Time Span**
* **Exclude filter**

 ![queues][png_QueuesTab]


### Evaluation Type
Different evaluation options exists:
* **None**
* **Fixed**
* **Percent**
* **Which comes first**

![evaluationtypes][2]


#### None
Queue is not evaluated on Fixed or Percent (only time).

#### Fixed
Queue is evaluated on the number of messages

* Warning Count
* Error Count

![fixed][3]

#### Percent
Queue is evaluated on the number of messages in percent of total allowed (Quota Evaluation).

* Warning Limit %
* Error Limit %

![percent][4]

#### Which comes first
Queue is evaluated on both **Fixed** and **Percent**, Whichever happens to Come First.

![WhichComesFirst][5]

### Warning Time Span
* **Warning Time Span** The longest allowed time span before Warning alert is raised should be in format (days.hours:minutes:seconds e.g 7.12:30:59). 

    NOTE:  Format is days.hours:minutes:seconds e.g 7.12:30:59

### Error Time Span
* **Error Time Span** The longest allowed time span before Error alert is raised should be in format (days.hours:minutes:seconds e.g 7.12:30:59).  

    NOTE:  Format is days.hours:minutes:seconds e.g 7.12:30:59

### :fa-filter: Exclude filter
Resources can be excluded using a **RegEx** based filter

Example resource filter: Remove all queues that starts with the letter '_'
```regular
^_
```

![filter][6]

 ### Specific Settings
Same properties to set as global but the specific settings has precedence over global settings.

![specific][1]

## Disk
Monitor settings can be **global** and **specific**.

 ### Global Settings Disk
Global settings are used for evaluations of nodes disk space if not specific settings have been set.

* **Evaluation Type**

 ![png_DiskTab][]

### Evaluation Type
Different evaluation options exists:
* **None**
* **Fixed**

![evaluationtypes][png_EvaluationTypeDisk]

#### None
Disk space is not generally evaluated (only for specific configured nodes).

#### Fixed
Disk is evaluated on amount of free space left before reaching the RabbitMQ disk limit.

* Warning Limit (MB)
* Error Limit (MB)

 ### Specific Settings
Same properties to set as global but the specific settings has precedence over global settings.

![specific][png_SpecificDisk]

 ## Memory
Monitor settings can be **global** and **specific**.


 ### Global Settings Memory
Global settings are used for evaluations of nodes memory space if not specific settings have been set.

* **Evaluation Type**

 ![png_MemoryTab][]

### Evaluation Type
Different evaluation options exists:
* **None**
* **Fixed**
* **Percent**

![evaluationtypes][png_EvaluationTypeMemory]

#### None
Memory space is not generally evaluated (only for specific configured nodes).

#### Fixed
Disk is evaluated on amount of free memory left before reaching the RabbitMQ memory limit.

* Warning Limit (MB)
* Error Limit (MB)

#### Percent
Disk is evaluated on amount of free memory left in percent of RabbitMQ memory limit.

 ### Specific Settings
Same properties to set as global but the specific settings has precedence over global settings.

![specific][png_SpecificMemory]

##-------

## :fa-save: Save
You must click **Save** for changes to be written to the agent and take effect.   
![png_SaveAndClose][]

    Note: Depending on the synchronization interval set for the agent, there might be a delay before the Web Client reflects upon the change. You can choose to force the agent to synchronize from the configuration of the Source. 

**Save** - Saves the current changes, browser refreshes content and stays on current page.

**Save and close** - Saves then closes the dialog returning the browser to the previous page.

**Cancel** Close the dialog without saving any changes.

___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-cloud-download: [Add or manage a Source][]  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]   
:fa-arrow-circle-o-up: [Update][]  

##### :fa-cubes: Related  
:fa-rocket: [Install RabbitMQ Monitor Agent][Install]   
:fa-cloud-download: [Source][Sources]  
:fa-dropbox: [Applications][]  


<!--References -->
[png_RabbitMQServerSettings]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/RabbitMQ/Settings.png
[png_RemoteConfigurationAddQueueManagers]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/RabbitMQ/RabbitMQTab.png
[png_SaveAndClose]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/SaveAndClose.png
[png_ApplicationFilter]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/RabbitMQ/ApplicationFilter.png
[png_RabbitMQTabs]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/RabbitMQ/QueueManager.png

[png_QueuesTab]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/RabbitMQ/QueuesTab.png
[png_DiskTab]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/RabbitMQ/DiskTab.png
[png_MemoryTab]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/RabbitMQ/MemoryTab.png
[png_EvaluationTypeDisk]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/RabbitMQ/EvaluationTypeDisk.png
[png_EvaluationTypeMemory]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/RabbitMQ/EvaluationTypeMemory.png
[png_SpecificDisk]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/RabbitMQ/SpecificDisk.png
[png_SpecificMemory]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/RabbitMQ/SpecificMemory.png

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/RabbitMQ/Categories.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/RabbitMQ/SpecificQueue.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/RabbitMQ/EvaluationType.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/RabbitMQ/Fixed.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/RabbitMQ/Percent.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/RabbitMQ/WhichComesFirst.png
[6]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/RabbitMQ/ExcludeFilter.png

[Install]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/RabbitMQ/2.%20Install.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Remote%20Configuration/Remote%20Configuration.md?at=master
[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[View All Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/1.%20View%20All%20Monitor%20Views/View%20All%20Monitor%20Views.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master
[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master  
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[YouTube]:https://www.youtube.com/watch?v=-ky8QqyQAHE**
[Service Bus Relaying]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/ServiceBusRelaying.md?at=master
[Install RabbitMQ Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/RabbitMQ/1.%20Install.md?at=master
[Applications]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/Applications.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/RabbitMQ/3.%20Configuration.md?at=master

[Update]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/RabbitMQ/4.%20Update.md?at=master

[Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/RabbitMQ/Categories/Overview.md?at=master
[Port8000]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Shared%20Port%208000.md?at=master