<tags data-value="Monitor,Azure,App Services,Logic Apps"></tags>

:fa-rocket: RabbitMQ - Disk
=============================================

## :fa-info: Information
Monitor the state of your **RabbitMQ nodes disk**. The RabbitMQ Monitor Agent creates one [Resource][Resources] per node that can be monitored using suitable [Monitor Views][].   
___

## :fa-question-circle: Whats evaluated
The following rules are evaluated for **disk**:

#### :fa-check-circle-o: OK - Used to indicate normal operation
* Free disk space is more then specified warning or error thresholds

#### :fa-exclamation-triangle: Warning - Used to indicate a warning condition
* Free disk space is less then specified warning thresholds

#### :fa-times-circle: Error - Used to indicate an error/fatal condition
* Free disk space is less then specified error thresholds

The evaluated state may be reconfigured using **Expected State** functionality that exists on every [Resource][Resources] within Integration Manager.
___

## :fa-edit: Configuration
    
___

## :fa-flash: Actions
The RabbitMQ Agent has support for remote actions. The following Actions are exposed:  
* Edit

 ![png_ActionEdit][]

<br/>
      
#### :fa-bolt: Edit

The dialog displayed from the action **Edit**  

 ![png_ActionEditSpecificDisk][]

Here it is possible to configure specific evaluation for a certain node. The evaluation has precedence over the global disk evaluation. The same options are available as in remote configuration for disk.
___

## Next step
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]  

#### :fa-cubes: Related  
:fa-lightbulb-o: [Resources][]  
:fa-cloud-download: [Monitor Agents][]  
:fa-desktop: [Monitor Views][]    

<!--References -->
[png_ActionEditSpecificDisk]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/RabbitMQ/ActionEditSpecificDisk.png
[png_ActionEdit]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/RabbitMQ/Action.png

[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master

