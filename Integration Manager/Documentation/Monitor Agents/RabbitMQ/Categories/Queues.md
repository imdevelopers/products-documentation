<tags data-value="Monitor,Azure,App Services,Logic Apps"></tags>

:fa-rocket: RabbitMQ - Queues
=============================================

## :fa-info: Information
Monitor the state of your **RabbitMQ queues**. The RabbitMQ Monitor Agent creates one [Resource][Resources] per queue can be monitored using suitable [Monitor Views][].   
___

## :fa-question-circle: Whats evaluated
The following rules are evaluated for **queues**:

#### :fa-check-circle-o: OK - Used to indicate normal operation
* Queue doesn't exceed specified warning or error thresholds

#### :fa-exclamation-triangle: Warning - Used to indicate a warning condition
* Queue exceeds specified warning thresholds

#### :fa-times-circle: Error - Used to indicate an error/fatal condition
* Queue exceeds specified error thresholds

The evaluated state may be reconfigured using **Expected State** functionality that exists on every [Resource][Resources] within Integration Manager.
___

## :fa-edit: Configuration
    
___

## :fa-flash: Actions
The RabbitMQ Agent has support for remote actions. The following Actions are exposed:  
* Edit

 ![png_ActionEdit][]

<br/>
      
#### :fa-bolt: Edit

The dialog displayed from the action **Edit**  

 ![png_ActionEditSpecificQueue][]

Here it is possible to configure specific evaluation for a certain queue. The evaluation has precedence over the global queue evaluation. The same options are available as in remote configuration for queues.
___

## Next step
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]  

#### :fa-cubes: Related  
:fa-lightbulb-o: [Resources][]  
:fa-cloud-download: [Monitor Agents][]  
:fa-desktop: [Monitor Views][]    

<!--References -->
[png_ActionEditSpecificQueue]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/RabbitMQ/ActionEditSpecificQueue.png
[png_ActionEdit]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/RabbitMQ/Action.png

[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master

