<tags data-value="Monitor,RabbitMQ,Queue"></tags>

:fa-reorder: RabbitMQ Agent
=============================================


![logo][svg_rabbitmqlogo]  
Monitor [RabbitMQ](https://www.rabbitmq.com/) with Integration Manager's **RabbitMQ Monitor agent**.
					
## :fa-info: About
This agent allows you to monitor RabbitMQ related resources like cluster, nodes, queues. For example, the **RabbitMQ Monitor agent** can evaluate the number of messages on the queue(s). Also the maximum age of the first message on the queue can be verified if [RabbitMQ Message TimeStamp plugin](https://github.com/rabbitmq/rabbitmq-message-timestamp) is installed. An administrator can decide in the configuration which broker/node and related settings for queues to monitor, as well as the number of messages on which Integration Manager should warn / fail for. 

This service has been developed due to the fact that in many cases, that if queues contain messages (or more then a certain amount) it is in many cases some kind of error, at least if the messages are there longer than an expected time span (e.g. 2 hours). This agent enables you to monitor each and every queue that is important for you and your business.

## :fa-sliders: Monitor Capabilities
List of resources that can be monitored by using this agent

* Queues
    * Global settings
    * Per queue specific settings 

    * Age verification (requires message timestamp plugin)
    * Count (warning / error)
    * Quota-evaluation

* Dead letter 

## :fa-flash: Actions
Edit evaluation type for a specific queue.

## :fa-usd: Pricing
This agent and all other Monitor agents are part of the license for Integration Manager. No additional costs exists for the use of the non-event Monitor Agent for Integration Manager licencees. 

## :fa-list-alt: Release Log
For detailed information about the features and bug fixes, please see the [Release Log](https://support.integrationsoftware.se/discussions/forums/1000229324)
___

### :fa-hand-o-right: Next Step
:fa-rocket: [Install RabbitMQ Monitor Agent][]  
:fa-edit: [Configuration][] of the agent  

#### :fa-cubes: Related
:fa-check-square-o: [PreRequisites for RabbitMQ Monitor Agent][]  
:fa-desktop: [Monitor Views][]    

[Install RabbitMQ Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/WMQ/2.%20Install.md?at=master  
[PreRequisites for RabbitMQ Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/WMQ/1.%20PreRequisites.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/WMQ/3.%20Configuration.md?at=master
[Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/WMQ/Categories/Overview.md?at=master
         
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[svg_rabbitmqlogo]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/RabbitMQ/RabbitMQlogo.svg
