<tags data-value="Monitor,Agent,SQL Server,SQL,Database"></tags>

:fa-database: Azure SQL Databases
=============================================

Support for **Azure SQL Databases** is provided from within the **[SQL Server Monitor Agent][]**
	

#### :fa-cubes: Related
:fa-check-square-o: [SQL Server Monitor Agent][]  


[SQL Server Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Overview.md?at=master  


