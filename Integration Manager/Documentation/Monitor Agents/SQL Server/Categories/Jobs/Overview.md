<tags data-value="Monitor Views, SQL Server, Monitoring"></tags>

:fa-database: Jobs
=====
					


___

## :fa-info: Information
Monitor how SQL Agent jobs perform. [SQL Agent Monitor][SQL Agent] automatically creates one [Resource][Resources] per SQL Agent Job and can be monitored using suitable [Monitor Views][].

    Note: SQL Express and Azure SQL Databases does not have an SQL Agent and SQL Jobs does therefore not exists 

___

## :fa-question-circle: Whats evaluated
The following rules are evaluated for a SQL Job:

#### :fa-check-circle-o: OK - Used to indicate normal operation
* **Enabled** and **running**

#### :fa-times-circle: Resource not available - 
* Error - Used to indicate an error/fatal condition

#### :fa-times-circle: Error
* A job that **fails** will render state 'Error'

#### :fa-exclamation-triangle: Warning  - Used to indicate an unexpected but not invalid state
* A **disabled** job 
* A job **not running** 
* A job that has **never run**
* A job that has not run **within specified time interval** 
* A job that runs for a longer **duration** than specified    

The evaluated state may be reconfigured using **Expected State** functionality that exists on every [Resource][Resources] within Integration Manager.
___

## :fa-edit: Configuration

SQL Jobs are enabled from the [Remote Configuration][]. Simply check **Enable monitoring of SQL Jobs**  
All SQL Jobs in the SQL Instance are monitored.

![configure][4]

Set the following properties, (global for all jobs):
* Application Id - the Id of an Application to assign an application to the Resources
* Warning Time Span - The longest allowed timespan for last run
* Error Time Span - The longest allowed timespan for last run
* Duration Warning - The longest allowed duration for SQL Job in seconds (any negative value disables the check)
* Description - User friendly description for resource

Timespan format (days.hours.minutes.seconds)
___

 ## :fa-flash: Actions
The [SQL Agent][] has support for remote actions. The folllowing Actions are exposed:
* View SQL Job History
* Start Job
* Enable
* Disable
* Edit SQL Job Configuration     
 ![actions][1]
 
 ### :fa-history: View SQL Job History
View the last n runs of the selected job. The number of available logs can be set from within SQL Server Managmenent Studio.  
 ![history][2]

In order to conserve bandwidth and improve performance the list is presented using pagination. 

The list can be updated by simply clicking the *Reload* button in the upper right corner, without the need to reopen the form.

### :fa-play-circle: Start Job

The dialog displayed from the action **Start Job**.  Click *Yes* to Start the Job, see **View SQL Job History** for outcome of the job.  

 ![actions][png_StartJob]

### :fa-toggle-on: Enable

The dialog displayed from the action **Enable**.  Click *Yes* to Enable the Job.  

 ![actions][png_Enable]
  
### :fa-toggle-off: Disable

The dialog displayed from the action **Disable**.  Click *Yes* to Disable the Job.   

 ![actions][png_Disable]
 
### :fa-edit: Edit SQL Job Configuration
Edit the thresholds with ease for a specific SQL Job.   
 ![edit][3]

The **Default** values are set using [Remote Configuration][]. These *global* settings can be overridden by specific jobs using the 'Edit SQL Job Configuration' action.


The following properties can be set:
* [Application][Applications] - a way of grouping resources
* Description, a user friendly description for the job
* Expected State - Checked if the the job is expected to be enabled
* Allowed Timespan for last evaluated run, expressed using timespan property: (days.hours:minutes:seconds)
    * Warning: Allowed time before state is evaluated as Warning 
    * Error: Allowed time before state is evaluated as Error
* Duration for last evaluated run, expressed in seconds (any negative value disables the check)
    * Warning: Allowed time before the job is evaluated as in the Warning state
    * Error: Allowed time before the job is evaluated as in the Error state

You must click **Save** for changes to be written to the agent and take effect. 

    Note: Depending on the synchronization interval set for the agent, there might be a delay before the Web Client reflects upon the change. You can choose to force the agent to synchronize from the configuration of the Source. 

___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]  

##### :fa-cubes: Related  
:fa-database: [SQL Agent][]  
    :fa-folder-o: [SQL Categories][]  
:fa-bulb: [Resources][]  
:fa-cloud-download: [Monitor Agents][]  
:fa-desktop: [Monitor Views][]    


<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/JobActions.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/JobHistory.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/JobEditThresholds.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/ConfigureSQLJobs.png
[png_StartJob]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/JobStartJob.png
[png_Enable]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/JobEnable.png
[png_Disable]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/JobDisable.png

[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master

[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Remote%20Configuration/Remote%20Configuration.md?at=master

[SQL Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/Overview.md
[Azure - SQL Size Checks]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/Size%20Checks/Azure%20-%20SQL%20Size%20Checks.md
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master
[Applications]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/Applications.md?at=master

[SQL Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Overview.md