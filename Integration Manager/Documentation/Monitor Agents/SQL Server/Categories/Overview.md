<tags data-value="Monitor Views, SQL Server, Monitoring"></tags>

:fa-database: SQL Monitor Categories
=====
					


___

## :fa-info: Information
The various monitoring capabilities for the SQL Server agent are expressed as [Resources][] and are grouped by [Categories][].
The **Categories** are unique for each [Monitoring Agent][Monitor Agent]. 

The following Categories are exposed by the **SQL Monitor Agent**.

## :fa-folder: Categories
* [Jobs][]
* [SSIS][]
* Size Checks
    * [SQL Size Checks][]
    * [SQL Size Checks - Specific][]
    * [Azure - SQL Size Checks][] 
* Backup
    * [SQL Backups][]
    * [SQL Backups - Specific][]
* Statements
    * [SQL Statements][]
    * [Azure SQL Statements][]
  
 ## :fa-edit: Remote Configuration
 The [SQL Agent Monitor][SQL Agent] has support for [Remote Configuration][] and monitors configured databases provided by the connection strings. 
 
 First the  [SQL Agent Monitor][SQL Agent] must be configured using [Remote Configuration][SQL Agent Remote Configuration] set on the [Source][Sources].
 
___

### :fa-hand-o-right: Next Step  
:fa-edit: [Configuration][] of the agent  
:fa-database: [SQL Agent][]

##### :fa-cubes: Related  
:fa-desktop: [Monitor Views][]    
:fa-cloud-download: [Sources][]  
:fa-cloud-upload: [Monitor Agent][]  
    :fa-lightbulb-o: [Resources][]  

<!--References -->
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master
[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Remote%20Configuration/Remote%20Configuration.md?at=master

[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master  

[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/3.%20Configuration.md?at=master

[Jobs]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/Jobs/Overview.md

[SSIS]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/SSIS/Overview.md

[SQL Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Overview.md

[Azure - SQL Size Checks]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/Size%20Checks/Azure%20-%20SQL%20Size%20Checks.md
[SQL Size Checks]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/Size%20Checks/SQL%20Size%20Checks.md
[SQL Size Checks - Specific]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/Size%20Checks/SQL%20Size%20Checks%20-%20Specific.md
[SQL Agent Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Remote%20Configuration/Overview.md
[SQL Backups]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/SQL%20Backups/SQL%20Backups.md?at=master
[SQL Backups - Specific]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/SQL%20Backups/SQL%20Backups-%20Specific.md?at=master
[SQL Statements]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/SQL%20Statements/SQL%20Statements.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Azure SQL Statements]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/SQL%20Statements/Azure%20-%20SQL%20Statements.md?at=master
[Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/Categories.md?at=master
