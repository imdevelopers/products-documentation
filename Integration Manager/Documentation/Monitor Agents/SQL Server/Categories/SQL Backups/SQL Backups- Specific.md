<tags data-value="Monitor Views, SQL Server, Monitoring"></tags>

:fa-database: Backup
=====
					


___

## :fa-info: Information
Monitor the size of **Microsoft SQL Server Databases**. [SQL Agent Monitor][SQL Agent] creates one [Resource][Resources] per SQL Server Database and can be monitored using suitable [Monitor Views][].

## :fa-retweet: General and Specific settings
The difference between the [SQL Backups][SQL Backups] and the **SQL Backups - Specific** is:  
* Global default threshold values are used in [SQL Backups][SQL Backups]
    * Applies to all non-specific databases  
* Customized threshold values are used in **SQL Backups - Specific**
    * Applied on only one (specific) database   

___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]  

##### :fa-cubes: Related   
:fa-database: [SQL Agent][]  
:fa-folder-o: [SQL Categories][]  
:fa-database: [SQL Backups][]  
:fa-lightbulb-o: [Resources][]  
:fa-cloud-upload: [Monitor Agents][]  
:fa-cloud-download: [Sources][]  
:fa-desktop: [Monitor Views][]    


<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/ActionEditBackupSettings.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/BackupEditSpecific.png

[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master

[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Remote%20Configuration/Remote%20Configuration.md?at=master

[SQL Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/Overview.md

[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master
[Applications]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/Applications.md?at=master

[SQL Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Overview.md
[SQL Backups]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/SQL%20Backups/SQL%20Backups.md?at=master


