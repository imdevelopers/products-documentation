<tags data-value="Monitor Views, SQL Server, Monitoring"></tags>

:fa-database: Backup
=====
					


___

## :fa-info: Information
Monitor the size of **Microsoft SQL Server Databases**. [SQL Agent Monitor][SQL Agent] creates one [Resource][Resources] per SQL Server Database and can be monitored using suitable [Monitor Views][].

## :fa-retweet: General and Specific settings
The difference between the **SQL Backups** and the **SQL Backups - Specific** is:  
* Global default threshold values are used in **SQL Backups**
    * Applies to all non-specific databases  
* Customized threshold values are used in **SQL Backups - Specific**
    * Applied on only one (specific) database   
 
___

## :fa-question-circle: Whats evaluated
The following rules are evaluated for SQL Backups:

#### :fa-check-circle-o: OK - Used to indicate normal operation
* The backup is **below** threshold values

#### :fa-times-circle: Unavailable - Resource not available 
* The Monitoring Server fails to communicate with the Monitoring Agent  

#### :fa-times-circle: Error - Used to indicate an error/fatal condition
* The database backup is **over** (older) than the error threshold value

#### :fa-exclamation-triangle: Warning - Used to indicate an unexpected but not invalid state
* The database backup is **over** (older) than the warning threshold value
* Backup has never been taken (Missing backup)

The evaluated state may be reconfigured using **Expected State** functionality that exists on every [Resource][Resources] within Integration Manager.
___

## :fa-edit: Configuration

SQL Backups are enabled from the [Remote Configuration][]. Simply check **Enable monitoring of SQL backups**  
Alla databases in the SQL Instances are monitored.

![configure][3]

Set the following properties, (global for all databases):
* Application Id - the Id of an Application to assign an application to the Resources
* Warning Time Span - The longest allowed timespan for warning on last backup
* Error Time Span - The longet allowed timestapn for error on last backup
* Description - User friendly description for resource

Timespan format (days.hours.minutes.seconds)
___

 ## :fa-flash: Actions
The [SQL Agent][] has support for remote actions. The folllowing Actions are exposed:  
* Edit Backup Configuration.  
  
 ![actions][1]
 
### :fa-edit: Edit Backup Configuration
Edit the thresholds with ease for the SQL Server Database.  
 ![edit][2]

The **Default** values are set using [Remote Configuration][]. These *global* settings can be overridden by using the 'Edit Backup Configuration' action.

    Note: Category is changed to 'SQL Backups - Specific' when the threshold values are modified.

The time of the last backup taken will be presented if it exists, else the text will be *Missing backup*.

The following properties can be set:
* [Application][Applications] - a way of grouping resources
* Description, a user friendly description for the database
* Timespan
    * Warning: Allowed timespan before state is evaluated as Warning 
    * Error: Allowed timespan before state is evaluated as Error
 
You must click **Save** for changes to be written to the agent and take effect. 

    Note: Depending on the synchronization interval set for the agent, there might be a delay before the Web Client reflects upon the change. You can choose to force the agent to synchronize from the configuration of the Source. 

___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]  

##### :fa-cubes: Related  
:fa-database: [SQL Agent][]  
:fa-folder-o: [SQL Categories][]  
:fa-database: [SQL Backups Specific][]  
:fa-lightbulb-o: [Resources][]  
:fa-cloud-download: [Monitor Agents][]  
:fa-desktop: [Monitor Views][]    


<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/ActionEditBackupSettings.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/BackupEditSpecific.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/ConfigureSQLBackups.png

[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master

[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/3.%20Configuration.md

[SQL Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/Overview.md
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master
[Applications]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/Applications.md?at=master
[SQL Backups Specific]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/SQL%20Backups/SQL%20Backups-%20Specific.md?at=master

[SQL Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Overview.md