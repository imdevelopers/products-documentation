<tags data-value="Monitor Views, SQL Server, Monitoring"></tags>

:fa-database: SQL Instance
=====
					


___

## :fa-info: Information
Monitor the SQL Instance for **Microsoft SQL Server Databases**. [SQL Agent Monitor][SQL Agent] creates one [Resource][Resources] per SQL Connection string and can be monitored using suitable [Monitor Views][].

___

## :fa-question-circle: Whats evaluated
The following rules are evaluated for a SQL Instance:

#### :fa-check-circle-o: OK - Used to indicate normal operation
* The SQL Servier instance is online, operational and accessible

#### :fa-times-circle: Unavailable - Resource not available 
* The Monitoring Server fails to communicate with the Monitoring Agent  

#### :fa-times-circle: Error - Used to indicate an error/fatal condition
* The SQL Instance is not available or cannot be reached

#### :fa-exclamation-triangle: Warning - Used to indicate an unexpected but not invalid state
* Not implemented

The evaluated state may be reconfigured using **Expected State** functionality that exists on every [Resource][Resources] within Integration Manager.
___

## :fa-edit: Configuration
    
Configuration of SQL Instances are described in [Remote Configuration][].

___

 ## :fa-flash: Actions
The [SQL Agent][] has support for remote actions. The folllowing Actions are exposed:  
* View SQL Server Log
  
 ![actionviewsqlserverlog][1]
 
    Note: Large logs are streamed from a file and may not always succeed to load within the web client. 

## View SQL Server Log
You can view the current log for SQL Server Instance. To reduce network traffic the result is displayed using pagination.

![viewsqlserverlog][2]
 
___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]  

##### :fa-cubes: Related  
:fa-database: [Azure - SQL Size Checks][]  
:fa-database: [SQL Agent][]  
:fa-folder-o: [SQL Categories][]  
:fa-lightbulb-o: [Resources][]  
:fa-cloud-download: [Monitor Agents][]  
:fa-desktop: [Monitor Views][]    


<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/ActionViewSQLServerLog.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/ViewSQLInstanceAction.png

[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master

[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/3.%20Configuration.md

[SQL Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/Overview.md
[Azure - SQL Size Checks]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/Size%20Checks/Azure%20-%20SQL%20Size%20Checks.md
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master
[Applications]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/Applications.md?at=master

[SQL Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Overview.md
