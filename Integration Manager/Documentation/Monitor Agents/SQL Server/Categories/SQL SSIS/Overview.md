<tags data-value="Monitor Views, SQL Server, Monitoring"></tags>

:fa-database: SSIS
=====
					


___

## :fa-info: Information
Monitor how SQL SSIS execution perform. [SQL Agent Monitor][SQL Agent] automatically creates one [Resource][Resources] per SQL SSIS package and can be monitored using suitable [Monitor Views][].

___

## :fa-question-circle: Whats evaluated
The following rules are evaluated for a SQL SSIS:

#### :fa-check-circle-o: OK - Used to indicate normal operation
* **Enabled** and **running**

#### :fa-times-circle: Resource not available - 
* Error - Used to indicate an error/fatal condition

#### :fa-times-circle: Error
* A SSIS execution that **fails** will render state 'Error'

The evaluated state may be reconfigured using **Expected State** functionality that exists on every [Resource][Resources] within Integration Manager.
___

## :fa-edit: Configuration

SQL SSIS are enabled from the [Remote Configuration][]. Simply check **Enable monitoring of SQL SSIS Packages**  
All SQL SSIS in the SQL Instance are monitored.

![configure][png_ConfigureSQLSSIS]

Set the following properties, (global for all SSIS executions):
* Application Id - the Id of an Application to assign an application to the Resources
* Warning Time Span - The longest allowed timespan for last run
* Error Time Span - The longest allowed timespan for last run
* Duration Warning - The longest allowed duration for SQL SSIS in seconds (any negative value disables the check)
* Description - User friendly description for resource

Timespan format (days.hours.minutes.seconds)
___

 ## :fa-flash: Actions
The [SQL Agent][] has support for remote actions. The following Actions are exposed:
* View SSIS run history
 ![actions][png_SSISActions]
 
 ### :fa-history: View SQL SSIS run History
View the last n runs of the selected SSIS package. The number of available logs can be set from within SQL Server Management Studio. See about [retention window][].
 ![history][png_SSISHistory]

In order to conserve bandwidth and improve performance the list is presented using pagination. 

The list can be updated by simply clicking the *Reload* button in the upper right corner, without the need to reopen the form.

The **Default** values are set using [Remote Configuration][]. These *global* settings can be overridden by specific SSIS using the 'Edit SQL SSIS Configuration' action.


The following properties can be set:
* [Application][Applications] - a way of grouping resources
* Description, a user friendly description for SSIS
* Expected State - Checked if SSIS is expected to be enabled
* Allowed Timespan for last evaluated run, expressed using timespan property: (days.hours:minutes:seconds)
    * Warning: Allowed time before state is evaluated as Warning 
    * Error: Allowed time before state is evaluated as Error
* Duration for last evaluated run, expressed in seconds (any negative value disables the check)
    * Warning: Allowed time before SSIS is evaluated as in the Warning state
    * Error: Allowed time before SSIS is evaluated as in the Error state

You must click **Save** for changes to be written to the agent and take effect. 

    Note: Depending on the synchronization interval set for the agent, there might be a delay before the Web Client reflects upon the change. You can choose to force the agent to synchronize from the configuration of the Source. 

___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]  

##### :fa-cubes: Related  
:fa-database: [SQL Agent][]  
    :fa-folder-o: [SQL Categories][]  
:fa-bulb: [Resources][]  
:fa-cloud-download: [Monitor Agents][]  
:fa-desktop: [Monitor Views][]    


<!--References -->
[png_SSISActions]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/SSISActions.png
[png_ConfigureSQLSSIS]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/ConfigureSQLSSIS.png
[png_SSISHistory]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/SSISHistory.png


[retention window]:https://www.mssqltips.com/sqlservertip/3307/managing-the-size-of-the-sql-server-ssis-catalog-database/
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master

[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Remote%20Configuration/Remote%20Configuration.md?at=master

[SQL Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/Overview.md
[Azure - SQL Size Checks]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/Size%20Checks/Azure%20-%20SQL%20Size%20Checks.md
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master
[Applications]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/Applications.md?at=master

[SQL Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Overview.md