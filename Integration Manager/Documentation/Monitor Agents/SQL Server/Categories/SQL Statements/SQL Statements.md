<tags data-value="Monitor Views, SQL Server, Monitoring, SQL Databases"></tags>

:fa-database: SQL Statements
=====
					


___

## :fa-info: Information
Monitor your own customized **SQL Statements**. [SQL Agent Monitor][SQL Agent] creates one [Resource][Resources] for the SQL Statement and can be monitored using suitable [Monitor Views][].  

___

## :fa-question-circle: Whats evaluated
The following rules are evaluated for **SQL Statements**

#### :fa-check-circle-o: OK - Used to indicate normal operation
* The statement is successful  

#### :fa-times-circle: Resource not available
* Error - Used to indicate an error/fatal condition  

#### :fa-times-circle: Error - Used to indicate an error/fatal condition
* The **Text** statement throws an exception with state = 1
* The **Stored Procedure** returns a fault code indicating error

#### :fa-exclamation-triangle: Warning - Used to indicate an unexpected but not invalid state
* The **Text** statement throws an exception with state > 1
* The **Stored Procedure** returns a fault code indicating warning

The evaluated state may be reconfigured using **Expected State** functionality that exists on every [Resource][Resources] within Integration Manager.
___

## :fa-edit: Configuration

New SQL Statements are added from the [Remote Configuration][]. Simply press **Add**  

![configure][3]
  
    
Configuration of SQL Statements:

![configure][4]

Set the following properties:
* Application Id - the Id of an Application to assign an application to the Resources
* Display Name - user friendly name of this resource as provided to end users within Integration Manager
* Command Type - the type of SQL command, **Text** or **Stored Procedure**
  
    
![configure][5]

* Execute Command - the SQL command(s) to execute
  
  
![configure][6]

* Description - user friendly description for SQL Statement resource

### :fa-edit: Stored Procedure
Call a stored procedure in a database using the connection string to point out the database then enter `[MyProcedure]` or you must give the full path `[Database].[dbo].[MyProcedure]`.
Note: Procedures MUST have just one (1) line or it will fail.

### :fa-edit: Text
A full query for the SQL of what to execute. This can be dangerous!

### Evaluation
Two ways to do this, both way we use [Execute Scalar][] to execute the statement. This will pick up data from the first column at the first row, and set this as Log Text, with exception for the throw (raiserror) where warning and error will take the Log Text from message of the error thrown.

#### Select + return code
SELECT is used to set the Log Text, no select will leave the Log Text empty.  
return code = 0 = OK  
return code < 0 = Error  
return code > 0 = Warning  

#### Select + throw (raiserror)
SELECT for OK no return code (or 0 as default) and will set Log Text, no select will leave the Log Text empty.  
  
throw State = 1 = Error  
throw State > 1 = Warning  
Message of the throw or raiserror will be used as Log Text  

___

## :fa-code: Examples

### Command Type: Text

Example how to use the SQL Statement with Command Type set to **Text**.
This example counts the rows in the actual table. Depending on the result, different states are returned.  

#### SQL Statement
```sql
DECLARE @rowCount INT,  
        @log VARCHAR(256),  
        @errorLimit INT = 3,  
        @warningLimit INT = 1  
  
SELECT @rowCount = COUNT(*) FROM [ConnectionFailures]  
            
IF (@rowCount > @errorLimit)  
   BEGIN  
      SET @log = 'ERROR Number of connection failures exceeded ' + CAST (@errorLimit AS VARCHAR(18));  
      THROW 51000, @log, 1;  
   END  
ELSE IF (@rowCount > @warningLimit)  
   BEGIN  
      SET @log = 'WARNING number of connection failures exceeded ' + CAST (@warningLimit AS VARCHAR(18));  
      THROW 51000, @log, 2;  
   END  
ELSE  
   BEGIN  
      SELECT 'OK'  
   END  
```

    Note: THROW is only a command in SQL Server 2012 and later in 2008 R2 you can use RAISERROR (@log, 16, 1); RAISERROR(Message, Severity, State), Severity should be set to 16 to get such a high value to stop the execution of the query.

### Command Type: Stored Procedure

Example how to use the SQL Statement with Command Type set to **Stored Procedure**

#### SQL Statement
```sql
[SP_CheckConnectionFailures]
```

Example of the called Stored Procedure (existing in the actual database):  

```sql
USE [MyDatabase]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[SP_CheckConnectionFailures] 
AS

BEGIN
   SET NOCOUNT ON;
            
   DECLARE  @rowCount INT,
            @errorLimit INT = 3,
            @warningLimit INT = 1,
            @ret INT = 0,
            @log VARCHAR(256)
            
  SELECT @rowCount = COUNT(*) from [dbo].[ConnectionFailures]
            
  IF (@rowCount > @errorLimit)
     BEGIN
        SET @log = 'ERROR Number of connection failures exceeded ' + CAST (@errorLimit AS VARCHAR(18));  
        SELECT @log
        SET @ret = -10
     END
  ELSE IF (@rowCount> @warningLimit)
     BEGIN
        SET @log = 'WARNING number of connection failures exceeded ' + CAST (@warningLimit AS VARCHAR(18));  
        SELECT @log
        SET @ret = 10
     END
  ELSE
     BEGIN
        SELECT 'OK'
     END
  RETURN @ret

END -- Procedure
```

    Note: Command Type set Stored Procedure cannot take arguments, use Command Type set to Text when passing arguments to a Stored Procedure

___
 ## :fa-flash: Actions
The [SQL Agent][] has support for remote actions. The folllowing Actions are exposed:  
* Edit SQL Statement.  

 ![actions][1]
   
### :fa-edit: Edit SQL Statement Configuration
Edit the statments with ease for the SQL Database.  
 ![edit][2]

New SQL Statements must be created using [Remote Configuration][]. These settings can be edited by using the 'Edit SQL Statement' action.


The following properties can be set:
* Display Name - a name to be associated with the SQL Statement
* [Application][Applications] - a way of grouping resources
* Command Type - specifies if the SQL Statement is a **Stored Procedure** or **Text**
* Description - a user friendly description for the SQL Statement
* Statement - the actual statement

You must click **Save** for changes to be written to the agent and take effect. 

    Note: Depending on the synchronization interval set for the agent, there might be a delay before the Web Client reflects upon the change. You can choose to force the agent to synchronize from the configuration of the Source. 

___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]  

##### :fa-cubes: Related  
:fa-database: [Azure SQL Statements][]  
:fa-database: [SQL Agent][]  
:fa-folder-o: [SQL Categories][]  
:fa-lightbulb-o: [Resources][]  
:fa-cloud-download: [Monitor Agents][]  
:fa-desktop: [Monitor Views][]    


<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/EditSQLStatementAction.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/SQLStatementConfiguration.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/AddNewSQLStatement.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/ConfigureSQLStatement1.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/ConfigureSQLStatement2.png
[6]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/ConfigureSQLStatement3.png

[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master

[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/3.%20Configuration.md

[SQL Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/Overview.md
[Azure SQL Statements]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/SQL%20Statements/Azure%20-%20SQL%20Statements.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master
[Applications]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/Applications.md?at=master

[SQL Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Overview.md


[Execute Scalar]:https://msdn.microsoft.com/en-us/library/system.data.sqlclient.sqlcommand.executescalar(v=vs.110).aspx