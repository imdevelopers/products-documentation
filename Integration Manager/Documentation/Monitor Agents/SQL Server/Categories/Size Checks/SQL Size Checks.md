<tags data-value="Monitor Views, SQL Server, Monitoring"></tags>

:fa-database: SQL Size Checks
=====
					


___

## :fa-info: Information
Monitor the size of **Microsoft SQL Server Databases**. [SQL Agent Monitor][SQL Agent] creates one [Resource][Resources] per SQL Server Database and can be monitored using suitable [Monitor Views][].

## :fa-retweet: General and Specific settings
The difference between the **SQL Size Check** and the **SQL Size Check - Specific** is:  
* Global default threshold values are used in **SQL Size Check**
    * Applies to all non-specific databases  
* Customized threshold values are used in **SQL Size Check - Specific**
    * Applied on only one database   
 
___

## :fa-question-circle: Whats evaluated
The following rules are evaluated for a SQL Server Database Size Check:

#### :fa-check-circle-o: OK - Used to indicate normal operation
* The database size is **below** both the threshold values

#### :fa-times-circle: Unavailable - Resource not available 
* The Monitoring Server fails to communicate with the Monitoring Agent  

#### :fa-times-circle: Error - Used to indicate an error/fatal condition
* The database size is **over** the error threshold value

#### :fa-exclamation-triangle: Warning - Used to indicate an unexpected but not invalid state
* The database size is **over** the warning threshold value

The evaluated state may be reconfigured using **Expected State** functionality that exists on every [Resource][Resources] within Integration Manager.
___

## :fa-edit: Configuration

SQL Size Check is enabled from the [Remote Configuration][]. Simply check **Enable Size Check**  
Alla databases in the SQL Instances are monitored.

![configure][3]

Set the following properties, (global for all databases):
* Size Evaluation Type - The type of evaluation to use for the size check, **Percent** or **Megabyte**
* Warning Size - The size when the database to trigger Warning alert
* Error Size - The size when the database to trigger Error alert
* Description - Short description for the size check

___

## :fa-flash: Actions
The [SQL Agent][] has support for remote actions. The folllowing Actions are exposed:  
* Edit Size Check Configuration.  
  
 ![actions][1]
 
### :fa-edit: Edit Size Check Configuration
Edit the thresholds with ease for the SQL Server Database.  
 ![edit][2]

The **Default** values are set using [Remote Configuration][]. These *global* settings can be overridden by using the 'Edit Size Check Configuration' action.

    Note: Category is changed to 'SQL Size Check - Specific' when the threshold values are modified.

The following properties can be set:
* [Application][Applications] - a way of grouping resources
* Description, a user friendly description for the database size check
* Database Size
    * Size Evaluation Type: **Percentage** or **Megabyte** 
    * Warning: Allowed size before state is evaluated as Warning 
    * Error: Allowed size before state is evaluated as Error
* Transaction Log Size
    * Size Evaluation Type: **Percentage** or **Megabyte** 
    * Warning: Allowed size before state is evaluated as Warning 
    * Error: Allowed size before state is evaluated as Error
 
You must click **Save** for changes to be written to the agent and take effect. 

    Note: Depending on the synchronization interval set for the agent, there might be a delay before the Web Client reflects upon the change. You can choose to force the agent to synchronize from the configuration of the Source. 

___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]  

##### :fa-cubes: Related  
:fa-database: [Azure - SQL Size Checks][]  
:fa-database: [SQL Agent][]  
:fa-folder-o: [SQL Categories][]  
:fa-lightbulb-o: [Resources][]  
:fa-cloud-download: [Monitor Agents][]  
:fa-desktop: [Monitor Views][]    


<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/DatabaseSizeCheckAction.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/SpecificDatabaseSizeCheckConfiguration.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/ConfigureSQLSizeCheck.png

[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master

[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Remote%20Configuration/Remote%20Configuration.md?at=master

[SQL Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/Overview.md
[Azure - SQL Size Checks]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/Size%20Checks/Azure%20-%20SQL%20Size%20Checks.md
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master
[Applications]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/Applications.md?at=master

[SQL Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Overview.md