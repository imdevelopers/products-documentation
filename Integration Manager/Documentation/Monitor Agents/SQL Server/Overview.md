<tags data-value="Monitor,Agent,SQL Server,SQL,Database"></tags>

:fa-database: SQL Server Monitor Agent
=============================================

Monitor OnPremise **SQL Server** instances and cloud based **Azure SQL Databases** with Integration Manager's **SQL Server Monitor Agent**.


This agent has been developed with demands set from a company specializing in providing services and 24/7 support for Microsoft SQL Server. This ensures capabilities, performance and overall quality. This is is together with the [BizTalk Monitor Agent][BizTalk Agent] one of our most appreciated and most commonly used agents.

<img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/FailingSQLResources.png" height="310" />

:fa-sitemap: Documentation support is provided by the [Repository][] model.

## Release Notes
For more information about release history and latest version, see [Release Notes][]. 

## :fa-info: About
This topic links to documentation resources about how to use the monitoring agent for SQL Server (including Azure SQL Databases).

The agent provides a way to monitor key features in SQL Server. For example; It is crucial to know when **backups are not taken/failing**, **jobs are not running** and your own **custom TSQL statements** are not returning the expected result, due to the fact that this might cause business critical errors. With this agent you can configure Integration Manager to execute and monitor all imaginable SQL statements and their result (designed to be extensible).


The current state of the [Resources][] are available in [Monitor Views][] and alerts can be distributed (pushed) to end users (Mail, Ticket handling systems, etc.).  
Alert Metrics can be displayed either in the dashboard or using custom [Reports][Reports] using [Power BI][Power BI] or any other tool that you prefer since data comes from our REST based [Web API][Web API].

* SQL Server and Azure SQL Databases are supported from the same installed agent
* Remote Actions are available on some resources which removes the need of the MMC/remote desktop connections  
* Any number of SQL Instances can be monitored from a single agent
* Multiple agents can be deployed on multiple servers
* Servers can be local or in the cloud, and even off site (partner/customer location)
* Low overhead with least privileges policy
* Report capabilities

## :fa-sliders: Monitor Capabilities 	
The [Resources][Resources] are grouped by Categories. The following  [SQL Categories][SQL Categories] exists:

* [SQL Jobs][] - Monitor how SQL Agent jobs perform	
* [SQL Backups][] - Monitor your backups, displays the time of the last backup taken (if any).
* [SQL SSIS][] - Monitor SSIS package execution
* [Size Checks][] - Monitor file usage (data and log separately)
* [SQL Instance][] - Provides general information about the SQL Instance 
* [SQL Statements][] - Monitor the state of your business data running custom TSQL statements (including stored procedures) 
* [Azure Size Checks][] - Monitor file/space usage
* [Azure SQL Statements][] - Monitor the state of your business data running custom TSQL statements

## :fa-flash: Actions

Fix your SQL related problems with ease from a distance without the MMC (SQL Server) or the Azure portal (SQL Databases). 

Using the [Web Client][Web Client] for Integration Manager, Actions can be sent to the monitor agent for SQL Server requesting operations to be performed on the monitored resources. With the existing privilege model you can allow certain users to perform operation on selected resources.

List of Actions on the [Resources][] of [SQL Categories][SQL Categories] that can be executed from using this agent:

* [SQL Jobs][]
	* View 'SQL job history' for individual jobs
	* Edit time thresholds with ease
	* Start job
	* Enable job
	* Disable job
* [SQL Backups][]
* [SQL SSIS][]
	* View 'SQL SSIS run history' for individual packages.
* [Size Checks][]
	* Edit size thresholds with ease, Data and Log separately
* [SQL Instance][]
	* View SQL Log (current)
* [SQL Statements][]
	* Edit SQL Statements with ease. Support for Stored Procedures and AdHoc TSQL.
* [Azure Size Checks][]
	* Edit size thresholds with ease
* [Azure SQL Statements][]
	* Edit SQL Statements with ease. Support for Stored Procedures and AdHoc TSQL.
	
## :fa-arrows-alt: Supported Versions 
* SQL Express (not SQL Jobs due to missing SQL Agent)
* SQL Server
* SQL Database (Azure)

**All editions**, Developer, Standard, Enterprise are supported.

**Default** and **named instances** are supported.

All SQL Server versions beginning with 2008 R2 and later are supported, older versions might work with limited set of features. Our aim is always to support the latest versions and standards, often including CTP releases.
* SQL Server 2016 is supported

## :fa-list-alt: Release Log
For information about the latest release, see [Release Log](http://support.integrationsoftware.se/support/discussions/forums/1000229028)
___

### :fa-hand-o-right: Next Step
:fa-rocket: [Install SQL Monitor Agent][]  
:fa-edit: [Configuration][] of the agent  

#### :fa-cubes: Related
:fa-check-square-o: [PreRequisites for SQL Monitor Agent][]  


[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/3.%20Configuration.md?at=master
[Install SQL Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/1.%20Install.md?at=master  
[PreRequisites for SQL Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/1.%20PreRequisites.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[SQL Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/Overview.md?at=master
[SQL Instance]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/SQL%20Instance/Overview.md?at=master
[SQL Jobs]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/Jobs/Overview.md?at=master
[SQL SSIS]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/SQL%20SSIS/Overview.md?at=master
[SQL Statements]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/SQL%20Statements/SQL%20Statements.md?at=master
[SQL Backups]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/SQL%20Backups/SQL%20Backups.md?at=master
[Size Checks]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/Size%20Checks/SQL%20Size%20Checks.md?at=master
[Azure Size Checks]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/Size%20Checks/Azure%20-%20SQL%20Size%20Checks.md?at=master
[Azure SQL Statements]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/SQL%20Server/Categories/SQL%20Statements/Azure%20-%20SQL%20Statements.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Reports]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Reports/Overview.md?at=master
[Power BI]:https://powerbi.microsoft.com/en-us/
[BizTalk Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Overview.md?at=master
[Web API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Web%20API/Overview.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md


[Release Notes]:http://support.integrationsoftware.se/support/discussions/forums/1000229028