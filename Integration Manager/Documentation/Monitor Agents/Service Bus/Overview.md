<tags data-value="Monitor,Azure,Service Bus,Queue"></tags>

:fa-reorder: Azure Service Bus
=============================================

Support for **Azure Service Bus** is provided from within the **[Message Queues Monitor Agent][]**
	

#### :fa-cubes: Related
:fa-check-square-o: [Message Queues Monitor Agent][]  
:fa: [Azure Service Bus](https://azure.microsoft.com/en-us/services/service-bus/) (external link)

[Message Queues Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Message%20Queues/Overview.md?at=master  


