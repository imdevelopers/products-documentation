<tags data-value="Monitor,Connectivity"></tags>

:fa-mixcloud: Service Bus Relaying 
=====
___

## :fa-info: Information
What is the Service Bus relay?
The [Service Bus relay service from Microsoft][] enables the [Monitoring Service][] of Integration Manager to talk with [Monitor Agents][] that run in both an Azure datacenter and your own on-premises enterprise environment (even remote on-premises, like your customers and business partners).

![sbtopology][2]

This means that you can use every [Monitor Agent][Monitor Agents] wherever you have the need! Simply install the agent and configure for Service Bus Relaying. Read more about this topic below.

## Firewall
The following Ports must be open for outbound communication:
* 443 (HTTPS)
* 5671, 5672 (Secure AMQP)
* 9350 - 9354 (Net.TCP)

https://docs.microsoft.com/en-us/power-bi/service-gateway-onprem

## :fa-dollar: Costs and pricing information

There is an additional cost associated with running [Monitor Agents][] off-site. This cost varies depending on the number of agents, the synchronization interval, and the number of [Resources][] that changes state.

Cost varies with:
* Number of installed agents using **Service Bus Relaying**
* Synchronization interval set on the [Source][Sources]
* State changes of exposed [Resources][] - error prone environments will cost more...
    * Also if the LogText changes - for custom built [Monitor Agents][], make sure the LogText only changes when necessary.
* Caching implemented or not - All Integration Manager [Monitor Agents][] are built with caching support
* Number of Remote Actions executed     

The [Monitor Agents][] for Integration Manager are designed and built with caching in mind to reduce costs and improve performance.
  
    Note: Using the Service Bus Relay induces costs and requires an account with a credit card associated 

![sbcosts][1]  
For the latest information about pricing, read more [here][Service Bus Relaying Costs].


## :fa-question: How to configure the Monitor Agents to use Service Bus Relaying
Here you will find generic information that applies to all [Monitor Agents][] and describes how to configure communication using **Service Bus Relaying**.

### :fa-check-square-o: Prerequisites
* Account with credit card
* Namespace 
    * End Point address must be unique, when you have 2 or more agents of the same type, you may either choose to use separate *Namespaces* or change the **endpoint address** in the config file and set correspondingly in the general tab.

**Shared Access Signature security Key**. See the following Microsoft article for more information [MDSN][SAS]. The [Monitor Agent][Monitor Agents] needs additional configuration for [Service Bus Relaying][]. 

### :fa-edit: Local Configuration
The local configuration file (**.config**) has the **Service Bus Relaying** configuration withing comments. You need to remove the wrapping Comments and use your Service Bus Namespace information.


    NOTE: This is a manual step and requires RDP session and administrative rights (restart of Windows service is required).  

![appconfig][4]

    NOTE: Changes on the configuration file requires the Monitor Agent to be restarted for these changes to be used

The **SourceInformation.txt** should now display the URI to use as a Source in later steps described below.

### :fa-edit: :fa-cloud-download: Configuration of the Source
You must edit the :fa-lock: **Security tab** of a [Source][Sources] and provide the connection information for the Service Bus Relay in format:

### :fa-chain: General
Enter the address to the installed agent (take from SourceInformation.txt for TCP connections or from **endpoint address** section of %monitor agent%.exe.config file for service bus relayed connections)

![general][7]  

### :fa-lock: Security
    SharedAccessKeyName=%RootManageSharedAccessKey%;ServiceBusRelayingAppConfig.pngaredAccessKey=%SharedAccessKey%;AuthenticationKey=%AuthenticationKey%

![securitytab][3]  
The 3 individual parts are:  

1. **SharedAccessKeyName** According to your policy, copy from Azure Portal (Default RootManageSharedAccessKey)

![SharedAccessKeyName][5]  
2. **SharedAccessKey** Copy from connection string information in the Azure Portal

![SharedAccessKey][6]  
3. **AuthenticationKey** Copy AuthenticationKey from SourceInformation.txt file


## :fa-ambulance: Troubleshooting

In order to debug connectivity related problems you may enable WCF diagnostics logging, read more [here](https://docs.microsoft.com/en-us/dotnet/framework/wcf/diagnostics/tracing/configuring-tracing)

Add the following  `<system.diagnostics>` section to the local configuration file (**.config**)

    NOTE: The Windows Service Account running the Agent must have write access to the configured folder (C:\Temp\WCFlog\ in the example below, change according to your needs/policy). Make sure the destination folder exists(!)
 
   ```xml
   ...
   <configuration>
  <system.diagnostics>
    <sources>
      <source name="System.ServiceModel"
              switchValue="Information, ActivityTracing"
              propagateActivity="true" >
        <listeners>
             <add name="xml"/>
        </listeners>
      </source>
      <source name="System.ServiceModel.MessageLogging">
        <listeners>
            <add name="xml"/>
        </listeners>
      </source>
      <source name="myUserTraceSource"
              switchValue="Information, ActivityTracing">
        <listeners>
            <add name="xml"/>
        </listeners>
      </source>
    </sources>
    <sharedListeners>
        <add name="xml"
             type="System.Diagnostics.XmlWriterTraceListener"
             initializeData="C:\Temp\WCFlog\Error.svclog" />
    </sharedListeners>
  </system.diagnostics>
</configuration>
   ...
   ```    
Contact our [Support][] for additional guidance if you fail to resolve the problem.

    Note: There may be additional information written to the Windows Event logs.  

___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-cloud-download: [Add or manage Sources][]

##### :fa-cubes: Related  
:fa-cloud-download: [Sources][]  
:fa-desktop: [Monitoring Service][]  
:fa-cloud: [Microsoft Service Bus Relaying][Service Bus relay service from Microsoft]  
:fa-dollar: [Service Bus Relaying Pricing][Service Bus Relaying Costs]  


[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/ServiceBusRelayingCosts.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/ServiceBusRelayingTopology.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/ServiceBusRelayingSecurityTab.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/ServiceBusRelayingAppConfig.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SharedAccessPolicy.png
[6]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/ConnectionStringAzure.png
[7]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/ServiceBusRelayingGeneralTab.png


[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Add or manage Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Service Bus relay service from Microsoft]:https://azure.microsoft.com/en-us/documentation/articles/service-bus-dotnet-how-to-use-relay
[Service Bus Relaying Costs]:https://azure.microsoft.com/en-us/pricing/details/service-bus/
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master
 [Support]:http://support.integrationsoftware.se