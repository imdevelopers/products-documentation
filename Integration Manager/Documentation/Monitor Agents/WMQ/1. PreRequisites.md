<tags data-value="Monitoring, WMQ Server"></tags>

:fa-check-square-o: The WMQ Monitoring Agent has the following pre requisites
=====
					

___

## :fa-info: Information
This page describes the prerequisites for installing and running the [WMQ Monitor Agent][WMQ Agent].

## Software requirements

* :fa-windows: Microsoft Server 2008 R2 or later
    * .NET Framework 4.5 or later*
* WMQ Client (7/8/9)
    * XTC Client 

* Security/User rights (listed below)
* DTC must have XA transactions allowed

## :fa-free-code-camp: Firewall

* Default channel and listener port 1414 (may be overridden by configuration)
* Default outgoing port 443 to Internet for Service Bus Relayed connections
* Default inbound and outbound port 8000 **IM Monitor Agent** and **IM WMQ Monitoring Agent**


## :fa-windows: Windows User Rights
* Local named account or domain account (preferred). Follow this guide '[How to set Logon As A Service][]' for more information.  
* Service Account running the **WMQ Monitor Agent** must have appropriate MQ rights to read queues and content.

### :fa-hand-o-right: Next Step
:fa-reorder: [WMQ Monitor Agent][WMQ Agent]  
:fa-rocket: [Install WMQ Monitor Agent][]  

#### :fa-cubes: Related
:fa-plus-square: :fa-download: [Add or manage a Source][]  
:fa-desktop: [Monitor Agents][]    
:fa-cogs: [Administration][]    


<!--References -->
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master

[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[ServiceBusRelaying]:https://azure.microsoft.com/sv-se/documentation/articles/service-bus-dotnet-how-to-use-relay/
[Install WMQ Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/WMQ/2.%20Install.md?at=master
                            
[How to set Logon As A Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Logon%20As%20A%20Service.md?at=master
[WMQ Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/WMQ/Overview.md?at=master
