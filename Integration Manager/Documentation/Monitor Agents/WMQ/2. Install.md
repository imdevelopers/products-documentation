<tags data-value="WMQ, Monitoring"></tags>


:fa-rocket: Install IM WMQ Monitor Agent
=====
___

## :fa-info: Information
In this section you will learn how to install the **IM WMQ Monitor Agent**.

**PreReq**: You must have downloaded the **IM WMQ Monitor Agent** version to install and configured the Windows and WMQ Server machines prior to installation. The Service account must have [Logon as Service Right][How to set Logon As A Service]. See [PreRequisites for WMQ Monitor Agent][] for more information. You must be a local administrator in order to have the right to install Windows Services.


#### :fa-play: Start the installer
From the server using [RDP](https://support.microsoft.com/en-us/help/17463/windows-7-connect-to-another-computer-remote-desktop-connection), double click on the **MSI file** to start the installation of the **IM WMQ Monitor Agent**.  
![msi][1]  

Depending on settings and/or the way the install package was copied to Windows, a security warning may be presented. Click **'Run'** to continue.  
![warning][2]   

#### Welcome Screen
On successful execution of the MSI file a **Welcome screen** will be presented.  
![Welcome][3]  

Click **'Next'** button to continue with the installation process or the **'Cancel'** button to quit the installer.

#### :fa-wrench: Custom Setup
Select Components and Location of installation. Default settings are recommended.  
![CustomSetup][4]  

Click **'Next'** button to continue with the installation process or the **'Cancel'** button to quit the installer.

#### :fa-user-secret: Service Account Information 
Enter the credentials for the **Account** to run **IM WMQ Monitor Agent** as a Windows Service. Specific user rights per queue manager is set later during [Configuration][].

**Domain**: The name of the Domain the Server belongs to. If a local account is being used, use the name of the server or . (dot) as Domain  
**User**: The name of the account  
**Password**: The password for the account.   

    NOTE: The account must have 'Log on as Service Right'

![DomainUser][5]

Click **'Next'** button to continue with the installation process or the **'Cancel'** button to quit the installer.

## Ready to install
![read][10]

##### :fa-exclamation-circle: Insufficient Privileges
Error message when account information does not meet [PreRequisites][PreRequisites for WMQ Monitor Agent].      
![InsufficientPrivileges][6]

If you get this message, the most common reason is that the account is **not allowed to run as a service** or is **not local Administrator** on the server. Follow the guide [How to set Logon As A Service][] and click retry. If the problem remains, click cancel and restart. Make sure that the credentials are typed correctly! 

    Tip: Check the local event log if there are errors (both System and Application)

#### :fa-flag-checkered: Completed setup
The last step of the installation gives either a **success** or a **premature exit**.    
**Successful installation example**  
![Completed][8]

:fa-times-circle: **Failed installation example**: A failed installation will render a premature exit    
![PrematureExit][7]

Click the **'Finish'** button to Exit and quit the installer.

When the setup has finished the default text editor will open a text file named the 'SourceInformation.txt'. The **SourceInformation.txt** file holds additional information that you need in order to add the agent as a [Source][Sources].  
![SourceInformation.txt][9]
* :fa-plus-square: :fa-cloud-download: [Add or manage a Source][]

#### :fa-ambulance: Support
Contact our [Support][] for additional guidance if you fail to resolve the installation problem.

    Note: There may be additional information written to the Windows Event logs.  
___

### :fa-hand-o-right: Next Step
:fa-edit: [Configuration][] of the agent  
:fa-arrow-circle-o-up: [Update][]  
:fa-remove: [Uninstall][]  

#### :fa-cubes: Related

:fa-plus-square: :fa-cloud-download: [Add or manage a Source][]  
:fa-cloud-download: [Sources][]  
:fa-check-square-o: [PreRequisites for WMQ Monitor Agent][]  

<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WMQ/msi.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WMQ/openfilesecuritywarning.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WMQ/Welcome.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WMQ/CustomSetup.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WMQ/DomainUser.png
[6]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WMQ/InsufficientPriviliges.png
[7]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WMQ/PrematureExit.png
[8]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WMQ/Completed.png
[9]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WMQ/SourceInformation.txt.png
[10]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WMQ/readytoinstall.png

[PreRequisites for WMQ Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/WMQ/1.%20PreRequisites.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/WMQ/3.%20Configuration.md?at=master
[Update]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/WMQ/4.%20Update.md?at=master
[Uninstall]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/WMQ/5.%20Uninstall.md?at=master

[How to set Logon As A Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Logon%20As%20A%20Service.md?at=master
[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Support]:http://support.integrationsoftware.se
