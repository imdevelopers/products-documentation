<tags data-value="BizTalk,Actions,Monitor,View,Resource"></tags>

:fa-folder: WMQ Categories
=====
					


___

## :fa-info: Information
This page is an overview for all the [Categories][] the WMQ Monitor agent provides.

The WMQ Monitoring Agent supports the execution of remote actions on resources using the Integration Manager Web Client tool or by programmatically calling the [Web API][].
In order to execute remote actions some [PreRequisites][PreRequisites for Windows Server Monitor Agent] must be satisfied. 

The user must be logged on to Integration Manager using a Windows Credential. The user must also be assigned a monitor view with [Resources][] from this agent where [Actions][] are allowed. See [Add or manage Monitor Views][] for more details.
___

The various monitoring capabilities for the WMQ Monitor agent are expressed as [Resources][] and are grouped by [Categories][].
The **Categories** are unique for each [Monitoring Agent][Monitor Agents]. 

## :fa-edit: Remote Configuration
 The [Windows Server Agent][] is configured using [Remote Configuration][]. From the [Configuration][] in a [Source][Sources] an Administrator can add and/or remove Windows Servers to monitor.  

![png_CategoriesTabs][]

## :fa-folder: Categories
The following [Categories][] are exposed by the **Windows Server Monitor Agent**.  

* [Queues][]  

___

### :fa-hand-o-right: Next Step
:fa-edit: [Remote Configuration][Configuration] of the agent  

#### :fa-cubes: Related
:fa-check-square-o: [PreRequisites][]  
:fa-folder: [Categories][]  
:fa-flash: [Actions][]  
:fa-desktop: :fa-cloud-upload: [Monitor Agents][]      
:fa-cloud-download: [Source][Sources]

<!--References -->

[Monitor View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/3.%20Monitor/Monitor.md?at=master
[Actions]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/1.%20View%20Monitor%20Views/2.%20View%20Single%20Monitor%20View/Actions/Actions.md?at=master

[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/integrationsoftware/products-documentation/src/master/Integration%20Manager/Integration%20Manager%20Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[Web API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Web%20API/Overview.md?at=master
[PreRequisites for BizTalk Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/1.%20PreRequisites.md?at=master
[Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/Categories.md?at=master
[Actions]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/1.%20View%20Monitor%20Views/2.%20View%20Single%20Monitor%20View/Actions/Actions.md?at=master
[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Remote%20Configuration/Remote%20Configuration.md?at=master
[Windows Server Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/Overview.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/3.%20Configuration.md?at=master
[PreRequisites for Windows Server Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/1.%20PreRequisites.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master  

[Queues]:Queues.md

[png_CategoriesTabs]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WMQ/WMQTab.png

