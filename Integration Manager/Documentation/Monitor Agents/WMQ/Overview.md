<tags data-value="Monitor,WMQ,WebSphere,IBM MQ,Queue"></tags>

:fa-reorder: IBM MQ Agent
=============================================


Monitor [IBM MQ](http://www-03.ibm.com/software/products/sv/ibm-mq) with Integration Manager's **IBM MQ Monitor agent**.
					
## :fa-info: About
This agent allows you to monitor IBM MQ related resources like queues, topics, channels and listeners. For example, the **IBM MQ Monitor agent** can evaluate the number of messages on the queue(s). Also the maximum age of the first message on the queue can be verified. An administrator can decide in the configuration which queue manager and related settings for queues and topics to monitor, as well as the number of messages on which Integration Manager should warn / fail for. 

This service has been developed due to the fact that in many cases, that if queues contain messages it is in many cases some kind of error, at least if the messages are there longer than an expected time span (e.g. 2 hours). This agent enables you to monitor each and every queue that is important for you and your business.

## :fa-sliders: Monitor Capabilities
List of resources that can be monitored by using this agent

* Queues
    * Global settings
    * Per queue specific settings 

    * Age verification
    * Count (warning / error)
    * Quota-evaluation

* Topics
* Listener
* Channels
* Dead letter 
* Get/Put inhibited 

## :fa-flash: Actions
No actions have yet been implemented.

## :fa-usd: Pricing
This agent and all other Monitor agents are part of the license for Integration Manager. No additional costs exists for the use of the non-event Monitor Agent for Integration Manager licencees. 

## :fa-list-alt: Release Log
For detailed information about the features and bug fixes, please see the [Release Log](https://support.integrationsoftware.se/discussions/forums/1000229324)
___

### :fa-hand-o-right: Next Step
:fa-rocket: [Install WMQ Monitor Agent][]  
:fa-edit: [Configuration][] of the agent  

#### :fa-cubes: Related
:fa-check-square-o: [PreRequisites for WMQ Monitor Agent][]  
:fa-desktop: [Monitor Views][]    

[Install WMQ Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/WMQ/2.%20Install.md?at=master  
[PreRequisites for WMQ Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/WMQ/1.%20PreRequisites.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/WMQ/3.%20Configuration.md?at=master
[Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/WMQ/Categories/Overview.md?at=master
         
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
