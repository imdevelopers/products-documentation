<tags data-value="Web Services, Uninstall"></tags>

:fa-remove: Uninstall
=====
					
___

## :fa-info: Information

The Web Services Monitoring Agent can be uninstalled running either the installer directly or by using 'Add or Remove Programs' in Windows.

:fa-exclamation: You should remove affected [Sources][] and [Resources][] in use within [Monitor Views][] before uninstalling the agent.

If you uninstall the agent without removing the [Source][Sources] alerts with errors will be sent to users. Also, the Source will be listed as unavailable in the list of available [Sources][]

Example of unavailable [Source][Sources]:  
![unavailablesource][9]
___

## Add or remove Programs
![RemovePrograms][]

![RemoveInstallation][]

#### :fa-play: Start the installer
Double click on the **MSI file** to start the installation of the **IM Web Services Monitor Agent**.  
![msi][1]  

Depending on settings in Windows a security warning may be presented. Click **'Run'** to continue.  
![warning][2]   

![CloseBeforeUninstall][4]

#### Welcome Screen
On successful execution of the MSI file or starting from 'Add or remove programs' a **Welcome screen** will be presented.  
![Welcome][3]  

Click **'Next'** button to continue with the installation process or the **'Cancel'** button to quit the installer.
   
## Change, Repair, Remove
In the 'Change, Repair, Remove' Dialog click the **'Remove'** button to uninstall the agent and proceed to the next step.  
![remove][0]

If the Service was not stopped before uninstalling a dialog may appear. Click 'Ok' to close and continue.

![InUse][5]

If the Service was not stopped before uninstalling a dialog may appear. Click 'Ok' to close and continue.

![UnableToClose][6]

## Remove Program
Click the  **'Remove'** button to remove the agent and proceed to the next step.  
![RemoveProgram.png][RemoveProgram]


#### :fa-flag-checkered: Completed setup
The last step of the un-installation gives either a **success** or a **premature exit**.      
**Successful un-installation example**

![Completed][8]

**Failed un-installation example**: A failed un-installation will render a premature exit  
![PrematureExit][7]

Click the **'Finish'** button to Exit and quit the installer.

#### :fa-ambulance: Support
Contact our [Support][] for additional guidance if you fail to resolve the installation problem.

    Note: There may be additional information written to the Windows Event logs.  
___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]  
:fa-plus-square: :fa-cloud-download: [Add or manage a Source][]

##### :fa-cubes: Related  
:fa-connectdevelop: [Web Services Agent][]  
:fa-lightbulb-o: [Resources][]  
:fa-cloud-upload: [Monitor Agents][]  
:fa-desktop: [Monitor Views][]    

[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Web Services Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Web%20Services/Overview.md?at=master
[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Support]:http://support.integrationsoftware.se


[RemoveInstallation]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/RemoveInstallation.png
[RemovePrograms]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/RemovePrograms.png
[RemoveProgram]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/RemoveProgram.png
[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/ChangeRepairRemove.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/msi.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/openfilesecuritywarning.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/Welcome.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/CloseBeforeUninstall.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/InUse.png
[6]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/UnableToClose.png
[7]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/PrematureExit.png
[8]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/Completed.png
[9]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/Sources/SourceNotAvailable.png