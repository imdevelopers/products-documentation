
<tags data-value="Monitor,Web Service,Integration Manager"></tags>

:fa-line-chart: Metrics
=====
 
___

## :fa-info: Information
Response times are measured and **Metrics** can be displayed on the [Dashboard][] as Widget.
![WebServicesMetrics][0]

## :fa-question-circle: Setup

### Select Resource
From the **Actions** button, select 'Response time chart':  
![actionmetrics][1]

### :fa-cog: Settings  
![MetricsModal][2]

*   **Select Metrics Content**
 in the Metrics modal you can select the type of graph  
![MetricsContent][3]

* **Time Range**  
![MetricsTime][4]

* **Id**   
![MetricsId][5]

    NOTE: This must be unique on the dashboard, make sure to change the default value (for example append a generated guid or the current datetime, whatever makes it unique among all other dashboard widgets) 

* **Title**  
![MetricsTitle][6]

* **Height**  
![MetricsHeight][7]

* **Refresh Interval**  
![MetricsRefreshInterval][8]

    NOTE: Make sure to set an appropriate refresh time interval, depending on underlying demand this may impact overall performance on your environment. Use as high value as possible

* **HTML for Dashboard**
When you are done editing the properties, the HTML is currently being generated for usage within the [Dashboard][]

___

### :fa-hand-o-right: Next Step  
:fa-desktop: [Monitor Views][]  

##### :fa-cubes: Related  
:fa-desktop: [Monitor Agents][]    
:fa-cogs: [Administration][]    

<!--References -->

[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master

[Dashboard]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/1.%20Dashboard/Overview.md?at=master

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/MetricsExample.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/ActionMetrics.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/MetricsModal.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/SelectMetricsContent.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/MetricsTimeRange.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/MetricsId.png
[6]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/MetricsTitle.png
[7]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/MetricsHeight.png
[8]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/MetricsRefreshInterval.png







