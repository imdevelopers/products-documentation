
<tags data-value="Monitor,Web Service,Integration Manager"></tags>

:fa-heartbeat: XPath
=====
 
___

## :fa-info: Information
The **Web Service Monitor Agent** for Integration Manager can be used to monitor the response body.


XPath is namespace sensitive. 
Use local-name() to get the correct path for XML / SOAP messages with namespace.  

/*[local-name()='List']/*[local-name()='Fields']/*[local-name()='Field']
 