
<tags data-value="Monitor,Web Service,Integration Manager"></tags>

:fa-heartbeat: SOAP Example
=====
 
___

## :fa-info: Information
The **Web Service Monitor Agent** for Integration Manager can be used to monitor SOAP based Web Services.


## :fa-question-circle: Setup

## :fa-dropbox: Add a new application  
Enter name and a unique id per application

Repeat as needed to identify your [Application][Applications]  
![png_SOAPApplication][]


## Add a new Web Service to monitor in this case the SOAP Web Service

### :fa-sign-out: Request
Display name: `Check if IM key expires` (or whatever you prefer to name this test)  
Description: `On error. The product key will expire within 14 days`  
URI: `http://localhost/IM/WebAPI/api/status` (Make sure to point to the correct WebAPI! Try to always use localhost if Web Service agent is on the same server)  
Polling Interval: `3600` (once each hour)  
![png_RequestApiStatus][]  


### :fa-sign-in: Response
First add expected http status codes:

Status Code: `200 OK`  
![png_ResponseOk][]  

### Evaluation

For the following SOAP Response

```xml
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
	<soap:Body>
		<ns2:getPersonResponse xmlns:ns2="http://complexnamespace.se/masterdata/1.0">
			<ns2:masterPerson>
				<firstName>John</firstName>
				<lastName>Doe</lastName>
			</ns2:masterPerson>
		</ns2:getPersonResponse>
	</soap:Body>
</soap:Envelope>
```

To find evaluate `firstName`

Expression Type: `XPath`  
Expression: `/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='getPersonResponse']/*[local-name()='masterPerson']/*[local-name()='firstName']`  
![png_ResponseEvaluation][]  

#### Authentication
Domain User: `DOMAIN\user` (User MUST have access to Integration Manager)  
Password: `********` (Password for the user)  
![png_RemoteConfigAuthentication][]  
 
  
## :fa-heartbeat: Monitor

### On OK
![png_ResourceOK][]  

### On Error
![png_ResourceError][]  


Make sure to setup [Alarm Plugin][] for the [Monitor View][Monitor Views]  

___

### :fa-hand-o-right: Next Step  
:fa-desktop: [Monitor Views][]  

##### :fa-cubes: Related  
:fa-plus-square: :fa-download: [Add or manage a Source][]  
:fa-desktop: [Monitor Agents][]    
:fa-cogs: [Administration][]    
:fa-cloud-download: [Sources][]  

<!--References -->

[Alarm Plugin]:http://documentation.integrationmanager.se/#/?file=Web%20Client%2F5.%20Administration%2F2.%20Monitor%2F1.%20Monitor%20Views%2F2.%20Add%20or%20manage%20Monitor%20View%2F2.%20Add%20Alarm%20Plugins%2FAdd%20Alarm%20Plugins.md?at=master

[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Applications]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/Applications.md?at=master



[png_SOAPApplication]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/Examples/SOAP/RemoteConfigApplication.png


[png_RequestApiStatus]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/Examples/IMKey/RemoteConfigRequestApiStatus.png
[png_ResponseOk]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/Examples/IMKey/RemoteConfigResponseOk.png
[png_ResponseEvaluation]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/Examples/SOAP/RemoteConfigResponseEvaluation.png
[png_RemoteConfigAuthentication]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/Examples/IMKey/RemoteConfigAuthentication.png

[png_ResourceOK]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/Examples/IMKey/ResourceOK.png

[png_ResourceError]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/Examples/IMKey/ResourceError.png



