<tags data-value="Monitor,Web Service,Web Application,WCF,Web API"></tags>

:fa-globe: Web Services Monitor Agent for Integration Manager
=============================================

Monitor your Web Application, Web Services and API enabled endpoints with ease. Integration Manager's monitor agent for web services automates your tests and ensures the quality and online presence of your distributed web services.
					
## :fa-info: About
Many systems and integrations are depending on web services. Monitoring them is important! With Integration Manager’s agent for web services you can easily monitor web API’s, SOAP services and even web sites. 

You monitor them by simply providing all required HTTP headers, client certificates as well as the services’ address and let the agent do its job – connecting to the service, check if the response’s HTTP status is one of the expected ones and evaluate the result of the request by using a regular expression, an XPath or a JSON Path. 

Not only can you check if the HTTP status is one of the expected ones, and not only evaluate the result by using a regular expression or an XPath, you can even monitor the response time of the request.

![WebServicesMonitorView][1]

## :fa-line-chart: Metrics
Response times are measured and Metrics can be displayed as Widget.
![WebServicesMetrics][0]

See more in the documentation [here](Categories/Metrics/Overview.md)

## :fa-sliders: Monitor Capabilities
List of resources that can be monitored by using this agent

* Any HTTP endpoint can be monitored
* Any number of web services can be monitored from a single agent
* Multiple agents can be deployed on any number of Windows Servers, on premise and/or in the cloud
* Many authentication schemes
* Remote Configuration uses service bus relaying and allows you to easily change the configuration even if the agent is deployed in another network

## :fa-microprocessor: Technical Features

* HTTP/HTTPS
	* SOAP with any message/text based Body as part of your request
	* REST with any parameters as part of your request
	* Web API
	* Web Applications
	* All HTTP operations are supported
	* All HTTP headers are supported 
* Response Times (with Metrics support)
	* Warning level
	* Error Level
* Validate response body using
	* RegEx
	* RegEx Match
	* XPath
	* JSON Path
* Evaluates expected HTTP response status code(s) 
* Authentication support
	* Client Cert
	* Impersonation
	* Basic authentication
	* API Key

## :fa-list-alt: Release Log
For detailed information about the features and bug fixes, please see the [Release Log](https://support.integrationsoftware.se/support/discussions/forums/1000229156)
___

### :fa-hand-o-right: Next Step
:fa-rocket: [Install Web Services Monitor Agent][]  
:fa-edit: [Configuration][] of the agent  

#### :fa-cubes: Related
:fa-check-square-o: [PreRequisites for Web Services Monitor Agent][]  
:fa-desktop: [Monitor Views][]    

[Install Web Services Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Web%20Services/2.%20Install.md?at=master  
[PreRequisites for Web Services Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Web%20Services/1.%20PreRequisites.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Web%20Services/3.%20Configuration.md?at=master
[Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Web%20Services/Categories/Overview.md?at=master
         
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master



[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/MetricsExample.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WebServices/WebServicesMonitorView.png