<tags data-value="Remote Configuration, Source, Windows Server"></tags>

:fa-edit: Configuration
=====
					


___

## :fa-info: Information
In this section you will learn how to configure the **IM Windows Server Monitor Agent** as a new [Source][Sources] for use within [Monitor Views][].

    Note: You must have installed the agent before configuration of the Source can be performed.
    
## :fa-rocket: [Install Windows Server Monitor Agent][Install]  
    
### :fa-sort-numeric-asc: Steps  
The following steps are required and must be performed in order after [installation][Install] before accessing the remote configuration:
1. Add a new [Source][Sources] 
After successful [Installation][Install] you must add a [Source][Sources] in the [Web Client][] to access the [Remote Configuration][] for the installed agent.  
:fa-plus-square: :fa-cloud-download: [Add or manage a Source][]  

2. Configure Communication options between the [Monitoring Service][] and the [Monitor Agent][]  
Either use:  
    2.1 TCP/IP (HTTP)  
    2.2 Service Bus Relaying

3. Use [Remote Configuration][] to configure the agent. Continued in topic below.
  
    Note: [Remote Configuration][] is available if the [Source][Sources] is authenticated or uses [Service Bus Relaying][]. 

4. Create [Monitor Views][] that uses the [Resources][] from the [Source][Sources] according to business needs.  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]

5. Fine tune specific settings using Actions if allowed by the [Monitor View][Monitor Views]  



:fa-edit: Remote Configuration
=====
The [Remote Configuration][] for the IM Windows Server Monitor Agent provides the ability to configure the agent remotely, even if the agent is installed off site.
Many specific configurations can also be made directly on the [Resource][Resources] using the **Action** button if the [Monitor View][Monitor Views] allows for Actions.

## :fa-wrench: Settings
There are some general settings for the agent available in the **Settings** tab.
* Environment
* Debug
* Culture Information

![png_WindowsServerServerSettings][]

### :fa-globe: Environment
Part of the common features shared with all [Monitor Agents][Monitor Agent], there is an option to set the name of the target **Environment**, for example TEST, QA, PROD.  

### :fa-bug: Debug
Part of the common features shared with all [Monitor Agents][Monitor Agent], there is an option to set the **Debug** flag for additional file logging that can be enabled/disabled as needed. Default is unchecked.

### :fa-globe: Culture Information
The 'Culture Information' setting determines how date time will be rendered for end users.


## :fa-server: Windows Server
The agent can monitor any number of server. Add and remove hosts from the 'Windows Server' tab.  
 ![png_RemoteConfigurationAddRemoveServers][]

[Resources][] per host will be grouped by an [Application][Applications] using the value from the **Name** text field. Read more about [Applications][] here:
* :fa-dropbox: [Applications][]  

![png_ApplicationFilter][]

### :fa-edit: Configure the added server
You must provide the **Name** (user friendly) and **Host name or IP-address** 

![png_CategoriesTabs][]

### :fa-folder: Windows Server Categories
For configuration of specific Windows Server Server Categories, see [Windows Server Monitor Agent Categories][Categories]:

* [Disk][]
* [CPU][]
* [Memory][]
* [Network][]
* [Windows Services][WindowsServices]
* [Scheduled Tasks][]
* [Event Log][]
* [Ping][]


## :fa-save: Save
You must click **Save** for changes to be written to the agent and take effect.   
![png_SaveAndClose][]

    Note: Depending on the synchronization interval set for the agent, there might be a delay before the Web Client reflects upon the change. You can choose to force the agent to synchronize from the configuration of the Source. 

**Save** - Saves the current changes, browser refreshes content and stays on current page.

**Save and close** - Saves then closes the dialog returning the browser to the previous page.

**Cancel** Close the dialog without saving any changes.

___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-cloud-download: [Add or manage a Source][]  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]   
:fa-arrow-circle-o-up: [Update][]  

##### :fa-cubes: Related  
:fa-rocket: [Install Windows Server Monitor Agent][Install]   
:fa-cloud-download: [Source][Sources]  
:fa-dropbox: [Applications][]  


<!--References -->
[png_WindowsServerServerSettings]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WindowsServer/Settings.png
[png_RemoteConfigurationAddRemoveServers]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WindowsServer/RemoteConfigurationAddRemoveServers.png
[png_SaveAndClose]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/SQL/SaveAndClose.png
[png_ApplicationFilter]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WindowsServer/ApplicationFilter.png
[png_CategoriesTabs]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WindowsServer/CategoriesTabs.png

[Install]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/2.%20Install.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Remote%20Configuration/Remote%20Configuration.md?at=master
[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[View All Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/1.%20View%20All%20Monitor%20Views/View%20All%20Monitor%20Views.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master
[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master  
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[YouTube]:https://www.youtube.com/watch?v=-ky8QqyQAHE**
[Service Bus Relaying]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/ServiceBusRelaying.md?at=master
[Applications]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/Applications.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/3.%20Configuration.md?at=master

[Update]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/4.%20Update.md?at=master

[Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/Categories/Overview.md?at=master

[WindowsServices]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/Categories/Windows%20Services.md?at=master
[CPU]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/Categories/CPU.md?at=master
[Memory]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/Categories/Memory.md?at=master
[Scheduled Tasks]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/Categories/Scheduled%20Tasks.md?at=master
[Disk]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/Categories/Disk.md?at=master
[Network]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/Categories/Network.md?at=master
[Event Log]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/Categories/Event%20Log.md?at=master
[Ping]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/Categories/Ping.md?at=master
