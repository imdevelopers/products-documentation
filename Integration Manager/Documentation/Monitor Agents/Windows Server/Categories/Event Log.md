<tags data-value="Monitor,Windows Server,Event Log,Integration Manager"></tags>

:fa-list-alt: Event Log
=====
 
___

## :fa-info: Information
The **Windows Service Monitor Agent** for Integration Manager can check for events in the **Windows Event Log** (from any Windows Server where the service account is local administrator)


The event log is like a filter function

Check for Events Where the following criteria are meet.

(Status: `Information` OR `Warning` OR `Error` OR `Critical`)  
**AND**  
(Event Provider Name: `Providername1` OR `Providername2`)  
**AND**  
(Event ID: `0` OR `2` OR `100`)  
**AND**  
(Date and Time: > Clear date time)  
