<tags data-value="Monitor,Windows Service,Integration Manager,Ping"></tags>

:fa-heartbeat: Ping
=====
 
___

## :fa-info: Information
The **Windows Service Monitor Agent** for Integration Manager can perform **ping** operations (from any Windows Server where the service account is local administrator)

![PingResourceWarning][]


Ping functionality was introduced with version 4.4.0.3.

## :fa-line-chart: Metrics
The **Ping** operations are measured and can be visualized on the [Dashboard][]  
![PingMetrics][]

### :fa-flash: Actions
From the [Resource][Resources], click on the Action button and select **Response time chart**  
![PingActionMetrics][PingActionMetrics]



Configuration of Metrics for the [Dashboard][] 
![PingMetrics2][0]


## :fa-wrench: Remote Configuration
In the [Sources][] pane, select the agent and press **Remote Configuration**

Configure **Ping** from the Ping Tab.
Any number of hosts/devices/servers may be pinged.

![PingTab][]


The following properties can be set:

* **Name**
* **Host name or IP-address**
* **Enable Ping**
* **Response Time Warning Threshold**
* **Response time Error threshold**
* **Sample Duration**

![PingTabConfiguration][]
___

### :fa-hand-o-right: Next Step  
:fa-desktop: [Monitor Views][]  

##### :fa-cubes: Related  
:fa-plus-square: :fa-download: [Add or manage a Source][]  
:fa-desktop: [Monitor Agents][]    
:fa-cogs: [Administration][]    
:fa-cloud-download: [Sources][]  

<!--References -->

[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Dashboard]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/1.%20Dashboard/Overview.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master

[PingResourceWarning]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WindowsServer/PingResourceWarning.png
[PingMetrics]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WindowsServer/PingMetrics.png

[PingActionMetrics]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WindowsServer/PingActionMetrics.png
[PingTab]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WindowsServer/RemoteConfig/Ping/PingTab.png
[PingTabConfiguration]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WindowsServer/RemoteConfig/Ping/PingTabConfiguration.png

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WindowsServer/RemoteConfig/Ping/PingMetrics.png

