<tags data-value="Configuration, Source, Scheduled Tasks"></tags>

:fa-edit: Scheduled Tasks
=====
					


___

## :fa-info: Information
In this section you will learn how to configure the [Remote Configuration][] for the IM Windows Server Monitor Agent provides the ability to monitor Scheduled Tasks



## :fa-wrench: Settings

Outcome of jobs. The Agent default OK values are 0, -2147479295, 267009 or -2147023840,

You may setup other OK values, first add the task as a Specific scheduled task, then add Expected exit code(s) for this jobb now only the values in the list will be used as OK values, any other value will set the resource in an error state.


![png_png_WindowsServerRemoteConfigTab_ScheduledTasks][]




[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Remote%20Configuration/Remote%20Configuration.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master  


[png_png_WindowsServerRemoteConfigTab_ScheduledTasks]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WindowsServer/RemoteConfig/Tab_ScheduledTasks.png