<tags data-value="Monitor,Windows,Server,Windows Server"></tags>

:fa-windows: Windows Server Monitor Agent
=============================================

Monitor Windows Servers with Integration Manager's monitor agent for Windows Servers.
					
## :fa-info: About
This agent allows you to easily measure, monitor and resolve problems by take actions for many common Windows features.

* Alerts for common Windows features 
* Any number of Windows Servers can be monitored from a single agent
* Multiple agents can be deployed on multiple servers for scalability, security and performance
* Different resources can be monitored on different servers
* :fa-line-chart: Metrics support
* :fa-flash: Remote Actions

![MetricsExample][2]  
*Metrics example*

## :fa-sliders: Monitor Capabilities
The agent has the following list of capabilities, within Integration Manger managed as [Categories][]:

* Any Windows Server anywhere
* [Disk][]
* [CPU][]
* [Memory][]
* [Network][]
* [Windows Services][WindowsServices]
* [Scheduled Tasks][]
* [Event Log][]
* [Ping][]

![CategoriesTabs][1]

## :fa-flash: Actions
By using the built in remote [Actions][], many of your Windows Server related problems can quickly, easily and event automatically be resolved. All Actions can be performed from the [WebClient][] without using MMC/RDP over flaky or unavailable VPN connections. 

Using the [Web Client][Web Client] for Integration Manager, Actions can be sent to the monitor agent for Windows Server requesting operations to be performed on the monitored resources. With the existing privilege model you can allow certain users to perform operations (Actions) on selected resources.

List of Actions on the [Resources][] of [Windows Server Categories][Categories]:

* **Windows Services**
    * Actions
        * Start
        * Stop
        * Restart
        * Change Startup type
        * Details
* **Disk**
    * Metrics
* **CPU**
    * Metrics
* **Memory**
    * Metrics
* **Network**
    * Metrics
* **Scheduled Tasks**
    * Actions
        * Run
        * Enable
        * Disable
* **Ping**
    * Metrics
* **Event Log**
    * Actions
        * Show (using custom display filter)
    
## :fa-line-chart: Metrics
Resource consumption/usage (CPU, memory, disk, ping and network) can be displayed on the [Dashboard][] as a Widget using the built in graphs.
Select between data for the past 15 minutes, past hour, past day or past week.

## :fa-arrows-alt: Supported Versions
* All Windows Versions and editions beginning from Windows Server 2008 R2 and later

See :fa-check-square-o: [PreRequisites for Windows Server Monitor Agent][] for additional details installing and running the agent.

## :fa-list-alt: Release Log
For detailed information about features and bug fixes, please see the [Release Log](http://support.integrationsoftware.se/support/discussions/forums/1000229209)

___

### :fa-hand-o-right: Next Step
:fa-rocket: [Install Windows Server Monitor Agent]  
:fa-edit: [Configuration] of the agent  

#### :fa-cubes: Related
:fa-check-square-o: [PreRequisites for Windows Server Monitor Agent]  

[Install Windows Server Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/2.%20Install.md?at=master  
[PreRequisites for Windows Server Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/1.%20PreRequisites.md?at=master
[Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/3.%20Configuration.md?at=master
[Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/Categories/Overview.md?at=master
        
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WindowsServer/CategoriesTabs.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WindowsServer/MetricsExample.png

[WindowsServices]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/Categories/Windows%20Services.md?at=master
[CPU]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/Categories/CPU.md?at=master
[Memory]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/Categories/Memory.md?at=master
[Scheduled Tasks]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/Categories/Scheduled%20Tasks.md?at=master
[Disk]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/Categories/Disk.md?at=master
[Network]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/Categories/Network.md?at=master
[Event Log]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/Categories/Event%20Log.md?at=master
[Ping]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/Categories/Ping.md?at=master


[Actions]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/1.%20View%20Monitor%20Views/2.%20View%20Single%20Monitor%20View/Actions/Actions.md?at=master


[Dashboard]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/1.%20Dashboard/Overview.md?at=master
