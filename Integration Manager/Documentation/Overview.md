<tags data-value="Information,About,Documentation"></tags>

Integration Manager<sup>:fa-trademark:</sup> Documentation
=====

Learn how to master Integration Manager and get in control of your system integration solutions. This documentation is provided as an integral part of the [Web Client Administration Console][Web Client] with latest version available on the Internet.

<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

:fa-check-square-o: Videos on  ![You Tube][iconYoutube] [YouTube][]  
:fa-check-square-o: Information on ![Twitter][iconTwitter] [Twitter][] <a href="https://twitter.com/IntegrationSoft" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @IntegrationSoft</a>  
:fa-check-square-o: Contact on  ![Linked In][iconLinkedIn] [LinkedIn][]  
  
:fa-check-square-o: :fa-home: Online documentation: [Latest version][]  
:fa-check-square-o: :fa-text: Latest release notes: [Release Notes][]  
:fa-check-square-o: Support site: [Support Site][]  

### What is Integration Manager
Integration Manager is a self service tool built for the business and the organization ([ICC][]) providing support and maintenance for System Integration Solutions.  
Integration Manager provides users and organizations with operations support, data insights and useful documentation at your fingertips when needed. You can easily track and monitor the current and past status of all resources part of an integration chain. This support comes from 4 major features that Integration Manager has.
 
* :fa-hdd-o: [Log][1]
* :fa-desktop: [Monitoring][2]
* :fa-sitemap: [Repository Model][3]
* :fa-pie-chart: [Reports and Power BI][Reports]


<table>
<tr>
<th>Integation Manager</th>
<th>1 tool, 1 License, all included</th>
</tr>
<tr>
<td style="min-width: 244px;"><img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/4PillarsCombined.png" /></td>
<td><a href="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/LogFromAnySource.png" target="blank"><img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/LogAgent/LogFromAnySource.png" /></a></td>
</tr>
<table>

:fa-key: Documentation is Key to a manageable Systems Integration portfolio. Integration Manager provides a way for organizations to unite on a common scheme by configuring and using the [Repository][] model capabilities. 

:fa-hdd-o: [Log][]  
Integration Manager provides logging capabilities from most Integration Platforms through the use of **Log Agents**.
* Microsoft BizTalk Server (2006->)
    * BizTalk DTA Tracking and/or
    * BizTalk Pipeline Components  
* Microsoft BizTalk Services  
* Microsoft Logic Apps  
* Log4Net  
* Powershell  
* VBScript / [TEIS][]  
* Any data source from custom code using the [Log API][]
    * iCore
    * Log4J (Mule)

Read more about the out of the box **Log Agents** [here][Log Agents]. 

Input from theses sources may be used within [Log Views][].
 
:fa-desktop: [Monitor][]

* [Monitor Agents][]
* [Monitor Views][]   

### :fa-hand-o-right: Next Step  
:fa-cogs: [Administration][]

##### :fa-cubes: Related  
:fa-sitemap: [Repository][]

<!--References -->
[Reports]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Reports/Overview.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
[Overview.txt]:https://bitbucket.org/imdevelopers/products-documentation/raw/10c62a8e0a8943d4ceb5f840028e417c0b1a8540/Integration%20Manager/Documentation/Overview.txt
[YouTube]:https://www.youtube.com/channel/UCwyKPt2EgxjHpS_dl1-QfnQ "Videos for Integration Manager on YouTube"
[Twitter]:https://twitter.com/IntegrationSoft
[LinkedIn]:https://www.linkedin.com/company/3300214

[Latest version]:http://documentation.integrationmanager.se/#/ "Latest version of the documentation Online"

[Release Notes]:http://support.integrationsoftware.se/support/discussions/forums/1000227207 "Access the release notes for Integration Manager"

[iconYoutube]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Logos/YouTube/YouTube-social-icon_red_24px.png
[iconTwitter]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Logos/Twitter/TwitterLogo_55acee_24px.png
[iconLinkedIn]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Logos/LinkedIn/LinkedInLogo_24px.png

[Support Site]:http://support.integrationsoftware.se/support/home "Support site for the products from Integration Software"
[1]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/Log.md?at=master
[2]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/Monitor.md?at=master
[3]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master

[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/4PillarsCombined.png

[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Overview.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[Monitor]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/Monitor.md?at=master
[Log]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/Log.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[ICC]:https://en.wikipedia.org/wiki/Integration_competency_center
[Log API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Log%20API/Overview.md?at=master
[TEIS]:https://www.tieto.se/tjanster/processtjanster/teis-tieto-enterprise-integration-server