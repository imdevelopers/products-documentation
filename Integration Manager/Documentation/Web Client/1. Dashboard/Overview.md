<tags data-value="Dashboard,Log,Monitor,Repository,Administration"></tags>

:fa-tachometer: Dashboard   
=====
					


___

## :fa-info: Information

The **Dashboard** is a customizable area for sharing information for end users of the [Web Client][].

<table>
<tr>
<th>Out of the box</th>
<th>With Metrics from Monitor Agents</th>
</tr>
<tr>
<td><img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Dashboard.png"/></td>
<td><img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/DashboardMetrics.png"/></td>
</tr>
<table>

Information and links may stem from:

* **Built in Dashboard items** 
    1. :fa-desktop: Information from [Monitor Views][] available for currently logged on [User][Users]
    2. :fa-cogs: Offline internal core services
        * [Logging Service][]
        * [Monitor Views][]        
    3. :fa-id-card-o: License issues
    4. :fa-user-secret: Last Audit Logs (user and core service related operations)
    5. :fa-hdd-o: Log Events per day
    6. :fa-puzzle-piece: Statistics for [Integrations][] in the repository model
    7. :fa-sign-in: Statistics for [End Points][] in the repository model



* **Custom Dashboard items**
    1. :fa-pie-chart: Embedded Power BI Reports
    2. :fa-bar-chart: Metrics charts from Monitor Agents 
    3. :fa-code: Any embedded HTML source


The following **Metrics** and type of **graphs** are available 

* :fa-windows: [Windows Server Monitor Agent][Windows Server Agent]
	* CPU (and individual cores)
	* Memory
	* Disc
	* Network

* :fa-connectdevelop: [BizTalk Server Monitor Agent][BizTalk Agent]
	* Throttling in BizTalk (global and individual hosts)  

* :fa-globe: [Web Services Monitor Agent][Web Services Agent]
	* Response-times for Web Services

* :fa-users: [Dynamics CRM Monitor Agent][Dynamics CRM Agent]
	* Logon hours statistics 
	* Logon history (last logon)

The Metrics data may be presented in any of the following formats
* :fa-pie-chart: Pie charts
* :fa-table: Tables
* :fa-bar-chart: Bar charts
* :fa-line-chart: Line charts
* :fa-list: Lists

## Built in Dashboard items

### :fa-desktop: 1. Monitor Views
Current state of available [Monitor Views][] are presented in a pie chart. Click on any part of the pie chart to open the list of accordingly filtered [Monitor Views][].

![monitorviews][1]

Click on the :fa-cogs: **Customize** button to present a modal with the option to view the current state from resources from chosen [Monitor View][Monitor Views]

![customizemonitorviews][2]

### :fa-cogs: 2. Offline internal core services
If any of the built in internal core services [Monitoring Service][] or the [Logging Service][] are offline/not available/stopped all users will be notified on top in the dashboard that there is a problem that needs to be addressed by an Administrator.

![offlineservices][3]

### :fa-id-card-o: 3. License issues
If there is a problem with the license key (most often expiration date is nearing) information about this is available on top in the dashboard. This is a problem that needs to be addressed by an Administrator.

### 4. Last Audit Logs
The last Log Audits entries are displayed. This is mostly implemented to notify users about the fact that Integration Manager is putting all user and service related operations into a tamper proof Log Audits table.

![LastLogAudits][5]

### :fa-user-secret: 5. Log Events per day
The number of archived transactions per day (filter may be applied to compare the last 4 weeks).

![LogEventsPerDay][6]

### :fa-puzzle-piece: 6. Statistics for Integrations
Statistics about the creation of [Integrations][] from within the [Repository][].

![IntegrationStat][7]


### :fa-sign-in: 7. Statistics for End Points
Statistics about the creation of [End Points][] from within the [Repository][].

![EndPointsStat][8]


## Custom Dashboard items
The Dashboard may be further customized by an Administrator. Some examples are listed below.

### :fa-pie-chart: 1. Embedded Power BI Reports
All information within Integration Manager may be consumed from within [Power BI][] Using the REST based [Web API][]. The following example is using Embedded HTML from the Power BI platform with data from the Web Api.

![powerbi][9]


### :fa-bar-chart: 2. Metrics charts from Monitor Agents  
Using *Metrics* from any of the supported [Monitor Agents][] an Administrator can add and organize information on the Dashboard.

![powerbidashboard][10]


#### :fa-code: Edit HTML
Follow the steps to add Metrics graphs to the Dashboard (you need RDP to the IIS server hosting the Web Client and administrative privileges to perform the steps)
1. Create an html file in the ```C:\Program Files (x86)\Integration Software\Integration Manager\WebClient\Plugins```

    NOTE: The specific folder may vary depending on your installation

2. Copy the HTML content from the Metrics modal (Resource in Monitor View where Actions are allowed and Metrics are implemented)

*CPU Metrics Example below taken from the Windows Service Monitor Agent*
![metricshtml][11]

3. Reload Dashboard page

The following template may be used to create a single framed box hosting 4 metrics graphs in a 2*2 table
Replace the panel-heading with your appropriate title and replace the comments with the code to embed from metrics enabled resources.

```html
<div class="row col-spacing">
	<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12">
		<div class="panel panel-primary widget">
			<div class="panel-heading">Your Title Goes Here</div>
			<div class="panel-body half-padding">
				<div class="row col-spacing">
					<!-- Replace me -->
					<!-- Replace me-->
				</div>
				<div class="row col-spacing">
					<!-- Replace me-->
					<!-- Replace me-->
				</div>
			</div>
		</div>
	</div>
</div>	
```


### :fa-code: 3. Any embedded HTML source
Using the technique in the previous step any code of your own liking may be added for display in the Dashboard of Integration Manager 

___

### :fa-hand-o-right: Next Step  
:fa-pie-chart: [Reports][]  
:fa-desktop: [Monitor][]  

##### :fa-cubes: Related  
:fa-globe: [Web Client][]    
:fa-desktop: [Monitor Views][]  
:fa-cogs: [Administration][]  
:fa-sitemap: [Repository][]  
:fa-hdd-o: [Logging Service][]  
:fa-user: [Users][]    
:fa-download: [Monitoring Service][]  
:fa-sign-in: [End Points][]   
:fa-cloud: [Web API][Web API]  
:fa-cloud-upload: [Monitor Agents][]  

<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Dashboard/MonitorViewsPieChart.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Dashboard/CustomizeMonitorViewsPieChart.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Dashboard/OfflineServices.png
<!-- 4 Licence image -->
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Dashboard/LastLogAudits.png
[6]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Dashboard/LogEventsPerDay.png
[7]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Dashboard/IntegrationStatistics.png
[8]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Dashboard/EndPointsStatistics.png
[9]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Dashboard/PowerBI.png
[10]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/PowerBIDashboard.png
[11]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Dashboard/MetricsHTML.png


[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Monitor]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/Monitor.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
[Reports]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Reports/Overview.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master

[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master

[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master

[End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/8.%20Included%20End%20Points/Included%20End%20Points.md?at=master

[Power BI]:https://powerbi.microsoft.com/en-us/
[Web API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Web%20API/Overview.md?at=master

[Integrations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/4.%20Included%20Integrations/Included%20Integrations.md?at=master

[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master

[Windows Server Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/Overview.md?at=master
[Web Services Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Web%20Services/Overview.md?at=master
[Dynamics CRM Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Dynamics%20CRM/Overview.md?at=master
[BizTalk Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Overview.md?at=master

