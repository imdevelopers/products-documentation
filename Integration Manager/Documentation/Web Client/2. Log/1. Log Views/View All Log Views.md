<tags data-value="Log,Views,Events,Messages,Search "></tags>

:fa-hdd-o: View All Log Views
=====
			
___

## :fa-info: Information
In the [Web Client][] **Log Views** provide a way for [Users][] to gain access to events, message payload and context. 
The list of **Log Views** granted to a [User][Users] by being part of one or more [Roles][] can be displayed by clicking on the **Logs** menu item.

The **name** and optional **Description** of the **Log View** is listed:

![LogViewMenu][0]
 
You can also Filter and Sort to find the right one more easily.
There are also **Actions** available for Administrators to **edit**, **delete** and  **clone** the selected Log View. 

Click on one of the **Log Views** to start your search, please read more [here](https://bytebucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/2.%20Log/3.%20How%20to%20Search%20the%20Log%20View/Search.md)

___

### :fa-filter: Filter the result
You can filter the list of  **Log Views** by typing characters part of either the **Name** or **Description**.

![Filter][4]

### :fa-sort: Sort
Sort the **Log Views** by Name or Description by clicking the words. 

The list is by default sorted from A-Z. 

![Sort][3]
### Edit, Delete or Clone
You'll find the options to edit, delete, or clone a Log View under Action.

#### :fa-edit: Edit
![Edit, Action and Clone][2]

Editing the Log View will take you to the editing page
 which is the same as the [Add or manage Log View][] page.
 
#### :fa-trash: Show deleted

Delete will not delete the Log View but instead flag it as deleted.

When [Show Deleted][] is activated, all the "deleted" **Log Views** will be shown.

![Show Deleted][5]

#### :fa-clone: Clone
Clone will make a new copy base on the **Log View** you select.

This feature is useful when you want to make a similar **Log View** without having to redo all of the settings.

Give the cloned Log View a name and you'll get to the editing page to modify the settings.  
![Clone Name][6]



___
### :fa-hand-o-right: Next Step
:fa-hdd-o: [Log Views][]

#### :fa-cubes: Related
:fa-globe: [Web Client][]    
:fa-user: [Users][]  
:fa-group: [Roles][]  



<!--References -->
[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Log/LogMenuItemWithLogViews.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/7.2%20Edit.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/7.4%20Sort.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/7.3%20Filter.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/includedeleted.png
[6]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/7.6%20Clone.png

[Add or manage Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Create%20New%20Log%20View.md?at=master
[Show Deleted]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Show%20Deleted.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master

[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
