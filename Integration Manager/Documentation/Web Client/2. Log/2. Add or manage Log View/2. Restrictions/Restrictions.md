<tags data-value="Restrictions,Log,Search Field,Expression,Operator"></tags>

Restrictions
=====
					


___

## :fa-info: Information

Restrictions is limiting the Search Field by using [Mathematical Operators][], Expressions and Logical Operators.


## Instructions

### Search Restrictions

This is the Search Restriction window.

![Search Restrictions][1]

### Search Field

Choose a Search Field to Restrict.

Filter the result to easier find the right one.

![Search Field][2]

### Mathematical Operator

The Matematical Operator is the operation of the expression to generate a rule of which messages will be excluded.

![Mathematical Operator][3]

### Expression and Logical Operator

Expression is the value which together with the Mathematical Operator will decide what's excluded.

Logical Operator only works if at least two Restrictions are added and decides if both of them will be restricted or just one at a time.

Logical Operators won't work as within brackets.

![Expression and Logical Operator][4]



<!--References -->
[Visible In Search]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/A10.%20Visible%20In%20Search/Visible%20in%20Search.md?at=master
[Mathematical Operators]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/3.%20How%20to%20Search%20the%20Log%20View/5.%20Mathematical%20Search%20Operators/Mathematical%20Search%20Operators.md
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A16.%20Restrictions.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/Restriction/SelectSearchField.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A16.%20Mathematical%20Operator.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A16.%20Expression%20and%20logical%20operator.png
___
### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  
