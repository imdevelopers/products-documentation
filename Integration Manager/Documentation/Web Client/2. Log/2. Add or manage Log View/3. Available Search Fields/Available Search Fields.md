<tags data-value="Log,Search,Fields,"></tags>

Available Search Fields
=====
					


___

## :fa-info: Information
Choose which [Search Fields][] should be available in the [Log View][].

___
## Instructions

You can Add, Filter and Remove Search Fields.

![Search Fields][1]


<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A17.%20Search%20Field.png

[Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/Search%20Fields.md?at=master
[Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master

___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  
