<tags data-value="Included,Integrations,Log,"></tags>

Included Integrations
=====
					


___
## :fa-info: Information

Choose which [Integrations][] to include in your [Log View][].

If the **Integrations** should be visible, click: [Visible In Search][].
___
## Instructions

You can add, filter, remove and rename the artifact.

![Include Integrations][1]



<!--References -->
[Visible In Search]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/A10.%20Visible%20In%20Search/Visible%20in%20Search.md?at=master
[Integrations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/4.%20Included%20Integrations/Included%20Integrations.md?at=master
[Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A18.%20Included%20Integrations.png
___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  
