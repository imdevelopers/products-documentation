<tags data-value="Included,Services,Log"></tags>

Included Services
=====
					


___

## :fa-info: Information
Choose which [Services][] to include in your [Log View][].

If the Services should be visible, click: [Visible In Search][].

## Instructions

You can add, filter, remove and rename the artifact.


![Include Integrations][1]




<!--References -->
[Visible In Search]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/A10.%20Visible%20In%20Search/Visible%20in%20Search.md?at=master
[Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/6.%20Included%20Services/?at=master
[Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master


[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A20.%20Services.png
___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  
