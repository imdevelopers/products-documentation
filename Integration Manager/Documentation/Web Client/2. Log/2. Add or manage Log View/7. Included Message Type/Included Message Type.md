<tags data-value="Included,Message Type,Log"></tags>

Included Message Type
=====
					


___

## :fa-info: Information
Choose which [Message Types][] to include in your [Log View][].

If the Message Types should be visible, click: [Visible In Search][].

## Instructions

You can add, filter, remove and rename the artifact.


![Include Integrations][1]


<!--References -->
[Visible In Search]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/A10.%20Visible%20In%20Search/Visible%20in%20Search.md?at=master
[Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/7.%20Included%20Message%20Type/Included%20Message%20Type.md?at=master
[Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master


[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A21.%20Message%20Type.png
___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  
