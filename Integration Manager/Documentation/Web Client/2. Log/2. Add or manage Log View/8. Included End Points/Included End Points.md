<tags data-value="Included,End Points,Log"></tags>

Included End Points
=====
					


___

## :fa-info: Information
Choose which [End Points][] to include in your [Log View][].

If the End Points should be visible, click: [Visible In Search][].

## Instructions

You can add, filter, remove and rename the artifact.


![Include Integrations][1]


<!--References -->
[Visible In Search]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/A10.%20Visible%20In%20Search/Visible%20in%20Search.md?at=master
[End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/8.%20Included%20End%20Points/Included%20End%20Points.md?at=master
[Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master


[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A22.%20End%20Points.png
___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  
