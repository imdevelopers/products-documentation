<tags data-value="Included,Log,Agents"></tags>

Included Log Agents
=====
					


___

## Inforamtion

Choose which [Log Agents][] to include in your [Log View][].

If the Log Agents should be visible, click: [Visible In Search][].

## Instructions

You can Add, Filter, Remove and Rename the Artifact.

![Include Integrations][1]


<!--References -->
[Visible In Search]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/A10.%20Visible%20In%20Search/Visible%20in%20Search.md?at=master
[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client%2F5.%20Administration%2F1.%20Log%2F1.%20Log%20Views%2F2.%20Add%20or%20manage%20Log%20View%2F9.%20Included%20Log%20Agents%2FIncluded%20Log%20Agents.md?at=master?at=master
[Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master


[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A23.%20Log%20Agents.png
___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  
