<tags data-value="Mathematical,Search,Operators,Restrictions"></tags>

Mathematical Search Operators
=====
					


___

## :fa-info: Information
Mathematical Operators are used in [Restrictions][] and in the Additional Fields within a [Log][].


| Symbol  |Meaning   |
|---|---|
|=   |Equals   |
|<>   |Isn't Equal to   |
|Like   |Like any bofore or after provided value   |
|Left Like   |Like any before provided value   |
|Right Like   |Like any after provided value   |
|Not Like   |Like no provided value, before or after   |
|Left Not like   |Like not any before provided value   |
|Right Not Like   |Like not any after provided value   |
|>=   |Equal or bigger than   |
|>  |Bigger than   |
|<   |Less than   |
|<=   |Equal or less than   |


Although, they look a bit different:

Additional Fields

![Mathematical Search Operators][1]
![Alternative][2]

Restrictions

![Restrictions][3]
![More Alternatives][4]

<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A15.%20Mathematical%20Search%20Parameters.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A15.%20Alternative.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A15.3%20Search%20restriction1.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A15.3%20Search%20restriction2.png

[Restrictions]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/2.%20Restrictions/Restrictions.md?at=master&fileviewer=file-view-default
[Log]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master
___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  
