<tags data-value="Resend,Messages,Allow,With,Selected"></tags>

Resend Message
=====
					


___

## :fa-info: Information
To resend a message may be useful when the message is fine but the receiver was faulty but is now fixed.

However, if you want to edit the message, it needs to be repaired. 

#### Allow resend of messages

First, make sure "Allow resend of messages" is checked in the edit view.

More information is available in [Add or manage Log View][].

![Allow resend of messages][2]

## Instructions

#### Resend Message

Resend a Message by Action.

![Resend][1]

#### Select All

Select All Messages by checking the box to the left of Log Date.

You can also select multiple messages by selecting multiple boxes.

![Select All][3]

#### With Selected

Resend the selected messages under "With selected".

![With Selected][6]

### Confirmation

After the message is supposed to have been resent, a dialog box will confirm if the message were resent or if an error occured.

#### Success

![Success][4]

#### Error

![Error][5]




___




<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A96.%20Resend%20Message.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A96.2%20Allow%20resend%20of%20messages.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A96.5%20Select%20All%20Messages.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A96.6%20Success.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A96.7%20Error.png
[6]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A96.3%20With%20selected%20-%20Resend.png

[Add or manage Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master
___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  
