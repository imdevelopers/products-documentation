<tags data-value="View,Message,Formatted,XSLT,Stylesheet"></tags>

View Message as formatted
=====
					


___

## :fa-info: Information

View a message from a [Log View][] formatted with XSLT.

___

## Instructions

To be able to view a message as formatted, with a stylesheet, you will need to [Add or manage a Stylesheet][] and [connect a Message Type to it][].

### Overview

![Overview][3]

#### Find Message Type

The Message Type is visible in the **Log View**.

![Message Type][2]

#### View

To view message as formatted, click Action and choose the right Stylesheet.

![Action][1]

If no "View message formatted as.." shows up, no Stylesheets are connected to the selected Message Type.





<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A97.%20Formatted%20message.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A98.%20Message%20Type.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A94.%20Search%20Log.png

[Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master
[Add or manage a Stylesheet]:https://bytebucket.org/imdevelopers/products-documentation/src/bd1221ab587a4bbcb51a7ae99b195f33281810de/Integration%20Manager/Documentation/5.%20Administration/4.%20Templating/Stylesheets/2.%20Add%20Stylesheet/Add%20Stylesheet.md?at=master
[connect a Message Type to it]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Templating/Stylesheets/2.%20Add%20Stylesheet/Connect%20Message%20Types%20to%20Stylesheet/Connect%20Message%20Types%20to%20Stylesheet.md?at=master
___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  
