<tags data-value="API,Excel,Power,Query,Log"></tags>

Use API in Excel with Power Query
=====
					


___
## :fa-info: Information

Integration Manager can be used as an API and the data from a [Log View][] can be transmitted to Microsoft Excel through Power Query.

___

## Instructions

### Download Power Query

Power Query can be downloaded [here][] as 64 or 32-bit, in your preferred language.

![Download][3]

### Copy API URL

In the Log View, after a search is done, there's an option to Copy API URL.

![Options][1]



Copy the URL

![Copy API URL][2]

___

### Excel

#### From web

Open Excel and go to the Power Query tab and click "From web".

![From the web][4]

#### Paste URL

Paste the URL.

![URL][5]

#### Authentication

Click Windows and "Use alternative authentication".

Log in with your user name and password from Integration Manager.

![Authentication][6]

#### Parse to JSON

Go to the "Transform" tab and click "Parse" and choose JSON.

![Parse to JSON][7]

#### Choose Data

If too much data is selected, it won't fit in Excel.

Therefore, in this example, a limited amount of data will be selected.

![Button][16]



![Collection][8]

		Tip: Uncheck the box at the bottom to save space in the columns.

#### Items

Unmark all columns and choose "Items"

![Items][9]

#### Data

![Data][10]

#### Message Type

![Message Type][11]

#### All

![Mark All][12]

#### Result

![Ready][13]

#### Close

Go to the start tab and hit "Close".

![Close][14]

#### Finished

Now you can use the data with Excel.

![Finish][15]







<!--References -->

[Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master
[here]:https://www.microsoft.com/en-US/download/details.aspx?id=39379


[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/B5.%20More%20Alternatives.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/B5.2%20Copy%20Api%20URL.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/C17.%20Download%20Power%20Query.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/C17.2%20From%20the%20web.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/C17.3%20URL.png
[6]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/C17.4%20Authentication.png
[7]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/C17.5%20JSON.png
[8]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/C17.6%20Collection.png
[9]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/C17.7%20Items.png
[10]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/C17.8%20Data.png
[11]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/C17.9%20Message%20Type.png
[12]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/C17.A10%20All%20Message%20Type%20information.png
[13]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/C17.A11%20All.png
[14]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/C17.A12%20Close.png
[15]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/C17.A13%20Finish.png
[16]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/C17.A14%20Button.png
___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  
