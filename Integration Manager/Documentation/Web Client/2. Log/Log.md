<tags data-value="Log,Events,Messages,View"></tags>

:fa-hdd-o: Log
=====
					


___

## :fa-info: Information
Events comes from [Log Agents][]. Events can have a message body attached. Every message  body is distinguished and known by its [MessageType][Message Types]. Messages are stored obfuscated and compressed in the Log databases.
An Event can also hold context data, this data is stored in a KeyValue Collection. Since Context data can be sensitive as well as the message body, Integration Manager stores this data obfuscated and compressed in the Log databases.
Data from message body and Context values can be extracted using [Search Fields][]. All logged events stem from a well known [EndPoint][End Points].

## :fa-hdd-o: Log Views
Using the information for a logged event an Administrator can create Log Views that gives end users, customers, and the business access and insights to this data.

[How to add or manage a Log View][]  

Here's an example of a **Log View** in Integration Manager: 

![Log][1]

### :fa-list-ol: Customize Log Views using your own set of selected columns
**Log Views** can be individually configured to render the result with all information elements available within Integration Manager using a pre-defined set of columns. This functionality is provided by the use of  [Display Field Configurations][]


    TIP: Create user friendly log views to provide self service for your organization, releasing time from the IT-department/operations 
___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-hdd-o: [How to add or manage a Log View][]    
:fa-plus-square: :fa-list-ol: [Add or manage Display Field Configurations][]  

##### :fa-cubes: Related  
:fa-hdd-o: [View All Monitor Views][]    
:fa-list-ol: [Display Field Configurations][]


<!--References -->

[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client%2F5.%20Administration%2F1.%20Log%2F1.%20Log%20Views%2F2.%20Add%20or%20manage%20Log%20View%2F9.%20Included%20Log%20Agents%2FIncluded%20Log%20Agents.md?at=master?at=master
[Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/7.%20Included%20Message%20Type/Included%20Message%20Type.md?at=master
[End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/8.%20Included%20End%20Points/Included%20End%20Points.md?at=master
[Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/3.%20Available%20Search%20Fields/Available%20Search%20Fields.md?
[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[How to add or manage a Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master
[View All Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/1.%20View%20All%20Monitor%20Views/View%20All%20Monitor%20Views.md?at=master


[Display Field Configurations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/1.%20Display%20Field%20Configurations/Overview.md?at=master&fileviewer=file-view-default
[Add or manage Display Field Configurations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/1.%20Display%20Field%20Configurations/Add%20or%20manage%20Display%20Field%20Configurations.md?at=master&fileviewer=file-view-default


[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A11.%20Log.png
