<tags data-value="Actions, Resources, Monitor Agents, Monitor Views"></tags>

:fa-flash: Actions
=====

___

## :fa-info: Information
**Actions** are implemented on many of the [Monitor Agents][] and provide fast access to interact with monitored [Resources][Resource]. For example get details, start and stop services, view history and run things on demand.

**Actions** are available operations if allowed on a [Resource][] in a [Monitor View][].

All executed actions are logged in the Audit Log.

## BizTalk
For example, some of the available **actions** for [BizTalk][] [Resources][Resource]:

<table>
<tr>
<th>Example 1: Suspended Messages (resumable)</th>
<th>Example 2: Receive Location</th>
</tr>
<tr>
<td><img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/BizTalk/Categories/SuspendedInstances/Actions.png"/></td>
<td><img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/C15.%20All%20Actions.png"/></td>
</tr>
<table>

## Windows Services
Some of the available actions for [Windows Server Monitor Agent][Windows Server Agent] Resources:

<table>
<tr>
<th>Example 1: Scheduled Tasks</th>
<th>Example 2: Services</th>
</tr>
<tr>
<td><img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WindowsServer/ScheduledTaskActions.png"/></td>
<td><img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/MonitorAgent/WindowsServer/ServicesActions.png"/></td>
</tr>
<table>
___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]  
:fa-plus-square: :fa-desktop: [Monitor Views][Monitor View]  

##### :fa-cubes: Related  
:fa-desktop: :fa-cloud-upload: [Monitor Agents][]      



<!--References -->
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Monitor View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/Monitor.md?at=master
[Add or manage Monitor View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20Monitor%20View/Add%20Monitor%20View.md?at=master
[View Single Monitor View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/1.%20View%20Monitor%20Views/2.%20View%20Single%20Monitor%20View/View%20Single%20Monitor%20View.md?at=master
[BizTalk]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/BizTalk/Overview.md?at=master
[Windows Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Services/Windows%20Services.md?at=master
[Resource]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master

[Windows Server Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Windows%20Server/Overview.md?at=master