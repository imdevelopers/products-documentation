<tags data-value="Create,Add,Monitor,View"></tags>

Link to Add or manage Monitor View
=====
					


___

[Add or manage Monitor View][]

<!--References -->
[Add or manage Monitor View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  
