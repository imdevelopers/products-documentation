<tags data-value="Monitor,Sources,Live,View,Events"></tags>

:fa-desktop: Monitor Views
=====
					


___
## :fa-info: Information
**Monitor Views** are used to group [Resources][] inteneded for self service. An Administrator of Integration Manager can provide rolebased **Monitor Views**. This means that the solutions may be partitioned and managed in a very fine grained way. A user is only allowed to see and touch explicitly allowed [Resources][]. Using **Monitor Views** in many cases removes the need for Remote Desktop Sessions and administrative privileges on servers. 


The **Monitoring** capabilites of Integration Manager is provided by the [Monitor Agents][]. These agents are setup as [Sources][] and are governed by the [Monitoring Service][]
The [Monitor Agents][] provide [Resources][] with different set of states. 
User specific alerts are provided by configuring [Monitor Views][]. **Monitor** is where you access and configure the [Monitor Views][].  

There can be any number of [Monitor Views][] defined.

You can [Add][Add or manage Monitor Views], [Edit][Add or manage Monitor Views] or [Delete][Show Deleted] records. 
The list of defined [Monitor Views][] are listed in the [overview][View All Monitor Views].
![Monitor][1]

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]  
:fa-plus-square: :fa-user: [Add or manage Users][]  
:fa-plus-square: :fa-group: [Add or manage Roles][]  
:fa-edit: :fa-lightbulb-o: [Resources][]  

##### :fa-cubes: Related  
:fa-user-secret: [Authorization][]  
:fa-plus-square: :fa-user: [Users][]    
:fa-plus-square: :fa-group: [Roles][]    
:fa-plus-square: :fa-hdd-o: [Log Views][]   
:fa-plus-square: :fa-desktop: [Monitor Views][]  
:fa-edit: :fa-lightbulb-o: [Resources][]  


<!--References -->                         
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/MonitorViews.png

[View All Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/1.%20View%20All%20Monitor%20Views/View%20All%20Monitor%20Views.md?at=master


[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
___

[Monitor]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/Monitor.md?at=master
[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Remote%20Configuration/Remote%20Configuration.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master

[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master

[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master


[View Alarm Plugins]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/6.%20Notifications/Alarm%205.%20Settings/1.%20Alarm%20Plugins/1.%20View%20All%20Alarm%20Plugins/View%20All%20Alarm%20Plugins.md?at=master
[Edit Alarm Plugins]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/1.%20Alarm%20Plugins/2.%20Edit%20the%20email%20plugin/Edit%20the%20email%20plugin.md?at=master
[Alarm Plugins]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/1.%20Alarm%20Plugins/Alarm%20Plugins.md?at=master

[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master

[Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/Categories.md?at=master


[Authorization]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/Authorization.md?at=master
[Show Deleted]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Show%20Deleted.md?at=master


[Add or manage Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/2.%20Add%20or%20manage%20Role/Add%20or%20manage%20Role.md?at=master
[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[View All Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master

[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Add or manage Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/2.%20Add%20or%20manage%20User/Add%20or%20manage%20User.md?at=master
[View All Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/1.%20View%20All%20Users/View%20All%20Users.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master

[Assign Users to the Role]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/2.%20Add%20or%20manage%20Role/Assigned%20Users%20in%20Role/Assigned%20Users%20in%20Role.md?at=master

