<tags data-value="Integration,Service,Custom Field,System"></tags>

:fa-puzzle-piece: View All Integrations
=====
					


___

## :fa-info: Information
The list of defined [Integrations][Integration]. A large list of **Integrations** can be narrowed down by typing characters into the filter text box.

You can [add new][Add or manage Integrations], [edit][Add or manage Integrations], [Delete][Show Deleted] records.

![Integrations][1]

___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-puzzle-piece: [Add or manage Integrations][]  
:fa-plus-square: :fa-cog: [Add or manage Services][]  


##### :fa-cubes: Related  
:fa-puzzle-piece: [Integrations][Integration]  
:fa-laptop: [Systems][]    
:fa-sitemap: [Repository][]    
:fa-cog: [Services][]    

<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/Integrations/Integrations.png

[Integration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/1.%20Integrations/Integrations.md?at=master
[Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master
[Custom Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/Custom%20Fields.md?at=master
[Show Deleted]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Show%20Deleted.md?at=master
[Add or manage Integrations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/1.%20Integrations/2.%20Add%20or%20manage%20Integration/Add%20or%20manage%20Integration.md?at=master
[Add or manage Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/2.%20Add%20or%20manage%20Service/Add%20or%20manage%20Service.md?at=master
[Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/Systems.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
