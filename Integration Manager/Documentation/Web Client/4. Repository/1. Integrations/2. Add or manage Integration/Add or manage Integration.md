<tags data-value="Add,Integration,System,Service,Custom Field"></tags>

:fa-puzzle-piece: Add or manage Integration
=====
					


___
## :fa-info: Information

In this section you will learn how to add or manage an [Integration][].

## Instructions
Add a new [Integration][] or Edit an existing from the list in the [overview][View All Integrations].
An [Integration][] can host [Service][Services] after you have successfully created the **Integration**. 

### Mandatory Fields
A **Name** is required to create the **Integration**.

![Name, Description, and Website][1]

## Optional fields
Adding a **Description** and a **Web Site** is optional. 
* **Description**: A user friendly description.
* **Web Site**: You can provide a quick link for users when working with and viewing the [Integration][]. This quick link is usually a WIKI/Sharepoint site with additional documentation. 

### :fa-wrench: Custom Fields 
As part of the [Repository][] model, You can also add [Custom Fields][] to provide additional documentation about your **Integration**.
You can connect one or more [Custom Fields][] to an [Integration][].  

![Services and custom Fields][2]

## :fa-cog: Services
You can connect one or more [Services][] to an [Integration][].  
You have the options to **Add**, **Remove**, and **Filter** the list of available **Services**.

![Connect Services to Integration][3]

___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-wrench: [Add or manage Custom Fields][]    
:fa-plus-square: :fa-cog: [Add or manage Services][]        

##### :fa-cubes: Related  
:fa-laptop: [Systems][]        
:fa-sitemap: [Repository][]      
:fa-cog: [Services][]     
:fa-wrench: [Custom Fields][]    


<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/Integrations/NameDescriptionWebsite.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/Integrations/ServicesAndCustomFields.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/Integrations/AddServicesToIntegration.png

[Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master
[Custom Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/Custom%20Fields.md?at=master
[Integration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/1.%20Integrations/Integrations.md?at=master
[Add or manage Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/2.%20Add%20or%20manage%20Service/Add%20or%20manage%20Service.md?at=master
[Add or manage Custom Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/2.%20Add%20or%20manage%20Custom%20Fields/Add%20or%20manage%20Custom%20Fields.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/Systems.md?at=master
[Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master
[End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master
[View All Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/1.%20View%20All%20Systems/View%20All%20Systems.md?at=master
[View All Custom Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/1.%20View%20All%20Custom%20Fields/View%20All%20Custom%20Fields.md?at=master
[Add or manage Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/2.%20Add%20or%20manage%20Service/Add%20or%20manage%20Service.md?at=master
[View All Integrations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/1.%20Integrations/1.%20View%20All%20Integrations/View%20All%20Integrations.md?at=master
