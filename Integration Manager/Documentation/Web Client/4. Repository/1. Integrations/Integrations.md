<tags data-value="Integration,Service,Custom Fields,System, End Points, Repository"></tags>

:fa-puzzle-piece: Integrations
=====
					


___

## :fa-info: Information
An **Integration** is the root in the [Repository][] model and holds the links for inbound and outbound [Services][]. Examples can be:
* INT001 Invoice Flow
* Monthly payments to Bank
* ... 

## :fa-exclamation: Important information 
* Integrations can be members of both [Log Views][Log View] and [Monitor Views][]

* Any number of [Services][] can be member of an **Integration**.

* Do use [Custom Fields][] to document your **Integrations** and provide useful information to key stakeholders when needed the most.

* If an **Integration** is configured as part of a [Log View][] a very restrictive filter will be in place for data available to end [Users][]. This filter is based on the selection of [End Points][] and [Message Types][]. 


    Tip: Configure Integrations properly and you will have less micro management configuring Log Views and you will also have a foundation for re-use.
    
    

___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-puzzle-piece: [Add or manage Integrations][]  
:fa-plus-square: :fa-cog: [Add or manage Services][]    
:fa-plus-square: :fa-wrench: [Add or manage Custom Field][]

##### :fa-cubes: Related  
:fa-sitemap: [Repository][]      
:fa-cog: [Services][]    
:fa-laptop: [Systems][]   
:fa-user: [Users][]  
:fa-wrench: [Custom Fields][]  
:fa-hdd-o: [Log Views][Log View]     
:fa-desktop: [Monitor Views][]  
:fa-sign-in: [End Points][]  
:fa-file: [Message Types][]  

<!--References -->
[Add or manage Integrations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/1.%20Integrations/2.%20Add%20or%20manage%20Integration/Add%20or%20manage%20Integration.md?at=master
[Add or manage Custom Field]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/2.%20Add%20or%20manage%20Custom%20Fields/Add%20or%20manage%20Custom%20Fields.md?at=master
[Custom Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/Custom%20Fields.md?at=master
[Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/Systems.md?at=master

[Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Add or manage Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/2.%20Add%20or%20manage%20Service/Add%20or%20manage%20Service.md?at=master
[Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master
[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master
[Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master

