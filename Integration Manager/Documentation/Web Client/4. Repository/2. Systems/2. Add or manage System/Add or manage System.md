<tags data-value="Repository,Systems,Services,Custom Fields"></tags>

:fa-laptop: Add or manage Systems
=====
					


___

## :fa-info: Information
In this section you will learn how to add or manage a [System][].

## Instructions
Add a new [System][] or Edit an existing from the list in the [overview][View All Systems].
A [System][] can be member of a [Service][Services] after you have successfully created the **System**. 

### Mandatory Fields
A **Name** is required to create the **System**.

![Name, Description, and Website][1]

## Optional fields
Adding a **Description** and a **Web Site** is optional. 
* **Description**: A user friendly description.
* **Web Site**: You can provide a quick link for users when working with and viewing the [System][]. This quick link is usually a WIKI/Sharepoint site with additional documentation. 

### :fa-wrench: Custom Fields 
As part of the [Repository][] model, You can also add [Custom Fields][] to provide additional documentation about your **System**.
![CustomFieldPicture][2]
___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-wrench: [Add or manage Custom Field][]    
:fa-plus-square: :fa-cog: [Add or manage Service][]        

##### :fa-cubes: Related  
:fa-laptop: [System][]        
:fa-sitemap: [Repository][]      
:fa-cog: [Services][]     
:fa-wrench: [Custom Fields][]    

<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/Systems/AddSystem.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/Systems/CustomFields.png

[Add or manage Custom Field]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/2.%20Add%20or%20manage%20Custom%20Fields/Add%20or%20manage%20Custom%20Fields.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[System]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/Systems.md?at=master
[Custom Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/Custom%20Fields.md?at=master
[Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master
[End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master
[View All Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/1.%20View%20All%20Systems/View%20All%20Systems.md?at=master
[View All Custom Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/1.%20View%20All%20Custom%20Fields/View%20All%20Custom%20Fields.md?at=master
[Add or manage Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/2.%20Add%20or%20manage%20Service/Add%20or%20manage%20Service.md?at=master
