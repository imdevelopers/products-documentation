<tags data-value="Repository,Systems, Service"></tags>

:fa-cog: Systems
=====
					


___

## :fa-info: Information
A **System** is part of a [Service][Services] in the [Repository][] model and represents the **Source** or **Destination** entity for a message exchange. Examples can be:
* BizTalk
* SAP
* Dynamics AX
* MES
* Customer ABC
* ... 

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-laptop: [Add or manage Systems][]    
:fa-plus-square: :fa-cog: [Add or manage Services][]    

##### :fa-cubes: Related  
:fa-laptop: [View All Systems][]  
:fa-sitemap: [Repository][]  
:fa-cog: [Services][]  
:fa-hdd-o: [Log Views][]

<!--References -->
[Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/5.%20Included%20Systems/Included%20Systems.md?at=master
[Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Add or manage Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/2.%20Add%20or%20manage%20Service/Add%20or%20manage%20Service.md?at=master
[Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master
[Add or manage Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/2.%20Add%20or%20manage%20System/Add%20or%20manage%20System.md?at=master
[View All Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/1.%20View%20All%20Systems/View%20All%20Systems.md?at=master


