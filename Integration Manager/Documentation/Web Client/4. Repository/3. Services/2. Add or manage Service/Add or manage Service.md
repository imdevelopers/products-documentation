<tags data-value="Add,Service,Transport Contracts,Message Types,End Points,Custom Fields"></tags>

:fa-cog: Add or manage Service
=====
					


___

## :fa-info: Information

In this section you will learn how to add or manage an [Service][Services].

## Instructions
Add a new [Service][Services] or Edit an existing from the list in the [overview][View All Services].
A [Service][Services] can host [Message Types][], [End Points][], [System][Systems] and [Custom Fields][] after you have successfully created the **Service**. 

![Add Service][2]

### Mandatory Fields
A **Name** is required to create the **Service**.

![Name, Description, and Website][1]

## Optional fields
Adding a **Description** and a **Web Site** is optional. 
* **Description**: A user friendly description.
* **Web Site**: You can provide a quick link for users when working with and viewing the [Service][Services]. This quick link is usually a WIKI/Sharepoint site with additional documentation. 
___
### :fa-laptop: System
Set the Source or Destination [System][] for Service. This setting provides information for the graphical overview in order to paint the symbols. This setting also acts as a filter when providing information about the Source Service (Only sending Services can be a Source for a Receiving Service).  

If the System is not yet created, it is possible to create it by clicking **Quick Add System**. Only the name will be is set in the System, further editing must be done within [Add or manage System][Add or manage System].  

![QuickAddSystem][7]


### :fa-microchip: Transport Contracts
Transport contracts are like a log point for the service a logged Event always contains something (Message Type) and was picked up at some place (End Point)  

![Transport Contract][png_TransportContractsModalList]

Add, Edit or Delete transport contracts  

#### :fa-pencil-square-o: Transport Contracts Add/Edit

![Transport Contract][png_TransportContractsModalEdit]

#### :fa-file: Message Types
Add the expected [Message Types][]. There can be any number of Message Type members. 

![Message Types][png_TransportContractsModalEditSelectMessageType]

#### :fa-sign-in: End Points
Add the expected [End Points][]. There can be any number of End Point members.

![End Point][png_TransportContractsModalEditSelectEndPoint]

### :fa-handshake-o: Relations
To draw a landscape with connections relations between Services are required  
![Relations Result][png_RelationsResult]  

A in Service connection to a out Service must be running on the same System to be connected.  
A out Service connection to a in Service can run on on same or different Systems.  
![Relations Edit][png_RelationsEdit]  

### :fa-wrench: Custom Fields 
As part of the [Repository][] model, You can also add [Custom Fields][] to provide additional documentation about your **Service**.

### :fa-exchange: Direction
Set the Direction. You can choose between
* Receive
* Send
* Direction two way Receive
* Direction two way Send
* None
* Unknown

If a Service in the graphical overview is not defined **Direction** *Unknown* will be displayed.  
![direction][6]
 

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-wrench: [Add or manage Custom Field][]    

##### :fa-cubes: Related  
:fa-laptop: [Systems][]        
:fa-sitemap: [Repository][]      
:fa-cog: [Services][]     
:fa-wrench: [Custom Fields][]    

<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/Services/AddNewService.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/Services/AddServiceButton.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/Services/CustomFields.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/Services/MessageTypes.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/Services/EndPoints.png
[6]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/Services/Direction.png
[7]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/Services/QuickAddSystem.png

[png_TransportContractsModalList]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/Services/TransportContractsModalList.png
[png_TransportContractsModalEdit]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/Services/TransportContractsModalEdit.png
[png_TransportContractsModalEditSelectMessageType]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/Services/TransportContractsModalEditSelectMessageType.png
[png_TransportContractsModalEditSelectEndPoint]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/Services/TransportContractsModalEditSelectEndPoint.png

[png_RelationsResult]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/Services/RelationsResult.png
[png_RelationsEdit]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/Services/RelationsEdit.png

[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master


[View All Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/1.%20View%20All%20Services/View%20All%20Services.md?at=master
[Add or manage Custom Field]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/2.%20Add%20or%20manage%20Custom%20Fields/Add%20or%20manage%20Custom%20Fields.md?at=master

[Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/Systems.md?at=master
[Add or manage System]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/2.%20Add%20or%20manage%20System/Add%20or%20manage%20System.md?at=master

[Connect Message Type]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master
[Connect End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/2.%20Add%20or%20manage%20Service/2.%20Connect%20End%20Points/Connect%20End%20Points.md?at=master
[Add or manage Custom Field to artifact]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/1.%20Integrations/2.%20Add%20or%20manage%20Integration/2.%20Add%20Custom%20Fields%20to%20artifact/Add%20Custom%20Fields%20to%20artifact.md?at=master
[Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master
[Custom Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/Custom%20Fields.md?at=master
[End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master
[Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master
