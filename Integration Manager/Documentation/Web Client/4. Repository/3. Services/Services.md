<tags data-value="Service,Message Types,End Points, 4.3"></tags>

:fa-cog: Services
=====
				


___

## :fa-info: Information
A **Service** is defined as a member of an [Integration][Integrations] within Integration Manager as one end of a communication chain. A **Service** contains information about:
* [End Points][]
* [Message Types][]
* [System][Systems]
* Direction
    * Send
    * Receive 

* A **Service** should include the [End Points][] in order for Integration Manager to dynamically figure out which [Integration][Integrations] the message exchange belongs to.  
* Restrictions within [Log Views][] can easily be enforced if the **Service** also is configured with the expected [Message Types][] that may flow on the provided [End Points][].

A Service should be named like:
* SVC001 - Receive Invoices from Customer A
* SVC001 - Send monthly salary to bank

    TIP:  Having a unique identifier part l ike SVC001 in this example makes it possible to filter large amounts of Services making administration simpler

With this naming convention the notion of **Services** will be easier to understand and follow. There are actually many information elements in the examples. Obviously messages are being transported on a physical medium, this is the [End Point][End Points]. The 'Invoice' and 'Salary' are the [Message Types][]. Direction is also provided in the examples. The Source or destination was also mentioned; 'Customer A' and 'Bank' is the [System][Systems].  

### :fa-exchange: Direction (4.2+)
* :fa-hourglass-start: **Coming soon** - Estimated release 4.2

* A **Service** is either inbound or outbound. An inbound **Service** should always be configured with one or more source **Services**.
* An outbound **Service** can be the start of the message flow. 

### :fa-adjust: Icon (4.3+)
In order to provide customization for the graphical overview an optional non default Icon can be specified.
* :fa-hourglass-start: **Coming soon** - Estimated release 4.3 

### :fa-clock: Trigger (4.3+)
* :fa-hourglass-start: **Coming soon** - Estimated release 4.3 

For outbound **Services** the trigger can be documented. Users can see this valuable piece of information within [Log Views][] and the graphical message flow.   

### :fa-stack-overflow: Expected Transaction Count (4.4+)
* :fa-hourglass-start: **Coming soon** - Estimated release 4.4 

For all **Services** the expected message count can be documented. Users can see this valuable piece of information within [Log Views][] and the graphical message flow.

Using the reporting capabilities of Integration Manager thru the use of Power BI/Excel, capacity managament planning can be performed.    
___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-cog: [Add or manage Services][]  
:fa-plus-square: :fa-laptop: [Add or manage Systems][]  
:fa-plus-square: :fa-sign-in: [Add or manage End Points][]  
:fa-plus-square: :fa-file: [Add or manage Message Types][]  
:fa-plus-square: :fa-puzzle-piece: [Add or manage Integrations][]     

##### :fa-cubes: Related  
:fa-sitemap: [Repository][]    
:fa-file: [Message Types][]     
:fa-sign-in: [End Points][]  
:fa-laptop: [Systems][]  
:fa-puzzle-piece: [Integrations][]  
:fa-hdd-o:  [Log Views][]  

<!--References -->
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[Add or manage Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/2.%20Add%20or%20manage%20Service/Add%20or%20manage%20Service.md?at=master
[Integrations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/1.%20Integrations/Integrations.md?at=master
[Add or manage Integrations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/1.%20Integrations/2.%20Add%20or%20manage%20Integration/Add%20or%20manage%20Integration.md?at=master

[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master

[Add or manage End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/2.%20Add%20or%20manage%20End%20Point/Add%20or%20manage%20End%20Point.md?at=master
[End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master

[Add or manage Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/2.%20Add%20or%20manage%20Message%20Type/Add%20or%20manage%20Message%20Type.md?at=master

[Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master

[Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master
[Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/Systems.md?at=master
[Add or manage Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/2.%20Add%20or%20manage%20System/Add%20or%20manage%20System.md?at=master
