<tags data-value="View, All,End Points,Direction"></tags>

:fa-sign-in: View all End Points
=====

___

## :fa-info: Information
The list of defined [End Points][]. A large list of **End Points** can be narrowed down by typing characters into the filter text box.

You can [add new][Add or manage End Points], [edit][Add or manage End Points], [Delete][Show Deleted] records.
![End Points][1]

The directions to choose between is **Send**, **Recieve** and **Unknown**.  
![Directions][2]
___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-sign-in: [Add or manage End Points][]

##### :fa-cubes: Related  
:fa-sign-in: [End Points][]     
:fa-puzzle-piece: [Integration][]  
:fa-hdd-o: [Log Views][]  
:fa-hdd-o: [Logging Service][]     
:fa-download: [Log API][LogAPI]   


<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A39.%20End%20Points.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A39.2%20Directions.png

[Show Deleted]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Show%20Deleted.md?at=master
[End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master
[Add or manage End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/2.%20Add%20or%20manage%20End%20Point/Add%20or%20manage%20End%20Point.md?at=master
[Integration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/1.%20Integrations/Integrations.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[LogAPI]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Log%20API/Overview.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
