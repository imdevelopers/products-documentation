<tags data-value="Add,End Point,URI,Type,Direction"></tags>

:fa-sign-in: Add or manage End Point
=====

___

## :fa-info: Information
In this section you will learn how to add or manage an [End Point][].

## Instructions
Add a new [End Point][] or Edit an existing from the list in the [overview][View All End Points].

### Mandatory Fields
A **Name** is required to create the **End Point**.

![Name, Description, and Website][1]

## Optional fields
Adding a **Description** and a **Web Site** is optional. 
* **Description**: A user friendly description.
* **Web Site**: You can provide a quick link for users when working with and viewing the [End Point][]. This quick link is usually a WIKI/Sharepoint site with additional documentation. 

#### URI
URI means Uniform Resource Identifier and should reflect the adress used for the message exchange in the system integration solution.

#### :fa-plug: End Point Types

Integration Manager provides default End Point Types: 

* AppFabric 
* AppFabric (Azure)	
* BizTalk - Dynamic Send	
* BizTalk - Receive, Biztalk - Send	
* MSMQ	
* Unknown	
* WCF	
* WMQ	

![End Point Type][4]

![End Point Type 2][5]

For a complete list, see [here](https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Log%20Event/End%20Point%20Types.md?at=master)

#### :fa-exchange: Directions
The directions to choose between are:
* Send
* Receive
* Unknown  
![Directions][3]

### :fa-wrench: Custom Fields 
As part of the [Repository][] model, You can also add [Custom Fields][] to provide additional documentation about your **End Point**.
![Custom Fields][2]
___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-cog: [Add or manage Services][]          
:fa-plus-square: :fa-wrench: [Add or manage Custom Fields][]       

##### :fa-cubes: Related  
:fa-laptop: [Systems][]        
:fa-sitemap: [Repository][]      
:fa-cog: [Services][]     
:fa-wrench: [Custom Fields][]    

<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/EndPoints/AddNewEndPoint.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A41.2%20Custom%20Fields.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A39.2%20Directions.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A44.%20End%20Point%20Type.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A44.2%20End%20Point%20Type%202.png

[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[View all End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/1.%20View%20All%20End%20Points/View%20All%20End%20Points.md?at=master
[End Point]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master
[Add or manage Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/2.%20Add%20or%20manage%20Service/Add%20or%20manage%20Service.md?at=master
[Add or manage Custom Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/2.%20Add%20or%20manage%20Custom%20Fields/Add%20or%20manage%20Custom%20Fields.md?at=master
[Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/Systems.md?at=master
[Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master
[Custom Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/Custom%20Fields.md?at=master
