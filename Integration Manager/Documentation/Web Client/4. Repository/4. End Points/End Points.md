<tags data-value="End Points,Services, Systems,Integrations"></tags>

:fa-sign-in: End Points
=====

___

## :fa-info: Information
System Integration Solutions typically communicate by sending messages on a physical transport medium to each other as part of a conversation. 
The participants in a conversation agrees on the type of protocol, security policys to comply with. The information about this transport channel is defined within Integraiton Manager as an **End Point**.

An **End Point** has a user friendly name and an adress (URI). The End Points are unique by their **Name**, **URI**, **Direction**, **End Point Type** and **Log Agent**.  
![EndPointExample][1]


#### :fa-sitemap: Repository
An Endpoint can be part of a Transport Contract in either a [Service][Services] or a [Contract][].

#### URI
The URI is expected to contain the filename like `order123.json`, `%MessageID%.xml` or `*.xml` if the last path is a folder than the path is expected to end with `\`.

An End Point can also have [Custom Meta Data][] and/or [Custom Fields][].

## :fa-plus-square: Origin of Message Types
**End Points** origin from the process of logging. There are 2 entry paths for messages:  
1. [BizTalk logging][BizTalk Logging], governed by the [Logging Service][].
2. [Log API][LogAPI] (Custom code, [Log4Net][], .NET, Java, ...)

New **End Points** will be automatically created and can be seen in the [overview][View all End Points]. 
All logged Messages will be processed by the [Logging Service][]. During this processing the **End Point** will get known.
Messages are not allowed to be logged without the information about an **End Point**. 

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-sign-in: [Add or manage End Points][]

##### :fa-cubes: Related  
:fa-sign-in: [View all End Points][]  
:fa-puzzle-piece: [Integration][]  
:fa-hdd-o: [Log Views][]  
:fa-hdd-o: [Logging Service][]     
:fa-download: [Log API][LogAPI]   
:fa-tags: [Custom Meta Data][] 
:fa-wrench: [Custom Fields][]
:fa-cog: [Service][Services]

<!--References -->
[View all End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/1.%20View%20All%20End%20Points/View%20All%20End%20Points.md?at=master
[Add or manage End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/2.%20Add%20or%20manage%20End%20Point/Add%20or%20manage%20End%20Point.md?at=master
[Integration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/1.%20Integrations/Integrations.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[LogAPI]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Log%20API/Overview.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master

[Log4Net]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Log4Net/Log4Net%20Appender.md?at=master

[BizTalk Logging]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Microsoft%20BizTalk%20Server/Overview.md?at=master

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/EndPoints/EndPointExample.png

[Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master
[Contract]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.5%20Contracts/Overview.md?at=master

[Custom Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/Custom%20Fields.md?at=master
[Custom Meta Data]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/7.%20Custom%20Meta%20Data/Overview.md?at=master