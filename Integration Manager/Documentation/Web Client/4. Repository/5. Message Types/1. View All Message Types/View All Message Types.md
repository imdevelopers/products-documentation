<tags data-value="Message Types,Search Fields, Logging Service"></tags>

:fa-file: View All Message Types
=====
					


___

## :fa-info: Information
The list of known/processed and user defined [Message Types][Message Type]. A large list of **Message Types** can be narrowed down by typing characters into the **filter** text box.

In this list you also get an overview of the setting for **Days to keep Events** and **Days to keep Messages** and when message was last seen.
![Message Type][1]

You can [Add][How to add or manage a Message Type] new [Message Types][Message Type] in advance, prior to messages being logged if you have the need for the configuration and documentation.

## :fa-flash: Actions
MessageTypes has the following available Actions:  
![Actions][2]

* [Edit][How to add or manage a Message Type] - Edit an existing [Message Type][]
* [Delete][Show Deleted] - Mark the [Message Type][] as **Deleted**
* **Reindex** - Reindex all logged  messages in all [Log Databases][] 
* **Export** - Export the selected [Message Type][] to file or an external instance of Integration Manager.

Changes to [Search Field][Search Fields] might require **Reindex** operation if you want to apply the new expression on already logged data. 

## :fa-exclamation: Reindex operation remark  
    The Reindex operation may be very resource intensive and can cause insufficient resources for other applications and database operations for the duration of the Reindex operation. Make sure to run reindex operations when you know there is less pressure on the environment.
___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-file: [How to add or manage a Message Type][]       
:fa-plus-square: :fa-search:[Add or manage Search Fields][]    
:fa-plus-square: :fa-cog: [Add or manage Service][]       
:fa-plus-square: :fa-laptop: [Add or manage Systems][]    

##### :fa-cubes: Related  
:fa-file: [Message Types][Message Type]  
:fa-sitemap: [Repository][]  
:fa-search: [Search Fields][]     
:fa-cog: [Services][]     
:fa-laptop: [System][Systems]      
:fa-sign-in: [End Points][]  
:fa-hdd-o: [Log Views][Log View]  
:fa-database: [Log Databases][]  

<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/MessageTypes/ViewAllMessage%20Types.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/MessageTypes/ActionsButton.png


[How to add or manage a Message Type]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/2.%20Add%20or%20manage%20Message%20Type/Add%20or%20manage%20Message%20Type.md?at=master
[Message Type]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master
[Add or manage Message Type]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/2.%20Add%20or%20manage%20Message%20Type/Add%20or%20manage%20Message%20Type.md?at=master
[Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Add or manage Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/2.%20Add%20or%20manage%20Service/Add%20or%20manage%20Service.md?at=master
[Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master
[Add or manage Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/2.%20Add%20or%20manage%20System/Add%20or%20manage%20System.md?at=master
[Show Deleted]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Show%20Deleted.md?at=master
[Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/Systems.md?at=master
[End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master
[Add or manage Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/Add%20or%20manage%20Search%20Field.md?at=master
[Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/Search%20Fields.md?at=master
[Log Databases]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Log%20Databases/Log%20Databases.md?at=master
