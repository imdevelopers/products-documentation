<tags data-value="Add,Message Type,Custom Field,Search Field"></tags>

:fa-file: Add or manage Message Type
=====
					


___

## :fa-info: Information
In this section you will learn how to manually add or manage a [Message Type][].

Add a new [Message Type][], Edit or [Delete][Show Deleted] an existing from the list in the [overview][View All Message Types].
A [Message Type][] can be member of a [Service][Services] and [Search Field Expressions][] after you have successfully created the **Message Type**. 
![MT][1]

### Mandatory Fields
* A **Name** is required to create the **Message Type**.

* **Days to keep Events** - Determines how long events should be archived. When the event is purged, the message body and context will also be removed.

* **Days to keep messages** - Determines how long messages and message context should be archived.

**Days to Keep Messages** and **Days to Keep Events*** means how long records are kept in the [Log Databases][].

If set to '0' (zero), Events and/or Messages will not be removed, data will be stored forever.

:fa-exclamation:
    If too many Messages or Events are logged, searching for data may be significantly slower than with less data.

    Tip Tune these settings according to your business needs. Do not save more data than needed.

## Optional fields
Adding a **Description** and a **Web Site** is optional. 
* **Description**: A user friendly description.
* **Web Site**: You can provide a quick link for users when working with and viewing the [System][]. This quick link is usually a WIKI/Sharepoint site with additional documentation. 

### :fa-wrench: Custom Fields 
As part of the [Repository][] model, You can also add [Custom Fields][] to provide additional documentation about your **Message Type**.
![CustomFieldPicture][2]

### :fa-search: Search Field Expressions

[Search Field Expression][Search Field Expressions] associated with the [Message Type][] will be listed.
You can [Add or manage Search Fields][] in the Log Administration.

![SearchField][3]

You can navigate to the associated [Search Fields][] by clicking the provided quick link.
___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-search:[Add or manage Search Fields][]    
:fa-plus-square: :fa-cog: [Add or manage Service][]       
:fa-plus-square: :fa-laptop: [Add or manage Systems][]    

##### :fa-cubes: Related  
:fa-file: [Message Types][Message Type]  
:fa-sitemap: [Repository][]  
:fa-search: [Search Fields][]     
:fa-cog: [Services][]     
:fa-laptop: [System][Systems]      
:fa-sign-in: [End Points][]  
:fa-hdd-o: [Log Views][Log View]  

<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/MessageTypes/AddNewMessageType.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/MessageTypes/CustomField.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/MessageTypes/SearchFieldExpression.png
[View All Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/1.%20View%20All%20Message%20Types/View%20All%20Message%20Types.md?at=master
[How to add or manage a Message Type]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/2.%20Add%20or%20manage%20Message%20Type/Add%20or%20manage%20Message%20Type.md?at=master
[Message Type]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master
[Add or manage Message Type]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/2.%20Add%20or%20manage%20Message%20Type/Add%20or%20manage%20Message%20Type.md?at=master
[Show Deleted]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Show%20Deleted.md?at=master
[Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Add or manage Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/2.%20Add%20or%20manage%20Service/Add%20or%20manage%20Service.md?at=master
[Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master
[Add or manage Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/2.%20Add%20or%20manage%20System/Add%20or%20manage%20System.md?at=master
[Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/Systems.md?at=master
[End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master
[Add or manage Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/Add%20or%20manage%20Search%20Field.md?at=master
[Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/Search%20Fields.md?at=master
[Add or manage a Custom Field to a Message Type]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/1.%20Integrations/2.%20Add%20or%20manage%20Integration/2.%20Add%20Custom%20Fields%20to%20artifact/Add%20Custom%20Fields%20to%20artifact.md?at=master
[Search Field Expressions]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/SearchFieldExpressions.md
[Log Databases]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Log%20Databases/Log%20Databases.md?at=master
[Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/Systems.md?at=master
