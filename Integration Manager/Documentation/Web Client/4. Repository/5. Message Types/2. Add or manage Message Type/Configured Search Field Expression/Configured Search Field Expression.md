<tags data-value="Add, Message Type"></tags>

:fa-search: Configure Search Field 
=====
					


___

## :fa-info: Information
Search Field Expressions are limitations of a Search Field and are edited in [Configure Search Field Expression][].



<!--References -->
[Configure Search Field Expression]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/1.%20Display%20Field%20Configurations/Add%20or%20manage%20Display%20Field%20Configurations.md

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  
