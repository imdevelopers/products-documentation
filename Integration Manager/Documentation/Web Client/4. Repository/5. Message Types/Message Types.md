<tags data-value="Message Types, Repository, Log, Logging Service, Service"></tags>

:fa-file: Message Types
=====
					


___

## :fa-info: Information
System Integration Solutions typically communicate by sending messages to each other as part of a conversation.
The participants in a conversation agrees on the name and content for each type of message.
A **Message Type** object defines a **unique** name for a message type and defines the type of data that the message contains.
Message types are persisted in the configuration database and is used by [Search Fields][] to extract values for use in search and restrictions in [Log Views][]. 

For a message type that specifies XML conforming to a particular schema collection, the message must contain well-formed XML that is valid for one of the schemas in the collection. 
For a message type that specifies no validation, Integration Manager accepts any message content. This includes binary data, XML, or empty messages.
Integration Manager uses a built-in DEFAULT message type named **Unparsed Interchange**. If the message type is not specified during the Log operation, the system will use the DEFAULT message type.

A **Message Type** can be a member of a [Service][Services] in the [Repository][] model and represents the type of message for a logged message. Examples can be:
* http://SAP.Invoice/1.0
* EDIFACT.DESADVD96A
* Orders/1.0#Order
* ...

Messages can be displayed for end users in a user friendly format by the use of [Stylesheets][].

## :fa-plus-square: Origin of Message Types
Messages origin from the process of logging. There are 2 entry paths for messages:
* BizTalk logging, governed by the [Logging Service][].
* [Log API][LogAPI] (Custom, [Log4Net][], iCore, .NET, Java, ...)

All logged Messages will be processed by the [Logging Service][]. During this processing the **Message Type** will get known.
Messages are not allowed to be logged without a Message Type. The proposed Message Type from the Source is stored as **Original Message Type**  

### Unprocessed Message
![Unprocessed][1]

### Processed Message
![Processed][2]

If the message is XML based the [Logging Service][] will try to extract the real **Message Type** from the message. The **Original Message Type** is kept on record level, but Integration Manager uses the value set/extracted for the **Message Type**. 

## :fa-search: Search Fields
Values for [Search Fields][] will be extracted during the processing of messages.
You can reprocess/reindex messages based on the **Message Type** if you add or change the configuration for a [Search Fields][Search Fields].

    NOTE: Adding Search Fields and Search Field Expressions on MessageTypes increases workload. Make sure to configure and use this powerful feature wisely

![reindex][3]
*Reindex operation available in the Action Menu*

## :fa-trash: Removal of old messages based on message type
The [Logging Service][] will periodically remove old **Events** and **Messages**. The period is user defined per **Message Type**. 

The Default values for a new Message Type is based on a **System Parameter** from the **SystemParameters** table in the configuration database:
* **DaysToKeepMessageEventsDefault**: The default number of days to keep events. Deleting an event will also delete its data/body message. This value will be assigned to new messagetypes as its default. The [Logging Service][] must be restarted if this value is changed.

* **DaysToKeepMessageDataDefault**: The default number of days to keep data in an event. This value will be assigned to new messagetypes as its default. The [Logging Service][] must be restarted if this value is changed.



## Microsoft BizTalk Server
The [Logging Service][] has special support for Microsoft BizTalk Server. Logged messages are processed in a special way for the message type  **Unparsed Interchange** and **Serialized Interchange**.
* For BizTalk Server this means that even if **Passthru pipeline components** are in use, the message type will still be known for Integration Manager.

* For flat file and EDI based messaging the logging service will query the BizTalk port for additional information in order to resolve the schema for the message. 

___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-file: [Add or manage Message Types][]  
:fa-plus-square: :fa-search:[Add or manage Search Fields][]  
:fa-plus-square: :fa-cog: [Add or manage Service][]        
:fa-file: [View All Message Types][]  


##### :fa-cubes: Related  
:fa-sitemap: [Repository][]    
:fa-cog: [Services][]     
:fa-hdd-o: [Logging Service][]   
:fa-download: [Log API][LogAPI]  
:fa-search:[Search Fields][]    
:fa-hdd-o: [Log Views][]  
:fa-file-text-o: [Stylesheets][]

<!--References -->
[Add or manage Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/2.%20Add%20or%20manage%20Message%20Type/Add%20or%20manage%20Message%20Type.md?at=master
[Add or manage Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/2.%20Add%20or%20manage%20Service/Add%20or%20manage%20Service.md?at=master 
[Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master
[LogAPI]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Log%20API/Overview.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[View All Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/1.%20View%20All%20Message%20Types/View%20All%20Message%20Types.md?at=master
[Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/Search%20Fields.md?at=master

[Add or manage Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/Add%20or%20manage%20Search%20Field.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[Log4Net]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Log4Net/Log4Net%20Appender.md?at=master
[Stylesheets]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/5.%20Stylesheets/Stylesheets.md?at=master

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/MessageTypes/UnprocessedOriginalMessageType.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/MessageTypes/ProcessedMessageType.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/MessageTypes/ActionsButton.png
