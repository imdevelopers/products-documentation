<tags data-value="Add,Custom Field,Value,Type"></tags>

:fa-wrench: Add or manage Custom Field
=====
					


___

## :fa-info: Information
In this section you will learn how to add or manage a [Custom Field][Custom Fields].
![Custom Field][1]

### Mandatory Fields
* Name
* Custom Field Type
* Single Value Checkbox

A **Name** is required to create the **Custom Field**.

![Name, Description, and Website][1]

A **Custom Field** Type must be selected. Choose between File, Text and from one of the installed plugins (if any).  
![Custom Field Type][2]

After a **Custom Field** Type is chosen, a Value may be added by clicking Edit.

![Values][3] 

:fa-tag: Single Value / :fa-tags: Multiple Values
Check the box if only a single value will be selected. If unchecked, multiple values can be set on the Custom Field.

## Optional fields
Adding a **Description** and a **Web Site** is optional. 
* **Description**: A user friendly description.
* **Web Site**: You can provide a quick link for users when working with and viewing the [System][]. This quick link is usually a WIKI/Sharepoint site with additional documentation. 


### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-wrench: [Add or manage Custom Fields][]  
:fa-plus-square: :fa-puzzle-piece: [Add or manage Integrations][]  
:fa-plus-square: :fa-laptop: [Add or manage Systems][]  
:fa-plus-square: :fa-cog: [Add or manage Services][]  
:fa-plus-square: :fa-sign-in: [Add or manage End Points][]  
:fa-plus-square: :fa-file: [Add or manage Message Types][]  

##### :fa-cubes: Related  
:fa-sitemap: [Repository][]  
:fa-puzzle-piece: [Integrations][]  
:fa-laptop: [Systems][]  
:fa-cog: [Services][]  
:fa-sign-in: [End Points][]  
:fa-file: [Message Types][]  

<!--References -->
<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A48.%20Add%20Custom%20Field.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A48.2%20Custom%20Field%20Type.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A48.3%20Values.png

[Show Deleted]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Show%20Deleted.md?at=master
[Custom Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/Custom%20Fields.md?at=master

[Add or manage Custom Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/2.%20Add%20or%20manage%20Custom%20Fields/Add%20or%20manage%20Custom%20Fields.md?at=master
[Integrations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/1.%20Integrations/Integrations.md?at=master
[Add or manage Integrations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/1.%20Integrations/2.%20Add%20or%20manage%20Integration/Add%20or%20manage%20Integration.md?at=master
[View All Integrations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/1.%20Integrations/1.%20View%20All%20Integrations/View%20All%20Integrations.md?at=master

[Add or manage Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/2.%20Add%20or%20manage%20Service/Add%20or%20manage%20Service.md?at=master
[Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master

[Add or manage End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/2.%20Add%20or%20manage%20End%20Point/Add%20or%20manage%20End%20Point.md?at=master
[End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master

[Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master
[Add or manage Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/2.%20Add%20or%20manage%20Message%20Type/Add%20or%20manage%20Message%20Type.md?at=master
[View All Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/1.%20View%20All%20Message%20Types/View%20All%20Message%20Types.md?at=master


[Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/Systems.md?at=master
[Add or manage Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/2.%20Add%20or%20manage%20System/Add%20or%20manage%20System.md?at=master
[View All Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/1.%20View%20All%20Systems/View%20All%20Systems.md?at=master

[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
