<tags data-value="Custom Field,Values,File,Image"></tags>

Custom Field Values
=====
					


___

## :fa-info: Information
A Custom Field Value is a File of any format or Text which can be added as extra information to the [Custom Field][]. 

In the example below, images are added to explain a process.

## Instructions

This example is used with File as Custom Field Type. 

Browse for a file or drop it within the cutted area. A Filename is required to add the File.

You can add a Description with additional information.



![Custom Field Values][1]

Clicking the Filename will open a new tab with the file. 

![Click Filename][3]

The other Custom Field Type is Text.

#### Text

![Text][4]





<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A47.%20Custom%20Field%20Values.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A42.2%20Action.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A47.2%20Custom%20Field%20Picture%20Name.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A47.3%20Custom%20Field%20Text.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A47.4%20Custom%20Field%20Hello%20World.png
[6]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A47.5%20Custom%20Field%20Live%20Data.png

[Custom Field]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/Custom%20Fields.md?at=master

___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  
