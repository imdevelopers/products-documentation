<tags data-value="Custom Meta Data,System,Integrations,Repository"></tags>

:fa-tags: Custom Meta Data
=====
					


___

## :fa-info: Information

Use **Custom Meta Data** to provide your delivery model. This is key to a manageable system integration plaftform.
Configure the [Repository][] model and provide useful information to key stakeholders when needed the most.

**Custom Meta Data** provide more powerful features compared to Custom Fields. The main difference is that Custom Meta Data is expected (Mandatory in a sense) and custom fields are totally optional everywhere. 

* :fa-tag: Single value attribute from list
* :fa-tags: Multi value attributes from list
* :fa-image: :fa-file-word-o: :fa-file-zip-o: Files (word documents, images, zip files, ...)


![overview][0]

Integration Manager comes pre-loaded with plugins for most common scenarios. 
Custom Meta Data can be Set, re-used and assigned on each of the artifacts in the [Repository][] model. 

:fa-puzzle-piece: [Integrations][]  
:fa-laptop: [Systems][]  
:fa-cog: [Services][]  
:fa-sign-in: [End Points][]  
:fa-file: [Message Types][]  

Information is displayed in both the [Web Client][] and as part of email alerts sent from the [Monitoring Service][]. 

The lists are "global" but different set of values can be used on the artifacts of the [Repository][] model.

## :fa-recycle: Reuse Example
* Integration "INT001" with Custom Meta Data "Technical Contacts" can have Lasse and Michael assigned
* Integration "INT002" with Custom Meta Data "Technical Contacts" can have only Lasse assigned
* Integration "INT003" with Custom Meta Data "Technical Contacts" can have Lasse, Andreas and Michael assigned

:fa-key:

    Tip: Use Custom Meta Data to provide any piece of information that you may share with your organization. This is Key to a successful support and maintenance service. 

## :fa-tag: Single Value Example
Use when there can only be one value selected for the **Custom Meta Data**. A good example would be **SLA Level**. It is not logical to assign more than exactly one value for this kind of **Custom Meta Data**.
Selected Value example from list (Gold, Silver, Bronze) could be **Gold**.  

## :fa-tags: Multi Value Attribute Example
Use when there can be more than one value selected for the **Custom Meta Data**. A good example would be **Technical Contact**. It is logical to assign more than one value for this kind of **Custom Meta Data**.
Selected Value example from list (Michael, Lasse, Andreas) could be **Michael** and **Andreas**. 

## :fa-plug: Dynamically loaded Example
When Integration Manager is about to render the Repository Model tab in the [Web Client][] the **Custom Meta Data plugin** is fed with all available data from Monitoring or Logging. 
The plugin can therefore hold logic to access web services, databases in order to return the value for the Custom Meta Data at run-time.
* One example could be for logged orders where the temperature of the destined City is displayed
* Another example could be a generated link to some documentation web site (WIKI/Sharepoing) based on provided data   

## :fa-image: :fa-file-word-o: :fa-file-zip-o: From File Example 
Any file can be uploaded. Images will be displayed in the [Web Client][].
Since there can be multiple files and potentially large files; email alerts are sent with links to these files. This is done to conserve bandwidth and the behaviour is by design.

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-wrench: [Add or manage Custom Meta Data][]  
:fa-plus-square: :fa-puzzle-piece: [Add or manage Integrations][]  
:fa-plus-square: :fa-laptop: [Add or manage Systems][]  
:fa-plus-square: :fa-cog: [Add or manage Services][]  
:fa-plus-square: :fa-sign-in: [Add or manage End Points][]  
:fa-plus-square: :fa-file: [Add or manage Message Types][]  

##### :fa-cubes: Related  
:fa-wrench: [View All Custom Meta Data][]  
:fa-sitemap: [Repository][]  
:fa-puzzle-piece: [Integrations][]  
:fa-laptop: [Systems][]  
:fa-cog: [Services][]  
:fa-sign-in: [End Points][]  
:fa-file: [Message Types][]  
<!--References -->

[Add or manage Custom Meta Data]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Meta Data/2.%20Add%20or%20manage%20Custom%20Meta Data/Add%20or%20manage%20Custom%20Meta Data.md?at=master
[Integrations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/1.%20Integrations/Integrations.md?at=master
[Add or manage Integrations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/1.%20Integrations/2.%20Add%20or%20manage%20Integration/Add%20or%20manage%20Integration.md?at=master
[View All Integrations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/1.%20Integrations/1.%20View%20All%20Integrations/View%20All%20Integrations.md?at=master
[View All Custom Meta Data]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Meta Data/1.%20View%20All%20Custom%20Meta Data/View%20All%20Custom%20Meta Data.md?at=master

[Add or manage Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/2.%20Add%20or%20manage%20Service/Add%20or%20manage%20Service.md?at=master
[Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master

[Add or manage End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/2.%20Add%20or%20manage%20End%20Point/Add%20or%20manage%20End%20Point.md?at=master
[End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master

[Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master
[Add or manage Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/2.%20Add%20or%20manage%20Message%20Type/Add%20or%20manage%20Message%20Type.md?at=master
[View All Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/1.%20View%20All%20Message%20Types/View%20All%20Message%20Types.md?at=master


[Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/Systems.md?at=master
[Add or manage Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/2.%20Add%20or%20manage%20System/Add%20or%20manage%20System.md?at=master
[View All Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/1.%20View%20All%20Systems/View%20All%20Systems.md?at=master

[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master


[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/CustomMetaData/CustomMetaDataOverview.png
