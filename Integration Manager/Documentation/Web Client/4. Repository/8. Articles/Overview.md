<tags data-value="Articles, Knowledge Base, Operator Instructions, Repository"></tags>

:fa-tags: Articles
=====
					
___

## :fa-info: Information

Use **Articles** to provide human readable content for end users. This could for example be:  
    * Operator instructions  
    * Knowledge base  
    * Design documents  
    * ...  

Configure the [Repository][] model and provide useful information to key stakeholders when needed the most.

* :fa-pencil-square-o: WYSIWYG Editor
    * :fa-image: Embed images with ease from clipboard
    * :fa-bold: Style your text
* :fa-tag: Tag your articles


![overview][0]


:fa-key:

    Tip: Use Articles to provide any piece of information that you may share with your organization. This is Key to a successful support and maintenance service. 


### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-wrench: [Add or manage Custom Meta Data][]  
:fa-plus-square: :fa-puzzle-piece: [Add or manage Integrations][]  
:fa-plus-square: :fa-laptop: [Add or manage Systems][]  
:fa-plus-square: :fa-cog: [Add or manage Services][]  
:fa-plus-square: :fa-sign-in: [Add or manage End Points][]  
:fa-plus-square: :fa-file: [Add or manage Message Types][]  

##### :fa-cubes: Related  
:fa-sitemap: [Repository][]  
:fa-puzzle-piece: [Integrations][]  
:fa-laptop: [Systems][]  
:fa-cog: [Services][]  
:fa-sign-in: [End Points][]  
:fa-file: [Message Types][]  
<!--References -->

[Add or manage Custom Meta Data]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/7.%20Custom%20Meta%20Data/Overview.md?at=master

[Add or manage Integrations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/1.%20Integrations/2.%20Add%20or%20manage%20Integration/Add%20or%20manage%20Integration.md?at=master
[View All Integrations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/1.%20Integrations/1.%20View%20All%20Integrations/View%20All%20Integrations.md?at=master

[View All Custom Meta Data]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Meta%20Data/1.%20View%20All%20Custom%20Meta%20Data/View%20All%20Custom%20Meta%20Data.md?at=master

[Add or manage Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/2.%20Add%20or%20manage%20Service/Add%20or%20manage%20Service.md?at=master
[Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master

[Add or manage End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/2.%20Add%20or%20manage%20End%20Point/Add%20or%20manage%20End%20Point.md?at=master
[End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master

[Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master
[Add or manage Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/2.%20Add%20or%20manage%20Message%20Type/Add%20or%20manage%20Message%20Type.md?at=master
[View All Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/1.%20View%20All%20Message%20Types/View%20All%20Message%20Types.md?at=master


[Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/Systems.md?at=master
[Add or manage Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/2.%20Add%20or%20manage%20System/Add%20or%20manage%20System.md?at=master
[View All Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/1.%20View%20All%20Systems/View%20All%20Systems.md?at=master

[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master
[Integrations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/4.%20Included%20Integrations/Included%20Integrations.md?at=master

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/Articles/Toolbar.png
