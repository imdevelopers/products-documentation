<tags data-value="Repository,System,Integration, Services, End Points, Custom Fields, Custom Meta Data"></tags>

:fa-sitemap: Repository
=====
					


___

## :fa-info: Information

Let your users gain access and insights to your valuable documentation sharing the information and creating a foundation for true co-operation when and where needed. Visualize your documentation using both the power of graphical models and tabular formats. The plugin based alert engine send the repository data directly to you, no need to spend time on asking and searching for the documentation. Saves your frustration, time and there by money.

Documentation for Integration Solutions are provided by populating the **Repository** model. The Repository level is accessed by users with administrators privileges. This information is readily available for any [Users][] from [Log Views][] and [Monitor Views][].

    Tip: Invest in the Repository model and you will be rewarded with helpful information when needed the most from the Log Views and Monitor Views

![IntegrationLandscape][4]

The **Repository** model provides:
* **Key for support and maintenance**
* Personal independence from a knowledge perspective
* Save time by having information at your fingertips
    * :fa-hdd-o: [Log Views][]
    * :fa-desktop: [Monitor Views][]
* Documents your processes   
* Foundation for reports
* A definition of your service portfolio.
* For [Log Views][] the **Repository** model can be part of the restrictions and filters for [Users][]

## :fa-sitemap: Repository artifacts
![Repository][1]

 The Repository model contains the following artifacts 
* :fa-puzzle-piece: [Integrations][]
* :fa-laptop: [Systems][]
* :fa-cog: [Services][]
* :fa-sign-in: [End Points][]
* :fa-file: [Message Types][]
* :fa-wrench: [Custom Fields][] 
* :fa-tags: [Custom Meta Data][] 

With these artifacts you can document your System Integration solutions.
___    

## :fa-pie-chart: Reports and sharing the data
It is very easy to create the data withing Reports using for example Microsoft Excel, Microsoft Power BI or any other tool that consumes JSON data. All data in the [Web Client][] is retreived 
from the [Web API][]. The adress for the data being presented can be copied from the **'Copy API URL'** button/dialog.  
![copyapiurl][2]

1. **'Copy API URL'** button
2. The expand [Repository][] button

![expandedexample042][3]

## :fa-share-alt: Example
All information within the **Repository** is available for all users (even non Administrators) in 
* [Monitor Views][]
* [Log Views][]

### :fa-anchor: Anchor links
The :fa-anchor: links witin the expanded modal takes the user directly to the associated item within the modal. This feature provides fast access to large amounts configured data within the **Repository**. The top table is the overview.

## :fa-desktop: From Monitor Views
From any [Monitor View][Monitor Views] where an assocation has been made to one or more [Integrations][] the **Repository** will be visible in the 'Repository Tab'.  
![RepositoryTabMonitorView.png][5]  
*read-only information example*  

## :fa-hdd-o: From Log Views
The same **Repository** data can be displayed from Logged Events by clicking on the small button to the left of the 'Action' button:  
![details][6]  

A modal hosting the very same **Repository** tab will be visible for end users.  
![RepositoryTabLogView.png][7]  

The magic behind the association lies within Transport Contracts that are either bound to [Services][] or Contracts.
___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-puzzle-piece: [Add or manage an Integration][]  
:fa-plus-square: :fa-laptop: [Add or manage System][]  
:fa-plus-square: :fa-cog: [Add or manage Service][]  
:fa-plus-square: :fa-sign-in: [Add or manage End Point][]  
:fa-plus-square: :fa-file: [Add or manage Message Type][]  

##### :fa-cubes: Related  
:fa-puzzle-piece: [Integrations][]  
:fa-laptop: [Systems][]  
:fa-cog: [Services][]  
:fa-sign-in: [End Points][]  
:fa-file: [Message Types][]  
:fa-wrench: [Custom Fields][]   
:fa-hdd-o: [Log Views][]  
:fa-desktop: [Monitor Views][]  


<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/Artifacts.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/copyapiurl.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/ExpandedExample042.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/Integrations/IntegrationLandscape.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/RepositoryTabMonitorView.png
[6]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/LogViewDetailsButton.png
[7]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Repository/RepositoryTabLogView.png





[Add or manage Message Type]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/2.%20Add%20or%20manage%20Message%20Type/Add%20or%20manage%20Message%20Type.md?at=master

[Add or manage End Point]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/2.%20Add%20or%20manage%20End%20Point/Add%20or%20manage%20End%20Point.md?at=master

[Add or manage Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/2.%20Add%20or%20manage%20Service/Add%20or%20manage%20Service.md?at=master

[Add or manage an Integration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/1.%20Integrations/2.%20Add%20or%20manage%20Integration/Add%20or%20manage%20Integration.md?at=master
[Add or manage System]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/2.%20Add%20or%20manage%20System/Add%20or%20manage%20System.md


[Integrations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/1.%20Integrations/Integrations.md?at=master
[Systems]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/2.%20Systems/Systems.md?at=master
[Services]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/3.%20Services/Services.md?at=master
[End Points]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/4.%20End%20Points/End%20Points.md?at=master
[Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master
[Custom Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/6.%20Custom%20Fields/Custom%20Fields.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Web API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Web%20API/Overview.md?at=master

[Custom Meta Data]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/7.%20Custom%20Meta%20Data/Overview.md?at=master


