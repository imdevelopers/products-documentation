<tags data-value="Log,Allow,These,Roles"></tags>

Allow These Roles
=====
					


___

[Allow These Roles][]


<!--References -->

[Allow These Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/1.%20Allow%20These%20Roles/Allow%20These%20Roles.md?at=master
___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  
