<tags data-value="Log,Add,View"></tags>

Add or manage Log View
=====
					


___


[Add or manage Log View]


<!--References -->

[Add or manage Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Create%20New%20Log%20View.md?at=master
___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  
