<tags data-value="Repository, Search Fields, Search Field Expressions, Message Types, Log Views"></tags>

:fa-search-plus: View All Search Fields
=====
					


___

## :fa-info: Information

The list of user defined [Search Fields][]. A large list of **Search Fields** can be narrowed down by typing characters into the **filter** text box.

![View All Search Fields][1]

[Search Fields][] with configuration problems are listed with a **Warning** icon. User action is required to resolve the problem.  
![warning][2] 

An example of an invalid configuration is a deleted [Message Type][] used in a [Search Field Expression][] for the [Search Field][Search Fields]. In which case the [Search Field Expression][] would have to be altered or removed from the **Search Field**.

<!--References -->

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-search: [Add or manage Search Fields][]       
:fa-plus-square: :fa-file: [Add or manage Message Type][]       

##### :fa-cubes: Related  
:fa-search: [Search Fields][]       
:fa-sitemap: [Repository][]   
:fa-search-plus: [Search Field Expressions][]  

<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/ViewAllSearchFields.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/SearchFieldWithWarning.png
[Add or manage Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/Add%20or%20manage%20Search%20Field.md?at=master
[Add or manage Message Type]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/2.%20Add%20or%20manage%20Message%20Type/Add%20or%20manage%20Message%20Type.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/Search%20Fields.md?at=master
[Message Type]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master
[Search Field Expressions]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/SearchFieldExpressions.md

