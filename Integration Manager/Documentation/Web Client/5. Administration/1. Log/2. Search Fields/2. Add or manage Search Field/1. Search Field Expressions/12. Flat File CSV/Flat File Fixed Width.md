<tags data-value="Flat,File,Fixed,Width,Expression"></tags>

Flat File Fixed Width
=====
					


___

## :fa-info: Information
Flat File Fixed Width is an Expression Type and is used to extract values from a message.

Expression Types are used in [Search Fields][].



How to [Configure Search Field Expression][].

How to [Add or manage Search Field][].

___

## Instructions


### Flat File Fixed Width

Flat File Fixed Width extracts data from a fixed width file.  
Important is if the start position + length is longer then the end of line the value will NOT be included in the result.  
Example if skip line 1 and take from position 0 to 5 then only Alice would be included if there are missing spaces after Bob  
```
Names
Alice
Bob  
```

<!--

Here's three examples of how you can use the expression.


	
	[{pos:4,len:6,row:2}]
	
or
	
	[{pos:4,len:6,skiprow:1}]

or

	[{endpos:8,endlen:2,rows:[{from:2}],skiprows:[{from:1,to:1},{from:3,to:4}]}]
	
	
___


Pos means position and starts with the character with that number in order.

	Example: pos:4 means it will start on the character _after_ the fourth character.
	
___

Len means length and is the amount of characters that will be collected.

	Example: len:6 means that it collects 6 characters.
	
A space is a character when it's between two letters or numbers.

If the first or last character is a space, it won't be included.

___

Row is which row it jumps over, and uses the one after.

	Example: row:2 means it uses the row _after_ the second row. 

___

Skiprow is how many rows it should skip. After that, it searches all rows.

	Example: skiprow:1 means that it skips the first row and starts on row 2 and after that number uses all rows.
	
___
	
Endpos means the number of characters starting from the end and going backwards.

	Example: endpos:8 means the eighth character starting from the end.

___

Endlen means how many characters from the end it should use.

	Example: endlen:2 means that it uses all characters from the starting point (pos or endpos) except the last 2.

___

[{from:N}]

	Example, rows:[{from:2}] means that it uses all rows after the second row.

___

[{from:N,to:N},{from:N,to:N}]
	
	Example, [{from:1,to:1},{from:3,to:4}] means that it uses row 1, 3, and 4.

___-->

#### Test Expression

You can test an expression when configuring a Search Field in the Test Expression tab

This example uses Takes both skips the first line by skip one line or includ multiple lines.
The first expression get the phone numbers.
The second expression get the state.
	
![Text Expression][png_FlatFileFixedWidth_TestExpression]

<!--References -->

[png_FlatFileFixedWidth_TestExpression]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/SearchFields/FlatFileFixedWidth_TestExpression.png

[Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/Search%20Fields.md?at=master
[Add or manage Search Field]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/Add%20or%20manage%20Search%20Field.md

[Configure Search Field Expression]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/1.%20Display%20Field%20Configurations/Add%20or%20manage%20Display%20Field%20Configurations.md
[Key]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/2.%20Key/Key.md?at=master
[XPath]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/5.%20XPath/XPath.md?at=master
___

### :fa-hand-o-right: Next Step  

##### :fa-cubes: Related  

