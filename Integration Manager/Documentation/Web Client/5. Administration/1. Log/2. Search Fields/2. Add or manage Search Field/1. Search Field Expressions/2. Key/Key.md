<tags data-value="Key,Value,Extract,Expression,Search"></tags>

Key
=====
					


___

## :fa-info: Information
Key is an Expression Type which extracts a value from a key value collection.


Expression Types are used in [Search Fields][].

The Expression Types to choose between when configuring a Search Field is:


How to [Configure Search Field Expression][].

How to [Add or manage Search Field][].

___

## Instructions

### :fa-key: Key
This default extractor finds the provided key in the message context and grabs the value.

![Key][1]

#### Test Expression

A **Key** value can be tested for using the **Test Expression tab**.
Simply add the relevant **Message Context** (any number of values can be added), and enter your **Expression**. If you get it right, the proper value should be extracted by the [Logging Service][] in the background and presented in the browser.

![keyexample][2]


<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/B7.%20Key.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/SearchFieldExpressionsKeyExample.png

[Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/Search%20Fields.md?at=master
[Add or manage Search Field]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/Add%20or%20manage%20Search%20Field.md
[Configure Search Field Expression]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/1.%20Display%20Field%20Configurations/Add%20or%20manage%20Display%20Field%20Configurations.md
[XPath]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/5.%20XPath/XPath.md?at=master
[Flat File Fixed Width]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/1.%20Flat%20File%20Fixed%20Width/Flat%20File%20Fixed%20Width.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master


___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  


|	|
|---|
|[Flat File Fixed Width][]	|
|[RegEx][]	|
|[XPath][]	|
