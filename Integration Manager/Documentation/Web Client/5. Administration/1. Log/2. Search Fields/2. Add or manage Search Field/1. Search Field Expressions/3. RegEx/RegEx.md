<tags data-value="RegEx,Expression,Type,Match,Characters"></tags>

:fa-file-code-o: RegEx
=====
					


___

## :fa-info: Information
RegEx is an Expression Type and extracts a value from a message using a regular expression.

This default extractor loads the entire message into RAM, so make make sure to apply this extraction method on smaller messages only. 

Expression Types are used in [Search Fields][].


How to [Configure Search Field Expression][].

How to [Add or manage Search Field][].

___

## Instructions

### RegEx

![Regex][1]

Some operations are:

Concatenation, which describes a sequence of subexpressions and matches results with the expression only.

	Example: if "h" is the Expression and "how" is the Message Type Data, the result will be that "h" is a match.
	
___
	
Alternatives, which is described by a vertical line, |, is used to match at least one alternative.

	Example: "dog|cat" matches both "dog" and "cat".

___

Iteration, which is described by an asterisk, *, is used to match an expression which is repeated 0 or more times.

	Example: "go*gle" has an infinite number of matches which includes: "ggle", "gogle", "google", "gooogle" and so on.

___

Grouping of expressions, which is done with parantheses.

	Example: a(ero|ir)plane will match both aeroplane and airplane.

___

Matching is described by brackets and uses one of some characters followed by certain characters.

	Example: [lsh][]and matches land, sand, and hand.

___

A dot, ., can be used as all characters.

	Example: writing ten dots will give you all unique results with ten different characters in every paragraph.

___

You can find more expressions [here][].






#### Test Expression

You can test an expression when configuring a Search Field in the Test Expression tab.


	
![Test Expression][2]


___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  

|	|
|---|
|[Flat File Fixed Width][]	|
|[Key][]	|
|[XPath][]	|


<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/B8.%20RegEx.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/B8.2%20Test%20Regex.png

[Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/Search%20Fields.md?at=master
[Add or manage Search Field]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/Add%20or%20manage%20Search%20Field.md
[Configure Search Field Expression]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/1.%20Display%20Field%20Configurations/Add%20or%20manage%20Display%20Field%20Configurations.md
[Key]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/2.%20Key/Key.md?at=master
[Flat File Fixed Width]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/1.%20Flat%20File%20Fixed%20Width/Flat%20File%20Fixed%20Width.md?at=master
[XPath]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/5.%20XPath/XPath.md?at=master
[here]:https://msdn.microsoft.com/en-us/library/ae5bf541(v=vs.90).aspx