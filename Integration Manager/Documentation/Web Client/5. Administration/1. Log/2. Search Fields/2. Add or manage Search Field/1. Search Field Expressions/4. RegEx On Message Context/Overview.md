<tags data-value="RegEx,Context,Expression,Type,Match,Characters"></tags>

RegEx on Message Context
=====
					
___

## :fa-info: Information
RegEx on Message Context is an Expression Type that extracts a value from a message context key by it's name and runs a regular expression over that key.
___

## Instructions

In this section you will learn how to extract data using RegEx on Message Context 

    Tip see RegEx for examples how to write Regular expressions
Link to [RegEx][] examples
     


## Test Expression

1. Provide the form with the RegEx and Context key
2. Add data to the Message Context tab  
    a. Data will be extracted automatically on change 

	
![Test Expression][2]

<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/B8.%20RegEx.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/SearchFields/RegEx_On_MessageContext_TestExpression.png

[Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/Search%20Fields.md?at=master
[Add or manage Search Field]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/Add%20or%20manage%20Search%20Field.md
[Configure Search Field Expression]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/1.%20Display%20Field%20Configurations/Add%20or%20manage%20Display%20Field%20Configurations.md
[Key]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/2.%20Key/Key.md?at=master
[Flat File Fixed Width]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/1.%20Flat%20File%20Fixed%20Width/Flat%20File%20Fixed%20Width.md?at=master
[XPath]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/5.%20XPath/XPath.md?at=master
[RegEx]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/3.%20RegEx/RegEx.md?at=master

[here]:https://msdn.microsoft.com/en-us/library/ae5bf541(v=vs.90).aspx
___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  

| Expression Types	|
|---|
|[Flat File Fixed Width][]	|
|[Key][]	|
|[RegEx][]	|
|[XPath][]	|
