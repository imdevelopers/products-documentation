<tags data-value="XPath,XML,Expression,Message"></tags>

Allowed XPath expressions
=====

___

## :fa-info: Information

This is a references guide for functions that can be used with the XPath plugin.

<table class="table table-striped table-bordered">
<thead>
<tr>
<th>Expression</th>
<th>Message</th>
<th>Output</th>
</tr>
</thead>
<tr>
<td>/e</td>
<td>&lt;e&gt;1&lt;/e&gt;</td>
<td>1</td>
</tr>
<tr>
<td>child::node()</td>
<td>&lt;e&gt;1&lt;/e&gt;</td>
<td>1</td>
</tr>
<tr>
<td>e/child::node()</td>
<td>&lt;e&gt;1&lt;/e&gt;</td>
<td>1</td>
</tr>
<tr>
<td>e/a/child::text()</td>
<td>
&lt;e&gt;<br/>&lt;a&gt;1&lt;/a&gt;<br/>&lt;b&gt;2&lt;/b&gt;<br/>&lt;/e&gt;
</td>
<td>1</td>
</tr>
<tr>
<td>e/a[@name='A']/child::text()</td>
<td>
&lt;e&gt;<br/>
&lt;a name=&quot;A&quot;&gt;1&lt;/a&gt;<br/>
&lt;a name=&quot;B&quot;&gt;2&lt;/a&gt;<br/>
&lt;/e&gt;
</td>
<td>1</td>
</tr>
<tr>
<td>
e/a/@name
</td>
<td>
&lt;e&gt;<br/>
&lt;a name=&quot;A&quot;&gt;1&lt;/a&gt;<br/>
&lt;a name=&quot;B&quot;&gt;2&lt;/a&gt;<br/>
&lt;/e&gt;
</td>
<td>
A<br/>
B
</td>
</tr>
<tr>
<td>
e/a/attribute::*
</td>
<td>
&lt;e&gt;<br/>
&lt;a name=&quot;A&quot;&gt;1&lt;/a&gt;<br/>
&lt;a name=&quot;B&quot;&gt;2&lt;/a&gt;<br/>
&lt;/e&gt;
</td>
<td>
A<br/>
B
</td>
</tr>
</table>

<!-- <tr>
<td></td>
<td></td>
<td></td>
</tr> -->

___

## Instructions


**:fa-exclamation-triangle: Not all types of XPaths can be evaluated using this XPath extractor, due to its a stream reader.**

___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  

|	|
|---|
|[Flat File Fixed Width][]	|
|[Key][]	|
|[RegEx][]	|
|[XPath][]	|




<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/SearchFields/XPath_Select_ExpressionType.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/SearchFields/XPath_TestExpression.png


[Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/Search%20Fields.md?at=master
[Add or manage Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/Add%20or%20manage%20Search%20Field.md?at=master
[Search Field Expressions]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/SearchFieldExpressions.md

[Key]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/2.%20Key/Key.md?at=master
[RegEx]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/3.%20RegEx/RegEx.md?at=master
[Flat File Fixed Width]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/1.%20Flat%20File%20Fixed%20Width/Flat%20File%20Fixed%20Width.md?at=master
[XPath]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/5.%20XPath/XPath.md?at=master

[here]:https://msdn.microsoft.com/en-us/library/ms256086(v=vs.110).aspx

[XPathReader]:https://www.microsoft.com/en-us/download/details.aspx?id=22677