<tags data-value="XPath,XML,Expression,Message"></tags>

:fa-file-code-o: XPath with RegEx
=====

___

## :fa-info: Information
The **XPath with RegEx** Expression Type plugin can be used to find one or more unique values from a string based RegEx on elements and attributes from provided XPath in an XML document.

![XPathWithRegEx][1]

___

## :fa-list-ol: Instructions
In order to extract values from messages with payload you must configure the following properties on a selected [Search Field][Search Fields]:
* Select the **XPath with RegEx** Expression Type Plugin
* Enter a valid XPath in the **Expression** text box
* Check the **Treat sub XML as a string** (the result from XPath is either xml node or string depending on this checkbox)
* Enter a valid RegEx expression in the **RegEx to Style Value** text box
* Enter the number or the name of RegEx Group(s) to return (leave empty for all matches)
* Check the **Global** checkbox to not return on first match
* Select [Message Type][] (The [Logging Service][] applies the Expression on messages of that [Message Type][])
    * **Global** - Applies the Expression on ALL messages (use with caution!!!). Global forces the Optional setting 
* **Optional** - Does not mark the Event as processed with Warnings if value could not be extracted. 

### :fa-code: XPath

For the **XPath** related part, see the [XPath][] expression type plugin.

___

## :fa-tint: Example sample

To exctract order id(s) from the message type `Common.Schemas/IMDEMO/1.0#Orders` you can use the following valid expression `Orders/Order/Id`. This expression extracts the values `101` and `102`  
```xml
<ns0:Orders xmlns:ns0="Common.Schemas/IMDEMO/1.0">
    <Order>
        <Id>101</Id>
        <Amount>1000</Amount>
        <City>Karlstad</City>
    </Order>
    <Order>
        <Id>102</Id>
        <Amount>10</Amount>
        <City>Stockholm</City>
    </Order>
</ns0:Orders>
``` 

## :fa-flask: Test Expression
You can test an expression when configuring a Search Field in the **Test Expression tab**.
* Select the **XPath with RegEx** expression type plugin
* Enter a valid XPath in the **Expression** text box
* Check the **Treat sub XML as a string** (the result from XPath is either xml node or string depending on this checkbox)
* Enter a valid RegEx expression in the **RegEx to Style Value** text box
* Enter the number or the name of RegEx Group(s) to return (leave empty for all matches)
* Check the **Global** checkbox to not return on first match
* Enter payload

Values will be automatically extracted by the [Logging Service][] and presented together with the evaluated processing state and the number of unique matches.

![Test Expression][2]


## :fa-check-square-o: Valid XPath examples	
You can find more examples of allowed XPath expressions [here][]
___

### :fa-hand-o-right: Next Step
:fa-plus-square: :fa-hdd-o: [Add or manage Log Views][]  
Expression Types are used in [Search Fields][]  
How to configure [Search Field Expressions][]  
How to [Add or manage Search Fields][]  


#### :fa-cubes: Related
[Flat File Fixed Width][]  
[Key][]  
[RegEx][]  
[RegEx On Message Context][]  
[XPath][]  
[XPath on wrapped XPath][]  
[JSON Path][]  

<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/SearchFieldExpressionsXPathWithRegEx.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/RegExOnXpathExample1.png




[Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/Search%20Fields.md?at=master
[Add or manage Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/Add%20or%20manage%20Search%20Field.md?at=master
[Search Field Expressions]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/SearchFieldExpressions.md

[Key]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/2.%20Key/Key.md?at=master
[RegEx]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/3.%20RegEx/RegEx.md?at=master
[Flat File Fixed Width]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/1.%20Flat%20File%20Fixed%20Width/Flat%20File%20Fixed%20Width.md
[here]:https://msdn.microsoft.com/en-us/library/ms256086(v=vs.110).aspx

[RegEx On Message Context]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/4.%20RegEx%20On%20Message%20Context/Overview.md?at=master

[XPath on wrapped XPath]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/6.%20Xpath%20on%20wrapped%20XPath/Overview.md

[JSON Path]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/7.%20JSON%20Path/Overview.md?at=master

[XPath]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/5.%20XPath/XPath.md?at=master

[Message Type]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master

[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master


[XPathReader]:https://www.microsoft.com/en-us/download/details.aspx?id=22677

[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master

