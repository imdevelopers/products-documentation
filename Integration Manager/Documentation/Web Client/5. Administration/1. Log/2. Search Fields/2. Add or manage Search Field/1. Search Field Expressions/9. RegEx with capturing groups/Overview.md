<tags data-value="RegEx,Expression,Message"></tags>

:fa-file-code-o: RegEx with capturing groups
=====

___

## :fa-info: Information
The **RegEx with capturing groups** Expression Type plugin can be used as an [Expression][Search Field Expressions] to find one or more unique values from a string based RegEx on **any** text based document, including for example:

* Flatfiles
* EDI/X12
* XML


![png_RegExWithCapturingGroups][]

___

## :fa-list-ol: Instructions

You must have an existing [Search Field][Search Fields], add or edit your **Regex with capturing groups** expression:

* Select the **RegEx with capturing groups** Expression Type Plugin
* Enter a valid RegEx expression in the **Expression** text box
* Enter the number or the name of **RegEx Groups** to return (leave empty for all matches)
* Check the **Global** checkbox to not return on first match
* Check the **Multi Line** to use either `^` for start of line and `$` for end of line

When done, assign the expression type on [messagetypes][Message Type]

* Select [Message Type][] (The [Logging Service][] applies the Expression on messages of that [Message Type][])
* **Global** - Applies the Expression on ALL messages (use with caution!!!). Global forces the Optional setting 
* **Optional** - Does not mark the Event as processed with Warnings if value could not be extracted. 

### :fa-code: Other RegEx related plugins

* **[RegEx][]** 
* **[RegEx On Message Context][]**
* **[RegEx On Message Context with Capturing groups][]**
* **[Xpath with RegEx][]** 

![png_RegExExpressionTypes][]  
*filtered list of RegEx related plugins*
___

## :fa-tint: Example sample #1: XML

To extract city id(s) from the message type `Common.Schemas/IMDEMO/1.1#Orders` you can use the following valid expression `cityId="([0-9]+)"` and group `1`. This expression extracts the values `10` and `11`  
```xml
<ns0:Orders xmlns:ns0="Common.Schemas/IMDEMO/1.1">
    <Order>
        <Id>101</Id>
        <Amount>1000</Amount>
        <City CityId="10">Karlstad</City>
    </Order>
    <Order>
        <Id>102</Id>
        <Amount>10</Amount>
        <City CityId="11">Stockholm</City>
    </Order>
</ns0:Orders>
``` 

## :fa-flask: Test Expression
You can test an expression when configuring a Search Field in the **Test Expression tab**.
* Select the **RegEx with capturing groups** expression type plugin
* Enter a valid RegEx expression in the **Expression** text box
* Enter the number or the name of RegEx Group(s) to return (leave empty for all matches)
* Check the **Global** checkbox to not return on first match
* Check the **Multi Line** to use either `^` for start of line and `$` for end of line
* Enter payload

Values will be automatically extracted by the [Logging Service][] and presented together with the evaluated processing state and the number of unique matches.

![Test Expression][png_SearchFieldExpressionsRegExWithCapturingGroups_Example]

___

## :fa-tint: Example sample #2: Flat File (| Delimited)

To extract the 4th field, for example Customer Code from the following pipe (`|`) delimited flat file you can use the following valid expression `^(?:(.*?)\|){4}` and **RegEx Groups** = `1`. This expression extracts the value `456`.
```txt
ORD|123|ExampleCompany1|456|Company Name ACME |Dieselgate 1|123 45|Flameburg|Fireland
``` 

## :fa-flask: Test Expression
Multiple values can be extracted by checking
* Global
* Multi Line

![Example2][png_RegExWithCapturingGroupsFlatFile]  
*Example extract multiple customer codes from flat file*
___

## :fa-tint: Example sample #3: Flat File (| Delimited with TAG/start of line)

To extract the 4th field, for example Customer Code from the following pipe (`|`) delimited flat file with at start tag `KV` you can use the following valid expression `^KV(?:(.*?)\|){4}` and **RegEx Groups** = `1`. This expression extracts the value `1337`.
```txt
ST|123
KV|Z|AN|1337|ABC123
ID|1912121212|
``` 

## :fa-flask: Test Expression
Multiple values can be extracted by checking
* Global
* Multi Line

![Example3][png_RegExWithCapturingGroupsFlatFile3]  
*Example value from flat file with TAG KV*


## :fa-tint: Example sample #4: Flat File (; Delimited with TAG/start of line)

To extract the 3rd field, for example Customer Code from the following (`;`) delimited flat file with at start tag `HDR` you can use the following valid expression `^HDR(?:(.*?)[;|\n|\r]){3}` and **RegEx Groups** = `1`. This expression extracts the value `42`.
```txt
HDR;ORDER;42
KV;Z;AN;1337;ABC123
ID;1912121212;
``` 


### :fa-hand-o-right: Next Step
:fa-plus-square: :fa-hdd-o: [Add or manage Log Views][]  
Expression Types are used in [Search Fields][]  
How to configure [Search Field Expressions][]  
How to [Add or manage Search Fields][]  
:fa-file: How to [Reindex MessageTypes][View All Message Types]

#### :fa-cubes: Related
1. [Flat File Fixed Width][]  
2. [Key][]  
3. [RegEx][]  
4. [RegEx On Message Context][]  
5. [XPath][]  
6. [XPath on wrapped XPath][]  
7. [JSON Path][]  
8. [XPath with RegEx][]
11. [RegEx On Message Context with Capturing groups][]

<!--References -->
[png_RegExWithCapturingGroups]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/SearchFieldExpressionsRegExWithCapturingGroups.png
[png_SearchFieldExpressionsRegExWithCapturingGroups_Example]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/SearchFieldExpressionsRegExWithCapturingGroups_Example.png

[png_RegExWithCapturingGroupsFlatFile]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/RegExWithCapturingGroupsExample1.png

[png_RegExWithCapturingGroupsFlatFile3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/RegExWithCapturingGroupsExample3.png
[png_RegExExpressionTypes]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/RegExExpressionTypes.png



[Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/Search%20Fields.md?at=master
[Add or manage Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/Add%20or%20manage%20Search%20Field.md?at=master
[Search Field Expressions]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/SearchFieldExpressions.md

[Key]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/2.%20Key/Key.md?at=master
[RegEx]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/3.%20RegEx/RegEx.md?at=master
[Flat File Fixed Width]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/1.%20Flat%20File%20Fixed%20Width/Flat%20File%20Fixed%20Width.md
[here]:https://msdn.microsoft.com/en-us/library/ms256086(v=vs.110).aspx

[RegEx On Message Context]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/4.%20RegEx%20On%20Message%20Context/Overview.md?at=master

[XPath on wrapped XPath]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/6.%20Xpath%20on%20wrapped%20XPath/Overview.md

[JSON Path]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/7.%20JSON%20Path/Overview.md?at=master

[XPath]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/5.%20XPath/XPath.md?at=master

[XPath with RegEx]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/8.%20XPath%20with%20RegEx/Overview.md

[Message Type]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master

[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master


[XPathReader]:https://www.microsoft.com/en-us/download/details.aspx?id=22677

[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master

[View All Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/1.%20View%20All%20Message%20Types/View%20All%20Message%20Types.md?at=master

[RegEx On Message Context with Capturing groups]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/11.%20RegEx%20On%20Message%20Context%20with%20Capturing%20Groups/Overview.md?at=master
