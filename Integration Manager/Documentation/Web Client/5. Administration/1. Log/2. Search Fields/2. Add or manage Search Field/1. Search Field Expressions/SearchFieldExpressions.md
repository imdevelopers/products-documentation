<tags data-value="Search Field Expressions, Search Fields, Message Types, Log Views, Logging Service"></tags>

:fa-search-plus: Search Field Expressions
=====
					


___

## :fa-info: Information

**Search field expressions** are used to extract data for a given [Search Field][]. For example, an **Order Number** may originate from multiple messages (of different message types) and in order to find this **order number** one or more search field expression are used.

![png_OrderNumberExample][]

* There can be any number of **search field expressions** for a given [Search Field][]
* One expression is used per one or more [message type][Message Types]
* The search field expression can be set as being **Global*** (used on all message types) - Use with care!!! This option may affect overall system performance.
* The search field expression can be set as being **Optional** - Missing data will nor render the event in the *Processed with Warnings** state


A **Search Field Expression** retrieves values from:
* :fa-envelope: Messages (message body/payload)
* :fa-key: Context data (Meta data stored in a Key/Value Collection for the Event

The extract operation performed by the [Logging Service][] uses the statement from the **Expression** on the selected [Message Type][] using the selected **Search Field Expression Type**. 
The latter is a .NET dll implementing the appropriate contracts. The DLL's are put into the 'Plugins' folder of the [Logging Service][].

#### :fa-flash: Action
You can either **Edit** or **Delete** search field expressions.

**Delete** - Removes the **Search Field Expression** from the list

**Edit** - Loads the configuration for the selected **Search field expression**. When **edit** has been  clicked, you will be able to test and update the configuration for the **Search Field Expression**.

![SearchFieldExpressions][png_SearchFieldExpressions]
 

#### Expression
The **Expression** is the statement the [Logging Service][] uses to extract values from the message body or context data on messages matching the [Message Type][Message Types].

#### Expression Type
**Expression Type** is the type of expression or plugin that extracts unique values.

#### :fa-file:  Message Type
Messages (payload/context) of [Message Type][] to apply expression on.

Select the [Message Types][] that this **Expression** should be applied on.

![Message Type][3]

Clicking on the link of the [Message Types][Message Types] will open the corresponding message type from the [Repository][].

![Link To Message Type][6]

:fa-globe: Global - Use expression on all Message Types. 

    NOTE: Use this feature only when you really have the need for it. All inbound messages will be processed, looking for a value to extract. This will add significant processing overhead to the Logging Service. 

:fa-question-circle: Optional - Optional does not generate flag the message with warnings when values could not be found/extracted.

### Search Field Expression

![Search Field Expression][5]

### Expression Types

The built in Expression Types to choose between are:

1. [Flat File Fixed Width][]
2. [Key][]
3. [RegEx][]
4. [RegEx on Message Context][]
5. [XPath][]
6. [XPath on Wrapped XPath][]
7. [JSON Path][]
8. [XPath with RegEx][]
9. [RegEx with capturing groups][]
10. [XPath On Message Context][]
11. [RegEx On Message Context with Capturing groups][]
12. [Flat File CSV][]

Since Integration Manager is an extensible platform, you may write and add your own plugins. Useful for special cases and encrypted messages.

<table>
<tr>
<td>1. Flat file fixed width<br><img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/SearchFieldExpressionsFlatFileFixedWidthSelection.png"/></td>
<td>2. Key<br><img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/SearchFieldExpressionsKeySelection.png"/></td>
<td>3. RegEx<br><img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/SearchFieldExpressionsRegExSelection.png"/></td>
</tr>
<tr>
<td>4. RegEx On Message Context<br><img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/SearchFieldExpressionsRegExOnMessageContextSelection.png"/></td>
<td>5. XPath<br><img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/SearchFieldExpressionsXPathSelection.png"/></td>
<td>6. XPath on wrapped XPath<br><img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/SearchFieldExpressionsXPathOnWrappedXPathSelection.png"/></td>
</tr>
<tr>
<td>7. JSON Path<br><img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/SearchFieldExpressionsJSONPathSelection.png"/></td>
<td>8. XPath with RegEx<br><img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/SearchFieldExpressionsXPathWithRegEx_Selection.png"/></td>
<td>9. RegEx with capturing groups<br><img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/SearchFieldExpressionsRegExWithCapturingGroups_Selection.png"/></td>

<tr>
<table>


### :fa-cog: Processing State

There are three types of processing states which is expressed as a cogwheel with a green, yellow or red sign.

1. OK - green, evaluation gave 1 or more results
2. Warning - yellow, evaluation gave no result (failed to find data from provided expression)
3. Error - Red, some error either in the expression or the [Logging Service][] is offline

<table>
<tr>
<th>1. OK</th>
<th>2. Warning</th>
<th>3. Error</th>
</tr>
<tr>
<td><img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A57.6%20Test%20Expression%20Fine.png"/></td>
<td><img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A57.4%20Test%20Expression.png"/></td>
<td><img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A57.7%20Test%20Expression%20Failed.png"/></td>
</tr>
<table>


## :fa-save: Save/Update
When done, make sure to click 'Update' to save changes to the Search Field Expressions.  
![Update][png_Update]

    NOTE: If you just click 'Ok' changes will be lost!

When closing the dialog you will be presented a modal to re-index selected messagetypes. 

    NOTE: Make sure to keep the number of re-index operations to a minimum, since all data (note read only IM Log databases) based on involved Message Types will be reprocessed by the Logging Service. A re-index operation cannot be aborted. 

___
### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-hdd-o: [Add or manage Log Views][]  
:fa-plus-square: :fa-file: [Add or manage Message Types][]  

##### :fa-cubes: Related  
:fa-search: [View All Search Fields][]    
:fa-search-plus: [Search Field Expressions][]  
:fa-sitemap: [Repository][]    
:fa-hdd-o: [Logging Service][]   
:fa-file: [View All Message Types][]  
:fa-file: [Message Type][]


<!--References -->

[Search Field Expressions]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/SearchFieldExpressions.md
[Search Field]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/Search%20Fields.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[View All Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/1.%20View%20All%20Message%20Types/View%20All%20Message%20Types.md?at=master
[View All Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/1.%20View%20All%20Search%20Fields/View%20All%20Search%20Fields.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master
[Add or manage Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/2.%20Add%20or%20manage%20Message%20Type/Add%20or%20manage%20Message%20Type.md?at=master


<!--References -->

[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/SearchFieldMessageTypeSelection.png

[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A57.%20Search%20Field%20Expression.png
[6]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A57.8%20Link%20to%20Message%20Type.png
[png_OrderNumberExample]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/OrderNumberExample.png
[png_SearchFieldExpressions]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/SearchFieldExpressions.png
[png_Update]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/Update.png

[Message Type]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master


[RegEx]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/3.%20RegEx/RegEx.md?at=master

[RegEx on Message Context]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/4.%20RegEx%20On%20Message%20Context/Overview.md?at=master

[XPath]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/5.%20XPath/XPath.md?at=master
[Flat File Fixed Width]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/1.%20Flat%20File%20Fixed%20Width/Flat%20File%20Fixed%20Width.md?at=master
[Key]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/2.%20Key/Key.md?at=master
[Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/7.%20Included%20Message%20Type/Included%20Message%20Type.md?at=master


[XPath on Wrapped XPath]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/6.%20Xpath%20on%20wrapped%20XPath/Overview.md?at=master

[JSON Path]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/7.%20JSON%20Path/Overview.md?at=master
[XPath with RegEx]:8.%20XPath%20with%20RegEx/Overview.md
[RegEx with capturing groups]:9.%20RegEx%20with%20capturing%20groups/Overview.md

[RegEx On Message Context with Capturing groups]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/11.%20RegEx%20On%20Message%20Context%20with%20Capturing%20Groups/Overview.md?at=master
