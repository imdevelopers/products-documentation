<tags data-value="Serach Field"></tags>

:fa-search: Add or manage Search Field
=====
					


___

## :fa-info: Information
In this section you will learn how to manually add or manage a [Search Field][].

Add a new [Search Field][] or Edit an existing from the list in the [overview][View All Search Fields].
A [Search Field][] can host any number of [Search Field Expressions][].   
![SF][1]

### Mandatory Fields
* A **Name** is required to create and name the **Search Field**.

* A **Data Type** is required in order to provide the proper relational operators when working with the [Log Views][].

The Data Types are driven by a database configuration and additional data types may be added. 
The following data types comes out of the box with Integration Manager:
* Text
* Integer
* Long Integer
* Real number with 2 decimals

![Data Type][3]

### Optional Fields
Adding a Description, Search Field Links and Web Site is optional. 

### Search Field Links
By clicking Edit a modal dialogue is shown where one can configure Search Field links. These links can be used to make the result from the search field expression to show as a link and when clicked opens a new tab with the specified URL.
![Search Field Links Modal][4]

The following can be configured for a Search Field Link:
* Enabled - Whether the Link should be enabled making search field results as a link or not. This is useful if one temporary want to disable the link without deleting it
* Name - The name of the link ex. OrderId, City.
* URL - The URL where to browse when the link is clicked (which opens a new tab in the web browser)
* Description - A suitable description that is shown when hovering the mouse pointer over the link

URL can contain an optional parameter '{value}' ex. https://www.google.com?q={value}. The value will be replaces by the Search Field Expression value. Ex. Delivery address City. By filling in a test value and clicking the link symbol at the right side of the URL-field one can test how the link is opened in a new tab in the browser.

![SearchFieldLinksModalURLTestValue][5]

#### Configured Search Field expressions

You can Edit the [Search Field Expressions][] for the Search Field. There can be any number of [Search Field Expressions][] assigned on a [Search Field][].  
![Configured Search Field expressions][2]
___
### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-hdd-o: [Add or manage Log Views][]  
:fa-plus-square: :fa-file: [Add or manage Message Types][]  

##### :fa-cubes: Related  
:fa-search: [View All Search Fields][]    
:fa-search-plus: [Search Field Expressions][]  
:fa-sitemap: [Repository][]    
:fa-hdd-o: [Logging Service][]   
:fa-file: [View All Message Types][]  


<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A56.%20Add%20Search%20Field.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A56.2%20Configured%20Search%20Field%20expression.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A56.3%20Data%20Type.png

[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/SearchFieldLinksModal.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/SearchFieldLinksModalURLTestValue.png

[Search Field Expressions]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/SearchFieldExpressions.md
[Search Field]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/Search%20Fields.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[View All Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/1.%20View%20All%20Message%20Types/View%20All%20Message%20Types.md?at=master
[View All Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/1.%20View%20All%20Search%20Fields/View%20All%20Search%20Fields.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master
[Add or manage Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/2.%20Add%20or%20manage%20Message%20Type/Add%20or%20manage%20Message%20Type.md?at=master
