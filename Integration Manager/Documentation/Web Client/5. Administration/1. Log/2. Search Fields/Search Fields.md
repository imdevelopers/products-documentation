<tags data-value="Search Fields Expression, Search Fields, Message Types, "></tags>

:fa-search: Search Fields
=====
				

___

## :fa-info: Information

A **Search Field** is used in [Log Views][] in order to facilitate search conditions and can also be used for applying restrictions. 
Another powerful use of **Search Fields** is the ability for users to peform Group By operations. An example can be; 'List all business transactions grouped by Order Number'.

Values for the **Search Field** comes from the use of [Search Field Expressions][]. There can be any number of [Search Field Expressions][] set for a given **Search Field**.
There can be more than one value extracted from a [Search Field Expression][Search Field Expressions]. This may be useful when dealing with batches and messages with more than one entity (like multiple orders). 
The [Web Client][] provides a test bench for testing [Search Field Expressions][]. These tests actually make  API calls to the [Logging Service][] in order to ensure proper operation for real life messages.

![plugins][1]

Any logged message may be reprocessed and additional Search Fields on existing data can be added to Integration Manager. From a support and maintenance perspective this means that you can find messages based on added criterias in a very late stage. You do not need to know, design and code in advance to find the data you are looking for. 

If old data already exists and you want to be able to search according to the new settings for the **Search Field**, a reindex user initiated operation must be executed.
The re-index operation is based on [Message Type][]. 

There are no pre-requisites on the logged data and the values can origin from:
* Message Body
* Context Values

The [Logging Service][] will process all messages based on [Message Type][]. Integration Manager supports the following formats for messages out of the box:
* XML
* Flat File
* EDI

Writing your own **Search Field Expressions Type Plugin** provides additional support for:
* Encrypted messages
* ZIP files
* Special cases, for example: 'Get last 2 characters on line 3 in reverse and with uppercase'
* Custom .NET code based

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-search: [Add or manage Search Fields][]    
:fa-plus-square: :fa-file: [Add or manage Message Types][]  
:fa-plus-square: :fa-hdd-o:  [Add or manage Log Views][]  

##### :fa-cubes: Related  
:fa-search-plus: [Search Field Expressions][]  
:fa-sitemap: [Repository][]    
:fa-hdd-o: [Logging Service][]   
:fa-file: [View All Message Types][]  
:fa-search: [View All Search Fields][]    

<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/SearchFieldExpressionTypePlugin.png
[Search Field Expressions]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/1.%20Search%20Field%20Expressions/SearchFieldExpressions.md
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Add or manage Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/2.%20Add%20or%20manage%20Message%20Type/Add%20or%20manage%20Message%20Type.md?at=master
[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master
[Message Type]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[View All Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/1.%20View%20All%20Message%20Types/View%20All%20Message%20Types.md?at=master
[Add or manage Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/2.%20Add%20or%20manage%20Search%20Field/Add%20or%20manage%20Search%20Field.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[View All Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/1.%20View%20All%20Search%20Fields/View%20All%20Search%20Fields.md?at=master
