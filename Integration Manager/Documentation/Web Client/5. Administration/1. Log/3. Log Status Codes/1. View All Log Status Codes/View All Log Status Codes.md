<tags data-value="View,All,Log,Status,Codes"></tags>

:fa-flag-o: View All Log Status Codes
=====
					


___

## :fa-info: Information
The overview shows all user defined [Log Status Codes][] and the result can be filtered by entering characters, edit, delete, [Show Deleted][], and [Add or manage Log Status Codes][].

[Log Status Codes][] are used within [Log Views][Log View] to represent the state for the logged Event.
Example of [Log Status Codes]:

![LogStatusCodes][1]
___


### :fa-hand-o-right: Next Step  
:fa-plus-square:  :fa-flag-o: [Add or manage Log Status Codes][]  
:fa-plus-square: :fa-group: [Add or manage Roles][]    
:fa-plus-square: :fa-hdd-o: [Add or manage Log Views][]    

##### :fa-cubes: Related  
:fa-group: [Roles][]      
:fa-hdd-o: [Log View][]      
:fa-user-secret: [Authorization][]  
:fa-globe: [HTTP Status Codes][]


<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/LogStatusCodes.png

[Log Status Codes]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/3.%20Log%20Status%20Codes/Log%20Status%20Codes.md?at=master
[Show Deleted]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Show%20Deleted.md?at=master
[Add or manage Log Status Codes]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/3.%20Log%20Status%20Codes/2.%20Add%20or%20manage%20Log%20Status%20Code/Add%20or%20manage%20Log%20Status%20Code.md?at=master

[HTTP Status Codes]:https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
   
<!--References -->
[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[Add or manage Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/2.%20Add%20or%20manage%20Role/Add%20or%20manage%20Role.md?at=master
[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master
[Add or manage Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/2.%20Add%20or%20manage%20Role/Add%20or%20manage%20Role.md?at=master
[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master
[Authorization]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/Authorization.md?at=master
[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/Log%20Agents.md?at=master
[Log4Net]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Log4Net/Log4Net/Log4Net%20Appender.md
