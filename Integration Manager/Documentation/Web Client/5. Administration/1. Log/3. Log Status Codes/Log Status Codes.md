<tags data-value="Log,Status,Codes"></tags>

:fa-flag-o: Log Status Codes
=====
					


___

## :fa-info: Information
Logged Events can have a **Log Status Code** set to represent the state of the event. The Events are logged from any of the [Log Agents][]. 
Since the **Log Status Code** is a number it is hard for end users to understand its semantics. Integration Manager provides a way to map these codes into meaningful textual represenations.

**Some Examples:**
* 0 from the BizTalk Log Agent can mean 'Message sucessfully transmitted' - **Ok**
* 76 from custom Dynamix Log Agent can mean 'Invoice successfully created' - **Ok**
* 401 from custom Web Api Log agent can mean 'Bad Request' - **Error**
* -16 from a custom [Log4Net][] Log Agent can mean 'Missing due date' - **Warning**

A **Log Status Code** is also being used to indicate state level, examples:
* :fa-check-circle-o: OK - Used to indicate normal operation
* :fa-exclamation-triangle: Warning  - Used to indicate an unexpected but not invalid state
* :fa-times-circle-o: Error - Used to indicate an error/fatal condition

The **Log Status Codes** are unique for each [Log Agent][Log Agents].

You must be part of the [Administrators][Roles] role to configure **Log Status Codes**.


___


### :fa-hand-o-right: Next Step  
:fa-plus-square:  :fa-flag-o: [Add or manage Log Status Codes][]   
:fa-eye: :fa-flag-o: [View All Log Status Codes][]  

:fa-plus-square: :fa-group: [Add or manage Roles][]    
:fa-plus-square: :fa-hdd-o: [Add or manage Log Views][]    

##### :fa-cubes: Related  
:fa-group: [Roles][]      
:fa-hdd-o: [Log View][]      
:fa-user-secret: [Authorization][]  
:fa-globe: [HTTP Status Codes][]


<!--References -->

[Add or manage Log Status Codes]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/3.%20Log%20Status%20Codes/2.%20Add%20or%20manage%20Log%20Status%20Code/Add%20or%20manage%20Log%20Status%20Code.md?at=master

[HTTP Status Codes]:https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
   
<!--References -->
[View All Log Status Codes]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/3.%20Log%20Status%20Codes/1.%20View%20All%20Log%20Status%20Codes/View%20All%20Log%20Status%20Codes.md?at=master

[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master

[Add or manage Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/2.%20Add%20or%20manage%20Role/Add%20or%20manage%20Role.md?at=master
[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master
[Add or manage Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/2.%20Add%20or%20manage%20Role/Add%20or%20manage%20Role.md?at=master
[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master
[Authorization]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/Authorization.md?at=master
[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/Log%20Agents.md?at=master
[Log4Net]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Log4Net/Log4Net%20Appender.md?at=master
