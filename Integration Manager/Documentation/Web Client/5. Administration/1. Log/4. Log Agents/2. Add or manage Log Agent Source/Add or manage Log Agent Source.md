<tags data-value="Add,Log,Agent,Source,Value"></tags>

Add and Manage Log Agent Source
=====

___

## :fa-info: Information
Events and messages comes from a [Log Agent Source][Log Agent] that Integration Manager identifies with a unique Id (*Log Agent Value Id*). 
This unique Id must be set manually by an **Administrator**. Events that stem from the LogApi must have this information embedded. On the first event, the Log Agent Source will be automatically created if it does not already exist. The name will be 'Not Set'.

## Instructions

1. Add a new Log Agent by clicking on the **Add Log Agent Source**  

<table>
<tr>
<th>From Web Client List</th>
<th>From modal</th>
</tr>
<tr>
<td><img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/LogAgents/AddLogAgentSourceWebClientMenu.png"/></td>
<td><img src="https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/LogAgents/AddLogAgentSourceMenuItem.png"/></td>
</tr>
<table>


2. Enter the following details
* a. **Name** - User friendly name for this *Log Agent Source* (Required)
* b. **Log Agent Value Id** - Uniqe Id (Required field) - Must not change once set!
* c. **Description** - User friendly description about the *Log Agent Source* (Optional)
* d. **Web Site** - Link to remote documentation (Optional)

![NewLogAgent][0]


#### Name
Name the **Log Agent Source**.

#### Log Agent Value Id
Log Agent Value Id is the value of the **Log Agent Source**.

#### :fa-text: Description
Add additional information about the **Log Agent Source**.

#### :fa-globe: URL
The URL to the Log Agent.
___

### :fa-hand-o-right: Next Step
* **[BizTalk][]** - Uses bult in Tracking and/or Pipeline Components  
    * BizTalk has special support, see more [here][Configure BizTalk LogAgent]
* **[Log4Net][]** - Simply add some dll:s and a new appender to the app.config file of your custom .NET application  
* **[Mule][]** - Uses Log4J from Logger and Business Events (code)  
* **[Log Api][LogAPI]** - Log from code (C# and Java)  
* **[VB6/COM/VBScript][VBScript]** - Log from VB6/VBScript code  

#### :fa-cubes: Related
:fa-hdd-o: [Logging Service][] - Processes (indexing) the messages   
:fa-hdd-o: [Log Views][] - Manage [User][Users] access to events and messages cross platform  
:fa-database: [Log Databases][] - Keep as much data as you like  


<!--References -->
[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/LogAgents/AddNewLogAgentSource.png

[BizTalk]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Microsoft%20BizTalk%20Server/Overview.md?at=master
[Log4Net]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Log4Net/Log4Net%20Appender.md?at=master
[Mule]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Mule/Overview.md?at=master
[Log Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/Log%20Agents.md?at=master
[LogAPI]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Log%20API/Overview.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Web API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Web%20API/Overview.md?at=master
[Log Databases]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log

[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master

[Log Databases]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Log%20Databases/Log%20Databases.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[Configure BizTalk LogAgent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Installation/BizTalk%20Log%20Agent.md?at=master