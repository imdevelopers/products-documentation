<tags data-value="Log Agents, Log Views, Status Codes"></tags>

:fa-archive: Manage Log Agents
=====
					


___

## :fa-info: Information

A **Log Agent** is a custom built agent which gets information from a source within a certain interval.
Integration Manager has support for logging from many integration platforms and custom build applications, see [Log Agents][] for more information.

Information about the **Log Agent** can be managed in the [Web Client][] of Integration Manager.

:fa-flag: Each Log Agent may log events and messages with an optional **status code**, for more information, see [Log Status Codes][].  


<!--References -->
[How to add or manage Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/2.%20Add%20or%20manage%20Log%20Agent%20Source/Add%20or%20manage%20Log%20Agent%20Source.md?at=master
___

### :fa-hand-o-right: Next Step
:fa-plus-square: :fa-hdd-o: [Add or manage Log Agents][]  
:fa-plus-square:  :fa-flag-o: [Log Status Codes][]  
:fa-hdd-o: [Log Views][]  

#### :fa-cubes: Related
:fa-hdd-o: [Log Views][]   
:fa-archive: List of [Log Agents][]     

[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Overview.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master
[Log Status Codes]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/3.%20Log%20Status%20Codes/Log%20Status%20Codes.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Add or manage Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/2.%20Add%20or%20manage%20Log%20Agent%20Source/Add%20or%20manage%20Log%20Agent%20Source.md?at=master
