<tags data-value="Log,Views,Databases,Status,Search"></tags>

:fa-hdd-o: Log
=====

___

## :fa-info: Information
Events and Messages comes from [Log Agents][] and/or from custom built applications using the [Log API][]. Message Payload and Context is stored encrypted in the [Log Databases][].

The [Logging Service][] will process all Events and extract values for configured [Search Fields][] in order to facilitate search and restrictions on [Log Views][]. 
[Users][] gain access and insights to this data using [Log Views][]. [Users][] must have a membership in [Roles][] to see and use the [Log Views][].

___
![log][1]

The **Log** menu pane will give **Administrators** access to manage the following **Log** related artifacts:

* :fa-hdd-o: [Log Views][] - Manage **Log views**
* :fa-search: [Search Fields][] - Manage **Search Fields**
* :fa-exchange: [Log Status Codes][] - Manage **Log Status Codes**
* :fa-archive: [Log Agents][] - Manage **Log Agents**

___

### :fa-hand-o-right: Next Step  
:fa-paint-brush: [Customize][]  
:fa-plus-square: :fa-group: [Add or manage Role][]  
:fa-plus-square: :fa-user: [Add or manage Users][]  


##### :fa-cubes: Related  
:fa-search: [Search Fields][]  
:fa-user: [Users][]     
:fa-group: [Roles][]     
:fa-hdd-o: [Log Views][]    
:fa-database: [Log Databases][]

<!--References -->
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Log API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Log%20API/Overview.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[Log Databases]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Log%20Databases/Log%20Databases.md?at=master
[Log Status Codes]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/3.%20Log%20Status%20Codes/Log%20Status%20Codes.md?at=master
[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/Log%20Agents.md?at=master
[Search Fields]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/Search%20Fields.md?at=master
[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[Add or manage Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/2.%20Add%20or%20manage%20User/Add%20or%20manage%20User.md?at=master
[Add or manage Role]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/2.%20Add%20or%20manage%20Role/Add%20or%20manage%20Role.md?at=master

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/log.png
[Customize]:http://documentation.integrationmanager.se/#/?file=Web%20Client%2F5.%20Administration%2F4.%20Customize%2FCustomize.md