<tags data-value="Monitor, Monitor Agents, Monitoring Service"></tags>

:fa-desktop: View All Monitor Views
=====
					


___

## :fa-info: Information
This is where you can perform administative configuration changes if you are an **administrator**.
 
The list of user defined [Monitor Views][]. A large list of **Monitor Views** can be narrowed down by typing characters into the filter text box.
You can [Add][Add or manage Monitor Views], [Edit][Add or manage Monitor Views] and [Delete][Show Deleted] records.

 

![MonitorViews][1]
___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-laptop: [Add or manage Monitor Views][]    
:fa-plus-square: :fa-user: [Add or manage Users][]  
:fa-plus-square: :fa-group: [Add or manage Roles][]

##### :fa-cubes: Related  
:fa-user-secret: [Authorization][]  
:fa-user: [Users][]    
:fa-group: [Roles][]  

<!--References -->
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[Add or manage Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/2.%20Add%20or%20manage%20User/Add%20or%20manage%20User.md?at=master
[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Authorization]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/Authorization.md?at=master
[Add or manage Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/2.%20Add%20or%20manage%20Role/Add%20or%20manage%20Role.md?at=master

[Show Deleted]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Show%20Deleted.md?at=master

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/MonitorViewsAdmin.png

