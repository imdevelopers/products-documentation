<tags data-value="Alarm,Plugin,Email,Errors"></tags>

:fa-flash: Alarm Plugins
=====
			

___

## :fa-info: Information
The [Monitoring Service][] iterates over the selected **Alarm plugin(s)** and thereby pushes alerts (positive and negative) to [users][Users] in [roles][Roles] for [Monitor Views][].  

In this section:  
* Select one or more plugins used to **push** the alert to end users
    * Out of the box plugins exists (mail, mail with options, Windows event log)
    * 3rd party plugins can be added
* **Customize the output** of the alert using [Stylesheets][]
* Configure for **Recurrence**

You can add more than one [Alarm Plugin][] to a [Monitor View][Monitor Views] to push information when errors or warnings occur (also for OK status change).
For example, the email plugin sends an email to concerned users (with an email address specified) to notify about changes in the state of [Resources][] part of the [Monitor View][Monitor Views].

Partners and customers can create custom Alarm plugins when there is a need to send an alert to another system. Typical use cases are:
* Ticket handling systems like Jira, Freshdesk, ZenDesk
* Databases for custom follow up
* SMS (however most of our customers and partners are using shared mailboxes)

A C# .NET based template exists and can be used for free, part of the license (SA agreement). Contact our [support][Support] to acquire the latest/compatible version.

## :fa-cog: Settings
The list of plugins can be narrowed down by entering a filter string. 
Press the 'Add' button to use the selected **Alarm plugin**.

You can Add, Filter and Remove Alarm Plugins from the [Monitor View][Monitor Views].
![Recurrence][2]

## :fa-file-text-o: Stylesheet support
Some of the plugins (email plugins out of the box) has support for user customization of content sent for an alert. These are based on the built in [Stylesheet][Stylesheets] support.


![emailalert][1]  
*example of alert*

## :fa-history: Recurrence
Alerts can be resent using the **Recurrence** feature available in the configuration modal. The values are based on the settings from the [Time Intervals][] configuration.
For each [Monitor View][Monitor Views] different settings for the **Recurence** of pushing the alert can be set. These can further be detailed on the severity level for the included [Resources]:  

* **Source unavailable** - If there is a problem for the [Monitoring Service][] to communicate with the [Monitor Agents][]  
* **Unavailable** - If there are problems (typically invalid configurations) with specific [Resources][]   
* **Error** - If there are [Resources][] in error state   
* **Warning** - If there are [Resources][] in warning state    
___

### :fa-hand-o-right: Next Step
:fa-desktop: [Monitor Views][]  

#### :fa-cubes: Related
:fa-user: [Users][]  
:fa-group: [Roles][]     
:fa-lightbulb-o: [Resources][]   
:fa-file-text-o: [Stylesheets][]  
:fa-clock-o: [Time Intervals][]

<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/emailalert.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/Recurrence.png

[Alarm Plugin]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/1.%20Alarm%20Plugins/Alarm%20Plugins.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master
[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Support]:http://support.integrationsoftware.se
[Stylesheets]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/5.%20Stylesheets/Stylesheets.md?at=master&fileviewer=file-view-default

[Time Intervals]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/3.%20Time%20Intervals/Time%20Intervals.md?at=master&fileviewer=file-view-default


[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
