<tags data-value="Monitor,Sources,View"></tags>

Monitor Sources
=====
					


___

## :fa-info: Information
Select the [Sources][] you want to monitor in the [Monitor View][].

## Instructions


You can Add, Filter and Remove Sources.

![Monitor Sources][1]



<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A59.Monitor%20Sources.png

[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Monitor View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  
