<tags data-value="Excluded,Monitoring,Resources,Source,Application"></tags>

Excluded Monitoring Resources
=====
					


___

## :fa-info: Information
After a Monitor [Source][] is chosen, you are able to exclude resources from within the chosen Source.

## Instructions

You can Add, Remove and Filter based on Source, [Application][], [Category][], and characters.

![Excluded Resources][1]

#### Filter on Source

![Filter on Source][2]

#### Filter on Application

![Filter on Application][3]

#### Filter on Category

![Filter on Category][4]



<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A61.%20Excluded%20Resources.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A60.2%20Filter%20on%20Source.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A60.3%20Filter%20on%20Application.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A60.4%20Filter%20on%20Category.png


[Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Application]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/Applications.md?at=master
[Category]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/Categories.md?at=master


___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  
