<tags data-value="Included,Integrations,Monitor,View"></tags>

Included Integrations
=====
					


___

## :fa-info: Information
Choose which [Integrations][] should be included in the [Monitor View][].

## Instructions

You can Add, Filter and Remove **Integrations**

![Include Integrations][1]


<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A62.%20Include%20Integrations.png

[Monitor View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Integrations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/1.%20Integrations/Integrations.md?at=master
___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  
