<tags data-value="Create,Add,Monitor,View,History"></tags>

Add or manage Monitor Views
=====
					

___

## How to add and manage a Monitor View
:fa-check-square-o: In this section you will learn how to add and manage a monitor view.

## Add a new Monitor View
![Add Monitor View][1]

## Mandatory fields
* **Name**: A monitor view must have at least a unique name assigned. 
* **Roles**: For end users to able to see and use the view it must have :fa-group: [Roles][] assigned. [Assign Roles][4].

Resources comes from Sources and you can add/exclude these from any combination of the following: 
* **Sources**: Sources or individual Resources. Resources can be excluded from a Source.
  * **Categories**: Add Resources of the same type
  * **Applications**: Add all Resources belonging to the same logical Application.     


## Optional fields
* **Alarm Plugins**: Select the plugin to use for raising the alert.  


    Tip You can create a Monitor View for Dashboard usage. Put a large screen TV in your support team room and use a dedicated user within Integration Manager. The Monitor View won't send any mails if there is no plugin selected.

* **Description**: You can and should give your Monitor View a user friendly description of its intended purpose.
* **Web Site**: You can provide a quick link for users when working with and viewing  the Monitor View. This quick link is usually a WIKI/Sharepoint site with additional documentation. 

* **Show history of monitor events**: Enables or disables the users ability to view historical records for resource. By enabling this feature the user will be able to see how all and individual resources have changed state within Integration Manager over time. The history is limited by a system parameter **'DaysToKeepMonitorEvents'** in the configuration database for Integration Manager.   
![ShowHistory][5]

* **Allow Actions**: Check this checkbox to allow users of the assigned Roles to execute [Actions][] on Resources part of this Monitor View.   
![AllowActions][3]
___

### Edit Monitor View 
You can manage the following settings of a Monitor View
* [Assign Roles][4]
* [Edit - Add/Remove Alarm Plugins][11]
* [Edit - Add/Remove Sources][10]
* [Included Monitoring Resources][]
* [Excluded Monitoring Resources][]
* [Included Integrations][]


![Monitor Extras][2]


##### :fa-cubes: Related  
:fa-history: [View history for Resource][]  
:fa-flash: [Actions][]



<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/EmptyMonitorView.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/Settings.png

[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/AllowActions.png

[4]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/1.%20Allow%20These%20Roles/Allow%20These%20Roles.md?at=master
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/ShowHistory.png
[11]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/2.%20Add%20Alarm%20Plugins/Add%20Alarm%20Plugins.md?at=master
[10]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/3.%20Monitor%20Sources/Monitor%20Sources.md?at=master

[Included Monitoring Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/4.%20Included%20Monitoring%20Resources/?at=master
[Excluded Monitoring Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/4.%20Included%20Monitoring%20Resources/?at=master
[Included Integrations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/6.%20Included%20Integrations/?at=master
[Monitor View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/3.%20Available%20Search%20Fields/Available%20Search%20Fields.md?at=master
[View history for Resource]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/3.%20View%20History/History.md?at=master
[Actions]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/1.%20View%20Monitor%20Views/2.%20View%20Single%20Monitor%20View/Actions/Actions.md?at=master
