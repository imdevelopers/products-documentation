<tags data-value="Log,Monitor"></tags>

:fa-desktop: Monitor Views Administration
=====
					


___

## :fa-info: Information
A **Monitor View** is a collection of [Resources][] from [Monitor Agents][]. [Resources][] are synced by the [Monitoring Service][] to provide an update to the **Monitor View**.
[Users][] gain access to the **Monitor View** depending on the [Role][] membership. 

You can [Add][Add or manage Monitor Views], [Edit][Add or manage Monitor Views] or [Delete][Show Deleted] records. 
The list of defined **Monitor Views** are listed in the [overview][View All Monitor Views].

___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]  
:fa-plus-square: :fa-user: [Add or manage Users][]  
:fa-plus-square: :fa-group: [Add or manage Roles][]  
:fa-plus-square: :fa-download:  [Add or manage Sources][]

##### :fa-cubes: Related  
:fa-desktop: [View All Monitor Views][]    
:fa-cloud-upload: [Monitor Agents][]    
:fa-cloud-download: [Sources][]  
:fa-group: [Roles][Role]  
:fa-user: [Users][]  
:fa-lightbulb-o: [Resources][]   
:fa-user-secret: [Authorization][]

<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/MonitorViewsAdmin.png

[Add or manage Monitor Views]:
http://documentation.integrationmanager.se/#/?file=Web%20Client%2F5.%20Administration%2F2.%20Monitor%2F1.%20Monitor%20Views%2F2.%20Add%20or%20manage%20Monitor%20View%2FAdd%20or%20manage%20Monitor%20View.md
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Add or manage Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master  
[View All Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/1.%20View%20All%20Monitor%20Views/View%20All%20Monitor%20Views.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master
[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Role]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Authorization]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/Authorization.md?at=master
[Show Deleted]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Show%20Deleted.md?at=master
[Add or manage Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/2.%20Add%20or%20manage%20Role/Add%20or%20manage%20Role.md?at=master
[Add or manage Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/2.%20Add%20or%20manage%20User/Add%20or%20manage%20User.md?at=master
