<tags data-value="Log,Monitor"></tags>

:fa-cloud-download: View All Sources
=====
					


___

## :fa-info: Information

The [Sources][] overview shows all **Sources** where you can filter by characters, edit, delete, [Show Deleted][], and [Add or manage Sources][].

    TIP: The page automatically reloads every 10 seconds to update the list of available Sources and current state.

The **Sources** overview provides information about the following key properties:
* **Name** - The user friendly name of the Source (used to uniquely identify one [Monitor Agent][Monitor Agents])
* **Description** - The user friendly *Description* for the [Source][Sources]
* **Log Text** - Status text from last synchronization. Normally empty.
* **Polling Interval** - Displays in number of seconds how often the [Monitoring Service][] should sync with the [Monitor Agent][Monitor Agents]

![View All Sources][1]

## :fa-flash: Actions
The following **Actions** are available
* **Edit** - Open the [Source][Sources] for [editing][Add or manage Sources]  
* **Delete** - Quick delete the selected [Source][Sources]  

![OverviewActions][2]

___
### :fa-hand-o-right: Next Step  
:fa-eye: :fa-cloud-download: [View All Sources][]  
:fa-plus-square: :fa-cloud-download: [Add or manage Sources][]    
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]   

##### :fa-cubes: Related  
:fa-download: [Sources][]
:fa-desktop: [View Monitor Views][]  
:fa-desktop: [Monitor View][]   
:fa-lightbulb-o: [Resources][]  


<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/Sources/SourcesOverview.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/Sources/OverviewActions.png

[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[Add or manage Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[View All Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/1.%20View%20All%20Sources/Overview.md?at=master
[Monitor View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master
[View Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/1.%20View%20All%20Monitor%20Views/View%20All%20Monitor%20Views.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master  
[Show Deleted]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Show%20Deleted.md?at=master
