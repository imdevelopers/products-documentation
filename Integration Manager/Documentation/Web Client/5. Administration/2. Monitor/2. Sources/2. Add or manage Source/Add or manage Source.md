<tags data-value="Source,Monitor,Resource,Application,Category"></tags>

:fa-cloud-download: Add or manage a Source
=====
					


___

## :fa-info: Information
In this section you will learn how to setup and configure a **Source**. 

One **Source** is used to uniquely target/identify/configure one installed [Monitor Agent][]

## Instructions

Click on the 'Add Source' button to begin enter the configuration details for one installed [Monitor Agent][].  
![AddSourceButton][0]

* **Name** is required to create the [Source][]

* **Description** is optional. Enter a user friendly Description about this configuration

* **Website** is optional. Provides a link for users    

![Add Source][1]

### :fa-link: General Tab

When configuring the **Source** you must choose and configure connection settings.

* **Service URL** is required - The Service URL is the address for the [Monitoring Service][] to communicate with the [Source][].

The [Monitor Agents][Monitor Agent] can either use: 
* Local LAN using TCP/IP
* Anywhere in the World using [Service Bus Relaying][]

The **Service URL** is always available in the :fa-file-text-o: 'SourceInformation.txt' file located in the installation folder of the [Monitor Agent][].

* **Polling Interval** is required -Set how often the [Monitoring Service][] polls for the actual state of the [Resources][]. This value is set in Seconds using a positive Integer value. 

**Default** is 60 seconds.

    TIP: Do not poll for status updates more often than really required by your business needs. A short poll interval will add pressure on valuable system resources.   

### :fa-lock: Security Tab
 
An **Authentication Key** must be entered to get access to **Remote Configuration** of the Monitor Agent. The [Monitoring Service][] must have this security setting to authenticate calls sent to the Monitor Agent.  
The Authentication Key is available in the :fa-file-text-o: 'SourceInformation.txt' file located in the folder of the installed [Monitor Agent][].

Check the '**Source** is authenticated' if you the [Monitoring Service][] should use the Authentication Key settings for the connection against the [Monitor Agent][].

* **Authentication Key** is required - The API key or [Service Bus Relaying][] connection information

In order to allow Remote Configuration of the [Monitor Agent][], the connection with the **Source** must be secured using either an API key or [Service Bus Relaying][]. 


    Note: If ServiceBus Relaying is being used to communicate with the Monitor Agent a different security scheme is being used,

**Shared Access Signature security Key**. See the following Microsoft article for more information [MDSN][SAS]. The Monitor Agent needs additional configuration for [Service Bus Relaying][]. 

Example of shared access key, use the Azure Portal to acquire real values.
``` 
SharedAccessKeyName=%RootManageSharedAccessKey%;SharedAccessKey=%SharedAccessKey%;AuthenticationKey=%AuthenticationKey%
```

### :fa-cog: Advanced Tab
Optional overrides are available in the Advanced tab.

Check the **Change default Timeout** if the **Source** should use a custom Timeout. The default timeout value is 120 seconds.

Check the **Change default Retry handling** to set custom retry values. This should only be used if the connection between the [Monitoring Service][] and the [Monitor Agent][] is using an unreliable WAN link.


### :fa-exclamation-triangle: Common Problems

If the Source is not available and/or cannot be reached it will be displayed in the list of Sources as being offline:  
![IconSourceNotAvailable][6]

A warning message can also be seen when configuring the specific view. The information is refreshed in the background and will disappear if the service comes back online or connectivity is re-established.
![SourceNotAvailable][5]

___

### :fa-info: Source Information
![Remote Configuration, Source Information, and Available Resources][2]

#### Server, Environment and Version
You can easily retreive actual run-time values for the chosen Monitor Agent.

#### Alive Check

Test the connection to the Source by clicking Check.

You can see the status for the last alive check as well as the time of the last check. 

![Source Information][3]

### :fa-file-text-o: Available Resources

Clickable List all [Resources][] provided by the [Source][].

The list contains information about the following:
* **Name** - The display *name* of the [Resource][Resources]  
* **Description** - The user friendly Description for the [Resource][Resources]   
* **Application** - The [Application][] the [Resource][Resources] belongs to
* **Category** - The [Categories][Category] the [Resource][Resources] belongs to

![Available Resources][4]
___

### :fa-hand-o-right: Next Step  

Learn how to configure [Remote Configuration][]

:fa-eye: :fa-cloud-download: [View All Sources][]  
:fa-plus-square: :fa-cloud-download: [Add or manage a Source][]    
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]   

##### :fa-cubes: Related  
:fa-eye: :fa-desktop: [View Monitor Views][]  
:fa-desktop: [Monitor Views][Monitor View]   
:fa-lightbulb-o: [Resources][]  


<!--References -->
[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/Sources/AddSourceButton.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/Sources/AddSource.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/C14.%20Remote.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/C14.3%20Source%20Information.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/Sources/AvailableResources.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/Sources/WarningSourceNotAvailable.png
[6]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/Sources/IconSourceNotAvailable.png
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Remote%20Configuration/Remote%20Configuration.md?at=master
[Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[SAS]:https://azure.microsoft.com/en-us/documentation/articles/storage-dotnet-shared-access-signature-part-1/
[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master
[Service Bus Relaying]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/ServiceBusRelaying.md?at=master
[Application]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/Applications.md?at=master
[Category]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/Categories.md?at=master
[View All Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/1.%20View%20All%20Sources/Overview.md?at=master
[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[View Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/1.%20View%20All%20Monitor%20Views/View%20All%20Monitor%20Views.md?at=master
[Monitor View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
