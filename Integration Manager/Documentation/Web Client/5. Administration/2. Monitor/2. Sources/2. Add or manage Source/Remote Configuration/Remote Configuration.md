<tags data-value="Remote,Configuration,BizTalk,Source,Resources"></tags>

:fa-edit: Remote Configuration
=====
				

___

## :fa-info: Information
In this section you will learn how to use Remote Configuration.
A pre-requisite for Remote Configuration is a Secure Connection set in the Security Tab for a Source, see [add or manage a Source][].  
![Remote Configuration, Source Information, and Available Resources][1]

### Remote Configuration
___

### :fa-hand-o-right: Next Step  

##### :fa-cubes: Related  

<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/C14.%20Remote.png
[add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master

