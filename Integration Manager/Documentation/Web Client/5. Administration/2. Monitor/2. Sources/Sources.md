<tags data-value="Sources,Monitor View, Monitor Agent, Resources"></tags>

:fa-cloud-download: Sources
=====

___

## :fa-info: Information
In this section you will learn about **Sources**.

Any number of [Monitor Agents][] can be installed on any number of hosts. In order to separate these from each other Integration Manager keeps track of each of them by using a **Source**. Each **Source** points to exactly 1 [Monitor Agent][Monitor Agents]. 
The [Monitoring Service][] is responsible for the communication with the **Source** ([Monitor Agent][Monitor Agents]) using the provided connection details set on a **Source**.  
[Monitor Agents][] provides run-time information about [Resources][]. Integration Manager keeps track of changes and this data can be used for further analysis.      

![Sources][1]
___
### :fa-hand-o-right: Next Step  
:fa-eye: :fa-cloud-download: [View All Sources][]  
:fa-plus-square: :fa-cloud-download: [Add or manage a Source][]    
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]   

##### :fa-cubes: Related  
:fa-eye: :fa-desktop: [View Monitor Views][]  
:fa-desktop: [Monitor View][]   
:fa-lightbulb-o: [Resources][]  


<!--References -->
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[View All Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/1.%20View%20All%20Sources/Overview.md?at=master
[Monitor View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master
[View Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/1.%20View%20All%20Monitor%20Views/View%20All%20Monitor%20Views.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/Sources/Sources.png