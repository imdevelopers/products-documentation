<tags data-value="View,All,Resources"></tags>

:fa-lightbulb-o: View All Resources
=====
					


___

## :fa-info: Information
The [Resources][] overview shows all the **Resources** where you can filter by characters, edit, delete, and [Show Deleted][].

This is the **Resources** overview.

![View All Resources][1]

___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-download: [Add or manage a Source][]  

##### :fa-cubes: Related  
:fa-download: [Sources][]  

<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A65.View%20All%20Resources.png

[Add or manage a Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master

[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Show Deleted]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Show%20Deleted.md?at=master

[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
