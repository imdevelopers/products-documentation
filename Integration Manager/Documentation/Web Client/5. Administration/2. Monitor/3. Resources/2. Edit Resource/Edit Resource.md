<tags data-value="Edit,Resources,Web,Site,Information"></tags>

:fa-lightbulb-o: Edit Resource
=====
					


___

## :fa-info: Information
Since **Resources** are automatically populated and synced from a [Source][Sources] the set of properties that can be changed is limited.
You can alter and modify:
* Web Site
* Expected State
  
![Edit Resource][1]

## :fa-edit: Edit   
A link to an external Web Site with additional information can be added. When set, the **Resource** is listed inside [Monitor Views][] with an :a-external-link: external link icon . Clicking on this link will open a new tab with the target URL (Web Site).  
![link][2]

## :fa-history: View Resource History
For as long as there are saved entries within the [Log Databases][] the history for the **Resource** is available. 
![viewhistory][4]

## :fa-cloud-download: Source
For information, The origin ([Source][Sources]) of the **Resource** is listed.

## :fa-dropbox: Application
For information, The [Application][Applications] for the **Resource** is listed.  

## :fa-folder-o: Category
For information, The [Category][Categories] for the **Resource** is listed.  

## Expected State
You can override the evaluation of the current state by setting an **Expected State** for the Resource.
For example, The SQL Monitoring Agent returns a SQL Job that is currently disabled. By design and normaly this would render a Warning. By setting the Expected State for Warning to OK the alert will be suppressed and displayed as being OK with the addition of a small colored circle. The latter indicates "We hear you - but hey, there's something strange going on here".
An example from a [Monitor View][] for a Disabled Receive Location in BizTalk (compare first row with second row):
![expectedstateicon][3]
   
___

### :fa-hand-o-right: Next Step  
:fa-eye: :fa-lightbulb-o: [View All Resources][]  
:fa-eye: :fa-folder-o: [View All Categories][]  
:fa-eye: :fa-dropbox: [View all Applications][]     
:fa-edit: :fa-dropbox: [Edit Applications][]  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]  
:fa-plus-square: :fa-cloud-download: [Add or manage Sources][]  
:fa-edit: :fa-lightbulb-o: [Edit Resources][]  

##### :fa-cubes: Related  
:fa-folder-o: [Categories][]  
:fa-dropbox: [Applications][]  
:fa-desktop: [Monitor Views][]    
:fa-cloud-download: [Sources][]     
:fa-lightbulb-o: [Resources][]  
:fa-database: [Log Databases][]
___

<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/EditResource.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/ResourceWithLink.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/ExpectedStateIcon.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/ResourceHistory.png

[Applications]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/Applications.md?at=master
[View All Applications]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/1.%20View%20All%20Applications/View%20All%20Applications.md?at=master
[Edit Applications]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/2.%20Edit%20Application/EditApplication.md?at=master
[Log Databases]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Log%20Databases/Log%20Databases.md?at=master

[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Add or manage Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master

[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Edit Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/2.%20Edit%20Resource/Edit%20Resource.md?at=master
[View All Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/1.%20View%20All%20Resources/View%20All%20Resources.md?at=master

[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[View All Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/1.%20View%20All%20Monitor%20Views/View%20All%20Monitor%20Views.md?at=master

[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Remote%20Configuration/Remote%20Configuration.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master


[View All Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/1.%20View%20All%20Categories/View%20All%20Categories.md?at=master
[Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/Categories.md?at=master
[Edit Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/2.%20Edit%20Category/Edit%20Category.md?at=master
