<tags data-value="Resources,Source,Categories,Applications"></tags>

:fa-lightbulb-o: Resources
=====
					


___
 :fa-lightbulb-o: [Resources][] :fa-long-arrow-right: 
 :fa-upload: [Monitor Agent][Monitor Agents] :fa-long-arrow-right:
 :fa-download: [Source][]  :fa-long-arrow-right:
 :fa-desktop: [Monitoring Service][] :fa-long-arrow-right:
 :fa-database: [Log Databases][] :fa-long-arrow-right:
 :fa-cloud: [Web API][] :fa-long-arrow-right:
 :fa-globe: [Web Client][] :fa-long-arrow-right:
 :fa-desktop: [Monitor View][] :fa-long-arrow-right:
 :fa-group: [Roles][] :fa-long-arrow-right:
 :fa-user: [User][Users]

## :fa-info: Information
In this section you will learn about **Resources**.

A **Resource** is specific object/unit being monitored by a specific [Monitoring Agent][Monitor Agents]. A **Resource** origin from [Sources][Source].  The current state of the **Resource** is tracked by the [Monitoring Service][]. 
The **Resource** can have one of the following states at any given moment:
* :fa-check-circle-o: OK - Used to indicate normal operation
* :fa-exclamation-triangle: Warning  - Used to indicate an unexpected but not invalid state
* :fa-times-circle-o: Error - Used to indicate an error/fatal condition
* :fa-times-circle-o: Unavailable - Used to indicate an error/fatal condition usually since the source or resource is not avaialble or can't be reached from the [Monitoring Agent][Monitor Agents]. Also, some [Monitor Agents][Monitor Agents] use this state to indicate bad configuration, or inabilito to evaluate the [Resource][Resources]  
* :fa-times-circle: Source Error - Used when the [Monitoring Service][] fails to communicate with the [Monitoring Agent][Monitor Agents]. Common causes are
    * Service is offline (server is shutdown/rebooting)
    * Connectivity issues (bad network)
    * Invalid [Source][Sources] configuration
    * Invalid [Remote Configuration][]



**Status Code colors and order**:  
![png_StatusCodes][]
  
**Examples**:  
![examples][1] 


The following properties are available on a **Resource**:
* Name - The name of the Resource
* Description - A user friendly description about the Resource
* **Web Site** - Provides the ability for additional external documentation with a **link**
* Source - The name of the [Source][] the Resource origins from
* Application - The name of the [Application][Applications]
* Category - The name of the [Category][Categories]

## Expected State
All resources can be reconfigured to provide a different outcome when evaluating the current state.
For example, a disabled SQL Job would normally be considered an error. By setting the expected State of the Resource different outcome will be the result for the current state.

The opposite can also hold true, for example a Receive Location in BizTalk is supposed to be stopped due to some ongoing problem pending solution. Meanwhile, the Receive Location should be stopped.
![expectedstate][2] 

From inside a [Monitor View][Monitor Views] a resource with reconfigured expected state will be displayed with a colored dot indicating the real state.
![expectedstateicon][3] 


    Note: The LogText should not change over time for a resource that does not change its state. This is due to the caching mechanism trying to conserve bandwidth and costs when using service bus relaying.  

   
___

### :fa-hand-o-right: Next Step  
:fa-eye: :fa-lightbulb-o: [View All Resources][]  
:fa-eye: :fa-folder-o: [View All Categories][]  
:fa-edit: :fa-lightbulb-o: [Edit Resources][]  
* :fa-edit: :fa-dropbox: [Edit Applications][]  
* :fa-edit: :fa-folder-o: [Edit Categories][]
  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]    
:fa-plus-square: :fa-download:  [Add or manage Sources][]  

##### :fa-cubes: Related  
:fa-desktop: [Monitor Views][]    
:fa-download:  [Sources][]  
:fa-lightbulb-o: [Resources][]    
* :fa-dropbox: [Applications][]  
* :fa-folder-o: [Categories][]
  
:fa-user-secret: [Authorization][]    
* :fa-user: [Users][]    
* :fa-group: [Roles][]   

<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/ResourceExamples.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/ExpectedState.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/ExpectedStateIcon.png
[png_StatusCodes]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/StatusCodes.png

[Source]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/Categories.md?at=master
[Applications]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/Applications.md?at=master
[Monitor View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Remote%20Configuration/Remote%20Configuration.md?at=master


[Applications]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/Applications.md?at=master

[Monitor Views]:https://bytebucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[View All Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/1.%20View%20All%20Monitor%20Views/View%20All%20Monitor%20Views.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master

[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[View All Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/1.%20View%20All%20Monitor%20Views/View%20All%20Monitor%20Views.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master

[Applications]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/Applications.md?at=master
[Edit Applications]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/2.%20Edit%20Application/EditApplication.md?at=master

[View Alarm Plugins]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/1.%20Alarm%20Plugins/1.%20View%20All%20Alarm%20Plugins/View%20All%20Alarm%20Plugins.md?at=master
[Edit Alarm Plugins]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/1.%20Alarm%20Plugins/2.%20Edit%20the%20email%20plugin/Edit%20the%20email%20plugin.md?at=master
[Alarm Plugins]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/1.%20Alarm%20Plugins/Alarm%20Plugins.md?at=master

[Add or manage Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Edit Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/2.%20Edit%20Resource/Edit%20Resource.md?at=master
[View All Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/1.%20View%20All%20Resources/View%20All%20Resources.md?at=master

[Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/Categories.md?at=master
[View All Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/1.%20View%20All%20Categories/View%20All%20Categories.md?at=master
[Edit Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/2.%20Edit%20Category/Edit%20Category.md?at=master


[Authorization]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/Authorization.md?at=master

[Add or manage Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/2.%20Add%20or%20manage%20Role/Add%20or%20manage%20Role.md?at=master
[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[View All Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/18e6667b7d0bb9f0a784cc6ee0aa78ebc11185e6/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master

[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Web API]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Web%20API/Overview.md?at=master
[Log Databases]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Log%20Databases/Log%20Databases.md?at=master
