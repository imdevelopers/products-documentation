<tags data-value="View,All,Categories"></tags>

View All Categories
=====
					


___

## :fa-info: Information

The list of defined [Categories][]. A large list of **Categories** can be narrowed down by typing characters into the filter text box or by selecting a specific [Source][Sources].

You can [edit][Edit Categories] and [Delete][Show Deleted] records.

![View All Categories][1]
___


### :fa-hand-o-right: Next Step  
:fa-edit: :fa-folder-o: [Edit Category][Edit Categories]  
:fa-edit: :fa-lightbulb-o: [Edit Resources][]  
:fa-plus-square: :fa-cloud-download: [Add or manage Sources][]  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]  

##### :fa-cubes: Related  
:fa-desktop: [Monitor Views][]  
:fa-cloud-download: [Sources][]  
:fa-lightbulb-o: [Resources][]  
:fa-dropbox: [Applications][]

<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/ViewAllCategories.png

[Show Deleted]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Show%20Deleted.md?at=master

[Applications]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/Applications.md?at=master

[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
 
[Edit Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/2.%20Edit%20Resource/Edit%20Resource.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Add or manage Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master

[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[View All Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/1.%20View%20All%20Monitor%20Views/View%20All%20Monitor%20Views.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master

[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Remote%20Configuration/Remote%20Configuration.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master

[Edit Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/2.%20Edit%20Category/Edit%20Category.md?at=master
                                                                                                                        
[Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/Categories.md?at=master
