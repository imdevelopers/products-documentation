<tags data-value="Categories,Source,Resource"></tags>

:fa-folder-o: Categories
=====
					


___

## :fa-info: Information
A **Category** is used to group related [Resources][] from [Monitor Agents][] in [Monitor Views][].

A **Category** can also provide 

## :fa-plus-square: Origin of Category names

**Category** is a way for the [Monitoring Agents][Monitor Agents] to partition [Resources][] in [Monitor Views][]. Use this feature to create [Monitor Views][] with Custom Filters based on the Category name; For example BizTalk Send Ports.

The **Category** names are automatically populated from [Resources][] during the sync operation of the [Monitor Service][]. The [Resources][] comes from the [Monitor Agents][]. 
The [Monitoring Service][] knows about the whereabouts of the [Monitor Agents][] from the settings in [Sources][].

You can add a link to provide additional external documentation by [adding][Edit Categories] a **'Web Site'** link to the Category.

![website][0] 
___

### :fa-hand-o-right: Next Step  
:fa-eye: :fa-folder-o: [View All Categories][]  
:fa-edit: :fa-folder-o: [Edit Categories][]  
:fa-edit: :fa-lightbulb-o: [Edit Resources][]  
:fa-plus-square: :fa-cloud-download: [Add or manage Sources][]  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]  

##### :fa-cubes: Related  
:fa-desktop: [Monitor Views][]  
:fa-cloud-download: [Sources][]  
:fa-lightbulb-o: [Resources][]  
:fa-dropbox: [Applications][]

<!--References -->

[Applications]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/5.%20Applications/Applications.md?at=master

[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Edit Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/2.%20Edit%20Resource/Edit%20Resource.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Add or manage Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master

[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[View All Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/1.%20View%20All%20Monitor%20Views/View%20All%20Monitor%20Views.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master

[Remote Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Remote%20Configuration/Remote%20Configuration.md?at=master
[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master

[Edit Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/2.%20Edit%20Category/Edit%20Category.md?at=master
[View All Categories]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/4.%20Categories/1.%20View%20All%20Categories/View%20All%20Categories.md?at=master

[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Monitor/CategoryWebSite.png
