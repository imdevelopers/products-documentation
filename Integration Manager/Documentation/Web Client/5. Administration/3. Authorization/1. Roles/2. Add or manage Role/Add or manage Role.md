<tags data-value="Roles,Users"></tags>

:fa-group: Add or manage Role
=====
					


___

## :fa-info: Information
In this section you will learn how to add and manage a **Role**.
You must have created the set of [Users][] to manage before a [Role][] can be properly configured. 

## Mandatory Fields
* **Name**: A [User][Users] must have a name. This name facilitates the selection in a [Log View][].  

## Optional Fields
* **Additional information**: A user friendly description.

![Add Role][1]

#### Assign Users in Role

You have the option to [Assign Users to the Role][].

![Assign Users in Role][2]

___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-user: [Assign Users to the Role][]


##### :fa-cubes: Related  
:fa-user: [Users][]    
:fa-group: [Role][]  
:fa-hdd-o: [Log View][]    
:fa-desktop: [Monitor View][]  
:fa-user-secret: [Authorization][]

<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Authorization/Roles/AddRoleButton.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Authorization/Roles/EditRolesForUser.png

[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Role]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[Assign Users to the Role]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/2.%20Add%20or%20manage%20Role/Assigned%20Users%20in%20Role/Assigned%20Users%20in%20Role.md?at=master
[Authorization]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/Authorization.md?at=master
[Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master
[Monitor View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/Monitor.md?at=master
