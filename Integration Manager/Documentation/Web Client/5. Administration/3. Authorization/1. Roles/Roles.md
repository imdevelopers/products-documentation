<tags data-value="Roles,Users,Authority"></tags>

:fa-group: Roles
=====

___

## :fa-info: Information

**Roles** are being used to group [Users][] to facilitate the assignments of what users are allowed to view and manage. 
These rights can only be assigned on the **Role** level.

For example, you can allow members of the **Economy** Role  have access to a specific **Invoice** [Log View][].

Any number of application **Roles** can be defined.

You must be member of the **Administrators** role to manage **Roles** (and all other administrative parts/objects).

## :fa-lock: Policy based authorization
There is a special role named **Administrators**. This **role** cannot be renamed or deleted. See [Authorization][] for additional details.
___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-group: [Add or manage Roles][]  
:fa-plus-square: :fa-hdd-o: [Add or manage Log Views][]  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]  
  

##### :fa-cubes: Related  
:fa-user: [Users][]      
:fa-hdd-o: [Log View][]      
:fa-desktop: [Monitor View][]   
:fa-user-secret: [Authorization][]
 
  
<!--References -->
[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master
[Add or manage Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/2.%20Add%20or%20manage%20Role/Add%20or%20manage%20Role.md?at=master
[Monitor View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/Monitor.md?at=master
[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[Authorization]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/Authorization.md?at=master
