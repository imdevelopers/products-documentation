<tags data-value="Add,User,Access,Mail,Roles"></tags>

:fa-user: Add or manage Users
=====
					


___

## :fa-info: Information
In this section you will learn how to add or manage a [User][].

![Add User][1]

## How to configure a **User**
Add a new [User][] or Edit an existing from the list in the [overview][View All Users].
A [User][] can be member of one or more  [Roles][] after you have successfully created the **User**. 

![adduser][3]

## Mandatory Fields

### :fa-font: User Name
A **User name** is required to create the User.

*Example validation error: Bad format for domain and user provided*  
![baduserdata][4]

## Optional Fields
The following fields are optional. For optimal use of Integration Manager a valid email address for the [User][] should be set either on the user level or in the alarm plugins   
* **Additional information**: A user friendly description.  
* **Email**: In order for the [Monitoring Service][] to send email through the email [Alarm Plugin][] a valid email adress for the user must be provided.


## :fa-group: Membership
You can assign the [User][] to any [Role][Roles] (1 or more).

![Edit Roles][2]
___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-group: [Add or manage Role][]  
:fa-plus-square: :fa-hdd-o: [Add or manage Log Views][]    
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]    

##### :fa-cubes: Related  
:fa-user: [User][]    
:fa-group: [Roles][]  
:fa-hdd-o: [Log Views][Log View]  
:fa-desktop: [Monitor Views][Monitor View]  
:fa-user-secret: [Authorization][]  

<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A72.%20Add%20User.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A72.%20Edit%20Roles.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Authorization/Users/AddUser.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Authorization/Users/BadUserData.png


[Add or manage Role]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/2.%20Add%20or%20manage%20Role/Add%20or%20manage%20Role.md?at=master

[User]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master
[Alarm Plugin]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/1.%20Alarm%20Plugins/Alarm%20Plugins.md?at=master
[View All Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/1.%20View%20All%20Users/View%20All%20Users.md?at=master
[Monitor View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/Monitor.md?at=master
[Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[Authorization]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/Authorization.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master