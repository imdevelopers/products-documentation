<tags data-value="Edit,Roles,User"></tags>

:fa-group: Edit Roles
=====
					
___

## :fa-info: Information
Choose which [Roles][] should be given to the [User][].

## Instructions

You can **Add**, **Filter** and **Remove** Roles.

![Edit Roles][1]


___


### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-hdd-o: [Add or manage Log Views][]    
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]    

##### :fa-cubes: Related  
:fa-user: [User][]    
:fa-group: [Roles][]  
:fa-hdd-o: [Log Views][Log View]  
:fa-desktop: [Monitor Views][Monitor View]  
:fa-user-secret: [Authorization][]  


<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A73.%20Edit%20Roles%20to%20Users.png


[User]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master
[Alarm Plugin]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/1.%20Alarm%20Plugins/Alarm%20Plugins.md?at=master
[View All Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/1.%20View%20All%20Users/View%20All%20Users.md?at=master
[Monitor View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/Monitor.md?at=master
[Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[Authorization]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/Authorization.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master