<tags data-value="Users,Roles,Account"></tags>

:fa-user: Users
=====


___

## :fa-info: Information
A **user** within Integration Manager is matched to a Windows Domain account. All non-Windows users must provide valid Windows domain credentials to pass the logon procedure.  

![user][1]

Configure **users** from within the [Administration][] section of the Web Client:    
![userpanel][2]

*List of users:*  
![userList][3]

In order for a **user** to gain access to work with Integration Manager an **Administrator** must 
[create][Add or manage Users] the **user** record.
   
* Members of the **Administrators** [Role][Roles] is an **Administrator**.  
* Only members of the **Administrators** [Role][Roles] can assign and change membership.
* Users must be part of at least one [Role][Roles] in a [Log View][] and/or a [Monitor View][Monitor Views] to be able to work with Integration Manager.
* **Users** must be defined and used from the same **Windows Domain** where Integration Manager is installed.

The install account being used on initial setup of Integration Manager will be the first and at that point in time the only Administrator. 
Any number of users can be part of the Administrators [Role][Roles]. The latter is not recommended usage of Integration Manager.
The [principle of least privilege][least] should be honored.



___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-user: [Add or manage Users][]  
:fa-plus-square: :fa-hdd-o: [Add or manage Log Views][]  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]  

##### :fa-cubes: Related  
:fa-desktop: [Monitor][]  
:fa-group: [Roles][]    
:fa-user-secret: [Authorization][]    
:fa-desktop: [Monitor Views][]     
:fa-hdd-o: [Log Views][Log View]  

<!--References -->
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master

[Monitor]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/Monitor.md?at=master
[Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[Add or manage Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/2.%20Add%20or%20manage%20User/Add%20or%20manage%20User.md?at=master
[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[least]:https://en.wikipedia.org/wiki/Principle_of_least_privilege
[Authorization]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/Authorization.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/User.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Authorization/Users/UserPanel.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Authorization/Users/UsersList.png
