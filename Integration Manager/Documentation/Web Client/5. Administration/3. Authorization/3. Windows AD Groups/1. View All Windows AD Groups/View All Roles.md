<tags data-value="Windows AD Group, User, Roles, Authorization"></tags>

:fa-group: View All Windows AD Groups
=====
					

___

## :fa-info: Information

The overview shows all allowed Windows AD groups [Windows AD Groups][] and the result can be filtered by entering characters, edit, delete, [Show Deleted][], and [Add or manage Role][]

Application [Roles][] are used within [Log Views][] and [Monitor Views][] to grant [Users][] access to Integration Manager
Example of [Roles][]

![View All Roles][1]
___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-windows: [Add or manage Windows AD Groups][]  
:fa-plus-square: :fa-group: [Add or manage Role][]  
:fa-plus-square: :fa-user: [Assign Users to the Role][]
##### :fa-cubes: Related  
:fa-user: [Users][]    
:fa-group: [Roles][]   
:fa-hdd-o: [Log Views][]    
:fa-desktop: [Monitor Views][]    
:fa-user-secret: [Authorization][]  

<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Authorization/DomainGroups/ListAll.png

[Show Deleted]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Show%20Deleted.md?at=master
[Add or manage Role]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/2.%20Add%20or%20manage%20Role/Add%20or%20manage%20Role.md?at=master
[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Assign Users to the Role]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/2.%20Add%20or%20manage%20Role/Assigned%20Users%20in%20Role/Assigned%20Users%20in%20Role.md?at=master
[Authorization]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/Authorization.md?at=master

[Windows AD Groups]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/3.%20Windows%20AD%20Groups/Overview.md?at=master

[Add or manage Windows AD Groups]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/3.%20Windows%20AD%20Groups/2.%20Add%20or%20Manage%20Windows%20AD%20Group/Add%20or%20manage%20Windows%20AD%20Group.md?at=master
