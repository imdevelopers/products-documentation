<tags data-value="Roles,Users,Windows AD Group"></tags>

:fa-group: Add or manage Windows AD Groups
=====
					


___

## :fa-info: Information
In this section you will learn how to add and manage a **Windows AD Group**.

You must have created the set of [Roles][] to manage before a [Windows AD Group][] can be properly configured. 

Click the **Add Windows AD Group** button to allow the users of the Windows group access to Integration Manager.  
![AddButton][3]

## Mandatory Fields
* **Name**: A **Windows AD Group** must have a name that is equal to that of the existing Windows Group. read more [here](https://technet.microsoft.com/en-us/library/cc754217(v=ws.11).aspx)

## Optional Fields
* **Description**: A user friendly description.

### :fa-group: [Roles][Role]

![AddADGroup][1]

#### Assign Users in Role

You have the option to assign [Roles][Role].

![Assign Users in Role][2]

___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-user: [Assign Users to the Role][]  
:fa-eye: :fa-windows: [View All Windows AD Groups][]


##### :fa-cubes: Related  
:fa-user: [Users][]    
:fa-group: [Role][]  
:fa-hdd-o: [Log View][]    
:fa-desktop: [Monitor View][]  
:fa-user-secret: [Authorization][]

<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Authorization/DomainGroups/EditWindowsGroup.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Authorization/DomainGroups/AddRolesList.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Authorization/DomainGroups/AddButton.png


[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Role]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[Assign Users to the Role]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/2.%20Add%20or%20manage%20Role/Assigned%20Users%20in%20Role/Assigned%20Users%20in%20Role.md?at=master
[Authorization]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/Authorization.md?at=master
[Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master
[Monitor View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/Monitor.md?at=master

[View All Windows AD Groups]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/3.%20Windows%20AD%20Groups/1.%20View%20All%20Windows%20AD%20Groups/View%20All%20Roles.md?at=master

