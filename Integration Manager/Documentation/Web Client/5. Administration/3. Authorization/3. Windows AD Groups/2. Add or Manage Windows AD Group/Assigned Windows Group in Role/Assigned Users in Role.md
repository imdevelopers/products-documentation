<tags data-value="Assigned,Users,Role"></tags>

:fa-user: Assigned Users in Role
=====
					


___

## :fa-info: Information
One or more [Users][] can be added to a [Role][].

You have the options to Add, Remove, and Filter Users.

![Assign User to Role][1]

<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A91.%20Assign%20Users%20to%20Role.png

[Role]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  
