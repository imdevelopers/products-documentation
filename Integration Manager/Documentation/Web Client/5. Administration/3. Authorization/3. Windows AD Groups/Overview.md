<tags data-value="Windows AD Groups,Roles,Users,Authority"></tags>

:fa-windows: Windows AD Groups
=====
					


___

## :fa-info: Information
Integration Manager has support to use **Windows AD Groups** and can be associated with [Roles][Roles]. This means that  Windows domain groups can be used instead of pinpointing individual [Users][] (combinations are still valid).  
![WindowsGroups][1]

Any number of **Windows AD Groups** can be added.

    NOTE:  There is no validation that the Windows group actually exists.

    NOTE: If the Windows AD Group is renamed the same change must be done within Integration Manager

You must be member of the **Administrators** role to manage **Windows AD Groups** (and all other administrative parts/objects).



## :fa-lock: Policy based authorization
There is a special role named **Administrators**. This **role** cannot be renamed or deleted. See [Authorization][] for additional details.

## :fa-check-square-o: Prerequisites
The IIS Windows Server hosting Integration Manager must be part of the domain/forest (with optional trusts) that IIS is expected to authenticate. 

### :fa-sliders:  System Parameters  
If you want to allow the usage of Windows AD groups a **System Parameter** must be set to reflect this. The [System Parameter][System Parameters] **UseActiveDirectoryGroups** controls this. The AppPool must be recycled and the browser must be re-read (typically ctrl+F5).

![allow][0]

___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-windows: [Add or manage Windows AD Groups][]    
:fa-eye: :fa-windows: [View All Windows AD Groups][]  
:fa-plus-square: :fa-hdd-o: [Add or manage Log Views][]    
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]   
  

##### :fa-cubes: Related  
:fa-group: [Roles][]  
:fa-user: [Users][]      
:fa-hdd-o: [Log View][]      
:fa-desktop: [Monitor View][]   
:fa-user-secret: [Authorization][]  
:fa-key: [System Parameters][] 
  
<!--References -->
[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master
[Add or manage Windows AD Groups]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/3.%20Windows%20AD%20Groups/2.%20Add%20or%20Manage%20Windows%20AD%20Group/Add%20or%20manage%20Windows%20AD%20Group.md?at=master

[Monitor View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/3.%20Monitor/Monitor.md?at=master
[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[Authorization]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/Authorization.md?at=master
[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master

[System Parameters]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/System%20Parameters/Overview.md?at=master


[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Authorization/DomainGroups/ListAll.png
[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Authorization/DomainGroups/AllowADGroups.png


[View All Windows AD Groups]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/3.%20Windows%20AD%20Groups/1.%20View%20All%20Windows%20AD%20Groups/View%20All%20Roles.md?at=master

