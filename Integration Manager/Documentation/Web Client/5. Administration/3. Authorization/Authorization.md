<tags data-value="Authorization,Users,Roles, Log Views, Monitor Views"></tags>

:fa-user-secret: Authorization
=====
					


___

## :fa-info: Information
The [Web Client][] uses Windows integrated security and users (Windows accounts and domain groups) granted logon rights are *automatically* logged on. 
[User][Users] access to the [Web Client][] is managed by an **Administratator** part of the built in **Administrators** [Role][Roles].

:fa-user: [Users][]  
:fa-group: [Roles][]  
:fa-windows: [Windows AD Groups][]  


Denied users wil be prompted by the browser to enter valid Windows credentials in order to access the [Web Client][].

![menu][0]

Integration Manager is built from bottom to the top with security and access control in mind. A Windows account must be listed within the  [User][Users] and/or [Windows AD Groups][].

Every user interacting with Integration Manager is **Authenticated** using Windows Domain/Workgroup based credentials. Every operation that changes information and
the viewing and/or download of logged messages are being audited into a tamper proof Log Audit.  

## :fa-lock: Policy based authorization
Users with appropriate rights can view logged data in [Log Views][] and supervise the status of monitored [Resources][] within [Monitor Views][].

* [Roles][] are used in Integration Manager to enforce a policy on either
    * [Log Views][]
    * [Monitor Views][]
* The policy is governed by a special built in [Role][Roles] named **Administrators**. 
    * Members of the **Administrators** [Role][Roles] can perform all available operations within Integration Manager.
* [Roles][] can either include
    * Windows accounts
    * Windows groups
* There are no **DENY** settings. By default everything is disallowed and an **Administrator** must explicitly allow what authenticated users can see and do


A user with no associations to either [Log Views][] or [Monitor Views][] are granted logon rights but can not see any of the available features. Very much the same logical idea as the grant *public* in SQL databases.

![restricted][1]

Users part of the **Administrators** role has access to the [Administration][] and full configuration of the [Repository Model][Repository]

![adminrights][2]

___


### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-user: [Add or manage Users][]    
:fa-plus-square: :fa-group: [Add or manage Role][]   
:fa-plus-square: :fa-windows: [Add or manage Windows AD Groups][]  

##### :fa-cubes: Related  
:fa-user: [Users][]  
    :fa-user: [View All Users][]    
:fa-group: [Roles][]  
    :fa-group: [View All Roles][]   
:fa-windows: [Windows AD Groups][]  
    :fa-group: [View All Windows AD Groups][]   

:fa-hdd-o: [Log Views][]  
:fa-desktop: [Monitor Views][]  
:fa-cogs: [Administration][]  
:fa-sitemap: [Repository][]

<!--References -->

[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master

[Add or manage Role]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/2.%20Add%20or%20manage%20Role/Add%20or%20manage%20Role.md?at=master
[View All Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/1.%20View%20All%20Roles/View%20All%20Roles.md?at=master
[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master

[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Add or manage Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/2.%20Add%20or%20manage%20User/Add%20or%20manage%20User.md?at=master
[View All Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/1.%20View%20All%20Users/View%20All%20Users.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master

[Add or manage Windows AD Groups]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/3.%20Windows%20AD%20Groups/2.%20Add%20or%20Manage%20Windows%20AD%20Group/Add%20or%20manage%20Windows%20AD%20Group.md?at=master

[View All Windows AD Groups]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/3.%20Windows%20AD%20Groups/1.%20View%20All%20Windows%20AD%20Groups/View%20All%20Roles.md?at=master


[Windows AD Groups]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/3.%20Windows%20AD%20Groups/Overview.md?at=master

[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?


[0]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Authorization/AuthorizationMenu.png
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Authorization/NonAdministrator.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Authorization/AdminRights.png

