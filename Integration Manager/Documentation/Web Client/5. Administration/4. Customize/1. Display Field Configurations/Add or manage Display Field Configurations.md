<tags data-value="Display Field Configurations, Search Fields, Log Views"></tags>

:fa-list-ol: Add or Manage Display Field Configurations
=====
					


___

## :fa-info: Information
Manage the display name and set of columns for use within [Log Views][].

## :fa-edit: Edit
A [Display Field Configuration][Display Field Configurations] has the following properties:
* **Name** - The user friendly name used for selection within [Log Views][] *(Required)
* **Description** - The user friendly description for this configuration *(Optional)*
* **Display Fields** - The set of selected columns (Fields) *(Optional)*

Either add a new [Display Field Configuration][Display Field Configurations] by clicking on the 'Add Display Field Configuration' button or by selecting one existing from the list.  
![Add][2]  
*Add Display Field Configuration button*

![Edit][1]

## :fa-plus-circle: Add or :fa-minus-circle: Remove
Add or remove fields from the list of **Available Fields**.     
![SelectFields][3]

The order can changed by pressing the **up** and **down** butttons.

### :fa-filter: Filter
The list can be filtered by typing characters in the dropdown list. The list can also be filtered by selecting the type of field.

![TypeOfFields][4]
*Example of type of selectable fields*

### :fa-font: :fa-arrow-right: :fa-bold: Artifact renaming
Check the **Show Artifact Renaming** checkbox in order to rename the **Selected Fields**.  
![ArtifactRenaming][5]


## :fa-save: Save
Remember to Confirm and save changes!

    NOTE: Changes to configuration may require a CTRL-F5 operation within the web browser since Integration Manager uses caching


___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-hdd-o: [How to add or manage a Log View][]    

##### :fa-cubes: Related  
:fa-list-ol: [Display Field Configurations][]  
:fa-hdd-o: [Log Views][]   

<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Settings/DisplayFields/EditDisplayFieldConfiguration.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Settings/DisplayFields/AddDisplayFieldConfiguration.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Settings/DisplayFields/DisplayFields.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Settings/DisplayFields/TypeOfFields.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Settings/DisplayFields/ArtifactRenaming.png

[Display Field Configurations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/1.%20Display%20Field%20Configurations/Overview.md?at=master&fileviewer=file-view-default

[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master

[How to add or manage a Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master