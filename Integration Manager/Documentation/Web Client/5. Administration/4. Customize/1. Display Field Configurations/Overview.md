<tags data-value="Display Field Configurations, Search Fields, Log Views"></tags>

:fa-list-ol: Display Field Configurations
=====
					


___

## :fa-info: Information
A **Display Field Configuration** holds a set of fields (Columns) that is used to display data for end users within [Log views][].

![DisplayFields][1]

## :fa-check-square-o: Trivia
* There can be any number of **Display Field Configurations**  
* There can be any number of fields selected within one **Display Field Configuration**
* Artifact renaming is supported and you can have the same set of columns with different names for different end users within different [Log Views][]
* Can be [re]-used in any [Log Views][]  
* The following type of fields can be selected 
    * :fa-info-circle: [Event Detail][]
    * :fa-sitemap: [Repository][]
    * :fa-search: [Search Fields][Search Field]

![TypeOfFields][2]  
*Example of type of selectable fields*

### :fa-user-secret: Required user rights
You must be part of the **Administrators** [role][Roles] to add and manage **Display Field Configurations**.


## :fa-trash: Show deleted

Delete will not delete the **Display Field Configuration** but instead mark it as deleted.

When [Show Deleted][] is checked, all the "deleted" **Display Field Configuration** will be shown in the list with red background color.

![Show Deleted][3]

A deleted **Display Field Configuration** can be restored by clickin on the Restore menu item within the Action drop down button.
![RestoreDeleted][4]

A deleted **Display Field Configuration** will not be selectable from within [Log Views][].
___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-list-ol: [Add or manage Display Field Configurations][]   
:fa-plus-square: :fa-hdd-o: [Add or manage Log Views][]  

##### :fa-cubes: Related  
:fa-group: [Roles][]   
:fa-hdd-o: [Log Views][]   
:fa-sitemap: [Repository][]  
:fa-search: [Search Fields][Search Field]  


<!--References -->


[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[Add or Manage Display Fields Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/Display%20Field%20/Configurations/Overview.md?at=master
[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master

[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
[Search Field]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/2.%20Search%20Fields/Search%20Fields.md?at=master

[Add or manage Display Field Configurations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/1.%20Display%20Field%20Configurations/Add%20or%20manage%20Display%20Field%20Configurations.md?at=master&fileviewer=file-view-default

[Show Deleted]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Show%20Deleted.md?at=master&fileviewer=file-view-default


[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Settings/DisplayFields/ListOfDisplayFieldConfigurations.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Settings/DisplayFields/TypeOfFields.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Settings/DisplayFields/ShowDeleted.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Settings/DisplayFields/RestoreDeleted.png


