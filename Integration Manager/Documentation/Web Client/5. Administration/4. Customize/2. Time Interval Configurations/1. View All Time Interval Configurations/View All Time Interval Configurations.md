<tags data-value="View,All,Time,Interval,Configurations"></tags>

:fa-clock-o: View All Time Interval Configurations
=====
					

___

## :fa-info: Information

The **Time Interval Configurations** list shows all the user defined [Time Interval Configurations][].
Enter text in the filter text boxto narrow down a large number of records in the list.

## Operations
 
An administrator can  [**Add**][Add or manage Time Interval Configuration] new [Time Interval Configurations][]. 
 
An administrator can [**Edit**][Add or manage Time Interval Configuration] a record by clicking on the record or by selecting the Edit using the Actions button.
   
An administrator can [**Delete**][Show Deleted] records. Deleting a [Time Interval Configuration][] may have user impact. The Delete operation changes the limit for what users can search for in [Log Views][] using the [Time Interval Configuration][Time Interval Configurations].

___
An example of available Time Interval Configurations  

![View All Time Interval Configurations][1]

___

### :fa-hand-o-right: Next Step  

[Add or manage Time Interval Configurations][Add or manage Time Interval Configuration]  


##### :fa-cubes: Related  
[Time Intervals][]
      
[Add or manage Time Interval][]
      
[Time Interval Configurations][]
    
[Log Views][]    



<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A79.%20View%20All%20Time%20Interval%20Configurations.png

[Show Deleted]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Show%20Deleted.md?at=master

[Add or manage Time Interval Configurations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/2.%20Time%20Interval%20Configurations/2.%20Add%20Time%20Interval%20Configurations/Add%20Time%20Interval%20Configurations.md?at=master&fileviewer=file-view-defau

[Time Interval Configurations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Time%20Interval%20Configurations/Time%20Interval%20Configurations.md?at=master

[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master

[Time Intervals]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/3.%20Time%20Intervals/Time%20Intervals.md?at=master&fileviewer=file-view-default


[Add or manage Time Interval]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/2.%20Time%20Interval%20Configurations/2.%20Add%20Time%20Interval%20Configurations/Add%20Time%20Interval%20Configurations.md?at=master&fileviewer=file-view-default
