<tags data-value="Add,Time,Interval,Configurations"></tags>

:fa-clock-o: Add or manage Time Interval Configurations
=====
					

___

## :fa-info: Information
In this section you will learn how to add and manage a **Time Interval Configuration**.
You must have created the set of [Time Intervals][] to use before a [Time Interval Configuration][] can be properly configured. 

## Mandatory Fields
* **Name**: A [Time Interval Configuration][] must have a name. This name facilitates the selection in a [Log View][Log Views].  

## Optional Fields
* **Description**: A user friendly description.  
* **Allow Detailed Time Search**: With this setting checked, the end user will be able to search entering any date. 

## Configuration
A [Time Interval Configuration][] consists of a set of [Time Intervals][]. Click **Edit** to change the selection of included **Time Intervals**

___

### :fa-hand-o-right: Next Step  

#### :fa-cubes: Related  
:fa-clock-o: [Time Intervals][]
:fa-hdd-o: [Log Views][]

<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A80.%20Add%20Time%20Interval%20Configuration.png
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master
[Time Intervals]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/2.%20Time%20Interval%20Configurations/2.%20Add%20Time%20Interval%20Configurations/Add%20Time%20Interval%20Configurations.md?at=master&fileviewer=file-view-default
[Time Interval Configuration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/2.%20Time%20Interval%20Configurations/Time%20Interval%20Configurations.md?at=master&fileviewer=file-view-default
