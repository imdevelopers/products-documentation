<tags data-value="Edit,Time,Interval"></tags>

:fa-clock-o: Edit Time Interval
=====
					


___

## :fa-info: Information
Choose [Time Intervals][] to add to a [Time Interval Configurations][]. 

## Instructions

You can Add, Filter and Remove Time Intervals.

![Add Time Interval Configuration][1]



<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A81.%20Edit%20Time%20Intervals.png

[Time Intervals]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/3.%20Time%20Intervals/Time%20Intervals.md?at=master&fileviewer=file-view-default

[Time Interval Configurations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/2.%20Time%20Interval%20Configurations/Time%20Interval%20Configurations.md?at=master&fileviewer=file-view-default
___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  
