<tags data-value="Time,Interval,Configurations"></tags>

:fa-clock-o: Time Interval Configurations
=====
					


___

## :fa-info: Information
Integration Manager provides an option to limit the time a **User** can search for data. This feature is the result from a configuration of the following 2 artifacts:
1. Time Interval Configuration with selected [Time Intervals][]
2. A [Log View][Log Views] that uses a **Time Interval Configuration**


    Tip Use Time Interval Configurations in Business Log Views for to make it easier and faster for end users to find relevant data.
___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-clock-o: [Add or manage Time Interval Configurations][]  

##### :fa-cubes: Related  
:fa-hdd-o: [Log Views][]  
:fa-clock-o: [Time Intervals][]

<!--References -->

[Time Intervals]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/2.%20Time%20Interval%20Configurations/Time%20Interval%20Configurations.md?at=master&fileviewer=file-view-default
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master
[Add or manage Time Interval Configurations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/2.%20Time%20Interval%20Configurations/2.%20Add%20Time%20Interval%20Configurations/Add%20Time%20Interval%20Configurations.md?at=master&fileviewer=file-view-defau
