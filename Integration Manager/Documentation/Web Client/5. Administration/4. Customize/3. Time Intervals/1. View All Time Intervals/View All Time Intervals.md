<tags data-value="View,All,Time,Intervals"></tags>

:fa-clock-o: View All Time Intervals
=====
					


___

## :fa-info: Information
The **Time Intervals** list shows all user defined [Time Intervals][]. Enter text in the filter text box to narrow down a large number of records in the list.

## Operations
 
An administrator can  [**Add**][Add or manage Time Interval] new Time Intervals. 
 
An administrator can [**Edit**][Add or manage Time Interval] a record by clicking on the record or by selecting the Edit using the Actions button.
   
An administrator can [**Delete**][Show Deleted] records. Deleting a [Time Interval][Time Intervals] will change the content of a [Time Interval Configuration][Time Interval Configurations]. The Delete operation changes the limit for what users can search for in [Log Views][] using [Time Interval Configurations][].

An example of available [Time Intervals][]

![View All Time Intervals][1]

___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-clock-o: [Add or manage Time Interval][]

##### :fa-cubes: Related  
:fa-clock-o: [Time Interval Configurations][]  
:fa-hdd-o: [Log Views][]  

<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A77.%20View%20All%20Time%20Intervals.png

[Show Deleted]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Show%20Deleted.md?at=master

[Time Intervals]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/3.%20Time%20Intervals/Time%20Intervals.md?at=master&fileviewer=file-view-default

[Add or manage Time Interval]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/2.%20Time%20Interval%20Configurations/2.%20Add%20Time%20Interval%20Configurations/Add%20Time%20Interval%20Configurations.md?at=master&fileviewer=file-view-default

[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master


[Time Interval Configurations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/2.%20Time%20Interval%20Configurations/Time%20Interval%20Configurations.md?at=master&fileviewer=file-view-default