<tags data-value="Add,Time,Interval,Quantity,Unit"></tags>

:fa-clock-o: Add or manage Time Interval
=====
					


___

## :fa-info: Information
A [**Time Interval**][Time Interval] is made out of a **Quantity** and a **Unit**.

![Add Time Intervals][1]


## Mandatory Fields
**Quantity**: Is a positive integer (1-n).     
**Unit**: Simply select from one of the pre-defined values:  

    * Seconds
    * Minutes
    * Hours
    * Days
    * Weeks
    * Months
    * Years

![Units][2]

For Example, the Quantity '**42**' and Unit '**Minutes**' represents a time span of **42 minutes**.

## Optional Fields
**Description**: A user friendly description.
___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-clock-o: [Add or manage a Time Interval][]   

##### :fa-cubes: Related  
:fa-clock-o: [Time Interval Configurations][]
:fa-hdd-o: [Log Views][]

<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A78.%20Add%20Time%20Interval.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A78.%20Time%20Interval%20Units.png

[Time Interval]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/3.%20Time%20Intervals/Time%20Intervals.md?at=master&fileviewer=file-view-default
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master
[Time Interval Configurations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/2.%20Time%20Interval%20Configurations/Time%20Interval%20Configurations.md?at=master&fileviewer=file-view-default
[Add or manage a Time Interval]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/3.%20Time%20Intervals/2.%20Add%20or%20manage%20Time%20Interval/Add%20or%20manage%20Time%20Interval.md?at=master&fileviewer=file-view-default



