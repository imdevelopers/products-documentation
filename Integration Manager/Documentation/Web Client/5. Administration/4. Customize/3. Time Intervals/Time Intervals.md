<tags data-value="Time,Intervals,Quantity,Unit"></tags>

:fa-clock-o: Time Intervals
=====
					


___

## :fa-info: Information
A time interval is a unit of time used within a [Time Interval Configuration][Time Interval Configurations] to put time restrictions in [Log Views][].  

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-clock-o: [How to add or manage a Time Interval][]   

##### :fa-cubes: Related  
[Time Interval Configurations][]  
[Log Views][]  

<!--References -->
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master
[Time Interval Configurations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/2.%20Time%20Interval%20Configurations/Time%20Interval%20Configurations.md?at=master&fileviewer=file-view-default
[How to add or manage a Time Interval]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/2.%20Time%20Interval%20Configurations/2.%20Add%20Time%20Interval%20Configurations/Add%20Time%20Interval%20Configurations.md?at=master&fileviewer=file-view-default

