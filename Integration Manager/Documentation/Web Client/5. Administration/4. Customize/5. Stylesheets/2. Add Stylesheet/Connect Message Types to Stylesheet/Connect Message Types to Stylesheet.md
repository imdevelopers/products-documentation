<tags data-value="Stylesheets,Message Type"></tags>

:fa-file-text-o: Connect Message Type to Stylesheet
=====
					


___

## :fa-info: Information
Connect a [Message Type][] to a [Stylesheet][] to apply the Template to certain Message Types.

More information about usage: [View Message as formatted][]. 

## Instructions

You can Add, Filter and Remove Message Types.

![View All Stylesheets][1]

<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A76.%20Connect%20Message%20Types.png

[Message Type]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master
[View Message as formatted]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/4.%20How%20to%20Search%20the%20Log%20View/View%20Message%20as%20formatted/View%20Message%20as%20formatted.md?at=master
[Stylesheet]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/5.%20Stylesheets/Stylesheets.md?at=master&fileviewer=file-view-default

___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  
