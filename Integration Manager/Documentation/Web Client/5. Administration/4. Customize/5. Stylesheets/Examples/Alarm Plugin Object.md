<tags data-value="Stylesheets,XSLT,Format,Email"></tags>

Alarm Plugin Object
=====
					


___

## :fa-info: Information
The following XML is an example of data for Alerts (Alarms)

Use this data to customize the default Stylesheet for Email Alarm Plugin using the preview tool within Integration Manager or Visual Studio if you prefer.

### RAW data

```xml
<?xml version="1.0" encoding="utf-8" ?>
<AlarmObject xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <MonitorViews>
    <MonitorView>
      <MonitorViewId>1</MonitorViewId>
      <Name>Test View</Name>
      <Description>Simple unit testing view.</Description>
      <StatusCode>
        <StatusCode>0</StatusCode>
        <Name>OK</Name>
      </StatusCode>
      <NumberOfMonitoredResources>2</NumberOfMonitoredResources>
      <Integrations>
        <Integration>
          <IntegrationId>1</IntegrationId>
          <Name>INT001 - Simple Integration</Name>
          <Description>Integration for unit testing.</Description>
          <WebSite>http://www.integrationsoftware.se/</WebSite>
          <CustomFields>
            <CustomField>
              <CustomFieldId>1</CustomFieldId>
              <Name>SLA</Name>
              <Description>SLA Level are Gold, Silver and Bronze</Description>
              <WebSite>http://www.integrationsoftware.se/SLA/</WebSite>
              <ValueType>
                <CustomFieldTypeId>1</CustomFieldTypeId>
                <Name>Text</Name>
                <Description>Custom Value are plain text values with optional Description and Web Site.</Description>
                <WebSite></WebSite>
              </ValueType>
              <CustomValues>
                <CustomValue>
                  <CustomValueId>1</CustomValueId>
                  <Value>Gold</Value>
                  <Description>Gold</Description>
                  <WebSite>http://www.integrationsoftware.se/SLA/Gold</WebSite>
                  <ValueType>
                    <CustomFieldTypeId>1</CustomFieldTypeId>
                    <Name>Text</Name>
                    <Description>Custom Value are plain text values with optional Description and Web Site.</Description>
                    <WebSite></WebSite>
                  </ValueType>
                </CustomValue>
              </CustomValues>
            </CustomField>
            <CustomField>
              <CustomFieldId>2</CustomFieldId>
              <Name>INT Images</Name>
              <Description>Images of the integrations</Description>
              <WebSite></WebSite>
              <ValueType>
                <CustomFieldTypeId>2</CustomFieldTypeId>
                <Name>File</Name>
                <Description>Custom Value are represented as uploaded files with optional Description.</Description>
                <WebSite></WebSite>
              </ValueType>
              <CustomValues>
                <CustomValue>
                  <CustomValueId>4</CustomValueId>
                  <Value>home.png</Value>
                  <Description>Home for integration</Description>
                  <WebSite></WebSite>
                  <ValueType>
                    <CustomFieldTypeId>2</CustomFieldTypeId>
                    <Name>File</Name>
                    <Description>Custom Value are represented as uploaded files with optional Description.</Description>
                    <WebSite></WebSite>
                  </ValueType>
                </CustomValue>
              </CustomValues>
            </CustomField>
          </CustomFields>
        </Integration>
      </Integrations>
      <ChangedResources>
        <Resource>
          <ResourceId>1</ResourceId>
          <Name>First Resource - Send Port</Name>
          <WebSite>http://www.integrationsoftware.se/resource/send-port</WebSite>
          <Source>
            <SourceId>1</SourceId>
            <Name>Test Source</Name>
            <Description>Simple source for testing purposes only.</Description>
            <WebSite>http://www.integrationsoftware.se/</WebSite>
          </Source>
          <Category>
            <CategoryId>1</CategoryId>
            <Name>Send Ports</Name>
            <Description>Send ports category.</Description>
          </Category>
          <Application>
            <ApplicationId>1</ApplicationId>
            <Name>BizTalk System</Name>
            <Description>Default biztalk application</Description>
            <WebSite>http://www.integrationsoftware.se/application/biztalk-system</WebSite>
          </Application>
          <StatusCode>
            <StatusCode>0</StatusCode>
            <Name>OK</Name>
          </StatusCode>
          <LogText>All OK!</LogText>
        </Resource>
        <Resource>
          <ResourceId>2</ResourceId>
          <Name>Second Resource - Receive Port</Name>
          <Source>
            <SourceId>1</SourceId>
            <Name>Test Source</Name>
            <Description>Simple source for testing purposes only.</Description>
            <WebSite>http://www.integrationsoftware.se/</WebSite>
          </Source>
          <Category>
            <CategoryId>2</CategoryId>
            <Name>Receive Ports</Name>
            <Description>Receive ports category.</Description>
            <WebSite>http://www.integrationsoftware.se/category/receive-ports</WebSite>
          </Category>
          <Application>
            <ApplicationId>1</ApplicationId>
            <Name>BizTalk System</Name>
            <Description>Default biztalk application</Description>
            <WebSite>http://www.integrationsoftware.se/application/biztalk-system</WebSite>
          </Application>
          <StatusCode>
            <StatusCode>0</StatusCode>
            <Name>OK</Name>
          </StatusCode>
          <LogText>All OK!</LogText>
        </Resource>
      </ChangedResources>
    </MonitorView>
    <MonitorView>
      <MonitorViewId>2</MonitorViewId>
      <Name>Second View</Name>
      <Description>Simple test view (second).</Description>
      <WebSite>http://www.integrationsoftware.se/</WebSite>
      <StatusCode>
        <StatusCode>2</StatusCode>
        <Name>ERROR</Name>
      </StatusCode>
      <NumberOfMonitoredResources>2</NumberOfMonitoredResources>
      <Integrations />
      <ChangedResources>
        <Resource>
          <ResourceId>10</ResourceId>
          <Name>LogAPI Queue</Name>
          <Description>Queue for Integration Manager's LogAPI.</Description>
          <Source>
            <SourceId>2</SourceId>
            <Name>MSMQ</Name>
            <Description>Source to monitor MSMQ queues.</Description>
            <Server>IBSS-BUILD02</Server>
            <Environment>Production</Environment>
            <Version>1.0</Version>
          </Source>
          <Category>
            <CategoryId>10</CategoryId>
            <Name>MSMQ Queue</Name>
            <Description>MSMQ Queues category</Description>
            <WebSite>https://msdn.microsoft.com/en-us/library/ms711472%28v=vs.85%29.aspx</WebSite>
          </Category>
          <Application>
            <ApplicationId>0</ApplicationId>
          </Application>
          <StatusCode>
            <StatusCode>2</StatusCode>
            <Name>ERROR</Name>
          </StatusCode>
          <LogText>Number of messages in queue exceeded error limit.</LogText>
        </Resource>
        <Resource>
          <ResourceId>11</ResourceId>
          <Name>LogAPI Test Queue</Name>
          <Description />
          <Source>
            <SourceId>2</SourceId>
            <Name>MSMQ</Name>
            <Description>Source to monitor MSMQ queues.</Description>
            <Server>IBSS-BUILD02</Server>
            <Environment>Production</Environment>
            <Version>1.0</Version>
          </Source>
          <Category>
            <CategoryId>10</CategoryId>
            <Name>MSMQ Queue</Name>
            <Description>MSMQ Queues category</Description>
            <WebSite>https://msdn.microsoft.com/en-us/library/ms711472%28v=vs.85%29.aspx</WebSite>
          </Category>
          <Application>
            <ApplicationId>0</ApplicationId>
          </Application>
          <StatusCode>
            <StatusCode>1</StatusCode>
            <Name>WARNING</Name>
          </StatusCode>
          <LogText>Number of messages in queue exceeded warning limit,but not error limit.</LogText>
        </Resource>
      </ChangedResources>
    </MonitorView>
  </MonitorViews>
  <Version>4.1.0.0</Version>
  <Environment>Test</Environment>
  <Customer>Integration Software</Customer>
  <Created>2018-01-01T01:00:00.000000Z</Created>
  <WebClientUrl>http://localhost/IM/WebClient/</WebClientUrl>
</AlarmObject>
```
___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  