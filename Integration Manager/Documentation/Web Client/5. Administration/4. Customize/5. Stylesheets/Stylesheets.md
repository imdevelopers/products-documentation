<tags data-value="Stylesheets,XSLT,Format,Email"></tags>

:fa-file-text-o: Stylesheets
=====
					


___

## :fa-info: Information
With Integration Manager's built in support for stylesheets you can present XML based data for end users in a user friendly way.

![example][1]

With [XSLT][xslt] you can transform an XML document into HTML. 

The Stylesheets are tied to [Message Types][]. There can be any number of different **Stylesheets** for each [Message Type][Message Types].

___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-file-text-o: [Add or manage Stylesheets][]  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]  
:fa-plus-square: :fa-hdd-o: [Add or manage Log Views][]  

##### :fa-cubes: Related  
:fa-desktop: [View all Monitor Views][]  
:fa-hdd-o: [Log Views][]  
:fa-file: [Message Types][]

<!--References -->
[Add or manage Stylesheets]:http://documentation.integrationmanager.se/#/?file=Web%20Client%2F5.%20Administration%2F4.%20Customize%2F4.%20Stylesheets%2F2.%20Add%20Stylesheet%2FAdd%20or%20manage%20Stylesheet.md

[xslt]:[http://www.w3schools.com/xml/xml_xsl.asp]
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master
[View all Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/1.%20View%20All%20Monitor%20Views/View%20All%20Monitor%20Views.md?at=master
[Add or manage Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/2.%20Add%20or%20manage%20Log%20View/Add%20or%20manage%20Log%20View.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[Message Types]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/5.%20Message%20Types/Message%20Types.md?at=master

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Templating/StylesheetsExample.png
