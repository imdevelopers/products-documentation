<tags data-value="Administration, Customize"></tags>

:fa-paint-brush: Customize
=====
					
___

## :fa-info: Information

Change visual details and configurations for end users. From the **Customize** submenu within the [Administration][] menu using the [Web Client][] an IM administrator can Edit and manage configuration items used within [Log Views][] and [Monitor Views][].


 Display Field Configurations
 Stylesheets
 Time Intervals
 Time Interval Configurations

 * :fa-list-ol: [Display Field Configurations][] - Manage the name and order of columns used within [Log Views][]  
 * :fa-file-text-o: [Stylesheets][] - Manage Stylesheets used within [Log Views][]
 * :fa-clock-o: [Time Intervals][] - Manage time units used within [Time Interval Configurations][] and Recurrence for alerts
 * :fa-clock-o: [Time Interval Configurations][] - Manage the set of time units used as restrictions within [Log Views][]
 
![CustomizeFromMenu][1]

___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-list-ol: [Add or manage Display Field Configurations][]  
:fa-file-text-o: [Stylesheets][]  
:fa-plus-square: :fa-clock-o: [Add or manage Time Interval][]  
:fa-plus-square: :fa-clock-o: [Add or manage Time Interval Configurations][]  

##### :fa-cubes: Related  
:fa-cogs: [Administration][]  
:fa-cog: [Settings][]  

:fa-globe: [Web Client][]    
:fa-hdd-o: [Log Views][]   
:fa-desktop: [Monitor Views][]   

<!--References -->
[Time Intervals]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/3.%20Time%20Intervals/Time%20Intervals.md?at=master&fileviewer=file-view-default


[Time Interval Configurations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/2.%20Time%20Interval%20Configurations/Time%20Interval%20Configurations.md?at=master&fileviewer=file-view-default

[Add or manage Time Interval]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/2.%20Time%20Interval%20Configurations/2.%20Add%20Time%20Interval%20Configurations/Add%20Time%20Interval%20Configurations.md?at=master&fileviewer=file-view-default
[Add or manage Time Interval Configurations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/2.%20Time%20Interval%20Configurations/2.%20Add%20Time%20Interval%20Configurations/Add%20Time%20Interval%20Configurations.md?at=master

[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master

[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master

[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master

[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Stylesheets]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/5.%20Stylesheets/?at=master

[Display Field Configurations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/1.%20Display%20Field%20Configurations/Overview.md?at=master&fileviewer=file-view-default
[Add or manage Display Field Configurations]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/1.%20Display%20Field%20Configurations/Add%20or%20manage%20Display%20Field%20Configurations.md?at=master&fileviewer=file-view-default

[ImportExport]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Import%20Export.md?at=master


[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Customize/AdministrationCustomizeMenu.png

[Settings]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/Settings.md?at=master
