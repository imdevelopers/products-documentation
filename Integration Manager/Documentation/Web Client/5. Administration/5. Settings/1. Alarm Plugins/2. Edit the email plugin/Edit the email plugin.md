<tags data-value="Edit,Email,Alarm,Plugin,Mail"></tags>

:fa-envelope-o: Edit the email plugin
=====
					


___

## :fa-info: Information

The email plugin is an [Alarm Plugin][] that sends an email when the status of a [Log View][] changes.

Emails are sent both when errors occur and when it's solved. 

## :fa-cog: Configuration
The Alarm Plugin may be Globally configured and specific settings may be applied in [Monitor Views][] that overrides the global settings.

### General

* **Description** - Description for Alarm Plugin
* **Web Site** - Optional Link for Alarm Plugin, typically to additional documentation
* **Version** - Displays version information (read only)

#### Stylesheet

A [Stylesheet][] may be selected to format the email. 

![Alarm Plugin][1]


#### Web Client URL

Enter the URL to Integration Manager.

#### Subject

Enter the subject of the email.

You can also add a variable. Choose between {Customer}, {Environment}, and {Version}.

#### Test Mail To

Enter an email address which will be sent test mails.

#### From

Enter an email address which the test mail will be sent from.

![Alarm Plugin 2][2]



#### Server

Enter an SMTP server, a DNS name or an IP-address.

#### Port

Enter the port used to connect to an SMTP server.

The standards for SSL are 25 or 465.

#### Enable SSL 

Check the box if an SSL connection should be used to connect to an SMTP server.

#### Use Authentication

Check the box if Authentication with an SMTP server should be used and enter User Name and Password.

![Alarm Plugin 3][3]


#### Execute and Restore Alarm test

When an Alarm test is executed, an email with error status will be sent to the Test Mail address.

If no mail appears, check the junk mail.

Restore Alarm test is similar to Execute Alarm but instead sends an OK status mail.

___

Here's an example of an executed test email with a Stylesheet.

![Email][4]

___
### :fa-hand-o-right: Next Step  
:fa-flash: [View Alarm Plugins][]  
:fa-desktop: [Monitor Views][]  

##### :fa-cubes: Related  
:fa-desktop: [Monitoring Service][]    
:fa-flash: [Alarm Plugin][]  


<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/C11.%20Alarm%20Plugin.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/C11.2%20Alarm%20Plugin%202.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/C11.3%20Alarm%20Plugin%203.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/C12.%20Email.png
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master

[Stylesheet]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/5.%20Stylesheets/Stylesheets.md?at=master&fileviewer=file-view-default

[Log View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master
[Alarm Plugin]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/1.%20Alarm%20Plugins/Alarm%20Plugins.md?at=master&fileviewer=file-view-default
[View Alarm Plugins]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/1.%20Alarm%20Plugins/1.%20View%20All%20Alarm%20Plugins/View%20All%20Alarm%20Plugins.md?at=master&fileviewer=file-view-default
[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
