<tags data-value="Alarm,Plugins,Email,Stylesheet"></tags>

:fa-flash: Alarm Plugins
=====
	


___

## :fa-info: Information

An **Alarm Plugin** is loaded by the [Monitoring Service][]. Each time a [Monitor View][] changes state an alert will be triggered calling the included **Alarm Plugins**.
For example an email Alarm Plugin will send and email every time the [Monitor View][] changes state.
All alerts can be customized using a [Stylesheet][]. The **Stylesheet** can be used format the message being sent to the [Users][] part of the included [Roles][] for the [Monitor View][].

## :fa-rocket: New installation

Drop your custom **Alarm Plugin** to the **plugins** folder where the [Monitoring Service][] is installed.  
![pluginfolder][1]

The following alarm plugins are available for use with Integration Manager:
1. Email, default part of installation (uses the email adress from [Users][])
2. Email with options, optional must be downloaded [here](http://download.integrationsoftware.se/Plugin/Alarm/) (assign mail address/distribution lists within [Monitor Views][Monitor View]) 
3. [Event Log][EventLog] 

## :fa-arrow-circle-o-up: Update

1. Stop **IM Monitoring Service**
2. Replace/update your DLL:s and dependent resources
3. Start **IM Monitoring Service**

## :fa-times-circle: Uninstall
1. Stop **IM Monitoring Service**
2. Remove the plugin from the folder
3. Delete the plugin from the list of installed Alarm Plugins in the WebClient
4. Start **IM Monitoring Service**

___

### :fa-hand-o-right: Next Step  
:fa-flash: [View Alarm Plugins][]    
:fa-plus-sqaure: :fa-flash: [Edit Alarm Plugins][] 

##### :fa-cubes: Related  
:fa-desktop: [Monitoring Service][]  
:fa-desktop: [Monitor Views][Monitor View]  
:fa-user: [Users][]  
:fa-windows: [Windows AD Groups][]

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Notifications/Alarm%20Plugins/AlarmPluginFolder.png
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master
[Stylesheet]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/5.%20Stylesheets/Stylesheets.md?at=master&fileviewer=file-view-default

[View Alarm Plugins]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/1.%20Alarm%20Plugins/1.%20View%20All%20Alarm%20Plugins/View%20All%20Alarm%20Plugins.md?at=master

[Edit Alarm Plugins]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/1.%20Alarm%20Plugins/2.%20Edit%20the%20email%20plugin/Edit%20the%20email%20plugin.md?at=master
[Monitor View]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[Windows AD Groups]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/3.%20Windows%20AD%20Groups/Overview.md?at=master

[EventLog]:./Event%20Log/Overview.md?at=master
