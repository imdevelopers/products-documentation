<tags data-value="Monitor,DataPower,IBM DataPower Gateway,Queue"></tags>

:fa-flash: Event Log - Alarm Plugin
=============================================


## :fa-info: About
The **Event Log** Alarm plugin is created to send alarms triggered by IM Monitoring Service from the [Monitor Views][] to the Windows Server Application Event Log.

![EventLog][1]

## :fa-cog: Configuration
The Alarm Plugin may be **Globally** configured and **Specific settings** may be applied in [Monitor Views][] that overrides the global settings.

### General

* **Description** - Description for Alarm Plugin
* **Web Site** - Optional Link for Alarm Plugin, typically to additional documentation
* **Version** - Displays version information (read only)

![EventLogPlugin][2]

### Global settings
* **Web Client URL** - Provides the base URL for the Web Client (used to generate a link to actual Monitor View with alert)
* **Event Id** - The Event Id to use when writing events to the Windows Application Event Log
* **Source** - see [Source Specific settings][]
* **Use Split By Resource** - Write each resource as a separate log event. NOT DEFAULT 
* **Send Alarm on OK** - If alarms for Monitor Views with status OK should be sent
* **Send Alarm on Warning** - If alarms for Monitor Views with status Warning should be sent
* **Send Alarm on Error** - If alarms for Monitor Views with status Error should be sent
* **Send Alarm on Unavailable** - If alarms for Monitor Views with status Unavailable should be sent
* **Send Alarm on Source Error** - If alarms for Monitor Views with status Source Error should be sent

![GlobalSettings][3]

### Specific Settings as set in Monitor Views
You can override the global settings in each [Monitor View][Monitor Views]
![SpecificSettings][5]

Example output:

![SpecificEvent][6]

### Test 
You can easily test the General and Specific settings by clicking on either button:
* **Restore Alarm Test** - Writes a positive trigger (State is evaluated as OK)
* **Execute Alarm Test** -  Writes a negative trigger (State is evaluated as Error)

![test][4]

The output can be seen in the Application Event Log on the server hosting the [Monitoring Service][]

## Misc

This plugin is not shipped with the product out of the box as default but can be downloaded for free [here](http://download.integrationsoftware.se/Plugin/Alarm/)


___

### :fa-hand-o-right: Next Step  
:fa-flash: [View Alarm Plugins][]    
:fa-plus-sqaure: :fa-flash: [Edit Alarm Plugins][] 

##### :fa-cubes: Related  
:fa-desktop: [Monitoring Service][]  
:fa-desktop: [Monitor Views][]  


[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Settings/ApplicationEventLog.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Settings/EventLogPlugin.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Settings/EventLogGlobalSettings.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Settings/TestAlarmPlugin.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Settings/EventLogSpecificSettings.png

[6]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Settings/SpecificEvent.png


[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[Source Specific settings]:./Source.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master

[View Alarm Plugins]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/1.%20Alarm%20Plugins/1.%20View%20All%20Alarm%20Plugins/View%20All%20Alarm%20Plugins.md?at=master

[Edit Alarm Plugins]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/1.%20Alarm%20Plugins/2.%20Edit%20the%20email%20plugin/Edit%20the%20email%20plugin.md?at=master
