<tags data-value="Event Log,Alarm Plugin"></tags>

:fa-flash: Source
=====
				
___

Default **Source** is: `Integration Manager: [{Customer} | {Environment}]`

For Example, the Source will be named: Integration Manager: IBSS | Demo

![SourceName][1]

	NOTE: Remember the Event Log will not accept some characters for Source 

Variables to use in the **Source** field:
* {Customer} - Customer name of the Integration Manager installation
* {Environment} -  Environment name (Prod, Test) of the Integration Manager installation
* {Version} - Integration Manager version
* {Status} - List of Monitor View statues **Errors, OK**
* {StatusAndCount} - List of Monitor View statues and the number for each status **2 Errors, 1 OK**
* {DateTime} - Date when the alarm was triggered.
* {Alarm.JsonPath:`...`} - Advanced option using JsonPath to extract data from the alarm object sent to the alarm.
    * `...` is a JsonPath to get name of the log views use expression `{Alarm.JsonPath:MonitorViews[*].Name}`

Example of the Json Object to apply the JsonPath on
```json
{
	"MonitorViews": [{
		"MonitorViewId": 1,
		"Name": "Test View",
		"Description": "Simple unit testing view.",
		"WebSite": null,
		"StatusCode": {
			"StatusCode": 0,
			"Name": "OK"
		},
		"NumberOfMonitoredResources": 2,
		"Integrations": [{
			"IntegrationId": 1,
			"Name": "INT001 - Orders to first company",
			"Description": "",
			"WebSite": "http://www.integrationsoftware.se",
			"CustomFields": [{
				"CustomFieldId": 1,
				"Name": "SLA",
				"Description": "",
				"WebSite": null,
				"ValueType": {
					"CustomFieldTypeId": 1,
					"Name": "Text",
					"Description": null,
					"WebSite": null
				},
				"CustomValues": [{
					"CustomValueId": 1,
					"Value": "Gold",
					"Description": "Act fast!!",
					"WebSite": "http://www.integrationsoftware.se",
					"ValueType": {
						"CustomFieldTypeId": 1,
						"Name": "Text",
						"Description": null,
						"WebSite": null
					}
				}]
			}],
			"CustomMetaDatas": [{
				"CustomMetaDataId": 1,
				"Name": "Summary",
				"Description": "My integration is the perfect one",
				"WebSite": "http://www.integrationsoftware.se",
				"DataType": 1,
				"CustomValues": [{
					"CustomValueId": 1,
					"Value": "My integration is the perfect one",
					"Description": "Act fast!!",
					"WebSite": "http://www.integrationsoftware.se",
					"ValueType": null
				}]
			}]
		}],
		"ChangedResources": [{
			"ResourceId": 1,
			"Name": "First Resource - Send Port",
			"Description": null,
			"WebSite": "http://www.integrationsoftware.se/resource/send-port",
			"Source": {
				"SourceId": 1,
				"Name": "Test Source",
				"Description": "Simple source for testing purposes only.",
				"Server": null,
				"Environment": null,
				"Version": null,
				"WebSite": "http://www.integrationsoftware.se/"
			},
			"Category": {
				"CategoryId": 1,
				"Name": "Send Ports",
				"Description": "Send ports category.",
				"WebSite": null
			},
			"Application": {
				"ApplicationId": 1,
				"Name": "BizTalk System",
				"Description": "Default biztalk application",
				"WebSite": "http://www.integrationsoftware.se/application/biztalk-system"
			},
			"StatusCode": {
				"StatusCode": 0,
				"Name": "OK"
			},
			"LogText": "All OK!"
		},
		{
			"ResourceId": 2,
			"Name": "Second Resource - Receive Port",
			"Description": null,
			"WebSite": null,
			"Source": {
				"SourceId": 1,
				"Name": "Test Source",
				"Description": "Simple source for testing purposes only.",
				"Server": null,
				"Environment": null,
				"Version": null,
				"WebSite": "http://www.integrationsoftware.se/"
			},
			"Category": {
				"CategoryId": 2,
				"Name": "Receive Ports",
				"Description": "Receive ports category.",
				"WebSite": "http://www.integrationsoftware.se/category/receive-ports"
			},
			"Application": {
				"ApplicationId": 1,
				"Name": "BizTalk System",
				"Description": "Default biztalk application",
				"WebSite": "http://www.integrationsoftware.se/application/biztalk-system"
			},
			"StatusCode": {
				"StatusCode": 0,
				"Name": "OK"
			},
			"LogText": "All OK!"
		}],
		"Users": [{
			"UserId": 1,
			"Name": "\\IBSS\\IMuser",
			"Description": null,
			"MailAddress": "support@integrationsoftware.se"
		}]
	},
	{
		"MonitorViewId": 2,
		"Name": "Second View",
		"Description": "Simple test view (second).",
		"WebSite": null,
		"StatusCode": {
			"StatusCode": 2,
			"Name": "ERROR"
		},
		"NumberOfMonitoredResources": 2,
		"Integrations": [],
		"ChangedResources": [{
			"ResourceId": 10,
			"Name": "LogAPI Queue",
			"Description": "Queue for Integration Manager's LogAPI.",
			"WebSite": null,
			"Source": {
				"SourceId": 2,
				"Name": "MSMQ",
				"Description": "Source to monitor MSMQ queues.",
				"Server": "IBSS-DEV01",
				"Environment": "Production",
				"Version": "1.0",
				"WebSite": null
			},
			"Category": {
				"CategoryId": 10,
				"Name": "MSMQ Queue",
				"Description": "MSMQ Queues category",
				"WebSite": "https://msdn.microsoft.com/en-us/library/ms711472%28v=vs.85%29.aspx"
			},
			"Application": null,
			"StatusCode": {
				"StatusCode": 2,
				"Name": "ERROR"
			},
			"LogText": "Number of messages in queue exceeded error limit."
		},
		{
			"ResourceId": 11,
			"Name": "LogAPI Test Queue",
			"Description": "",
			"WebSite": null,
			"Source": {
				"SourceId": 2,
				"Name": "MSMQ",
				"Description": "Source to monitor MSMQ queues.",
				"Server": "IBSS-DEV01",
				"Environment": "Production",
				"Version": "1.0",
				"WebSite": null
			},
			"Category": {
				"CategoryId": 10,
				"Name": "MSMQ Queue",
				"Description": "MSMQ Queues category",
				"WebSite": "https://msdn.microsoft.com/en-us/library/ms711472%28v=vs.85%29.aspx"
			},
			"Application": null,
			"StatusCode": {
				"StatusCode": 1,
				"Name": "WARNING"
			},
			"LogText": "Number of messages in queue exceeded warning limit,but not error limit."
		}],
		"Users": [{
			"UserId": 1,
			"Name": "\\IBSS\\IMuser",
			"Description": null,
			"MailAddress": "support@integrationsoftware.se"
		}]
	}],
	"Version": "4.3.0.0",
	"Environment": "Test",
	"Customer": "Integration Software",
	"Created": "2018-01-01T01:00:00.000000Z",
	"WebClientUrl": "http://localhost/IM/WebClient/",
	"ProductStatus": {
		"IsTrial": false,
		"ExpirationDate": null
	}
}
```

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Settings/EventLogSourceName.png

