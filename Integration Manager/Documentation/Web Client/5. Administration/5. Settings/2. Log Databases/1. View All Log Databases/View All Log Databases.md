<tags data-value="Log,Database,View,All"></tags>

:fa-database: View All Log Databases
=====
					


___

## :fa-info: Information
The **Log Databases** list shows all user defined [Log Databases][]. Enter text in the filter text box to narrow down a large number of records in the list.

An example of available [Log Databases][]
![Log Databases][1]

## Operations
 
An administrator can  [**Add**][Add or manage Log Database] new Log Databases. 
 
An administrator can [**Edit**][Add or manage Log Database] a record by clicking on the record or by selecting the Edit using the Actions button.  
![Action][2]

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-database: [Add or manage Log Database][]

##### :fa-cubes: Related  
:fa-database: [Log Databases][]

<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A49.%20Log%20databases.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A49.2%20Action.png


[Log Databases]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Log%20Databases/Log%20Databases.md?at=master
[Add or manage Log Database]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Log%20Databases/2.%20Add%20or%20manage%20Log%20Database/Add%20or%20manage%20Log%20Database.md?at=master&fileviewer=file-view-default


___
