<tags data-value="Add,Log,Database,Server"></tags>

:fa-database: Add or manage Log Database(s)
=====
					


___

## :fa-info: Information
In this section you will learn how to configure [Log Databases][Log Database].
When you **add** or **remove** a database from Integration Manager the corresponding log database itself will not be touched. 

These administration pages are usually being used when you move a database from one SQL Server Instance to another or when you remove (Delete) a database.

This configuration reflects Integration Managers knowledge on the whereabouts of the physical log databases.

This means that you can safely move a database from one SQL Instance to another, and then simply change its configuration accordingly. The same goes for rename operations.


    Tip: Move old databases to your SQL Hotel where disc might be cheaper
    
    Tip: Move old databases to better utilize hardware resources
    
    Tip: Empty databases can be removed.
    
## :fa-pencil-square-o: Instructions

A name for the **Database** is required.

If you create the databases ahead you must set the active intervals with **Start Date** and **End Date**.

The available fields exists for a Log Database:	
* Database
* Server
* Description
* Start Date
* End Date
* Read Only

![Add Log Database][1]

#### Name

A Database name is required to create the **Log Database** configuration.

#### Server

Add a name of the Server. It should be 'localhost' if the log database is located on the same server as the configuration database, else the name corresponding to information for the linked server. Non-local log databases are accessed using linked servers.

#### Read Only

Checked whe the log database should be marked as **read only**. Integration Manager will use this database only to search for data (No reindex, remove, or add operations will then be performed part of internal maintenance jobs)

    NOTE: Read Only can only be changed if 'End date' has passed current date.

#### Remote Server

Check the box if the database is located on a remote server in relation with the configuration database. Remote Log Databases requires a linked server between the instance with the configuration database and the remote Log Database. 

#### Start date and End date

The date and time used or will use to log data as well as the date and time stopped or stop when log new data. The top most database (online) is not allowed to have an end date. 

![Start Date][2]

#### Description
Description can be used to give this configuration some 
Adding a Description is optional.


## :fa-trash-o: Delete
An administrator can [**Delete**][Show Deleted] records. Deleting a [Log Database][] will prevent users from accessing the data from Integration Manager.
The online database cannot be deleted.

    NOTE: The actual SQL database will not be deleted. You must delete it from within the SQL MMC.

___

### :fa-hand-o-right: Next Step  
:fa-hdd-o: [Log][]  
* [Log Views][]
 
##### :fa-cubes: Related  
:fa-archive: [Log Agents][]   
:fa-globe: [LogAPI][]  
:fa-: [Log Agents][]  

<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A50.%20Add%20Log%20Database.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A50.2%20Start%20Date.png
[Log]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/Log.md?at=master
[Log Database]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Log%20Databases/Log%20Databases.md?at=master
[Show Deleted]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Show%20Deleted.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Overview.md?at=master
[LogAPI]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Log%20API/Overview.md?at=master