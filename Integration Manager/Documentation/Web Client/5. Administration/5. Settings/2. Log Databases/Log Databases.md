<tags data-value="Log, Monitor, Archive, Data, Database, Events, Messages"></tags>

:fa-database: Log Databases
=====
					


___

## :fa-info: Information
All data is being stored in data tables in Microsoft SQL Server databases. In order for Integration Manager to store infinite amount of data without
loosing  performance the data may/will be partitioned over time in several databases. Integration Manager will resolve queries dynamically 
and  end users will not know (does not need to know) where the data came from.

:fa-key: Sensitive data within the databases are protected using an obfuscator. This means that a SQL DBA will not be able to read the content of messages like patient journals and salaries.
  
:fa-flash: Data is partitioned for performance
   
:fa-archive: Archiving is built into Integration Manager. Databases will be split on **Size** and/or **Time** . This is governed by the run time values found in the **SystemParameters** table in the configuration database.
 
* **SizeToSplitDatabaseOn** - The maximum size (in GB) the current log database should have until a new IM Log database should be created.
* **DaysToSplitDatabaseOn** - The number of days after which new IM Log databases should be created on, to only use size split (recomended) set a large number like 10000 days.

:fa-bar-chart-o: Large amounts of data and customers with high transaction count needs to scale the hardware and software configuration for the environment running Integration Manager. See **Recommendations** for additional reading.

:fa-user: Security settings on Log Databases
The **Log databases** must have appropriate SQL Rights set. This is controlled by the following 2 [System Parameters][]:

* **ImLogAccessRoles** - Database role(s) to grant the service user(s) to allow access to new IM Log database(s). 
* **ImLogServiceUsers** - Service user(s) to grant access to new IM Log database(s). Separated by ; (semicolon). 


See more about these 2 [System Parameters][] [here][ImLogAccessRolesImLogServiceUsers].

## Supported Versions
* SQL Server 2008 R2 or later running on Windows 2008 R2 or later

## Recommendations
* Keep the [Logging Service][] close to the SQL Servers hosting the Log Databases for Integration Manager. 
* Use -T1118 Trace flag on SQL Instances - Optimizes TEMPDB
* Disable PAC Verification if policy allows for this option on all Windows Servers part of scope for Integration Manager (Core Services, Monitor Agents, Logging Service, LogAPI, Web Client)
* Typical IM installations requires 20GB / Month. Normally data is erased after 32 days. If data is and or should be preserved additional disc space is required. This has to be carefully monitored by IT operations. Part of planned maintenance actions is to deal with historical data. This affects mainly disc usage. Large IMLog* databases will affect performance. IM has support to store infinite (only restricted by disc space) amounts of data, but this function should depend on the use of multiple IM databases. The recommendation is to keep each IMLog database < 20 GB. If possible the initial volume size should be 50GB and must (again) be carefully monitored by IT operations. All IM databases are on initial setup today configured in “simple recovery mode”. This is today by design but may change over time. It is supported to have IM* databases in full recovery mode, but this will affect log disc space use until the next backup.
* IM should be installed on machines with dedicated Windows swap volumes (>2,5*physical RAM) and SQL discs should have > 300 MB/S R/W. There should be a secured backup volume (or network share) available with sufficient free space for IM* databases.
* Should have have 16GB RAM or more for environments with high load / amounts
* Must be configured with 1 extra static tempdb file with 128 MB in size / core up to 8 tempdb files. NOTE: This applies to all instances, BizTalk and IM.
* Make sure to tune BizTalk databases according to msgboxviewer best practices, this includes database growth options and size and number of files / database.
* IM should run in dedicated SQL instances in order to guarantee/dedicate HW resources and avoid competing demands for resources, aid in the troubleshooting and avoid blame game situations.
* IM databases should be kept in Simple recovery mode (default)
  
  
* BizTalk Customers should tune the **BizTalkDTADB** and **BizTalkMSGBoxDB** databases with the following commands. Keeps small amounts of data on the same SQL Page.  

 
``` SQL 
-- MessageBoxDB : Set text in row  option
exec sp_tableoption N'Spool', 'text in row', '6000'
go
exec sp_tableoption N'Parts', 'text in row', '6000'
go

-- BizTalkDTADB : Set text in row  option
exec sp_tableoption N'[Tracking_Parts1]', 'text in row', '6000' 
go
exec sp_tableoption N'[Tracking_Parts2]', 'text in row', '6000' 
go                       
exec sp_tableoption N'[Tracking_Spool1]', 'text in row', '6000'
go
exec sp_tableoption N'[Tracking_Spool2]', 'text in row', '6000'
go
exec sp_tableoption N'[Tracking_Fragments1]', 'text in row', '6000'
go
exec sp_tableoption N'[Tracking_Fragments2]', 'text in row', '6000'
go
```

### :fa-database: Optimize TempDB
1. Add one extra file per core (up to 8)
    * Static / No autogrowth
    * 128 MB
2. Set the -T1118 flag on ALL BizTalk and IM releated SQL Server Instances


### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-database: [Add or manage a Log Database][]  

##### :fa-cubes: Related  
:fa-download: [Log API][]  
:fa-hdd-o: [Logging Service][]  
:fa-desktop: [Monitoring Service][]  
___

<!--References -->	
[Add or manage a Log Database]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Log%20Databases/2.%20Add%20or%20manage%20Log%20Database/Add%20or%20manage%20Log%20Database.md?at=master&fileviewer=file-view-default
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Log APi]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Log%20API/Overview.md?at=master
[System Parameters]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/System%20Parameters/Overview.md?at=master
[ImLogAccessRolesImLogServiceUsers]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/System%20Parameters/ImLogServiceUsers%20and%20ImLogAccessRoles.md?at=master