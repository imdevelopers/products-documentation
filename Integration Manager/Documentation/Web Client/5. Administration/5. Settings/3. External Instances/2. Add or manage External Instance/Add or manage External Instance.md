<tags data-value="Add,External,Instance,URI"></tags>

Add or manage External Instance
=====
					


___

## :fa-info: Information

An [External Instance][] is another instance of Integration Manager.

## Instructions

![Add External Instance][1]


A Name is required to create the External Instance.

Adding a Description is optional.

### URI

URI stands for Uniform Resource Identifier and is the link to the Web API.

It's written without api, for example: http://servername/IM/WebAPI

<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A83.%20Add%20External%20Instance.png

[External Instance]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/3.%20External%20Instances/Overview.md?at=master
___

### :fa-hand-o-right: Next Step  
##### :fa-cubes: Related  
