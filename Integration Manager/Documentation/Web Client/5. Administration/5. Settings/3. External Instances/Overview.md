<tags data-value="External,Instance,Demo"></tags>

:fa-external-link: External Instance
=====
					


___

## :fa-info: Information
An **External Instance** is another instance of Integration Manager.

Any number of instances can be governed by the [Install and Update][IMUpdate] tool.

In order to Export items from one instance of integration manager to another, the external instance must be configured within the source/origin environment.

___

### :fa-hand-o-right: Next Step  
[How to add or manage an External Instance][]


##### :fa-cubes: Related  
:fa-rocket: [IMUpdate][]


<!--References -->
[How to add or manage an External Instance]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/3.%20External%20Instances/2.%20Add%20or%20manage%20External%20Instance/Add%20or%20manage%20External%20Instance.md?at=master
[IMUpdate]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Overview.md?at=master