<tags data-value="Settings,Time,Intervals,Configurations,External "></tags>

:fa-cog: Settings
=====
		

___

## :fa-info: Information
From the **Settings** submenu within the [Administration][] menu using the [Web Client][] an IM administrator can Edit and manage configuration items used within [Log Views][] and [Monitor Views][].

![Options][2]

 * :fa-flash: [Alarm Plugins][] - Manage the set of time units used as restrictions within [Log Views][]
 * :fa-external-link: [External Instances][] - Manage connectivity to other instances of Integration Manager used for [Export][ImportExport] operations
 * :fa-database: [Log Databases][] - Manage Log databases  

![Settings][1]

___

### :fa-hand-o-right: Next Step  
:fa-flash: [Alarm Plugins][]   
:fa-plus-square: :fa-external-link: [External Instances][]  
:fa-database: [Log Databases][]  

##### :fa-cubes: Related  
:fa-cogs: [Administration][]  
:fa-paint-brush: [Customize][]  
:fa-globe: [Web Client][]    
:fa-desktop: [Monitor Views][]   
:fa-hdd-o: [Log Views][]   

<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Settings/Settings.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Settings/SettingsOptions.png


[External Instances]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/3.%20External%20Instances/Overview.md?at=master

[Alarm Plugins]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/1.%20Alarm%20Plugins/Alarm%20Plugins.md?at=master

[Log Databases]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Log%20Databases/Log%20Databases.md?at=master&fileviewer=file-view-default

[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master

[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master

[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/1.%20Log%20Views/Log%20Views.md?at=master

[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master

[Customize]:http://documentation.integrationmanager.se/#/?file=Web%20Client%2F5.%20Administration%2F4.%20Customize%2FCustomize.md
