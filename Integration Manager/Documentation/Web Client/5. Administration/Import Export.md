<tags data-value="Administration,Log,Monitor,Authorization,Settings"></tags>

:fa-arrows-h: Import Export
=====
	


___

## :fa-info: Information
:fa-sitemap: [Repository][] and :fa-hdd-o: [Log][] artifacts can easily be **exported** and **imported** between different instances of Integration Manager.

* Export can be performed:
    * :fa-link: **Directly** - uses the [WebAPI][] between instances (if configured and access on LAN/WAN between instances are allowed)
    * :fa-file-code-o: **File** - for environments where there is no allowed communication)

In order to perform the Import Export Actions either click on the **Import/Export** button in the [Administration][] overview or select **Export** from the Actions button where applicable.  
![AdminImportExport][1]
* Access to Import/Export from Administration menu*


![ActionExportButton][3]  
* Example access to Export from Action button*


The following general steps are required to copy content from one instance of Integration Manager to another:

* 1. Export ->  
    * 1a. Select items to export  
    * 1a. File - Use file When you do not have direct access to target environment  
    * 1b. Instance - Upload step not required   
* 2. Upload (only required when using files)  

* 3. Import  
    * 3a. Select Items to Import and resolve detected conflicts if any  

### :fa-user-secret: Security/User rights
You must be member of the **Administrators** [Role][Roles] in both source and target environment.

The Actions from above steps are accessible from the Import / Export menu:
![ImportExportMenu][2]

## 1. :fa-sign-out: Export

* 1.Depending on starting choice the number of selectable items will differ.
![ExportItems][4]

* 2. Select one or more items for Export. All related child-items are selectable for **export**.
![ExportProgressBar][5]

When any parent-item is selected, all related child items are also selected.

* 3. Select Transfer method
    * File - Use when you do not have direct access to the target environment on network (opens a **pop-up** window, that might be blocked depending on the browser settings. [Allow pop-ups][])
    * Upload to External Instance

![TransferMethod][6]

4. If you select to perform a file based Export an Export file in JSON format will be downloaded in your browser. Copy/Move this file to target environment and proceede with the **upload** step.

5. Finish
Click the Close button to exit the Export-dialog.
![Close][7]


## 2. :fa-upload: Upload
Exported configurations must be uploaded into the target environment before an Import can be performed. This uploads may originate from direct transfers during Export operations (API -> API) or from File.
![UploadFile][8]

Using the [Web Client][] [Administration][] On the target environment the **Export-file** can be uploaded.

After selecting a file for upload, press the **Upload** button to upload the file.
![UploadButton][9]

A successful upload operation will continue the dialog using the **Import** Wizard.

## 3. :fa-sign-in: Import

The **uploads** from either file or instance are listed in the Imports tab. 

![ImportList][10]
___

### :fa-hand-o-right: Next Step  
:fa-external-link: [External Instances][]

##### :fa-cubes: Related  
* :fa-user-secret: [Authorization][]  
* :fa-globe: [Web Client][]
<!--References -->


[Allow pop-ups]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Common/Allow%20pop-ups.md?at=master
[Authorization]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/Authorization.md?at=master
[Log]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/Log.md?at=master

[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/ImportExport/AdminImportExport.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/ImportExport/ImportExportMenu.gif
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/ImportExport/ActionExportButton.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/ImportExport/ExportItems.gif
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/ImportExport/CheckParentChild.gif
[6]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/ImportExport/ExportTransferMethod.png
[7]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Close.png
[8]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/ImportExport/Uploadfile.png
[9]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/ImportExport/UploadButton.png
[10]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/ImportExport/ImportList.png

[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[External Instances]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/3.%20External%20Instances/Overview.md?at=master