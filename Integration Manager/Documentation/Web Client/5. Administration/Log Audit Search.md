<tags data-value="Log Agents, Log Views, Status Codes"></tags>

:fa-search: Log Audit Search
=====
					
___

## :fa-info: Information

**Log Audit Search** 


![LogAuditSearch.png][1]


___

 **Log Audit Search** is a tool intended to make it easy for your users to search and find specifik logs.
  
  You can search on: 
* Date: Specify the time threshold for your search.
* Operation: Search for keywords or sentences in Log audits operations.
* Users: Search for a specifik users linked to the logs. (NOTE: users from [AD-windows][] groups is not accsesseble).
* Level: Search for a specifik type of errors e.g Information, error, warnings, critical. 



![LogAuditSearch.png][2]



**Show Related** 
To get a better oversight on what has happen use the action button "show related", it will generate a view with all the related logs linked to that event. 


![LogAuditSearch.png][3]


___
### :fa-hand-o-right: Next Step
:fa-plus-square: :fa-hdd-o: [Add or manage Log Agents][]  
:fa-plus-square:  :fa-flag-o: [Log Status Codes][]  
:fa-hdd-o: [Log Views][]  

#### :fa-cubes: Related
:fa-hdd-o: [Log Views][]   
:fa-archive: List of [Log Agents][]     

[1]:https://bitbucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/LogAuditSearch.png
[2]:https://bitbucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/LogAuditSearchOverView.png
[3]:https://bitbucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Log/LogAuditSearchAction.png



[AD-windows]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/3.%20Windows%20AD%20Groups/Overview.md?at=master&fileviewer=file-view-default
[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Log%20Agents/Overview.md?at=master
[Log Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/2.%20Log/Log.md?at=master
[Log Status Codes]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/3.%20Log%20Status%20Codes/Log%20Status%20Codes.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Add or manage Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/2.%20Add%20or%20manage%20Log%20Agent%20Source/Add%20or%20manage%20Log%20Agent%20Source.md?at=master
