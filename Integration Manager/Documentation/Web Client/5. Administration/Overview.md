<tags data-value="Administration,Log,Monitor,Authorization,Settings"></tags>

:fa-cogs: Administration
=====
___

## :fa-info: Information
This is the **Administration** overview. All modifiable artifacts within Integration Manager can be further explored from the Administration summary page:  
![Administration][1]

### :fa-user-secret: Security/User rights
You must be member of the **Administrators** [Role][Roles] to gain access to this menu item.

## :fa-eye: Overview
The **Overview** displays information about everything installed for Integration Manager. Here you can typically see:
* Name
* Server (URI)
* Version
* Folder (Installation path)
* Account

![Overview][2]

    NOTE:  It may take some time to gather the information on this page depending on the amount and current state of installed services.

1. [Architecture][CoreServicesArchitecture]  
2. [Monitor Agents][]   
3. [Log Agents][]
4. [Logging Service][]
5. [Monitoring Service][]
6. [Log Databases][]


## :fa-arrows-h: Import/Export
Manage [Import and Export][ImportExport] functions. 

## :fa-search: Log Audit Search
Find user and system related events. Every user and system related change/action is being logged to a Log Audits table.

Read more about this feature [here][Log Audit Search]:

___

### :fa-hand-o-right: Next Step  
* :fa-user-secret: [Authorization][]  
* :fa-plus-square: :fa-group: [Add or manage Roles][]  

##### :fa-cubes: Related  
* :fa-hdd-o: [Log][]
* :fa-desktop: [Monitor][]
* :fa-file-text-o: [Stylesheets][]
* :fa-cog: [Settings][]
* :fa-paint-brush: [Customize][]

<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Administration.png
<!-- [2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/Overview.png -->
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Admin/OverviewAnimated.gif

[Log]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/Log.md?at=master
[Monitor]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/Monitor.md?at=master
[Authorization]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/Authorization.md?at=master
[Stylesheets]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/4.%20Customize/5.%20Stylesheets/Stylesheets.md?at=master
[Settings]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/Settings.md?at=master
[Customize]:http://documentation.integrationmanager.se/#/?file=Web%20Client%2F5.%20Administration%2F4.%20Customize%2FCustomize.md
[Add or manage Roles]: https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/2.%20Add%20or%20manage%20Role/Add%20or%20manage%20Role.md?at=master
[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master

[CoreServicesArchitecture]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Architecture.md?at=master

[Monitor Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Overview.md?at=master

[ImportExport]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Import%20Export.md?at=master

[Log Agents]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/1.%20Log/4.%20Log%20Agents/Log%20Agents.md?at=master

[Log Audit Search]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Log%20Audit%20Search.md?at=master

[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master

[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master

[Log Databases]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/5.%20Settings/2.%20Log%20Databases/Log%20Databases.md?at=master
