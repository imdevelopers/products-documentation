<tags data-value="Status,IM,Service,Online,Views"></tags>

:fa-street-view: Environment
=====
					

___

## :fa-info: Information
Information about the **customer name** of the licensee and **type of environment** can be viewed in the menu bar of the [Web Client][].  
![environment][1]
 
Every installation of Integration Manager is licensed. The license is tied to a customer. The name of the customer is extracted from the **License Key**.
The Web based [install][Install] and [update][Update] client provides functionality to enter the license key and the type of environment.

Common real life examples of **type of environments**:
* Demo
* Test
* QA
* **PROD**

The name of the type of environment is user defined and is usually set during first install. The value can be changed in the [update][Update] client.

## :fa-paint-brush: Environment Color
It is possible to apply a color scheme on a bar to easier separate multiple environments, read more [here][EnvironmentColor]

 
___

### :fa-hand-o-right: Next Step  
:fa-rocket: [Install][]    
:fa-arrow-circle-o-up: [Update][]  

##### :fa-cubes: Related  
:fa-globe: [Web Client][]    
:fa-file-text-o: [Release Notes][]    
:fa-key: [System Parameters][] 
  
<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Environment.png

[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Update]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Update/Overview.md?at=master
[Install]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Overview.md?at=master
[System Parameters]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/System%20Parameters/Overview.md?at=master
[Release Notes]:http://support.integrationsoftware.se/support/discussions/forums/1000227207 "Access the release notes for Integration Manager"

[EnvironmentColor]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/System%20Parameters/EnvironmentColor.md?at=master