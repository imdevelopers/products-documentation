<tags data-value="Help,FAQ,Bug,Report,Feature"></tags>

:fa-question-circle: Help
=====
					


***

## :fa-info: Information

The Help button is available in the upper right corner of the menu bar and provides links to:

* **General**
    1. View Help Documentation (this documentation) in a new tab
    2. View Support Page for FAQ in a new tab
    3. Send Bug Report in a new tab
    4. Send Feature Request in a new tab
* **More**
    1. View run-time information about Integration Manager.

![Help][1]

## Documentation
![FAQ][3]

## Bug Report and Feature Request
![Bug Report or Feature Request][4]

## About
![About][5]

### :fa-hand-o-right: Next Step  
:fa-arrow-circle-o-up: [Update][]  
:fa-rocket: [Install][]  

##### :fa-cubes: Related  
:fa-globe: [Web Client][]    

<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A86.%20Help.png
[3]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A86.3%20FAQ.png
[4]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A86.4%20Bug%20Report%20or%20Feature%20Request.png
[5]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/A86.5%20About.png

[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Update]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Update/Overview.md?at=master
[Install]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Overview.md?at=master
