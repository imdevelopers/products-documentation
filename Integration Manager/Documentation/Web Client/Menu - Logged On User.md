<tags data-value="User, Roles, Authorization"></tags>

:fa-user: Logged on User
=====
					


___

## :fa-info: Information
The currently [authorized][Authorization] (logged on) [user][Users] is displayed in the menu bar of the [Web Client][].  
![user][1]
___

### :fa-hand-o-right: Next Step  
:fa-plus-square: :fa-user:  [Add or manage Users][]    
:fa-plus-square: :fa-group: [Add or manage Roles][]      
:fa-eye: :fa-user: [View All Users][]   
:fa-eye: :fa-group: [View All Roles][]   

##### :fa-cubes: Related  
:fa-user-secret: [Authorization][]  
:fa-user:  [Users][]  
:fa-group: [Roles][]  

[Authorization]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/Authorization.md?at=master
[Add or manage Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/2.%20Add%20or%20manage%20Role/Add%20or%20manage%20Role.md?at=master
[Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master
[View All Roles]:https://bitbucket.org/imdevelopers/products-documentation/src/18e6667b7d0bb9f0a784cc6ee0aa78ebc11185e6/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/1.%20Roles/Roles.md?at=master

[Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/Users.md?at=master
[Add or manage Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/2.%20Add%20or%20manage%20User/Add%20or%20manage%20User.md?at=master
[View All Users]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/3.%20Authorization/2.%20Users/1.%20View%20All%20Users/View%20All%20Users.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/User.png
