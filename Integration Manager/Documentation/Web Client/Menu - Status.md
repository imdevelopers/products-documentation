<tags data-value="Status,IM,Service,Online,Views"></tags>

Status
=====

___

## :fa-info: Information
The current **status** of **Integration Manager** is available in the menu bar of the [Web Client][].  
![Status][1]

 Run-time information details about the **status** of the following **Core Services** is displayed:
* [Logging Service][]
* [Monitoring Service][]
* Count of [Monitor Views][] with any of the following statuses: 
    * :fa-times-circle: **Source unavailable** - If there is a problem communicating with the [Monitor Agent][]
    * :fa-times-circle: **Unavailable** - If there are problems (typically invalid configurations) with specific [Resources][] 
    * :fa-times-circle: **Error** - If there are [Resources][] in the Error state 
    * :fa-exclamation-triangle: **Warning** - If there are [Resources][] in the Warning state
    * :fa-check-circle-o: **OK** - Number of Monitor Views in the OK state

  ![Log and Monitor][2]
  
*Notice 1: The evaluated state is based on the Resource with the highest level* 

*Notice 2: The actual Monitor View may have 1 or more resources of the evaluated state*

___

### :fa-hand-o-right: Next Step  
:fa-hdd-o: [Logging Service][]  
:fa-download: [Monitoring Service][]  
:fa-plus-square: :fa-desktop: [Add or manage Monitor Views][]    
:fa-plus-square: :fa-download: [Add or manage Sources][]  

##### :fa-cubes: Related  
:fa-desktop: [Monitor Views][]    
:fa-download:  [Sources][]  
:fa-lightbulb-o: [Resources][]    

<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/AnimatedStatus.gif
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/Status.png

[Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/Monitor%20Views.md?at=master
[View All Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/1.%20View%20All%20Monitor%20Views/View%20All%20Monitor%20Views.md?at=master
[Add or manage Monitor Views]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/1.%20Monitor%20Views/2.%20Add%20or%20manage%20Monitor%20View/Add%20or%20manage%20Monitor%20View.md?at=master

[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Monitor Agent]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Monitor%20Agents/Monitor%20Agents.md?at=master
[Resources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/3.%20Resources/Resources.md?at=master
[Add or manage Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/2.%20Add%20or%20manage%20Source/Add%20or%20manage%20Source.md?at=master
[Sources]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/2.%20Monitor/2.%20Sources/Sources.md?at=master
[Logging Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Logging%20Service/Overview.md?at=master
[Monitoring Service]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Core%20Services/Monitoring%20Service/Overview.md?at=master
