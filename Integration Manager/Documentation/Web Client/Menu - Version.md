<tags data-value=""></tags>

:fa-slack: Version
=====
					


___

## :fa-info: Information
The currently installed running version of Integration Manager is displayed in the top bar of the [Web Client][]  
![version][1]
___

### :fa-hand-o-right: Next Step  
:fa-arrow-circle-o-up: [Update][]

##### :fa-cubes: Related  
:fa-rocket: [Install][]  
:fa-file-text-o: [Release Notes]  
:fa-globe: [Web Client][]    

<!--References -->
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/VersionInfo.png
[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master

[Update]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Update/Overview.md?at=master

[Install]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Install%20and%20Update/Overview.md?at=master

[Release Notes]:http://support.integrationsoftware.se/support/discussions/forums/1000227207 "Access the release notes for Integration Manager"
