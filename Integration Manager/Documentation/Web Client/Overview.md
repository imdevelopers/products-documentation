<tags data-value="WebClient, Log, Monitor, Repository"></tags>

:fa-globe: Web Client
=====
					


___

## :fa-info: Information

The Integration Manager Administration Console, named **Web Client** in this documentation is an Integration Solutions Support
 and Maintenance Console that you can use to view logged messages, manage and monitor your on-premise, hybrid or cloud Integration Platforms and dependent systems.
 You can also use the **Web Client** to  manage your Integration Manager configuration.
  
The left side of the **Web Client**, the console panels, consists of tiles with child tiles that represent different types of artifacts that you can manage.
When you select a tile in the console, the details pane on the right side of the Web Client displays information about the items.

In addition, in the upper right corner is a circle :fa-question-circle: that includes links to this offline documentation and forms that can be used to submit bugs and feature requests.

![WebClient][1]

The **Web Client** has the following informational elements:
* [Environment][]
* [Help][]
* [Logged on user][]
* [Status][]
* [Version][]
* [Show Deleted][]

### :fa-hand-o-right: Next Step  
:fa-cogs: [Administration][]  

##### :fa-cubes: Related  

<!--References -->
[Administration]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/5.%20Administration/Overview.md?at=master
[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/WebClient.png

[Environment]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Menu%20-%20Environment.md?at=master
[Help]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Menu%20-%20Help.md?at=master
[Logged on user]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Menu%20-%20Logged%20On%20User.md?at=master
[Status]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Menu%20-%20Status.md?at=master
[Version]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Menu%20-%20Version.md?at=master
[Show Deleted]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Show%20Deleted.md?at=master
