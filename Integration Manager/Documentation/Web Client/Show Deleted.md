<tags data-value="Repository,Log Views,Monitor Views"></tags>

:fa-trash-o: Show Deleted
=====
					


- - -

## :fa-info: Information
Administrative items within Integration Manager can be disabled for use by deleting them. For traceability reasons these are not really deleted from the database.

* A delete operation toggles an internal flag indicating that the object is not meant to be actively used   
* Deleted items can be restored
* Deleted items can be included for viewing and editing in most list views
* Deleted items are marked with red background when visible to indicate that the item is in state deleted
* Delete/Restore operations are tracked in the Log Audits table

The **Show Deleted** checkbox is found in many places within the Integration Manager [Web Client][].  
![Include deleted][1]

When **Show Deleted** is checked, you will be able to restore a deleted item by clicking the **Action** button and then click **Restore**.  
![Restore][2]

___

### :fa-hand-o-right: Next Step  
:fa-globe: [Web Client][]  

##### :fa-cubes: Related  
:fa-sitemap: [Repository][]  

<!--References -->

[1]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/IM/WebClient/includedeleted.png
[2]:https://bytebucket.org/imdevelopers/products-documentation/raw/master/Media/Documentation%20Pictures/B4.%20Restore.png

[Web Client]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/Overview.md?at=master
[Repository]:https://bitbucket.org/imdevelopers/products-documentation/src/master/Integration%20Manager/Documentation/Web%20Client/4.%20Repository/Repository.md?at=master
