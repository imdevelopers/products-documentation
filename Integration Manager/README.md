Integration Manager
====================

Homepage:
[www.integrationsoftware.se](http://www.integrationsoftware.se/)

Sales: [lasse.lund@integrationsoftware.se](mailto:lasse.lund@integrationsoftware.se)

Technical information: [michael.olsson@integrationsoftware.se](mailto:michael.olsson@integrationsoftware.se)

Support: [support@integrationsoftware.se](mailto:support@integrationsoftware.se)

## Online Documentation


RegEx to find some type of broken links in Notepad++ (You must manualy check so not database and stored procedure is removed like [IMConfig].[dbo]....)

Search
([^\]#^!])(\[[\w -]{1,}\])([^:(\[])

Replace
\1\2[]\3
 