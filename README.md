Integration Software
====================

:fa-globe: **Homepage**

[www.integrationsoftware.se](http://www.integrationsoftware.se/)

**Documentation**

[documentation.integrationmanager.se](http://documentation.integrationmanager.se/)

**Contact**

Sales: [lasse.lund@integrationsoftware.se](mailto:lasse.lund@integrationsoftware.se)

Technical information: [michael.olsson@integrationsoftware.se](mailto:michael.olsson@integrationsoftware.se)

**info**
info@integrationsoftware.se

:fa-ambulance: **Support**
[support.integrationmanager.se](https://support.integrationsoftware.se/)
